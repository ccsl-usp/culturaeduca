# -*- coding: utf-8 -*-
from __future__ import print_function

import os

from django.contrib.gis.gdal import (
    DataSource,
    SpatialReference,
    CoordTransform,
    OGRGeometry,
    OGRGeomType
)
from django.contrib.gis.gdal.geometries import Polygon
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from ibge.models import UF, Municipio, SetorCensitario

KEEP_LOWCASE = ('de', 'da', 'das', 'do', 'dos',)

UF_SIGLAS = (
    (11, 'RO'),
    (12, 'AC'),
    (13, 'AM'),
    (14, 'RR'),
    (15, 'PA'),
    (16, 'AP'),
    (17, 'TO'),
    (21, 'MA'),
    (22, 'PI'),
    (23, 'CE'),
    (24, 'RN'),
    (25, 'PB'),
    (26, 'PE'),
    (27, 'AL'),
    (28, 'SE'),
    (29, 'BA'),
    (31, 'MG'),
    (32, 'ES'),
    (33, 'RJ'),
    (35, 'SP'),
    (41, 'PR'),
    (42, 'SC'),
    (43, 'RS'),
    (50, 'MS'),
    (51, 'MT'),
    (52, 'GO'),
    (53, 'DF')
)


UF_SIGLAS_DICT = dict([(str(c), u) for c, u in UF_SIGLAS])


TIPOS = {
    'UF': UF,
    'MU': Municipio,
    'SE': SetorCensitario,
}


def capitalize_name(s):
    res = []
    for w in s.lower().split(' '):
        if w in KEEP_LOWCASE:
            res.append(w)
        else:
            res.append(w.capitalize())
    return u" ".join(res)


class Command(BaseCommand):
    args = '<IBGE shapefile>'
    help = u'''Importa shapefile do IBGE para os modelos UF, Municipio e Setor Censitário
    Determina o tipo de dado pela convenção de nome de arquivo do IBGE.
    Ex:
        - BRUFE250GC_SIR.shp = UF
        - 35MUE250GC_SIR.shp = Município
        - 35SEE250GC_SIR.shp = Setor Censitário
    '''

    def add_arguments(self, parser):
        parser.add_argument('shapefile', type=str, help='Arquivo Shapefile do IBGE')
        parser.add_argument('--encoding', type=str, default='latin1')

    def detecta_modelo(self, nome_arquivo):
        tipo = os.path.basename(nome_arquivo)[2:4]
        return TIPOS.get(tipo, None)

    def importar(self):
        ds = DataSource(self.nome_arquivo, encoding=self.encoding)
        layer = ds[0]

        settings_srs = SpatialReference(settings.SRID)
        transform_coord = None
        if layer.srs != settings_srs:
            transform_coord = CoordTransform(
                layer.srs, settings_srs)

        ct = 0
        for f in layer:

            # 3D para 2D se necessário
            if f.geom.coord_dim != 2:
                f.geom.coord_dim = 2

            # converte para MultiPolygon se necessário
            if isinstance(f.geom, Polygon):
                g = OGRGeometry(OGRGeomType('MultiPolygon'))
                g.add(f.geom)
            else:
                g = f.geom

            # transforma coordenadas se necessário
            if transform_coord:
                g.transform(transform_coord)

            # força 2D
            g.coord_dim = 2
            kwargs = {}

            if self.modelo == UF:
                kwargs['nm_estado'] = capitalize_name(f.get('NM_ESTADO'))
                kwargs['geom'] = g.ewkt
                kwargs['cd_geocuf'] = f.get('CD_GEOCUF')
                kwargs['nm_regiao'] = capitalize_name(f.get('NM_REGIAO'))
                kwargs['uf'] = UF_SIGLAS_DICT.get(kwargs['cd_geocuf'])
            elif self.modelo == Municipio:
                kwargs['nm_municip'] = capitalize_name(f.get('NM_MUNICIP'))
                kwargs['geom'] = g.ewkt
                kwargs['cd_geocodm'] = f.get('CD_GEOCODM')
                kwargs['uf_id'] = int(kwargs['cd_geocodm'][:2])
                kwargs['uf_sigla'] = UF_SIGLAS_DICT.get(str(kwargs['uf_id']))
            elif self.modelo == SetorCensitario:
                kwargs['cd_geocodi'] = f.get('CD_GEOCODI')
                kwargs['geom'] = g.ewkt
                kwargs['tipo'] = f.get('TIPO')
                kwargs['cd_geocodb'] = f.get('CD_GEOCODB')
                kwargs['nm_bairro'] = f.get('NM_BAIRRO')
                kwargs['cd_geocods'] = f.get('CD_GEOCODS')
                kwargs['nm_subdist'] = f.get('NM_SUBDIST')
                kwargs['cd_geocodd'] = f.get('CD_GEOCODD')
                kwargs['nm_distrit'] = f.get('NM_DISTRIT')
                kwargs['cd_geocodm'] = f.get('CD_GEOCODM')
                kwargs['nm_municip'] = f.get('NM_MUNICIP')
                kwargs['nm_micro'] = f.get('NM_MICRO')
                kwargs['nm_meso'] = f.get('NM_MESO')
                kwargs['municipio_id'] = long(kwargs['cd_geocodm'])

            instance = self.modelo(**kwargs)
            instance.save(force_insert=True)

            ct += 1

        return ct

    def handle(self, *args, **options):
        self.nome_arquivo = options['shapefile']
        self.encoding = options['encoding']
        print(u"Convertendo '{}':".format(self.nome_arquivo))
        print(u"Encoding: {}".format(self.encoding))
        self.modelo = self.detecta_modelo(self.nome_arquivo)
        if self.modelo is None:
            raise CommandError(u'''Não foi possível determinar o tipo do dado pelo nome do arquivo "{}"\n\nEx:
                - BRUFE250GC_SIR.shp = UF
                - 35MUE250GC_SIR.shp = Município
                - 35SEE250GC_SIR.shp = Setor Censitário'''.format(
                os.path.basename(self.nome_arquivo)))

        print(u"Tipo: {}".format(self.modelo._meta.verbose_name))
        ct = self.importar()
        print(u"{} registros importados".format(ct))
