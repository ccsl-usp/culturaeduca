# -*- coding: utf-8 -*-
import decimal

from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.gis.db.models import Count, Sum, Avg
from django.contrib.gis.db.models.query import GeoQuerySet
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import A


def conv_int(value):
    if value is not None:
        value = int(value)
    return value


def conv_decimal(value, places=1):
    if value is not None:
        value = decimal.Decimal('%.{}f'.format(places) % value)
    return value


class UF(models.Model):
    '''UFs do Brasil'''
    cd_geocuf = models.SmallIntegerField(primary_key=True)
    nm_estado = models.CharField(max_length=50)
    nm_regiao = models.CharField(max_length=20)
    uf = models.CharField(max_length=2)
    geom = models.MultiPolygonField(srid=settings.SRID)

    objects = models.GeoManager()

    class Meta:
        verbose_name = 'UF'
        verbose_name_plural = 'UFs'

    @property
    def id(self):
        return self.cd_geocuf

    def __unicode__(self):
        return self.uf

    @property
    def tipo(self):
        return u'Estado'


class Municipio(models.Model):
    '''Municípios do Brasil'''
    cd_geocodm = models.BigIntegerField(primary_key=True)
    nm_municip = models.CharField(max_length=60, blank=True)
    uf = models.ForeignKey(UF)
    uf_sigla = models.CharField(max_length=2)
    geom = models.MultiPolygonField(srid=settings.SRID)

    objects = models.GeoManager()

    class Meta:
        verbose_name = u'Município'
        verbose_name_plural = u'Municípios'

    @property
    def id(self):
        return self.cd_geocodm

    def __unicode__(self):
        return u'{} - {}'.format(self.nm_municip, self.uf_sigla)

    @property
    def tipo(self):
        return self._meta.verbose_name


class Area(models.Aggregate):
    function = 'ST_Area'

    def __init__(self, expression, **extra):
        super(Area, self).__init__(
            expression,
            output_field=models.FloatField(),
            **extra)


class Transform(models.Aggregate):
    # supports COUNT(distinct field)
    function = 'ST_Transform'
    template = '%(function)s(%(expressions)s, %(srid)s)'

    def __init__(self, expression, srid, **extra):
        super(Transform, self).__init__(
            expression,
            srid=srid,
            **extra)


class SetorCensitarioQuerySet(GeoQuerySet):
    def entorno(self, latitude, longitude, raio=1):
        ponto = Point(float(longitude), float(latitude))

        # Near the equator, 1km is going to equal roughly 0.008 degrees
        # (1 km / 40,000 km * 360 degrees) of latitude and longitude
        km_in_dd = 0.008

        ponto_buffer = ponto.buffer(float(raio) * km_in_dd)

        # usa buffer em ponto e interseção para agilizar a
        # resposta, pois a query de distância é mais complexa
        return self.filter(geom__intersects=ponto_buffer)

    def perfil(self):
        from ibge_universo.models import (
            Basico,
            Domicilio01,
            Domicilio02,
            Entorno01,
            Entorno03,
            DomicilioRenda,
            PessoaRenda,
            Pessoa01,
            Pessoa02,
            Pessoa03,
            Pessoa11,
            Pessoa12,
            Pessoa13,
            Responsavel01,
            Responsavel02,
            ResponsavelRenda,
        )

        # Geral

        res = self.aggregate(
            num_setores=Count('cd_geocodi'),
            area=Sum(Area(Transform('geom', 3857))))

        # se area for None retorna 0
        res['area'] = conv_decimal(A(sq_m=res['area']).sq_km, 2) if res['area'] else 0

        basico = Basico.objects.filter(cod_setor__in=self).aggregate(
            num_domicilios=Sum('V001'),
            num_moradores=Sum('V002'),
            med_moradores_domicilios=Sum('V002') / Sum('V001'))

        res.update(basico)

        res['num_domicilios'] = conv_int(basico['num_domicilios'])
        res['num_moradores'] = conv_int(basico['num_moradores'])
        res['med_moradores_domicilios'] = conv_decimal(
            basico['med_moradores_domicilios'], 2)

        # Características dos Domicílios

        domicilio01 = Domicilio01.objects.filter(cod_setor__in=self).aggregate(
            num_dom_casas=Sum('V003') + Sum('V004'),
            perc_dom_casas=((Sum('V003') + Sum('V004')) * 100) / Sum('V002'),
            num_dom_ap=Sum('V005'),
            perc_dom_ap=(Sum('V005') * 100) / Sum('V002'),
            num_dom_sem_agua=Sum('V002') - Sum('V012'),
            perc_dom_sem_agua=((Sum('V002') - Sum('V012')) * 100) / Sum('V002'),
            num_dom_sem_san=Sum('V023'),
            perc_dom_sem_san=(Sum('V023') * 100) / Sum('V002'),
            num_dom_lixo=Sum('V002') - Sum('V035'),
            perc_dom_lixo=((Sum('V002') - Sum('V035')) * 100) / Sum('V002'),
            num_dom_sem_ee=Sum('V046'),
            perc_dom_sem_ee=(Sum('V046') * 100) / Sum('V002'),
        )

        res.update(domicilio01)

        for k in ('num_dom_casas', 'num_dom_ap', 'num_dom_sem_agua', 'num_dom_sem_san', 'num_dom_lixo', 'num_dom_sem_ee',):
            res[k] = conv_int(res[k])

        for k in ('perc_dom_casas', 'perc_dom_ap', 'perc_dom_sem_agua', 'perc_dom_sem_san', 'perc_dom_lixo', 'perc_dom_sem_ee',):
            res[k] = conv_decimal(res[k])

        domicilio02 = Domicilio02.objects.filter(cod_setor__in=self).aggregate(
            num_mor_casas=Sum('V003') + Sum('V004'),
            perc_mor_casas=((Sum('V003') + Sum('V004')) * 100) / Sum('V002'),
            num_mor_ap=Sum('V005'),
            perc_mor_ap=(Sum('V005') * 100) / Sum('V002'),
            num_mor_sem_agua=Sum('V002') - Sum('V012'),
            perc_mor_sem_agua=((Sum('V002') - Sum('V012')) * 100) / Sum('V002'),
            num_mor_sem_san=Sum('V023'),
            perc_mor_sem_san=(Sum('V023') * 100) / Sum('V002'),
            num_mor_lixo=Sum('V002') - Sum('V030'),
            perc_mor_lixo=((Sum('V002') - Sum('V030')) * 100) / Sum('V002'),
            num_mor_sem_ee=Sum('V041'),
            perc_mor_sem_ee=(Sum('V041') * 100) / Sum('V002'),
        )

        res.update(domicilio02)

        for k in ('num_mor_casas', 'num_mor_ap', 'num_mor_sem_agua', 'num_mor_sem_san', 'num_mor_lixo', 'num_mor_sem_ee',):
            res[k] = conv_int(res[k])

        for k in ('perc_mor_casas', 'perc_mor_ap', 'perc_mor_sem_agua', 'perc_mor_sem_san', 'perc_mor_lixo', 'perc_mor_sem_ee',):
            res[k] = conv_decimal(res[k])

        # Infraestrutura Urbana

        entorno01 = Entorno01.objects.filter(cod_setor__in=self).aggregate(
            arborizacao_dom_sim=Sum('V044') + Sum('V046') + Sum('V048'),
            arborizacao_dom_nao=Sum('V045') + Sum('V047') + Sum('V049'),
            calcada_dom_sim=Sum('V020') + Sum('V022') + Sum('V024'),
            calcada_dom_nao=Sum('V021') + Sum('V023') + Sum('V025'),
            iluminacao_dom_sim=Sum('V008') + Sum('V010') + Sum('V012'),
            iluminacao_dom_nao=Sum('V009') + Sum('V011') + Sum('V013'),
            pavimentacao_dom_sim=Sum('V014') + Sum('V016') + Sum('V018'),
            pavimentacao_dom_nao=Sum('V015') + Sum('V017') + Sum('V019'),
            rampa_dom_sim=Sum('V038') + Sum('V040') + Sum('V042'),
            rampa_dom_nao=Sum('V039') + Sum('V041') + Sum('V043'),
            esgoto_dom_sim=Sum('V050') + Sum('V052') + Sum('V054'),
            esgoto_dom_nao=Sum('V051') + Sum('V053') + Sum('V055'),
            lixo_dom_sim=Sum('V056') + Sum('V058') + Sum('V060'),
            lixo_dom_nao=Sum('V057') + Sum('V059') + Sum('V061'),
        )
        res.update(entorno01)

        for k in entorno01.keys():
            res[k] = conv_int(res[k])

        entorno03 = Entorno03.objects.filter(cod_setor__in=self).aggregate(
            arborizacao_mor_sim=Sum('V465') + Sum('V467') + Sum('V469'),
            arborizacao_mor_nao=Sum('V466') + Sum('V468') + Sum('V470'),
            calcada_mor_sim=Sum('V441') + Sum('V443') + Sum('V445'),
            calcada_mor_nao=Sum('V442') + Sum('V444') + Sum('V446'),
            iluminacao_mor_sim=Sum('V429') + Sum('V431') + Sum('V433'),
            iluminacao_mor_nao=Sum('V430') + Sum('V432') + Sum('V434'),
            pavimentacao_mor_sim=Sum('V435') + Sum('V437') + Sum('V439'),
            pavimentacao_mor_nao=Sum('V436') + Sum('V438') + Sum('V440'),
            rampa_mor_sim=Sum('V459') + Sum('V461') + Sum('V463'),
            rampa_mor_nao=Sum('V460') + Sum('V462') + Sum('V464'),
            esgoto_mor_sim=Sum('V471') + Sum('V473') + Sum('V475'),
            esgoto_mor_nao=Sum('V472') + Sum('V474') + Sum('V476'),
            lixo_mor_sim=Sum('V477') + Sum('V479') + Sum('V481'),
            lixo_mor_nao=Sum('V478') + Sum('V480') + Sum('V482'),
        )
        res.update(entorno03)

        for k in entorno03.keys():
            res[k] = conv_int(res[k])

        # Renda

        domiciliorenda = DomicilioRenda.objects.filter(cod_setor__in=self).aggregate(
            dom_sem_rend=Sum('V014'),
            dom_meio_sm=Sum('V005') + Sum('V006') + Sum('V007'),
            dom_meio_1_sm=Sum('V008'),
            dom_1_2_sm=Sum('V009'),
            dom_2_3_sm=Sum('V010'),
            dom_3_5_sm=Sum('V011'),
            dom_5_10_sm=Sum('V012'),
            dom_10_25_sm=Sum('V013'),
        )
        res.update(domiciliorenda)

        for k in domiciliorenda.keys():
            res[k] = conv_int(res[k])

        pessoarenda = PessoaRenda.objects.filter(cod_setor__in=self).aggregate(
            p_h_sem_rend=Sum('V032'),
            p_h_meio_sm=Sum('V023'),
            p_h_meio_1_sm=Sum('V024'),
            p_h_1_2_sm=Sum('V025'),
            p_h_2_3_sm=Sum('V026'),
            p_h_3_5_sm=Sum('V027'),
            p_h_5_10_sm=Sum('V028'),
            p_h_10_25_sm=Sum('V029'),
            p_h_15_20_sm=Sum('V030'),
            p_h_20_sm=Sum('V031'),
            p_m_sem_rend=Sum('V054'),
            p_m_meio_sm=Sum('V045'),
            p_m_meio_1_sm=Sum('V046'),
            p_m_1_2_sm=Sum('V047'),
            p_m_2_3_sm=Sum('V048'),
            p_m_3_5_sm=Sum('V049'),
            p_m_5_10_sm=Sum('V050'),
            p_m_10_25_sm=Sum('V051'),
            p_m_15_20_sm=Sum('V052'),
            p_m_20_sm=Sum('V053'),
            # p_t_sem_rend=Sum('V010'),
            # p_t_meio_sm=Sum('V001'),
            # p_t_meio_1_sm=Sum('V002'),
            # p_t_1_2_sm=Sum('V003'),
            # p_t_2_3_sm=Sum('V004'),
            # p_t_3_5_sm=Sum('V005'),
            # p_t_5_10_sm=Sum('V006'),
            # p_t_10_25_sm=Sum('V007'),
            # p_t_15_20_sm=Sum('V008'),
            # p_t_20_sm=Sum('V009'),
        )
        res.update(pessoarenda)

        for k in pessoarenda.keys():
            res[k] = conv_int(res[k])

        # População - Pirâmide Etária

        pessoa11 = Pessoa11.objects.filter(cod_setor__in=self).aggregate(
            p_h_100=Sum('V134'),
            p_h_95_99=Sum('V129') + Sum('V130') + Sum('V131') + Sum('V132') + Sum('V133'),
            p_h_90_94=Sum('V124') + Sum('V125') + Sum('V126') + Sum('V127') + Sum('V128'),
            p_h_85_89=Sum('V119') + Sum('V120') + Sum('V121') + Sum('V122') + Sum('V123'),
            p_h_80_84=Sum('V114') + Sum('V115') + Sum('V116') + Sum('V117') + Sum('V118'),
            p_h_75_79=Sum('V109') + Sum('V110') + Sum('V111') + Sum('V112') + Sum('V113'),
            p_h_70_74=Sum('V104') + Sum('V105') + Sum('V106') + Sum('V107') + Sum('V108'),
            p_h_65_69=Sum('V099') + Sum('V100') + Sum('V101') + Sum('V102') + Sum('V103'),
            p_h_60_64=Sum('V094') + Sum('V095') + Sum('V096') + Sum('V097') + Sum('V098'),
            p_h_55_59=Sum('V089') + Sum('V090') + Sum('V091') + Sum('V092') + Sum('V093'),
            p_h_50_54=Sum('V084') + Sum('V085') + Sum('V086') + Sum('V087') + Sum('V088'),
            p_h_45_49=Sum('V079') + Sum('V080') + Sum('V081') + Sum('V082') + Sum('V083'),
            p_h_40_44=Sum('V074') + Sum('V075') + Sum('V076') + Sum('V077') + Sum('V078'),
            p_h_35_39=Sum('V069') + Sum('V070') + Sum('V071') + Sum('V072') + Sum('V073'),
            p_h_30_34=Sum('V064') + Sum('V065') + Sum('V066') + Sum('V067') + Sum('V068'),
            p_h_25_29=Sum('V059') + Sum('V060') + Sum('V061') + Sum('V062') + Sum('V063'),
            p_h_20_24=Sum('V054') + Sum('V055') + Sum('V056') + Sum('V057') + Sum('V058'),
            p_h_15_19=Sum('V049') + Sum('V050') + Sum('V051') + Sum('V052') + Sum('V053'),
            p_h_10_14=Sum('V044') + Sum('V045') + Sum('V046') + Sum('V047') + Sum('V048'),
            p_h_5_9=Sum('V039') + Sum('V040') + Sum('V041') + Sum('V042') + Sum('V043'),
            p_h_0_4=Sum('V022') + Sum('V035') + Sum('V036') + Sum('V037') + Sum('V038'),
        )
        res.update(pessoa11)

        for k in pessoa11.keys():
            res[k] = conv_int(res[k])

        pessoa12 = Pessoa12.objects.filter(cod_setor__in=self).aggregate(
            p_m_100=Sum('V134'),
            p_m_95_99=Sum('V129') + Sum('V130') + Sum('V131') + Sum('V132') + Sum('V133'),
            p_m_90_94=Sum('V124') + Sum('V125') + Sum('V126') + Sum('V127') + Sum('V128'),
            p_m_85_89=Sum('V119') + Sum('V120') + Sum('V121') + Sum('V122') + Sum('V123'),
            p_m_80_84=Sum('V114') + Sum('V115') + Sum('V116') + Sum('V117') + Sum('V118'),
            p_m_75_79=Sum('V109') + Sum('V110') + Sum('V111') + Sum('V112') + Sum('V113'),
            p_m_70_74=Sum('V104') + Sum('V105') + Sum('V106') + Sum('V107') + Sum('V108'),
            p_m_65_69=Sum('V099') + Sum('V100') + Sum('V101') + Sum('V102') + Sum('V103'),
            p_m_60_64=Sum('V094') + Sum('V095') + Sum('V096') + Sum('V097') + Sum('V098'),
            p_m_55_59=Sum('V089') + Sum('V090') + Sum('V091') + Sum('V092') + Sum('V093'),
            p_m_50_54=Sum('V084') + Sum('V085') + Sum('V086') + Sum('V087') + Sum('V088'),
            p_m_45_49=Sum('V079') + Sum('V080') + Sum('V081') + Sum('V082') + Sum('V083'),
            p_m_40_44=Sum('V074') + Sum('V075') + Sum('V076') + Sum('V077') + Sum('V078'),
            p_m_35_39=Sum('V069') + Sum('V070') + Sum('V071') + Sum('V072') + Sum('V073'),
            p_m_30_34=Sum('V064') + Sum('V065') + Sum('V066') + Sum('V067') + Sum('V068'),
            p_m_25_29=Sum('V059') + Sum('V060') + Sum('V061') + Sum('V062') + Sum('V063'),
            p_m_20_24=Sum('V054') + Sum('V055') + Sum('V056') + Sum('V057') + Sum('V058'),
            p_m_15_19=Sum('V049') + Sum('V050') + Sum('V051') + Sum('V052') + Sum('V053'),
            p_m_10_14=Sum('V044') + Sum('V045') + Sum('V046') + Sum('V047') + Sum('V048'),
            p_m_5_9=Sum('V039') + Sum('V040') + Sum('V041') + Sum('V042') + Sum('V043'),
            p_m_0_4=Sum('V022') + Sum('V035') + Sum('V036') + Sum('V037') + Sum('V038'),
        )
        res.update(pessoa12)

        for k in pessoa12.keys():
            res[k] = conv_int(res[k])

        # População - Raça/Cor

        pessoa03 = Pessoa03.objects.filter(cod_setor__in=self).aggregate(
            raca_cor_branca=Sum('V002'),
            raca_cor_preta=Sum('V003'),
            raca_cor_amarela=Sum('V004'),
            raca_cor_parda=Sum('V005'),
            raca_cor_indigena=Sum('V006'),
        )

        res.update(pessoa03)

        for k in pessoa03.keys():
            res[k] = conv_int(res[k])

        # Responsáveis por domicílios

        responsavel02 = Responsavel02.objects.filter(cod_setor__in=self).aggregate(
            resp_den_perc=Sum('V001'),
            resp_h=Sum('V109'),
            resp_h_nao_alfa=Sum('V109') - Sum('V201'),
            resp_h_18=Sum('V110') + Sum('V111') + Sum('V112') + Sum('V113') + Sum('V114') + Sum('V115') + Sum('V116') + Sum('V117') + Sum('V118'),
            resp_t=Sum('V001'),
            resp_t_nao_alfa=Sum('V001') - Sum('V093'),
            resp_t_18=Sum('V002') + Sum('V003') + Sum('V004') + Sum('V005') + Sum('V006') + Sum('V007') + Sum('V008') + Sum('V009') + Sum('V010'),
        )

        res.update(responsavel02)

        for k in responsavel02.keys():
            res[k] = conv_int(res[k])
            if res[k] is not None:
                res['{}_perc'.format(k)] = conv_decimal(
                    (res[k] * 100) / float(res['resp_den_perc']))

        responsavel01 = Responsavel01.objects.filter(cod_setor__in=self).aggregate(
            resp_m=Sum('V001'),
            resp_m_nao_alfa=Sum('V001') - Sum('V093'),
            resp_m_18=Sum('V002') + Sum('V003') + Sum('V004') + Sum('V005') + Sum('V006') + Sum('V007') + Sum('V008') + Sum('V009') + Sum('V010'),
        )

        res.update(responsavel01)

        for k in responsavel01.keys():
            res[k] = conv_int(res[k])
            if res[k] is not None:
                res['{}_perc'.format(k)] = conv_decimal(
                    (res[k] * 100) / float(res['resp_den_perc']))

        responsavelrenda = ResponsavelRenda.objects.filter(cod_setor__in=self).aggregate(
            resp_h_sem_rend=Sum('V032'),
            resp_h_rend_1_sm=Sum('V023') + Sum('V024'),
            resp_m_sem_rend=Sum('V054'),
            resp_m_rend_1_sm=Sum('V045') + Sum('V046'),
            resp_t_sem_rend=Sum('V010'),
            resp_t_rend_1_sm=Sum('V001') + Sum('V002'),
        )

        res.update(responsavelrenda)

        for k in responsavelrenda.keys():
            res[k] = conv_int(res[k])
            if res[k] is not None:
                res['{}_perc'.format(k)] = conv_decimal(
                    (res[k] * 100) / float(res['resp_den_perc']))

        # Alfabetização

        pessoa01 = Pessoa01.objects.filter(cod_setor__in=self).aggregate(
            alfa_t_5_9=Sum('V002') + Sum('V003') + Sum('V004') + Sum('V005') + Sum('V006'),
            alfa_t_10_14=Sum('V007') + Sum('V008') + Sum('V009') + Sum('V010') + Sum('V011'),
            alfa_t_15_19=Sum('V012') + Sum('V013') + Sum('V014') + Sum('V015') + Sum('V016'),
            alfa_t_20_24=Sum('V017') + Sum('V018') + Sum('V019') + Sum('V020') + Sum('V021'),
            alfa_t_25_29=Sum('V022') + Sum('V023') + Sum('V024') + Sum('V025') + Sum('V026'),
            alfa_t_30_34=Sum('V027') + Sum('V028') + Sum('V029') + Sum('V030') + Sum('V031'),
            alfa_t_35_39=Sum('V032') + Sum('V033') + Sum('V034') + Sum('V035') + Sum('V036'),
            alfa_t_40_44=Sum('V037') + Sum('V038') + Sum('V039') + Sum('V040') + Sum('V041'),
            alfa_t_45_49=Sum('V042') + Sum('V043') + Sum('V044') + Sum('V045') + Sum('V046'),
            alfa_t_50_54=Sum('V047') + Sum('V048') + Sum('V049') + Sum('V050') + Sum('V051'),
            alfa_t_55_59=Sum('V052') + Sum('V053') + Sum('V054') + Sum('V055') + Sum('V056'),
            alfa_t_60_64=Sum('V057') + Sum('V058') + Sum('V059') + Sum('V060') + Sum('V061'),
            alfa_t_65_69=Sum('V062') + Sum('V063') + Sum('V064') + Sum('V065') + Sum('V066'),
            alfa_t_70_74=Sum('V067') + Sum('V068') + Sum('V069') + Sum('V070') + Sum('V071'),
            alfa_t_75_79=Sum('V072') + Sum('V073') + Sum('V074') + Sum('V075') + Sum('V076'),
            alfa_t_80=Sum('V077'),
        )
        res.update(pessoa01)
        for k in pessoa01.keys():
            res[k] = conv_int(res[k])

        pessoa13 = Pessoa13.objects.filter(cod_setor__in=self).aggregate(
            nao_alfa_t_5_9=Sum('V039') + Sum('V040') + Sum('V041') + Sum('V042') + Sum('V043'),
            nao_alfa_t_10_14=Sum('V044') + Sum('V045') + Sum('V046') + Sum('V047') + Sum('V048'),
            nao_alfa_t_15_19=Sum('V049') + Sum('V050') + Sum('V051') + Sum('V052') + Sum('V053'),
            nao_alfa_t_20_24=Sum('V054') + Sum('V055') + Sum('V056') + Sum('V057') + Sum('V058'),
            nao_alfa_t_25_29=Sum('V059') + Sum('V060') + Sum('V061') + Sum('V062') + Sum('V063'),
            nao_alfa_t_30_34=Sum('V064') + Sum('V065') + Sum('V066') + Sum('V067') + Sum('V068'),
            nao_alfa_t_35_39=Sum('V069') + Sum('V070') + Sum('V071') + Sum('V072') + Sum('V073'),
            nao_alfa_t_40_44=Sum('V074') + Sum('V075') + Sum('V076') + Sum('V077') + Sum('V078'),
            nao_alfa_t_45_49=Sum('V079') + Sum('V080') + Sum('V081') + Sum('V082') + Sum('V083'),
            nao_alfa_t_50_54=Sum('V084') + Sum('V085') + Sum('V086') + Sum('V087') + Sum('V088'),
            nao_alfa_t_55_59=Sum('V089') + Sum('V090') + Sum('V091') + Sum('V092') + Sum('V093'),
            nao_alfa_t_60_64=Sum('V094') + Sum('V095') + Sum('V096') + Sum('V097') + Sum('V098'),
            nao_alfa_t_65_69=Sum('V099') + Sum('V100') + Sum('V101') + Sum('V102') + Sum('V103'),
            nao_alfa_t_70_74=Sum('V104') + Sum('V105') + Sum('V106') + Sum('V107') + Sum('V108'),
            nao_alfa_t_75_79=Sum('V109') + Sum('V110') + Sum('V111') + Sum('V112') + Sum('V113'),
            nao_alfa_t_80=Sum('V114') + Sum('V115') + Sum('V116') + Sum('V117') + Sum('V118') + Sum('V119') + Sum('V120') + Sum('V121') + Sum('V122') + Sum('V123') + Sum('V124') + Sum('V125') + Sum('V126') + Sum('V127') + Sum('V128') + Sum('V129') + Sum('V130') + Sum('V131') + Sum('V132') + Sum('V133'),
        )
        res.update(pessoa13)
        for k in pessoa13.keys():
            if res[k] is not None and res[k.replace('nao_', '')] is not None:
                res[k] = conv_int(res[k]) - res[k.replace('nao_', '')]

        pessoa02 = Pessoa02.objects.filter(cod_setor__in=self).aggregate(
            alfa_h_5_9=Sum('V087') + Sum('V088') + Sum('V089') + Sum('V090') + Sum('V091'),
            alfa_h_10_14=Sum('V092') + Sum('V093') + Sum('V094') + Sum('V095') + Sum('V096'),
            alfa_h_15_19=Sum('V097') + Sum('V098') + Sum('V099') + Sum('V100') + Sum('V101'),
            alfa_h_20_24=Sum('V102') + Sum('V103') + Sum('V104') + Sum('V105') + Sum('V106'),
            alfa_h_25_29=Sum('V107') + Sum('V108') + Sum('V109') + Sum('V110') + Sum('V111'),
            alfa_h_30_34=Sum('V112') + Sum('V113') + Sum('V114') + Sum('V115') + Sum('V116'),
            alfa_h_35_39=Sum('V117') + Sum('V118') + Sum('V119') + Sum('V120') + Sum('V121'),
            alfa_h_40_44=Sum('V122') + Sum('V123') + Sum('V124') + Sum('V125') + Sum('V126'),
            alfa_h_45_49=Sum('V127') + Sum('V128') + Sum('V129') + Sum('V130') + Sum('V131'),
            alfa_h_50_54=Sum('V132') + Sum('V133') + Sum('V134') + Sum('V135') + Sum('V136'),
            alfa_h_55_59=Sum('V137') + Sum('V138') + Sum('V139') + Sum('V140') + Sum('V141'),
            alfa_h_60_64=Sum('V142') + Sum('V143') + Sum('V144') + Sum('V145') + Sum('V146'),
            alfa_h_65_69=Sum('V147') + Sum('V148') + Sum('V149') + Sum('V150') + Sum('V151'),
            alfa_h_70_74=Sum('V152') + Sum('V153') + Sum('V154') + Sum('V155') + Sum('V156'),
            alfa_h_75_79=Sum('V157') + Sum('V158') + Sum('V159') + Sum('V160') + Sum('V161'),
            alfa_h_80=Sum('V162'),
            alfa_m_5_9=Sum('V172') + Sum('V173') + Sum('V174') + Sum('V175') + Sum('V176'),
            alfa_m_10_14=Sum('V177') + Sum('V178') + Sum('V179') + Sum('V180') + Sum('V181'),
            alfa_m_15_19=Sum('V182') + Sum('V183') + Sum('V184') + Sum('V185') + Sum('V186'),
            alfa_m_20_24=Sum('V187') + Sum('V188') + Sum('V189') + Sum('V190') + Sum('V191'),
            alfa_m_25_29=Sum('V192') + Sum('V193') + Sum('V194') + Sum('V195') + Sum('V196'),
            alfa_m_30_34=Sum('V197') + Sum('V198') + Sum('V199') + Sum('V200') + Sum('V201'),
            alfa_m_35_39=Sum('V202') + Sum('V203') + Sum('V204') + Sum('V205') + Sum('V206'),
            alfa_m_40_44=Sum('V207') + Sum('V208') + Sum('V209') + Sum('V210') + Sum('V211'),
            alfa_m_45_49=Sum('V212') + Sum('V213') + Sum('V214') + Sum('V215') + Sum('V216'),
            alfa_m_50_54=Sum('V217') + Sum('V218') + Sum('V219') + Sum('V220') + Sum('V221'),
            alfa_m_55_59=Sum('V222') + Sum('V223') + Sum('V224') + Sum('V225') + Sum('V226'),
            alfa_m_60_64=Sum('V227') + Sum('V228') + Sum('V229') + Sum('V230') + Sum('V231'),
            alfa_m_65_69=Sum('V232') + Sum('V233') + Sum('V234') + Sum('V235') + Sum('V236'),
            alfa_m_70_74=Sum('V237') + Sum('V238') + Sum('V239') + Sum('V240') + Sum('V241'),
            alfa_m_75_79=Sum('V242') + Sum('V243') + Sum('V244') + Sum('V245') + Sum('V246'),
            alfa_m_80=Sum('V247'),
        )
        res.update(pessoa02)
        for k in pessoa02.keys():
            res[k] = conv_int(res[k])

        pessoa11 = Pessoa11.objects.filter(cod_setor__in=self).aggregate(
            nao_alfa_h_5_9=Sum('V039') + Sum('V040') + Sum('V041') + Sum('V042') + Sum('V043'),
            nao_alfa_h_10_14=Sum('V044') + Sum('V045') + Sum('V046') + Sum('V047') + Sum('V048'),
            nao_alfa_h_15_19=Sum('V049') + Sum('V050') + Sum('V051') + Sum('V052') + Sum('V053'),
            nao_alfa_h_20_24=Sum('V054') + Sum('V055') + Sum('V056') + Sum('V057') + Sum('V058'),
            nao_alfa_h_25_29=Sum('V059') + Sum('V060') + Sum('V061') + Sum('V062') + Sum('V063'),
            nao_alfa_h_30_34=Sum('V064') + Sum('V065') + Sum('V066') + Sum('V067') + Sum('V068'),
            nao_alfa_h_35_39=Sum('V069') + Sum('V070') + Sum('V071') + Sum('V072') + Sum('V073'),
            nao_alfa_h_40_44=Sum('V074') + Sum('V075') + Sum('V076') + Sum('V077') + Sum('V078'),
            nao_alfa_h_45_49=Sum('V079') + Sum('V080') + Sum('V081') + Sum('V082') + Sum('V083'),
            nao_alfa_h_50_54=Sum('V084') + Sum('V085') + Sum('V086') + Sum('V087') + Sum('V088'),
            nao_alfa_h_55_59=Sum('V089') + Sum('V090') + Sum('V091') + Sum('V092') + Sum('V093'),
            nao_alfa_h_60_64=Sum('V094') + Sum('V095') + Sum('V096') + Sum('V097') + Sum('V098'),
            nao_alfa_h_65_69=Sum('V099') + Sum('V100') + Sum('V101') + Sum('V102') + Sum('V103'),
            nao_alfa_h_70_74=Sum('V104') + Sum('V105') + Sum('V106') + Sum('V107') + Sum('V108'),
            nao_alfa_h_75_79=Sum('V109') + Sum('V110') + Sum('V111') + Sum('V112') + Sum('V113'),
            nao_alfa_h_80=Sum('V114') + Sum('V115') + Sum('V116') + Sum('V117') + Sum('V118') + Sum('V119') + Sum('V120') + Sum('V121') + Sum('V122') + Sum('V123') + Sum('V124') + Sum('V125') + Sum('V126') + Sum('V127') + Sum('V128') + Sum('V129') + Sum('V130') + Sum('V131') + Sum('V132') + Sum('V133'),
        )
        res.update(pessoa11)
        for k in pessoa11.keys():
            if res[k] is not None and res[k.replace('nao_', '')] is not None:
                res[k] = conv_int(res[k]) - res[k.replace('nao_', '')]

        pessoa12 = Pessoa12.objects.filter(cod_setor__in=self).aggregate(
            nao_alfa_m_5_9=Sum('V039') + Sum('V040') + Sum('V041') + Sum('V042') + Sum('V043'),
            nao_alfa_m_10_14=Sum('V044') + Sum('V045') + Sum('V046') + Sum('V047') + Sum('V048'),
            nao_alfa_m_15_19=Sum('V049') + Sum('V050') + Sum('V051') + Sum('V052') + Sum('V053'),
            nao_alfa_m_20_24=Sum('V054') + Sum('V055') + Sum('V056') + Sum('V057') + Sum('V058'),
            nao_alfa_m_25_29=Sum('V059') + Sum('V060') + Sum('V061') + Sum('V062') + Sum('V063'),
            nao_alfa_m_30_34=Sum('V064') + Sum('V065') + Sum('V066') + Sum('V067') + Sum('V068'),
            nao_alfa_m_35_39=Sum('V069') + Sum('V070') + Sum('V071') + Sum('V072') + Sum('V073'),
            nao_alfa_m_40_44=Sum('V074') + Sum('V075') + Sum('V076') + Sum('V077') + Sum('V078'),
            nao_alfa_m_45_49=Sum('V079') + Sum('V080') + Sum('V081') + Sum('V082') + Sum('V083'),
            nao_alfa_m_50_54=Sum('V084') + Sum('V085') + Sum('V086') + Sum('V087') + Sum('V088'),
            nao_alfa_m_55_59=Sum('V089') + Sum('V090') + Sum('V091') + Sum('V092') + Sum('V093'),
            nao_alfa_m_60_64=Sum('V094') + Sum('V095') + Sum('V096') + Sum('V097') + Sum('V098'),
            nao_alfa_m_65_69=Sum('V099') + Sum('V100') + Sum('V101') + Sum('V102') + Sum('V103'),
            nao_alfa_m_70_74=Sum('V104') + Sum('V105') + Sum('V106') + Sum('V107') + Sum('V108'),
            nao_alfa_m_75_79=Sum('V109') + Sum('V110') + Sum('V111') + Sum('V112') + Sum('V113'),
            nao_alfa_m_80=Sum('V114') + Sum('V115') + Sum('V116') + Sum('V117') + Sum('V118') + Sum('V119') + Sum('V120') + Sum('V121') + Sum('V122') + Sum('V123') + Sum('V124') + Sum('V125') + Sum('V126') + Sum('V127') + Sum('V128') + Sum('V129') + Sum('V130') + Sum('V131') + Sum('V132') + Sum('V133'),
        )
        res.update(pessoa12)
        for k in pessoa12.keys():
            if res[k] is not None and res[k.replace('nao_', '')] is not None:
                res[k] = conv_int(res[k]) - res[k.replace('nao_', '')]

        return res


SetorCensitarioManager = models.GeoManager.from_queryset(SetorCensitarioQuerySet)


class SetorCensitario(models.Model):
    '''Setores Censitários do IBGE'''
    cd_geocodi = models.BigIntegerField(primary_key=True)
    tipo = models.CharField(max_length=10, blank=True)
    cd_geocodb = models.CharField(max_length=20, blank=True)
    nm_bairro = models.CharField(max_length=60, blank=True)
    cd_geocods = models.CharField(max_length=20, blank=True)
    nm_subdist = models.CharField(max_length=60, blank=True)
    cd_geocodd = models.CharField(max_length=20, blank=True)
    nm_distrit = models.CharField(max_length=60, blank=True)
    cd_geocodm = models.CharField(max_length=20, blank=True)
    nm_municip = models.CharField(max_length=60, blank=True)
    nm_micro = models.CharField(max_length=100, blank=True)
    nm_meso = models.CharField(max_length=100, blank=True)
    municipio = models.ForeignKey(Municipio)
    geom = models.MultiPolygonField(srid=settings.SRID)

    objects = SetorCensitarioManager()

    class Meta:
        verbose_name = u'Setor Censitário'
        verbose_name_plural = u'Setores Censitários'

    @property
    def id(self):
        return self.cd_geocodi

    def __unicode__(self):
        return unicode(self.cd_geocodi)
