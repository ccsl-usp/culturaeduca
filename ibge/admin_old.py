from django.contrib.gis import admin

from ibge.models import Uf
from ibge.models import Municipio
from ibge.models import SetorCensitario

class MunicipioAdmin(admin.OSMGeoAdmin):

    list_display = ('nm_municip',)
    search_fields = ('nm_municip',)


class UfAdmin(admin.OSMGeoAdmin):

    list_display = ('nome',)


class SetorCensitarioAdmin(admin.OSMGeoAdmin):

    list_display = ('cd_geocodi',)


admin.site.register(Uf, UfAdmin)
admin.site.register(Municipio, MunicipioAdmin)
admin.site.register(SetorCensitario, SetorCensitarioAdmin)
