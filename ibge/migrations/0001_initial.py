# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Municipio',
            fields=[
                ('cd_geocodm', models.BigIntegerField(serialize=False, primary_key=True)),
                ('nm_municip', models.CharField(max_length=60, blank=True)),
                ('uf_sigla', models.CharField(max_length=2)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4674)),
            ],
            options={
                'verbose_name': 'Munic\xedpio',
                'verbose_name_plural': 'Munic\xedpios',
            },
        ),
        migrations.CreateModel(
            name='SetorCensitario',
            fields=[
                ('cd_geocodi', models.BigIntegerField(serialize=False, primary_key=True)),
                ('tipo', models.CharField(max_length=10, blank=True)),
                ('cd_geocodb', models.CharField(max_length=20, blank=True)),
                ('nm_bairro', models.CharField(max_length=60, blank=True)),
                ('cd_geocods', models.CharField(max_length=20, blank=True)),
                ('nm_subdist', models.CharField(max_length=60, blank=True)),
                ('cd_geocodd', models.CharField(max_length=20, blank=True)),
                ('nm_distrit', models.CharField(max_length=60, blank=True)),
                ('cd_geocodm', models.CharField(max_length=20, blank=True)),
                ('nm_municip', models.CharField(max_length=60, blank=True)),
                ('nm_micro', models.CharField(max_length=100, blank=True)),
                ('nm_meso', models.CharField(max_length=100, blank=True)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4674)),
                ('municipio', models.ForeignKey(to='ibge.Municipio')),
            ],
            options={
                'verbose_name': 'Setor Censit\xe1rio',
                'verbose_name_plural': 'Setores Censit\xe1rios',
            },
        ),
        migrations.CreateModel(
            name='UF',
            fields=[
                ('cd_geocuf', models.SmallIntegerField(serialize=False, primary_key=True)),
                ('nm_estado', models.CharField(max_length=50)),
                ('nm_regiao', models.CharField(max_length=20)),
                ('uf', models.CharField(max_length=2)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4674)),
            ],
            options={
                'verbose_name': 'UF',
                'verbose_name_plural': 'UFs',
            },
        ),
        migrations.AddField(
            model_name='municipio',
            name='uf',
            field=models.ForeignKey(to='ibge.UF'),
        ),
    ]
