from django.conf.urls import include, url
import ibge.views

urlpatterns = [
	url(r'^contorno_municipio/(?P<ibge>[-\w]+).geojson$', ibge.views.contorno_municipio, name='contorno_municipio'),
    url(r'^contorno_uf/(?P<nome>[-\w]+).geojson$', ibge.views.contorno_uf, name='contorno_uf'),
]