
from django.conf.urls import url
from blog.views import EntryCreateView, EntryUpdateView, EntryShortLink

urlpatterns = [
    url(r'^cadastro/$', EntryCreateView.as_view(), name='entry_form'),

    url(
        r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
        EntryUpdateView.as_view(),
        name='entry_update'
    ),
    url(
        r'^(?P<token>[\dA-Z]+)/$',
        EntryShortLink.as_view(),
        name='entry_shortlink'
    ),
]