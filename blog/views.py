from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.views.generic.base import RedirectView
from django.views.generic.edit import (
    UpdateView, CreateView
)
from zinnia.models import Entry
from axis.core.views import AddRequestUserMixin
from blog.forms import EntryForm


class EntryMixin(object):
    model = Entry
    form_class = EntryForm
    template_name = 'zinnia/entry_form.html'


class EntryCreateView(EntryMixin, AddRequestUserMixin, CreateView):
    success_message = 'Artigo criado com sucesso!'
    success_url = '/blog/'


class EntryUpdateView(EntryMixin, AddRequestUserMixin, UpdateView):
    success_message = 'Artigo atualizado com sucesso!'
    success_url = '/blog/'


class EntryShortLink(RedirectView):
    """
    View for handling the shortlink of an Entry,
    simply do a redirection.
    """
    permanent = True

    def get_redirect_url(self, **kwargs):
        """
        Get entry corresponding to 'pk' encoded in base36
        in the 'token' variable and return the get_absolute_url
        of the entry.
        """
        entry = get_object_or_404(Entry.published, pk=int(kwargs['token'], 36))
        publication_date = entry.publication_date
        if timezone.is_aware(publication_date):
            publication_date = timezone.localtime(publication_date)
        return reverse('zinnia_artigos:entry_update', kwargs={
            'year': publication_date.strftime('%Y'),
            'month': publication_date.strftime('%m'),
            'day': publication_date.strftime('%d'),
            'slug': entry.slug
        })
