# coding: utf-8
from django.utils import timezone
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper
from django_select2.forms import ModelSelect2MultipleWidget
from django.contrib.sites.models import Site

from axis.core.models import User

from mptt.forms import TreeNodeChoiceField
from tagging.models import Tag

from zinnia.models.entry import Entry
from zinnia.models.category import Category
from zinnia.admin.widgets import MiniTextarea
from zinnia.admin.widgets import MPTTFilteredSelectMultiple
from zinnia.admin.fields import MPTTModelMultipleChoiceField


class CustomModelSelect2MultipleWidget(ModelSelect2MultipleWidget):

    def label_from_instance(self, obj):
        return unicode(obj)

    def build_attrs(self, base_attrs, extra_attrs=None):
        attrs = super(CustomModelSelect2MultipleWidget, self).build_attrs(
            base_attrs, extra_attrs=extra_attrs,
        )

        attrs.update({
            'data-minimum-input-length': 3,
            'data-placeholder': u'Digite o nome do %s' % (
                self.model._meta.verbose_name
            ),
        })

        return attrs


class CategoryForm(forms.ModelForm):
    """
    Form for Category's Admin.
    """
    parent = TreeNodeChoiceField(
        label=_('Parent category'),
        empty_label=_('No parent category'),
        level_indicator='|--', required=False,
        queryset=Category.objects.all())

    # def __init__(self, *args, **kwargs):
    #     super(CategoryForm, self).__init__(*args, **kwargs)
    #     self.fields['parent'].widget = RelatedFieldWidgetWrapper(
    #         self.fields['parent'].widget,
    #         Category.parent.field.rel,
    #         self.admin_site)

    def clean_parent(self):
        """
        Check if category parent is not selfish.
        """
        data = self.cleaned_data['parent']
        if data == self.instance:
            raise forms.ValidationError(
                _('A category cannot be parent of itself.'),
                code='self_parenting')
        return data

    class Meta:
        """
        CategoryForm's Meta.
        """
        model = Category
        fields = forms.ALL_FIELDS


class EntryForm(forms.ModelForm):
    """
    Form for Entry's Admin.
    """

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        super(EntryForm, self).__init__(*args, **kwargs)
        self.fields['publication_date'].widget = forms.SplitDateTimeWidget(
            attrs={'class': 'dateTimeWidget'}
        )
        self.fields['publication_date'].label = u'Data de Publicação'
        self.fields['start_publication'].widget = forms.SplitDateTimeWidget(
            attrs={'class': 'dateTimeWidget'}
        )
        self.fields['end_publication'].widget = forms.SplitDateTimeWidget(
            attrs={'class': 'dateTimeWidget'}
        )

    def save(self, commit=True):
        entry = super(EntryForm, self).save(commit=False)
        entry.pingback_enabled = False
        entry.trackback_enabled = False
        entry.content_template = 'zinnia/_entry_detail.html'
        entry.detail_template = 'entry_detail.html'
        entry.comment_count = 0
        entry.pingback_count = 0
        entry.trackback_count = 0
        entry.creation_date = timezone.now()
        entry.last_update = timezone.now()
        entry.save()
        self.save_m2m()
        return entry

        # self.fields['categories'].widget = RelatedFieldWidgetWrapper(
        #     self.fields['categories'].widget,
        #     Entry.categories.field.rel,
        #     self.admin_site
        # )

    class Meta:
        """
        EntryForm's Meta.
        """
        model = Entry
        exclude = (
            'creation_date',
            'last_update',
            'pingback_enabled',
            'trackback_enabled',
            'content_template',
            'detail_template',
            'comment_count',
            'pingback_count',
            'trackback_count',
        )
        widgets = {
            'lead': MiniTextarea,
            'excerpt': MiniTextarea,
            'image_caption': MiniTextarea,
            'sites': ModelSelect2MultipleWidget(model=Site, search_fields=['domain_icontains', ]),
            'tags': ModelSelect2MultipleWidget(
                model=Tag,
                search_fields=['title__icontains', ]
            ),
            'authors': CustomModelSelect2MultipleWidget(
                model=User,
                search_fields=['nome__icontains', ]
            ),
            'related': ModelSelect2MultipleWidget(
                model=Entry,
                search_fields=['title__icontains', ]
            ),
            'categories': ModelSelect2MultipleWidget(
                model=Category,
                search_fields=['title__icontains', ]
            )
        }
