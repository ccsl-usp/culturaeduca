# -*- coding: utf-8 -*-
from django.contrib.gis.serializers.geojson import Serializer as GeoJSONSerializer


class Serializer(GeoJSONSerializer):
    """ Custom GeoJSON serializer """

    def get_dump_object(self, obj):
        data = super(Serializer, self).get_dump_object(obj)
        # insere o id do model
        data.update(id=obj.pk)
        # insere os fields selecionados em properties
        # (normalmente não pega @property do objeto)
        properties = {}
        for field in self.selected_fields:
            if field != self.geometry_field:
                properties[field] = getattr(obj, field)
        data.update(properties=properties)
        return data
