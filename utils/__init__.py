# -*- coding: utf-8 -*-
import subprocess
from geopy.distance import vincenty
from django.apps import apps
from django.conf import settings


def clean_tilestache_cache(point, layer, raio=1, km_in_dd=0.008):
    point_buffer = point.buffer(float(raio) * km_in_dd)
    point_buffer.transform(4674)

    # west south east north
    extent = point_buffer.envelope.extent

    # south west north east
    extent = [extent[1], extent[0], extent[3], extent[2]]

    layer = ['-l', layer, ]
    config = ['-c', settings.OPT_TILESTACHE_CONF_FILE, ]
    extension = ['-e', 'pbf', ]

    # bbox with coords rounded with 3 decimal places
    bbox = ['-b', ] + map(lambda c: '%.3f' % c, extent)

    # positional args
    zoom = map(str, range(0, 22))

    # full cmd to clean cache
    subprocess_args = ['tilestache-clean.py', '-q'] + config + layer + extension + bbox + zoom
    try:
        subprocess.Popen(subprocess_args)
    except Exception:
        pass


def equipamentos_entorno(modelo, raio=1, geom_field='geom', km_in_dd=0.008):
    modelos = dict(apps.get_app_config('equipamento').models.items())

    # Near the equator, 1km is going to equal roughly 0.008 degrees
    # (1 km / 40,000 km * 360 degrees) of latitude and longitude
    # km_in_dd = 0.008

    modelo_geom = getattr(modelo, geom_field)

    ponto_buffer = modelo_geom.buffer(float(raio) * km_in_dd)

    equipamentos = []

    ct_total = 0

    for model_name in settings.EQUIPAMENTOS_ENTORNO_LIST_ORDER:
        equipamento = modelos[model_name]

        __geometry__ = equipamento.layer_config['fields'].get('__geometry__', {
            'field': 'geom',
        })

        # usa buffer em ponto e interseção para agilizar a
        # resposta, pois a query de distância é mais complexa
        qs = equipamento.objects.filter(**{'{}__intersects'.format(__geometry__['field']): ponto_buffer})

        if isinstance(modelo, equipamento):
            qs = qs.exclude(pk=modelo.pk)

        # if equipamento.layer_config['filters'] is False:

        equips = qs
        ct = equips.count()
        if ct == 1:
            nome_tipo_equipamento = equipamento._meta.verbose_name
        else:
            nome_tipo_equipamento = equipamento._meta.verbose_name_plural

        if nome_tipo_equipamento.islower():
            nome_tipo_equipamento = nome_tipo_equipamento.capitalize()

        # pode ser substituído por:
        # from django.contrib.gis.db.models.functions import Distance
        # usando Django 1.9
        for e in equips:
            e.distancia = int(
                vincenty(modelo_geom.coords, e.geom.coords).m)

        equipamentos.append([nome_tipo_equipamento, ct, equips, model_name])
        ct_total += ct

        # else:
        #     for layer_suffix, filter_dict in equipamento.layer_config['filters']:
        #         equips = qs.filter(**filter_dict['kwargs'])
        #         ct = equips.count()
        #         if ct == 1:
        #             nome_tipo_equipamento = equipamento._meta.verbose_name
        #         else:
        #             nome_tipo_equipamento = equipamento._meta.verbose_name_plural

        #         if nome_tipo_equipamento.islower():
        #             nome_tipo_equipamento = nome_tipo_equipamento.capitalize()

        #         nome_tipo_equipamento = '{} - {}'.format(nome_tipo_equipamento, filter_dict['label'])

        #         # pode ser substituído por:
        #         # from django.contrib.gis.db.models.functions import Distance
        #         # usando Django 1.9
        #         for e in equips:
        #             e.distancia = int(
        #                 vincenty(modelo_geom.coords, e.geom.coords).m)

        #         equipamentos.append([nome_tipo_equipamento, ct, equips, '{}_{}'.format(model_name, layer_suffix)])
        #         ct_total += ct

    return ct_total, equipamentos
