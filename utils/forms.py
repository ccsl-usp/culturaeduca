import floppyforms.__future__ as forms


class DatePicker(forms.DateInput):
    template_name = 'forms/datepicker.html'

    class Media:
        js = (
            'js/jquery.min.js',
            'js/ui/jquery-ui.min.js'
            )

        css = {
            'all': (
                'js/ui/jquery-ui.css',
                )
        }


class Monetario(forms.TextInput):
    template_name = 'forms/monetario.html'


class Checkbox(forms.CheckboxSelectMultiple):
    template_name = 'forms/checkbox.html'


class SelectMulti(forms.SelectMultiple):
    template_name = 'forms/select_multi.html'

    class Media:
        js = (
            'js/multiselect/js/jquery.multi-select.js',
            )

        css = {
            'all': (
                'js/multiselect/css/multi-select.css',
            )
        }


class SelectMultiBusca(forms.SelectMultiple):
    template_name = 'forms/select_multi_busca.html'
