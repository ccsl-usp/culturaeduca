# coding: utf-8
from django import template
from django.forms import widgets
from django.template.loader import get_template
from django.utils.safestring import SafeText

register = template.Library()


@register.filter
def as_fdt(field, col=None):

    try:
        widget = field.field.widget
    except:
        if isinstance(field, str) or isinstance(field, SafeText):
            return field
        raise ValueError("Expected a Field, got a %s" % type(field))

    try:
        clazz = {'class': widget.attrs['class'] + ' validate'}
    except KeyError:
        clazz = {'class': 'validate'}
    widget.attrs.update(clazz)

    if field.field.required:
        widget.attrs['required'] = 'true'

    if isinstance(widget, widgets.TextInput):
        input_type = u'text'
    elif isinstance(widget, widgets.Textarea):
        input_type = u'textarea'
    elif isinstance(widget, widgets.CheckboxInput):
        input_type = u'checkbox'
    elif isinstance(widget, widgets.CheckboxSelectMultiple):
        input_type = u'multicheckbox'
    elif isinstance(widget, widgets.RadioSelect):
        input_type = u'radioset'
    elif isinstance(widget, widgets.Select):
        input_type = u'select'
    else:
        input_type = u'default'

    return get_template("foundation_filter/field.html").render(
        {
            'field': field,
            'input_type': input_type,
        }
    )
