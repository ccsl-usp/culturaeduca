# Requisitos

Os comandos de instalação assumem que esteja utilizando uma distribuição Ubuntu/Debian de Linux

Caso esteja utilizando Windows, é altamente recomendado utilizar [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install-win10) com Ubuntu

## Docker

Utilizaremos [Docker](https://www.docker.com/) para rodar o banco de dados e o ambiente de desenvolvimento. ([Vídeo rápido com os conceitos principais](https://www.youtube.com/watch?v=Gjnup-PuquQTem))

No caso de Windows com WSL2, basta baixar e instalar o [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows)

No caso de uma distribuição Linux:

```sh
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```

Pra rodar docker sem sudo:

```sh
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
```

É pra estar rodando agora:

```sh
docker --version
docker run hello-world
```

Tem mais uma coisa que vamos precisar, o Docker Compose:

```sh
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

É pra estar rodando agora:

```
docker-compose --version
```

# Instalação

## Configuração docker-compose.yml

Todos os comandos desta seção são rodados na raiz do projeto
Todos os exemplos encapsulados entre < > devem ser substituídos sem os símbolos

Crie um arquivo `docker-compose.yml` a partir do arquivo `default.docker-compose.dev.yml`

```sh
cp default.docker-compose.dev.yml docker-compose.yml
```

Altere `<DB_PORT>`, `<DB_USER>`, `<DB_PASSWORD>`,`<DB_NAME>` e `<SECRET_KEY>` para sua configuração desejada.

Para rodar os containers de banco de dados e de desenvolvimento:

```sh
docker-compose up --build
```

Para parar os containers: `CTRL+C`

Para rodar os containers em background:

```sh
docker-compose up --build -d
```

Para parar os containers que estão em background:

```sh
docker-compose down
```

Para visualizar os containers que estão rodando:

```sh
docker container ps
```

Para executar comandos dentro dos containers
`-i` para manter a stdin aberta (modo interativo)
`-t` para receber comandos do teclado (pseudo-tty)

```sh
docker exec -it <container_name> <comando>
```

Exemplos:

```sh
docker exec -it culturaeduca_django bash
docker exec -it culturaeduca_django pipenv run python manage.py migrate
docker exec -it culturaeduca_db psql -U username -d database
```

## Inicializando o banco a partir de um backup

Com os containers rodando, basta rodar o psql no container do banco (como neste caso a entrada vem de um arquivo, é preciso remover a opção `-t`)

```sh
docker exec -i culturaeduca_db psql -U username -d database < backup.sql
```

## Inicializando o banco com migrations e fixtures

Todos os comandos devem ser rodados de dentro do container e com o pipenv ativo

```sh
docker exec -it culturaeduca_django bash
pipenv shell
...comandos
```

ou

```sh
docker exec -it culturaeduca_django pipenv run <comando>
```

Comandos para migration:

```sh
python manage.py migrate
```

Em um momento a migration quebra por falta de dados, daí tem que rodar essas 3 fixtures

```sh
python manage.py loaddata fixture_areaatuacao
python manage.py loaddata fixture_ocupacao
python manage.py loaddata fixture_tipoespaco
```

E continuar o migrate pra finalizar

```sh
python manage.py migrate
```

Depois rodar o resto das fixtures

```sh
python manage.py loaddata fixture_publicofocal
python manage.py loaddata fixture_manifestacaocultural
python manage.py loaddata fixture_areaestudopesquisa
python manage.py loaddata fixture_IdentidadeEtinicoCultural
python manage.py loaddata fixture_nivelescolaridade
python manage.py loaddata fixture_linguagensartisticas
python manage.py loaddata fixture_naturezajuridica
```
