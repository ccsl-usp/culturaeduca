FROM ubuntu:20.04

# set the locale correctly
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# instalação dependências instalação geospatial libs
RUN apt-get update && apt-get install -y \
  g++ gcc make wget

# GEOS
ENV CXX="g++ -std=c++98"
RUN wget http://download.osgeo.org/geos/geos-3.4.3.tar.bz2
RUN tar xjf geos-3.4.3.tar.bz2
WORKDIR /geos-3.4.3
RUN ./configure
RUN make
RUN make install
WORKDIR /
RUN rm -rf geos-3.4.3

# PROJ-4
RUN wget http://download.osgeo.org/proj/proj-4.8.0.tar.gz
RUN wget http://download.osgeo.org/proj/proj-datumgrid-1.5.tar.gz
RUN tar xzf proj-4.8.0.tar.gz
WORKDIR /proj-4.8.0/nad
RUN tar xzf ../../proj-datumgrid-1.5.tar.gz
WORKDIR /proj-4.8.0
RUN ./configure
RUN make
RUN make install
WORKDIR /
RUN rm -rf proj-4.8.0

# GDAL
ENV CXX="g++ -std=c++11"
RUN wget http://download.osgeo.org/gdal/1.10.1/gdal-1.10.1.tar.gz
RUN tar xzf gdal-1.10.1.tar.gz
WORKDIR /gdal-1.10.1
RUN CXXFLAGS="-fpermissive" ./configure
RUN make
RUN make install
WORKDIR /
RUN rm -rf gdal-1.10.1

RUN ldconfig

# Add the deadsnakes PPA to install Python 3.7
RUN apt-get update && apt-get install -y \
  software-properties-common && \
  add-apt-repository ppa:deadsnakes/ppa

# TODO verificar o que é realmente necessário
RUN apt-get update && apt-get upgrade -y && apt-get install -y --fix-missing \
  apt-utils git supervisor nginx \
  libpq-dev libjpeg-dev zlibc zlib1g-dev \
  language-pack-pt-base memcached \
  python2 python2-dev python3-pip python3.6 python3.6-dev python3.6-distutils

# stop Python from generating .pyc files
ENV PYTHONDONTWRITEBYTECODE 1
# enable Python tracebacks on segfaults
ENV PYTHONFAULTHANDLER 1

RUN python3.6 -m pip install 'pipenv==2021.5.29'

RUN useradd -m django

RUN mkdir /opt/culturaeduca
RUN chown -R django:django /opt/culturaeduca
RUN mkdir -p /opt/static_files/media
RUN mkdir -p /opt/static_files/static
RUN chown -R django:django /opt/static_files
RUN mkdir -p /opt/cache/ibge_cache
RUN mkdir -p /opt/cache/stache
RUN chown -R django:django /opt/cache

WORKDIR /opt/culturaeduca

COPY ./Pipfile ./Pipfile.lock ./

USER django

# Instala pasta contendo virtual env em uma pasta .venv (/opt/culturaeduca/.venv)
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

# Altera regex da verificação de versão do GEOS no código-fonte do django (fix temporário)
# RUN sed -i '133s/\$/(\\s)*\$/'  \
#   "$(pipenv --venv)/lib/python2.7/site-packages/django/contrib/gis/geos/libgeos.py"

# caso queira pular os "pipenv run", adiciona o bin do virtual env no PATH do sistema
# ENV PATH="$(pipenv --venv)/bin:$PATH"

USER root

RUN mkdir -p /var/log/supervisor
COPY deploy/conf/supervisord.conf /etc/supervisor/supervisord.conf
COPY deploy/conf/supervisord_culturaeduca.conf /etc/supervisor/conf.d/culturaeduca.conf
COPY deploy/conf/supervisord_tilestache.conf /etc/supervisor/conf.d/tilestache.conf

COPY deploy/conf/nginx.conf /etc/nginx/sites-available/default

COPY --chown=django:django . .
