var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('sass', function() {
    var config = {
        includePaths: [
            'static/rede/sass/foundation/scss',
            'static/rede/sass/foundation-icon-fonts'
        ]
    }

    return gulp.src('static/rede/sass/*.scss')
        .pipe(sass(config))
        .pipe(gulp.dest('static/rede/css/'));
});

gulp.task('watch', function() {
    gulp.watch('static/rede/sass/*.scss', ['sass']); // TODO add minify task
});

gulp.task('default', ['watch']);
