# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import csv
import codecs
import fnmatch

from django.apps import apps
from django.db.models import FloatField, IntegerField, BigIntegerField
from django.core.management.base import BaseCommand, CommandError


def unicode_csv_reader(
        unicode_csv_data, dialect=csv.excel, encoding='utf-8', **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(line_encoder(unicode_csv_data, encoding),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, encoding) for cell in row]


def line_encoder(unicode_csv_data, encoding):
    for line in unicode_csv_data:
        yield line.encode(encoding)



class Command(BaseCommand):
    help = u'''Importa dados, de arquivos CSV, de Universo do Censo do IBGE.
    Ex:
        - Basico_AC.csv
        - Domicilio01_AC.csv

    --diretorio
    '''

    def add_arguments(self, parser):
        parser.add_argument('nome_arquivo', type=str, help='Arquivo CSV')
        parser.add_argument('--encoding', type=str, default='latin1')
        parser.add_argument('--diretorio', action="store_true", default=False)

    def detecta_modelo(self, nome_arquivo):
        tipo = os.path.basename(nome_arquivo).split('_')[0]
        return apps.get_model('ibge_universo', tipo)

    def importar(self, nome_arquivo):
        print(u"Convertendo '{}':".format(nome_arquivo))
        print(u"Encoding: {}".format(self.encoding))
        try:
            self.modelo = self.detecta_modelo(nome_arquivo)
        except Exception:
            raise CommandError(u'''Não foi possível determinar o tipo do dado pelo nome do arquivo "{}"\n\nEx:
                - Basico_AC.csv
                - Domicilio01_AC.csv'''.format(
                os.path.basename(nome_arquivo)))

        print(u"Tipo: {}".format(self.modelo))
        self.campos = [
            f.name for f in self.modelo._meta.get_fields()
            if not f.primary_key
        ]
        ct = 0
        with codecs.open(nome_arquivo, encoding=self.encoding) as arquivo:
            for row in unicode_csv_reader(
                    arquivo, encoding=self.encoding, delimiter=';'):
                if ct != 0:
                    instance = self.modelo()
                    for fieldname, value in zip(self.campos, row):
                        field = self.modelo._meta.get_field_by_name(fieldname)[0]
                        if isinstance(field, (FloatField, IntegerField, BigIntegerField)):
                            value = value.replace(',', '.')
                            if value in ('', 'X', 'x'):
                                value = None
                        setattr(instance, fieldname, value)
                    instance.save()
                ct += 1
        print(u"{} registros importados".format(ct - 1))
        return ct - 1

    def handle(self, *args, **options):
        self.nome_arquivo = options['nome_arquivo']
        self.encoding = options['encoding']

        if options['diretorio']:
            if os.path.isdir(self.nome_arquivo):
                print(u"Importando diretório '{}':".format(self.nome_arquivo))

                for root, dirs, files in os.walk(self.nome_arquivo):
                    csv_files = [fn for fn in files
                                 if fnmatch.fnmatch(fn.lower(), '*.csv')]
                    for fn in csv_files:
                        self.importar(os.path.join(root, fn))

            else:
                raise CommandError(u'Caminho informado não é um diretório!')
        else:
            self.importar(self.nome_arquivo)
