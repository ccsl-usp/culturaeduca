# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ibge_universo', '0031_auto_20160720_0137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pessoarenda',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='responsavel01',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
    ]
