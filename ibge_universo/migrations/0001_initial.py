# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Basico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cod_setor', models.BigIntegerField(null=True, verbose_name=b'cod_setor', blank=True)),
                ('cod_grandes_regioes', models.BigIntegerField(null=True, verbose_name=b'cod_grandes_regioes', blank=True)),
                ('nome_grande_regiao', models.CharField(max_length=200, null=True, verbose_name=b'nome_grande_regiao', blank=True)),
                ('cod_uf', models.BigIntegerField(null=True, verbose_name=b'cod_uf', blank=True)),
                ('nome_uf', models.CharField(max_length=200, null=True, verbose_name=b'nome_uf', blank=True)),
                ('cod_meso', models.BigIntegerField(null=True, verbose_name=b'cod_meso', blank=True)),
                ('nome_meso', models.CharField(max_length=200, null=True, verbose_name=b'nome_meso', blank=True)),
                ('cod_micro', models.BigIntegerField(null=True, verbose_name=b'cod_micro', blank=True)),
                ('nome_micro', models.CharField(max_length=200, null=True, verbose_name=b'nome_micro', blank=True)),
                ('cod_rm', models.BigIntegerField(null=True, verbose_name=b'cod_rm', blank=True)),
                ('nome_rm', models.CharField(max_length=200, null=True, verbose_name=b'nome_rm', blank=True)),
                ('cod_municipio', models.BigIntegerField(null=True, verbose_name=b'cod_municipio', blank=True)),
                ('nome_municipio', models.CharField(max_length=200, null=True, verbose_name=b'nome_municipio', blank=True)),
                ('cod_distrito', models.BigIntegerField(null=True, verbose_name=b'cod_distrito', blank=True)),
                ('nome_distrito', models.CharField(max_length=200, null=True, verbose_name=b'nome_distrito', blank=True)),
                ('cod_subdistrito', models.BigIntegerField(null=True, verbose_name=b'cod_subdistrito', blank=True)),
                ('nome_subdistrito', models.CharField(max_length=200, null=True, verbose_name=b'nome_subdistrito', blank=True)),
                ('cod_bairro', models.BigIntegerField(null=True, verbose_name=b'cod_bairro', blank=True)),
                ('nome_bairro', models.CharField(max_length=200, null=True, verbose_name=b'nome_bairro', blank=True)),
                ('situacao_setor', models.IntegerField(null=True, verbose_name=b'situacao_setor', blank=True)),
                ('tipo_setor', models.IntegerField(null=True, verbose_name=b'tipo_setor', blank=True)),
                ('V001', models.FloatField(null=True, verbose_name=b'V001', blank=True)),
                ('V002', models.FloatField(null=True, verbose_name=b'V002', blank=True)),
                ('V003', models.FloatField(null=True, verbose_name=b'V003', blank=True)),
                ('V004', models.FloatField(null=True, verbose_name=b'V004', blank=True)),
                ('V005', models.FloatField(null=True, verbose_name=b'V005', blank=True)),
                ('V006', models.FloatField(null=True, verbose_name=b'V006', blank=True)),
                ('V007', models.FloatField(null=True, verbose_name=b'V007', blank=True)),
                ('V008', models.FloatField(null=True, verbose_name=b'V008', blank=True)),
                ('V009', models.FloatField(null=True, verbose_name=b'V009', blank=True)),
                ('V010', models.FloatField(null=True, verbose_name=b'V010', blank=True)),
                ('V011', models.FloatField(null=True, verbose_name=b'V011', blank=True)),
                ('V012', models.FloatField(null=True, verbose_name=b'V012', blank=True)),
            ],
        ),
    ]
