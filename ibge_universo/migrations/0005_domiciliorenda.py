# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ibge_universo', '0004_domicilio02'),
    ]

    operations = [
        migrations.CreateModel(
            name='DomicilioRenda',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cod_setor', models.BigIntegerField(null=True, verbose_name=b'cod_setor', blank=True)),
                ('situacao_setor', models.IntegerField(null=True, verbose_name=b'situacao_setor', blank=True)),
                ('V001', models.FloatField(null=True, verbose_name=b'V001', blank=True)),
                ('V002', models.FloatField(null=True, verbose_name=b'V002', blank=True)),
                ('V003', models.FloatField(null=True, verbose_name=b'V003', blank=True)),
                ('V004', models.FloatField(null=True, verbose_name=b'V004', blank=True)),
                ('V005', models.FloatField(null=True, verbose_name=b'V005', blank=True)),
                ('V006', models.FloatField(null=True, verbose_name=b'V006', blank=True)),
                ('V007', models.FloatField(null=True, verbose_name=b'V007', blank=True)),
                ('V008', models.FloatField(null=True, verbose_name=b'V008', blank=True)),
                ('V009', models.FloatField(null=True, verbose_name=b'V009', blank=True)),
                ('V010', models.FloatField(null=True, verbose_name=b'V010', blank=True)),
                ('V011', models.FloatField(null=True, verbose_name=b'V011', blank=True)),
                ('V012', models.FloatField(null=True, verbose_name=b'V012', blank=True)),
                ('V013', models.FloatField(null=True, verbose_name=b'V013', blank=True)),
                ('V014', models.FloatField(null=True, verbose_name=b'V014', blank=True)),
            ],
        ),
    ]
