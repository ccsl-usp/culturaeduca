# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ibge_universo', '0030_auto_20160720_0121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entorno02',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='entorno03',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='entorno04',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='entorno05',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa01',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa02',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa03',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa04',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa05',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa06',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa07',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa08',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa09',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa10',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa11',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa12',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='pessoa13',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
    ]
