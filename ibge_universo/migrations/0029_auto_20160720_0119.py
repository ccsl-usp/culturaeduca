# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ibge_universo', '0028_auto_20160720_0056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domiciliorenda',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
    ]
