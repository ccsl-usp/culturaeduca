# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ibge_universo', '0032_auto_20160720_0152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='responsavel02',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
        migrations.AlterField(
            model_name='responsavelrenda',
            name='cod_setor',
            field=models.BigIntegerField(unique=True, verbose_name=b'cod_setor', db_index=True),
        ),
    ]
