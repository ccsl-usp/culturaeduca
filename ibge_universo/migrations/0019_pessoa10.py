# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ibge_universo', '0018_pessoa09'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pessoa10',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cod_setor', models.BigIntegerField(null=True, verbose_name=b'cod_setor', blank=True)),
                ('situacao_setor', models.IntegerField(null=True, verbose_name=b'situacao_setor', blank=True)),
                ('V001', models.FloatField(null=True, verbose_name=b'V001', blank=True)),
                ('V002', models.FloatField(null=True, verbose_name=b'V002', blank=True)),
                ('V003', models.FloatField(null=True, verbose_name=b'V003', blank=True)),
            ],
        ),
    ]
