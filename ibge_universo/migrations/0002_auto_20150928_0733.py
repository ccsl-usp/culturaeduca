# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ibge_universo', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basico',
            name='cod_setor',
            field=models.BigIntegerField(default=1, verbose_name=b'cod_setor'),
            preserve_default=False,
        ),
    ]
