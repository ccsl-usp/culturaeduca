#!/bin/bash

/etc/init.d/memcached start
pipenv run /opt/culturaeduca/manage.py gerarconfigtilestache
pipenv run tilestache-server.py -c /opt/culturaeduca/tsconfig.json -i 0.0.0.0 -p 8181 . &
pipenv run /opt/culturaeduca/manage.py runserver 0.0.0.0:8000
