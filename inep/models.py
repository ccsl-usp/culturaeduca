# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django.conf import settings
from equipamento.models import Escola

TIPO_GEORREF = (
    (0, u'Coordenada da Base Original'),
    (1, u'Geocodificação Automática por Endereço'),
    (2, u'Geocodificação Manual por Endereço'),
    (3, u'Coordenada da Sede Municipal'),
)


class MatriculaPerfilINEP(models.Model):

    escola = models.ForeignKey(
        Escola,
        null=True
    )

    n_matriculas = models.IntegerField(u'Total de Matrículas', null=True)
    n_alunos = models.IntegerField(u'Total de Alunos', null=True)
    creche = models.IntegerField(u'Educação Infantil - Creche', null=True)
    pre = models.IntegerField(u'Educação Infantil - Pré-escola', null=True)
    fund_ai = models.IntegerField(u'Ensino Fundamental - Anos Iniciais', null=True)
    fund_af = models.IntegerField(u'Ensino Fundamental - Anos Finais', null=True)
    medio = models.IntegerField(u'Ensino Médio', null=True)
    medio_magisterio = models.IntegerField(u'Ensino Médio Normal/Magistério', null=True)
    medio_integrado = models.IntegerField(u'Ensino Médio Integrado', null=True)
    prof = models.IntegerField(u'Educação Profissional', null=True)
    eja_fund = models.IntegerField(u'Educação de Jovens e Adultos - Ensino Fundamental', null=True)
    eja_medio = models.IntegerField(u'Educação de Jovens e Adultos - Ensino Médio', null=True)
    eja_prof = models.IntegerField(u'Educação de Jovens e Adultos - Profissionalizante', null=True)
    especial = models.IntegerField(u'Especial', null=True)
    presencial = models.IntegerField(u'Presencial', null=True)
    semi = models.IntegerField(u'Semi Presencial', null=True)
    ead = models.IntegerField(u'EaD', null=True)
    n_alunos_at_compl = models.IntegerField(u'Atividade Complementar', null=True)
    n_alunos_aee = models.IntegerField(u'AEE', null=True)
    masculino = models.IntegerField(u'Masculino', null=True)
    feminino = models.IntegerField(u'Feminino', null=True)
    cor_raca_nd = models.IntegerField(u'Não definido', null=True)
    branca = models.IntegerField(u'Branca', null=True)
    preta = models.IntegerField(u'Preta', null=True)
    parda = models.IntegerField(u'Parda', null=True)
    amarela = models.IntegerField(u'Amarela', null=True)
    indigena = models.IntegerField(u'Indígena', null=True)
    naturalizado = models.IntegerField(u'Naturalizados', null=True)
    estrangeira = models.IntegerField(u'Estrangeiros', null=True)
    uf_nasc_dif = models.IntegerField(u'Nascidos em Estado diferente da escola', null=True)
    in_necessidade_especial = models.IntegerField(u'Possui deficiência, transtorno global do desenvolvimento ou altas habilidades/superdotação.', null=True)
    in_transporte_publico = models.IntegerField(u'Utiliza transporte escolar público', null=True)


    def __unicode__(self):
        return self.escola.no_entidade or ''

    def __getattr__(self, attr):

        if attr.startswith('get_') and attr.endswith('_percent'):  # se o acesso ao attribute tiver o prefixo 'get_'

            get_attr = attr.replace('get_', '').replace('_percent', '')  # remove o prefixo 'get_%s_percent' em get_attr
            percent = (getattr(self, get_attr) or 0) * 100 / float(self.n_alunos)
            return '%.1f' % percent

        # caso nao tenha o prefixo get_ e o sufixo _percent retorna o padrão
        return super(MatriculaPerfilINEP, self).__getattr__(attr)


class EscolaPerfilINEP(models.Model):
    class TP_AEE:
        NAO_OFERECE = 0
        NAO_EXCLUSIVAMENTE = 1
        EXCLUSIVAMENTE = 2
        CHOICES = (
            (NAO_OFERECE, u'Não oferece'),
            (NAO_EXCLUSIVAMENTE, u'Não exclusivamente'),
            (EXCLUSIVAMENTE, u'Exclusivamente'),
        )

    class TP_SITUACAO_FUNCIONAMENTO:
        EM_ATIVIDADE = 1
        PARALISADA = 2
        EXTINTA_ANO_CENSO = 3
        EXTINTA_ANOS_ANTERIORES = 4
        CHOICES = (
            (EM_ATIVIDADE, u'Em atividade'),
            (PARALISADA, u'Paralisada'),
            (EXTINTA_ANO_CENSO, u'Extinta (ano do Censo)'),
            (EXTINTA_ANOS_ANTERIORES, u'Extinta em anos anteriores'),
        )

    class TP_DEPENDENCIA:
        FEDERAL = 1
        ESTADUAL = 2
        MUNICIPAL = 3
        PRIVADA = 4
        CHOICES = (
            (FEDERAL, u'Federal'),
            (ESTADUAL, u'Estadual'),
            (MUNICIPAL, u'Municipal'),
            (PRIVADA, u'Privada'),
        )

    class TP_LOCALIZACAO:
        URBANA = 1
        RURAL = 2
        CHOICES = (
            (URBANA, u'Urbana'),
            (RURAL, u'Rural'),
        )

    class TP_LOCALIZACAO_DIFERENCIADA:
        NAO_APLICA = 0
        AREA_DE_ASSENTAMENTO = 1
        TERRA_INDIGENA = 2
        AREA_REMANESCENTE_QUILOMBOS = 3
        UNID_USO_SUSTENTAVEL = 4
        UNID_USO_SUSTENTAVEL_TERRA_INDIGENA = 5
        UNID_USO_SUSTENTAVEL_AREA_REMANESCENTE_QUILOMBOS = 6
        CHOICES = (
            (NAO_APLICA, u'Não se aplica.'),
            (AREA_DE_ASSENTAMENTO, u'Área de assentamento.'),
            (TERRA_INDIGENA, u'Terra indígena.'),
            (AREA_REMANESCENTE_QUILOMBOS, u'Área remanescente de quilombos.'),
            (UNID_USO_SUSTENTAVEL, u'Unidade de uso sustentável.'),
            (UNID_USO_SUSTENTAVEL_TERRA_INDIGENA, u'Unidade de uso sustentável em terra indígena.'),
            (UNID_USO_SUSTENTAVEL_AREA_REMANESCENTE_QUILOMBOS, u'Unidade de uso sustentável em área remanescente de quilombos.'),
        )

    class TP_REDE_LOCAL:
        NAO_HA = 0
        A_CABO = 1
        WIRELESS = 2
        CABO_WIRELESS = 3
        N_INFORMADO = 9
        CHOICES = (
            (NAO_HA, u'Não há rede local interligando computadores'),
            (A_CABO, u'A cabo'),
            (WIRELESS, u'Wireless'),
            (CABO_WIRELESS, u'A cabo e Wireless'),
            (N_INFORMADO, u'Não informado'),
        )

    class TP_OPTION:
        NAO = 0
        SIM = 1
        NAO_POSSUI = 2
        N_INFORMADO = 9
        CHOICES = (
            (NAO, u'Não'),
            (SIM, u'Sim'),
            (NAO_POSSUI, u'Wireless'),
            (N_INFORMADO, u'Não informado'),
        )

    escola = models.ForeignKey(
        Escola,
        null=True
    )

    tp_situacao_funcionamento = models.IntegerField(u'Situação de Funcionamento', choices=TP_SITUACAO_FUNCIONAMENTO.CHOICES)  # 1
    tp_dependencia = models.IntegerField(u'Dependência Administrativa', choices=TP_DEPENDENCIA.CHOICES)  # 3
    tp_localizacao = models.IntegerField(u'Localização', choices=TP_LOCALIZACAO.CHOICES)  # 2
    tp_localizacao_diferenciada = models.IntegerField(u'Localização Diferenciada', null=True, choices=TP_LOCALIZACAO_DIFERENCIADA.CHOICES)  # 0, 1, 2, null
    in_sala_diretoria = models.NullBooleanField(u'Sala para a diretoria')  # 0
    in_sala_professor = models.NullBooleanField(u'Sala para os professores')  # 0
    in_laboratorio_informatica = models.NullBooleanField(u'Laboratório de Informática')  # 0
    in_laboratorio_ciencias = models.NullBooleanField(u'Laboratório de ciências')  # 0
    in_sala_atendimento_especial = models.NullBooleanField(u'Sala de atendimento especial')  # 0
    in_quadra_esportes_coberta = models.NullBooleanField(u'Quadra de esportes coberta')  # 0
    in_quadra_esportes_descoberta = models.NullBooleanField(u'Quadra de esportes descoberta')  # 0
    in_cozinha = models.NullBooleanField(u'Cozinha')  # 1
    in_biblioteca = models.NullBooleanField(u'Biblioteca')  # 1
    in_biblioteca_sala_leitura = models.NullBooleanField(u'Biblioteca e/ou Sala de Leitura')  # 1
    in_auditorio = models.NullBooleanField(u'Auditório')  # 0
    in_patio_coberto = models.NullBooleanField(u'Pátio coberto')  # 0
    in_patio_descoberto = models.NullBooleanField(u'Pátio descoberto')  # 0
    in_area_verde = models.NullBooleanField(u'Área Verde')  # 0
    in_sala_atelie_artes = models.NullBooleanField(u'Sala/ateliê de artes')
    in_sala_musica_coral = models.NullBooleanField(u'Sala de música/coral')
    in_sala_estudio_danca = models.NullBooleanField(u'Sala/estúdio de dança')
    in_sala_multiuso = models.NullBooleanField(u'Sala multiuso (música, dança e artes)')
    qt_salas_utilizadas = models.IntegerField(u'Salas de aula utilizadas', null=True)
    qt_salas_utilizadas_acessiveis = models.IntegerField(u'Salas de aula com acessibilidade ', null=True)
    in_banheiro_pne = models.NullBooleanField(u'Banheiro adequado ao uso dos alunos com deficiência ou mobilidade reduzida')  # 1
    in_acessibilidade_corrimao = models.NullBooleanField(u'Corrimão e guarda corpos')
    in_acessibilidade_elevador = models.NullBooleanField(u'Elevador')
    in_acessibilidade_pisos_tateis = models.NullBooleanField(u'Pisos táteis')
    in_acessibilidade_vao_livre = models.NullBooleanField(u'Portas com vão livre de no mínimo 80 cm')
    in_acessibilidade_rampas = models.NullBooleanField(u'Rampas')
    in_acessibilidade_sinal_sonoro = models.NullBooleanField(u'Sinalização sonora')
    in_acessibilidade_sinal_tatil = models.NullBooleanField(u'Sinalização tátil (piso/paredes)')
    in_acessibilidade_sinal_visual = models.NullBooleanField(u'Sinalização visual (piso/paredes)')
    in_acessibilidade_inexistente = models.NullBooleanField(u'Nenhum dos recursos de acessibilidade listado')
    nu_funcionarios = models.IntegerField(u'Número de profissionais', null=True)  # 6
    in_alimentacao = models.NullBooleanField(u'Fornece alimentação')  # 1
    tp_aee = models.IntegerField(u'Atendimento Educacional Especializado', choices=TP_AEE.CHOICES, null=True)  # 0
    in_educacao_indigena = models.NullBooleanField(u'Educação indígena')  # 0
    in_final_semana = models.NullBooleanField(u'Abre no fim de semana')  # 0
    qt_prof_administrativos = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_servicos_gerais = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_bibliotecario = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_saude = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_coordenador = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_fonaudiologo = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_nutricionista = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_psicologo = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_alimentacao = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_pedagogia = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_secretario = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_seguranca = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_monitores = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_gestao = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_assist_social = models.IntegerField(u'Número de profissionais', null=True)
    qt_prof_assist_social = models.IntegerField(u'Número de profissionais', null=True)
    qt_equip_som = models.IntegerField(u'Número de equipamentos', null=True)
    qt_equip_tv = models.IntegerField(u'Número de equipamentos', null=True)
    qt_equip_lousa_digital = models.IntegerField(u'Número de equipamentos', null=True)
    qt_equip_multimidia = models.IntegerField(u'Número de equipamentos', null=True)
    qt_desktop_aluno = models.IntegerField(u'Número de equipamentos', null=True)
    qt_comp_portatil_aluno = models.IntegerField(u'Número de equipamentos', null=True)
    qt_tablet_aluno = models.IntegerField(u'Número de equipamentos', null=True)
    in_internet = models.IntegerField(u'Internet', null=True)
    in_banda_larga = models.IntegerField(u'Banda Larga', null=True)
    in_internet_alunos = models.NullBooleanField(u'Para uso dos alunos')
    in_internet_administrativo = models.NullBooleanField(u'Para uso administrativo')
    in_internet_aprendizagem = models.NullBooleanField(u'Para uso nos processos de ensino e aprendizagem')
    in_internet_comunidade = models.NullBooleanField(u'Para uso da comunidade')
    tp_rede_local = models.IntegerField(u'Rede local de interligação de computadores', null=True, choices=TP_REDE_LOCAL.CHOICES)
    in_redes_sociais = models.IntegerField(u'Redes Sociais', null=True, choices=TP_OPTION.CHOICES)
    in_espaco_atividade = models.IntegerField(u'Redes Sociais', null=True, choices=TP_OPTION.CHOICES)
    in_espaco_equipamento = models.IntegerField(u'Redes Sociais', null=True, choices=TP_OPTION.CHOICES)
    in_orgao_ass_pais = models.NullBooleanField(u'Associação de Pais')
    in_orgao_ass_pais_mestres = models.NullBooleanField(u'Associação de Pais e Mestres')
    in_orgao_conselho_escolar = models.NullBooleanField(u'Conselho Escolar')
    in_orgao_gremio_estudantil = models.NullBooleanField(u'Grêmio Estudantils')
    in_orgao_outros = models.NullBooleanField(u'Outros')
    in_orgao_nenhum = models.NullBooleanField(u'Não há órgãos colegiados em funcionamento')
    tp_proposta_pedagogica = models.IntegerField(u'Redes Sociais', null=True, choices=TP_OPTION.CHOICES)


    def __unicode__(self):
        if self.escola is not None:
            return u'{}'.format(self.escola.no_entidade)
        return u'{}'.format(u'Sem equipamento.Escola')


class IndicadoresPerfilINEP(models.Model):


    escola = models.ForeignKey(
        Escola,
        null=True
    )

    # ADF - Adequação da Formação Docente
    adf_ei_1 = models.FloatField(u'Educação Infantil - Grupo 1', null=True, blank=True)
    adf_ei_2 = models.FloatField(u'Educação Infantil - Grupo 2', null=True, blank=True)
    adf_ei_3 = models.FloatField(u'Educação Infantil - Grupo 3', null=True, blank=True)
    adf_ei_4 = models.FloatField(u'Educação Infantil - Grupo 4', null=True, blank=True)
    adf_ei_5 = models.FloatField(u'Educação Infantil - Grupo 5', null=True, blank=True)
    adf_ef_1 = models.FloatField(u'Ensino Fundamental - Grupo 1', null=True, blank=True)
    adf_ef_2 = models.FloatField(u'Ensino Fundamental - Grupo 2', null=True, blank=True)
    adf_ef_3 = models.FloatField(u'Ensino Fundamental - Grupo 3', null=True, blank=True)
    adf_ef_4 = models.FloatField(u'Ensino Fundamental - Grupo 4', null=True, blank=True)
    adf_ef_5 = models.FloatField(u'Ensino Fundamental - Grupo 5', null=True, blank=True)
    adf_ef1_1 = models.FloatField(u'Anos Iniciais - Grupo 1', null=True, blank=True)
    adf_ef1_2 = models.FloatField(u'Anos Iniciais - Grupo 2', null=True, blank=True)
    adf_ef1_3 = models.FloatField(u'Anos Iniciais - Grupo 3', null=True, blank=True)
    adf_ef1_4 = models.FloatField(u'Anos Iniciais - Grupo 4', null=True, blank=True)
    adf_ef1_5 = models.FloatField(u'Anos Iniciais - Grupo 5', null=True, blank=True)
    adf_ef2_1 = models.FloatField(u'Anos Finais - Grupo 1', null=True, blank=True)
    adf_ef2_2 = models.FloatField(u'Anos Finais - Grupo 2', null=True, blank=True)
    adf_ef2_3 = models.FloatField(u'Anos Finais - Grupo 3', null=True, blank=True)
    adf_ef2_4 = models.FloatField(u'Anos Finais - Grupo 4', null=True, blank=True)
    adf_ef2_5 = models.FloatField(u'Anos Finais - Grupo 5', null=True, blank=True)
    adf_em_1 = models.FloatField(u'Ensino Médio - Grupo 1', null=True, blank=True)
    adf_em_2 = models.FloatField(u'Ensino Médio - Grupo 2', null=True, blank=True)
    adf_em_3 = models.FloatField(u'Ensino Médio - Grupo 3', null=True, blank=True)
    adf_em_4 = models.FloatField(u'Ensino Médio - Grupo 4', null=True, blank=True)
    adf_em_5 = models.FloatField(u'Ensino Médio - Grupo 5', null=True, blank=True)
    adf_ejaf_1 = models.FloatField(u'EJA Fundamental - Grupo 1', null=True, blank=True)
    adf_ejaf_2 = models.FloatField(u'EJA Fundamental - Grupo 2', null=True, blank=True)
    adf_ejaf_3 = models.FloatField(u'EJA Fundamental - Grupo 3', null=True, blank=True)
    adf_ejaf_4 = models.FloatField(u'EJA Fundamental - Grupo 4', null=True, blank=True)
    adf_ejaf_5 = models.FloatField(u'EJA Fundamental - Grupo 5', null=True, blank=True)
    adf_ejam_1 = models.FloatField(u'EJA Médio - Grupo 1', null=True, blank=True)
    adf_ejam_2 = models.FloatField(u'EJA Médio - Grupo 2', null=True, blank=True)
    adf_ejam_3 = models.FloatField(u'EJA Médio - Grupo 3', null=True, blank=True)
    adf_ejam_4 = models.FloatField(u'EJA Médio - Grupo 4', null=True, blank=True)
    adf_ejam_5 = models.FloatField(u'EJA Médio - Grupo 5', null=True, blank=True)


    def __unicode__(self):
        return self.escola.no_entidade or ''
