# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0002_auto_20170118_1129'),
    ]

    operations = [
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='amarela',
            field=models.IntegerField(null=True, verbose_name='Amarela'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='branca',
            field=models.IntegerField(null=True, verbose_name='Branca'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='cor_raca_nd',
            field=models.IntegerField(null=True, verbose_name='N\xe3o definido'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='feminino',
            field=models.IntegerField(null=True, verbose_name='Feminino'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='indigena',
            field=models.IntegerField(null=True, verbose_name='Ind\xedgena'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='masculino',
            field=models.IntegerField(null=True, verbose_name='Masculino'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='parda',
            field=models.IntegerField(null=True, verbose_name='Parda'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='preta',
            field=models.IntegerField(null=True, verbose_name='Preta'),
        ),
    ]
