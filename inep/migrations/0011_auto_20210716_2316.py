# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0010_escolaperfilinep_tp_rede_local'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='escolaperfilinep',
            name='nu_salas_existentes',
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_biblioteca',
            field=models.NullBooleanField(verbose_name='Biblioteca e/ou Sala de Leitura'),
        ),
    ]
