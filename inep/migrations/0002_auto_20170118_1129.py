# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='escolaperfilinep',
            old_name='tp_situacao_funcionamento_tmp',
            new_name='tp_situacao_funcionamento',
        ),
    ]
