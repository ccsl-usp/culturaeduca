# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0016_auto_20170125_1636'),
        ('inep', '0006_auto_20170124_1522'),
    ]

    operations = [
        migrations.CreateModel(
            name='IndicadoresPerfilINEP',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('adf_ei_1', models.FloatField(null=True, verbose_name='Educa\xe7\xe3o Infantil - Grupo 1', blank=True)),
                ('adf_ei_2', models.FloatField(null=True, verbose_name='Educa\xe7\xe3o Infantil - Grupo 2', blank=True)),
                ('adf_ei_3', models.FloatField(null=True, verbose_name='Educa\xe7\xe3o Infantil - Grupo 3', blank=True)),
                ('adf_ei_4', models.FloatField(null=True, verbose_name='Educa\xe7\xe3o Infantil - Grupo 4', blank=True)),
                ('adf_ei_5', models.FloatField(null=True, verbose_name='Educa\xe7\xe3o Infantil - Grupo 5', blank=True)),
                ('adf_ef_1', models.FloatField(null=True, verbose_name='Ensino Fundamental - Grupo 1', blank=True)),
                ('adf_ef_2', models.FloatField(null=True, verbose_name='Ensino Fundamental - Grupo 2', blank=True)),
                ('adf_ef_3', models.FloatField(null=True, verbose_name='Ensino Fundamental - Grupo 3', blank=True)),
                ('adf_ef_4', models.FloatField(null=True, verbose_name='Ensino Fundamental - Grupo 4', blank=True)),
                ('adf_ef_5', models.FloatField(null=True, verbose_name='Ensino Fundamental - Grupo 5', blank=True)),
                ('adf_ef1_1', models.FloatField(null=True, verbose_name='Anos Iniciais - Grupo 1', blank=True)),
                ('adf_ef1_2', models.FloatField(null=True, verbose_name='Anos Iniciais - Grupo 2', blank=True)),
                ('adf_ef1_3', models.FloatField(null=True, verbose_name='Anos Iniciais - Grupo 3', blank=True)),
                ('adf_ef1_4', models.FloatField(null=True, verbose_name='Anos Iniciais - Grupo 4', blank=True)),
                ('adf_ef1_5', models.FloatField(null=True, verbose_name='Anos Iniciais - Grupo 5', blank=True)),
                ('adf_ef2_1', models.FloatField(null=True, verbose_name='Anos Finais - Grupo 1', blank=True)),
                ('adf_ef2_2', models.FloatField(null=True, verbose_name='Anos Finais - Grupo 2', blank=True)),
                ('adf_ef2_3', models.FloatField(null=True, verbose_name='Anos Finais - Grupo 3', blank=True)),
                ('adf_ef2_4', models.FloatField(null=True, verbose_name='Anos Finais - Grupo 4', blank=True)),
                ('adf_ef2_5', models.FloatField(null=True, verbose_name='Anos Finais - Grupo 5', blank=True)),
                ('adf_em_1', models.FloatField(null=True, verbose_name='Ensino M\xe9dio - Grupo 1', blank=True)),
                ('adf_em_2', models.FloatField(null=True, verbose_name='Ensino M\xe9dio - Grupo 2', blank=True)),
                ('adf_em_3', models.FloatField(null=True, verbose_name='Ensino M\xe9dio - Grupo 3', blank=True)),
                ('adf_em_4', models.FloatField(null=True, verbose_name='Ensino M\xe9dio - Grupo 4', blank=True)),
                ('adf_em_5', models.FloatField(null=True, verbose_name='Ensino M\xe9dio - Grupo 5', blank=True)),
                ('adf_ejaf_1', models.FloatField(null=True, verbose_name='EJA Fundamental - Grupo 1', blank=True)),
                ('adf_ejaf_2', models.FloatField(null=True, verbose_name='EJA Fundamental - Grupo 2', blank=True)),
                ('adf_ejaf_3', models.FloatField(null=True, verbose_name='EJA Fundamental - Grupo 3', blank=True)),
                ('adf_ejaf_4', models.FloatField(null=True, verbose_name='EJA Fundamental - Grupo 4', blank=True)),
                ('adf_ejaf_5', models.FloatField(null=True, verbose_name='EJA Fundamental - Grupo 5', blank=True)),
                ('adf_ejam_1', models.FloatField(null=True, verbose_name='EJA M\xe9dio - Grupo 1', blank=True)),
                ('adf_ejam_2', models.FloatField(null=True, verbose_name='EJA M\xe9dio - Grupo 2', blank=True)),
                ('adf_ejam_3', models.FloatField(null=True, verbose_name='EJA M\xe9dio - Grupo 3', blank=True)),
                ('adf_ejam_4', models.FloatField(null=True, verbose_name='EJA M\xe9dio - Grupo 4', blank=True)),
                ('adf_ejam_5', models.FloatField(null=True, verbose_name='EJA M\xe9dio - Grupo 5', blank=True)),
                ('escola', models.ForeignKey(to='equipamento.Escola', null=True)),
            ],
        ),
    ]
