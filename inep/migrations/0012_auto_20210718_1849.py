# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0011_auto_20210716_2316'),
    ]

    operations = [
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_administrativos',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_alimentacao',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_assist_social',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_bibliotecario',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_coordenador',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_fonaudiologo',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_gestao',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_monitores',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_nutricionista',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_pedagogia',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_psicologo',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_saude',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_secretario',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_seguranca',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_prof_servicos_gerais',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
        migrations.AlterField(
            model_name='escolaperfilinep',
            name='in_biblioteca',
            field=models.NullBooleanField(verbose_name='Biblioteca'),
        ),
        migrations.AlterField(
            model_name='escolaperfilinep',
            name='nu_funcionarios',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de profissionais'),
        ),
    ]
