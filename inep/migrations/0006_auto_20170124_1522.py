# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0005_auto_20170119_1102'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='escolaperfilinep',
            name='raw_csv',
        ),
        migrations.RemoveField(
            model_name='matriculaperfilinep',
            name='raw_csv',
        ),
    ]
