# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0015_auto_20170116_1723'),
    ]

    operations = [
        migrations.CreateModel(
            name='EscolaPerfilINEP',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw_csv', models.CharField(max_length=650, verbose_name='Linha CSV do arquivo de importa\xe7\xe3o')),
                ('tp_situacao_funcionamento_tmp', models.IntegerField(verbose_name='Situa\xe7\xe3o de Funcionamento', choices=[(1, 'Em atividade'), (2, 'Paralisada'), (3, 'Extinta (ano do Censo)'), (4, 'Extinta em anos anteriores')])),
                ('tp_dependencia', models.IntegerField(verbose_name='Depend\xeancia Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Privada')])),
                ('tp_localizacao', models.IntegerField(verbose_name='Localiza\xe7\xe3o', choices=[(1, 'Urbana'), (2, 'Rural')])),
                ('tp_localizacao_diferenciada', models.IntegerField(null=True, verbose_name='Localiza\xe7\xe3o Diferenciada', choices=[(0, 'N\xe3o se aplica.'), (1, '\xc1rea de assentamento.'), (2, 'Terra ind\xedgena.'), (3, '\xc1rea remanescente de quilombos.'), (4, 'Unidade de uso sustent\xe1vel.'), (5, 'Unidade de uso sustent\xe1vel em terra ind\xedgena.'), (6, 'Unidade de uso sustent\xe1vel em \xe1rea remanescente de quilombos.')])),
                ('in_sala_diretoria', models.NullBooleanField(verbose_name='Sala para a diretoria')),
                ('in_sala_professor', models.NullBooleanField(verbose_name='Sala para os professores')),
                ('in_laboratorio_informatica', models.NullBooleanField(verbose_name='Laborat\xf3rio de Inform\xe1tica')),
                ('in_laboratorio_ciencias', models.NullBooleanField(verbose_name='Laborat\xf3rio de ci\xeancias')),
                ('in_sala_atendimento_especial', models.NullBooleanField(verbose_name='Sala de atendimento especial')),
                ('in_quadra_esportes_coberta', models.NullBooleanField(verbose_name='Quadra de esportes coberta')),
                ('in_quadra_esportes_descoberta', models.NullBooleanField(verbose_name='Quadra de esportes descoberta')),
                ('in_cozinha', models.NullBooleanField(verbose_name='Cozinha')),
                ('in_biblioteca_sala_leitura', models.NullBooleanField(verbose_name='Biblioteca e/ou Sala de Leitura')),
                ('in_banheiro_fora_predio', models.NullBooleanField(verbose_name='Sanit\xe1rio fora do pr\xe9dio da escola')),
                ('in_banheiro_dentro_predio', models.NullBooleanField(verbose_name='Sanit\xe1rio dentro do pr\xe9dio da escola')),
                ('in_dependencias_pne', models.NullBooleanField(verbose_name='Depend\xeancias e vias adequadas a alunos com defici\xeancia ou mobilidade reduzida')),
                ('in_auditorio', models.NullBooleanField(verbose_name='Audit\xf3rio')),
                ('in_patio_coberto', models.NullBooleanField(verbose_name='P\xe1tio coberto')),
                ('in_patio_descoberto', models.NullBooleanField(verbose_name='P\xe1tio descoberto')),
                ('in_area_verde', models.NullBooleanField(verbose_name='\xc1rea Verde')),
                ('nu_salas_existentes', models.IntegerField(null=True, verbose_name='Salas de aula existentes')),
                ('nu_salas_utilizadas', models.IntegerField(null=True, verbose_name='Salas de aula utilizadas')),
                ('nu_funcionarios', models.IntegerField(null=True, verbose_name='N\xfamero de funcion\xe1rios')),
                ('in_alimentacao', models.NullBooleanField(verbose_name='Fornece alimenta\xe7\xe3o')),
                ('tp_aee_tmp', models.IntegerField(null=True, verbose_name='Atendimento Educacional Especializado', choices=[(0, 'N\xe3o oferece'), (1, 'N\xe3o exclusivamente'), (2, 'Exclusivamente')])),
                ('in_educacao_indigena', models.NullBooleanField(verbose_name='Educa\xe7\xe3o ind\xedgena')),
                ('in_final_semana', models.NullBooleanField(verbose_name='Abre no fim de semana')),
                ('escola', models.ForeignKey(to='equipamento.Escola', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MatriculaPerfilINEP',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw_csv', models.CharField(max_length=512, verbose_name='Linha CSV do arquivo de importa\xe7\xe3o')),
                ('creche', models.IntegerField(null=True, verbose_name='Educa\xe7\xe3o Infantil - Creche')),
                ('eja_fund', models.IntegerField(null=True, verbose_name='Educa\xe7\xe3o de Jovens e Adultos - Ensino Fundamental')),
                ('eja_medio', models.IntegerField(null=True, verbose_name='Educa\xe7\xe3o de Jovens e Adultos - Ensino M\xe9dio')),
                ('fund_af', models.IntegerField(null=True, verbose_name='Ensino Fundamental - Anos Finais')),
                ('fund_ai', models.IntegerField(null=True, verbose_name='Ensino Fundamental - Anos Iniciais')),
                ('medio', models.IntegerField(null=True, verbose_name='Ensino M\xe9dio')),
                ('medio_integrado', models.IntegerField(null=True, verbose_name='Ensino M\xe9dio Integrado')),
                ('medio_magisterio', models.IntegerField(null=True, verbose_name='Ensino M\xe9dio Normal/Magist\xe9rio')),
                ('pre', models.IntegerField(null=True, verbose_name='Educa\xe7\xe3o Infantil - Pr\xe9-escola')),
                ('prof', models.IntegerField(null=True, verbose_name='Educa\xe7\xe3o Profissional')),
                ('estrangeira', models.IntegerField(null=True, verbose_name='Estrangeiros')),
                ('in_necessidade_especial', models.IntegerField(null=True, verbose_name='Possui defici\xeancia, transtorno global do desenvolvimento ou altas habilidades/superdota\xe7\xe3o.')),
                ('in_transporte_publico', models.IntegerField(null=True, verbose_name='Utiliza transporte escolar p\xfablico')),
                ('n_alunos', models.IntegerField(null=True, verbose_name='Total de Alunos')),
                ('uf_nasc_dif', models.IntegerField(null=True, verbose_name='Nascidos em Estado diferente da escola')),
                ('escola', models.ForeignKey(to='equipamento.Escola', null=True)),
            ],
        ),
    ]
