# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0013_auto_20210718_2006'),
    ]

    operations = [
        migrations.RenameField(
            model_name='escolaperfilinep',
            old_name='nu_salas_utilizadas',
            new_name='qt_salas_utilizadas',
        ),
        migrations.RenameField(
            model_name='escolaperfilinep',
            old_name='tp_aee_tmp',
            new_name='tp_aee',
        ),
        migrations.RemoveField(
            model_name='escolaperfilinep',
            name='in_banheiro_dentro_predio',
        ),
        migrations.RemoveField(
            model_name='escolaperfilinep',
            name='in_banheiro_fora_predio',
        ),
        migrations.RemoveField(
            model_name='escolaperfilinep',
            name='in_dependencias_pne',
        ),
        migrations.RemoveField(
            model_name='escolaperfilinep',
            name='nu_comp_administrativo',
        ),
        migrations.RemoveField(
            model_name='escolaperfilinep',
            name='nu_comp_aluno',
        ),
        migrations.RemoveField(
            model_name='escolaperfilinep',
            name='nu_computador',
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_acessibilidade_corrimao',
            field=models.NullBooleanField(verbose_name='Corrim\xe3o e guarda corpos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_acessibilidade_elevador',
            field=models.NullBooleanField(verbose_name='Elevador'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_acessibilidade_inexistente',
            field=models.NullBooleanField(verbose_name='Nenhum dos recursos de acessibilidade listado'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_acessibilidade_pisos_tateis',
            field=models.NullBooleanField(verbose_name='Pisos t\xe1teis'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_acessibilidade_rampas',
            field=models.NullBooleanField(verbose_name='Rampas'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_acessibilidade_sinal_sonoro',
            field=models.NullBooleanField(verbose_name='Sinaliza\xe7\xe3o sonora'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_acessibilidade_sinal_tatil',
            field=models.NullBooleanField(verbose_name='Sinaliza\xe7\xe3o t\xe1til (piso/paredes)'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_acessibilidade_sinal_visual',
            field=models.NullBooleanField(verbose_name='Sinaliza\xe7\xe3o visual (piso/paredes)'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_acessibilidade_vao_livre',
            field=models.NullBooleanField(verbose_name='Portas com v\xe3o livre de no m\xednimo 80 cm'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_banheiro_pne',
            field=models.NullBooleanField(verbose_name='Banheiro adequado ao uso dos alunos com defici\xeancia ou mobilidade reduzida'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_sala_atelie_artes',
            field=models.NullBooleanField(verbose_name='Sala/ateli\xea de artes'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_sala_estudio_danca',
            field=models.NullBooleanField(verbose_name='Sala/est\xfadio de dan\xe7a'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_sala_multiuso',
            field=models.NullBooleanField(verbose_name='Sala multiuso (m\xfasica, dan\xe7a e artes)'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_sala_musica_coral',
            field=models.NullBooleanField(verbose_name='Sala de m\xfasica/coral'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_salas_utilizadas_acessiveis',
            field=models.IntegerField(null=True, verbose_name='Salas de aula com acessibilidade '),
        ),
    ]
