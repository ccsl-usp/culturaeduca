# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0008_auto_20181116_1236'),
    ]

    operations = [
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='eja_prof',
            field=models.IntegerField(null=True, verbose_name='Educa\xe7\xe3o de Jovens e Adultos - Profissionalizante'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='naturalizado',
            field=models.IntegerField(null=True, verbose_name='Naturalizados'),
        ),
    ]
