# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0007_indicadoresperfilinep'),
    ]

    operations = [
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='ead',
            field=models.IntegerField(null=True, verbose_name='EaD'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='especial',
            field=models.IntegerField(null=True, verbose_name='Especial'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='n_alunos_aee',
            field=models.IntegerField(null=True, verbose_name='AEE'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='n_alunos_at_compl',
            field=models.IntegerField(null=True, verbose_name='Atividade Complementar'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='n_matriculas',
            field=models.IntegerField(null=True, verbose_name='Total de Matr\xedculas'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='presencial',
            field=models.IntegerField(null=True, verbose_name='Presencial'),
        ),
        migrations.AddField(
            model_name='matriculaperfilinep',
            name='semi',
            field=models.IntegerField(null=True, verbose_name='Semi Presencial'),
        ),
    ]
