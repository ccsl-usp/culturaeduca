# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0009_auto_20181116_1634'),
    ]

    operations = [
        migrations.AddField(
            model_name='escolaperfilinep',
            name='tp_rede_local',
            field=models.IntegerField(null=True, verbose_name='Rede local de interliga\xe7\xe3o de computadores', choices=[(0, 'N\xe3o h\xe1 rede local interligando computadores'), (1, 'A cabo'), (2, 'Wireless'), (3, 'A cabo e Wireless'), (9, 'N\xe3o informado')]),
        ),
    ]
