# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0012_auto_20210718_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_espaco_atividade',
            field=models.IntegerField(null=True, verbose_name='Redes Sociais', choices=[(0, 'N\xe3o'), (1, 'Sim'), (2, 'Wireless'), (9, 'N\xe3o informado')]),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_espaco_equipamento',
            field=models.IntegerField(null=True, verbose_name='Redes Sociais', choices=[(0, 'N\xe3o'), (1, 'Sim'), (2, 'Wireless'), (9, 'N\xe3o informado')]),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_internet_administrativo',
            field=models.NullBooleanField(verbose_name='Para uso administrativo'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_internet_alunos',
            field=models.NullBooleanField(verbose_name='Para uso dos alunos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_internet_aprendizagem',
            field=models.NullBooleanField(verbose_name='Para uso nos processos de ensino e aprendizagem'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_internet_comunidade',
            field=models.NullBooleanField(verbose_name='Para uso da comunidade'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_orgao_ass_pais',
            field=models.NullBooleanField(verbose_name='Associa\xe7\xe3o de Pais'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_orgao_ass_pais_mestres',
            field=models.NullBooleanField(verbose_name='Associa\xe7\xe3o de Pais e Mestres'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_orgao_conselho_escolar',
            field=models.NullBooleanField(verbose_name='Conselho Escolar'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_orgao_gremio_estudantil',
            field=models.NullBooleanField(verbose_name='Gr\xeamio Estudantils'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_orgao_nenhum',
            field=models.NullBooleanField(verbose_name='N\xe3o h\xe1 \xf3rg\xe3os colegiados em funcionamento'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_orgao_outros',
            field=models.NullBooleanField(verbose_name='Outros'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_redes_sociais',
            field=models.IntegerField(null=True, verbose_name='Redes Sociais', choices=[(0, 'N\xe3o'), (1, 'Sim'), (2, 'Wireless'), (9, 'N\xe3o informado')]),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_comp_portatil_aluno',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de equipamentos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_desktop_aluno',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de equipamentos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_equip_lousa_digital',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de equipamentos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_equip_multimidia',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de equipamentos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_equip_som',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de equipamentos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_equip_tv',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de equipamentos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='qt_tablet_aluno',
            field=models.IntegerField(null=True, verbose_name='N\xfamero de equipamentos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='tp_proposta_pedagogica',
            field=models.IntegerField(null=True, verbose_name='Redes Sociais', choices=[(0, 'N\xe3o'), (1, 'Sim'), (2, 'Wireless'), (9, 'N\xe3o informado')]),
        ),
    ]
