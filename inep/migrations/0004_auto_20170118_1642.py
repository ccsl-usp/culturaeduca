# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import Q


inep_matriculas_csv_cols = ('co_entidade', 'creche', 'pre', 'fund_ai', 'fund_af', 'medio', 'medio_integrado', 'prof', 'eja_fund', 'eja_medio', 'masculino', 'feminino', 'cor_raca_nd', 'branca', 'preta', 'parda', 'amarela', 'indigena', 'brasileira', 'naturalizado', 'estrangeira', 'uf_nasc_dif', 'in_transporte_publico', 'in_necessidade_especial', 'n_alunos', 'n_alunos_at_compl', 'n_alunos_aee', 'n_matriculas_turma', 'n_break', )


def reimport_sexo_raca_cor(apps, schema_editor):
    MatriculaPerfilINEP = apps.get_model("inep", "MatriculaPerfilINEP")
    cols = ['masculino', 'feminino', 'amarela', 'branca', 'cor_raca_nd', 'indigena', 'parda', 'preta']
    matriculas = MatriculaPerfilINEP.objects.all()
    q_filter = None

    for col in cols:
        if q_filter is not None:
            q_filter = q_filter | Q(**{'{}__isnull'.format(col): True})
        else:
            q_filter = Q(**{'{}__isnull'.format(col): True})

    matriculas = matriculas.filter(q_filter)

    for obj in matriculas:
        row = dict(zip(inep_matriculas_csv_cols, obj.raw_csv.split(';')))
        for col in cols:
            value = row[col]
            if value.strip() == '':
                value = None
            else:
                value = int(value)
            setattr(obj, col, value)
        obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0003_auto_20170118_1637'),
    ]

    operations = [
        migrations.RunPython(reimport_sexo_raca_cor),
    ]
