# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inep', '0004_auto_20170118_1642'),
    ]

    operations = [
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_banda_larga',
            field=models.IntegerField(null=True, verbose_name='Banda Larga'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='in_internet',
            field=models.IntegerField(null=True, verbose_name='Internet'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='nu_comp_administrativo',
            field=models.IntegerField(null=True, verbose_name='Computadores para uso administrativo'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='nu_comp_aluno',
            field=models.IntegerField(null=True, verbose_name='Computadores para uso dos alunos'),
        ),
        migrations.AddField(
            model_name='escolaperfilinep',
            name='nu_computador',
            field=models.IntegerField(null=True, verbose_name='Total de Computadores'),
        ),
    ]
