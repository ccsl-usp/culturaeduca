# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import csv
import codecs
import fnmatch

from django.apps import apps
from django.conf import settings
from django.db.models import NullBooleanField, BooleanField, FloatField, IntegerField, BigIntegerField
from django.core.management.base import BaseCommand, CommandError
from ibge.models import Municipio
from django.contrib.gis.geos import Point


csv_campos = {
    'escolas_geo': ['pk_cod_municipio', 'pk_cod_entidade', 'id_longitude', 'id_latitude', 'tipo_geo', 'sigla', 'pk_cod_estado', 'no_municipio_lower', 'no_entidade', 'desc_endereco', 'tipo', 'logradouro', 'num_endereco', 'desc_endereco_complemento', 'desc_endereco_bairro', 'num_cep', 'num_ddd', 'num_telefone', 'num_telefone_publico1', 'num_telefone_publico2', 'num_fax', 'no_email', ],
    'inep_escolas': ['nu_ano_censo', 'co_entidade', 'no_entidade', 'co_orgao_regional', 'tp_situacao_funcionamento', 'dt_ano_letivo_inicio', 'dt_ano_letivo_termino', 'co_regiao', 'co_mesorregiao', 'co_microrregiao', 'co_uf', 'co_municipio', 'co_distrito', 'tp_dependencia', 'tp_localizacao', 'tp_localizacao_diferenciada', 'in_vinculo_secretaria_educacao', 'in_vinculo_seguranca_publica', 'in_vinculo_secretaria_saude', 'in_vinculo_outro_orgao', 'tp_categoria_escola_privada', 'in_conveniada_pp', 'tp_convenio_poder_publico', 'in_mant_escola_privada_emp', 'in_mant_escola_privada_ong', 'in_mant_escola_privada_oscip', 'in_mant_escola_priv_ong_oscip', 'in_mant_escola_privada_sind', 'in_mant_escola_privada_sist_s', 'in_mant_escola_privada_s_fins', 'tp_regulamentacao', 'tp_responsavel_regulamentacao', 'co_escola_sede_vinculada', 'co_ies_ofertante', 'in_local_func_predio_escolar', 'tp_ocupacao_predio_escolar', 'in_local_func_socioeducativo', 'in_local_func_unid_prisional', 'in_local_func_prisional_socio', 'in_local_func_galpao', 'tp_ocupacao_galpao', 'in_local_func_salas_outra_esc', 'in_local_func_outros', 'in_predio_compartilhado', 'in_agua_potavel', 'in_agua_rede_publica', 'in_agua_poco_artesiano', 'in_agua_cacimba', 'in_agua_fonte_rio', 'in_agua_inexistente', 'in_energia_rede_publica', 'in_energia_gerador_fossil', 'in_energia_renovavel', 'in_energia_inexistente', 'in_esgoto_rede_publica', 'in_esgoto_fossa_septica', 'in_esgoto_fossa_comum', 'in_esgoto_fossa', 'in_esgoto_inexistente', 'in_lixo_servico_coleta', 'in_lixo_queima', 'in_lixo_enterra', 'in_lixo_destino_final_publico', 'in_lixo_descarta_outra_area', 'in_tratamento_lixo_separacao', 'in_tratamento_lixo_reutiliza', 'in_tratamento_lixo_reciclagem', 'in_tratamento_lixo_inexistente', 'in_almoxarifado', 'in_area_verde', 'in_auditorio', 'in_banheiro', 'in_banheiro_ei', 'in_banheiro_pne', 'in_banheiro_funcionarios', 'in_banheiro_chuveiro', 'in_biblioteca', 'in_biblioteca_sala_leitura', 'in_cozinha', 'in_despensa', 'in_dormitorio_aluno', 'in_dormitorio_professor', 'in_laboratorio_ciencias', 'in_laboratorio_informatica', 'in_patio_coberto', 'in_patio_descoberto', 'in_parque_infantil', 'in_piscina', 'in_quadra_esportes', 'in_quadra_esportes_coberta', 'in_quadra_esportes_descoberta', 'in_refeitorio', 'in_sala_atelie_artes', 'in_sala_musica_coral', 'in_sala_estudio_danca', 'in_sala_multiuso', 'in_sala_diretoria', 'in_sala_leitura', 'in_sala_professor', 'in_sala_repouso_aluno', 'in_secretaria', 'in_sala_atendimento_especial', 'in_terreirao', 'in_viveiro', 'in_dependencias_outras', 'in_acessibilidade_corrimao', 'in_acessibilidade_elevador', 'in_acessibilidade_pisos_tateis', 'in_acessibilidade_vao_livre', 'in_acessibilidade_rampas', 'in_acessibilidade_sinal_sonoro', 'in_acessibilidade_sinal_tatil', 'in_acessibilidade_sinal_visual', 'in_acessibilidade_inexistente', 'qt_salas_utilizadas_dentro', 'qt_salas_utilizadas_fora', 'qt_salas_utilizadas', 'qt_salas_utiliza_climatizadas', 'qt_salas_utilizadas_acessiveis', 'in_equip_parabolica', 'in_computador', 'in_equip_copiadora', 'in_equip_impressora', 'in_equip_impressora_mult', 'in_equip_scanner', 'in_equip_nenhum', 'in_equip_dvd', 'qt_equip_dvd', 'in_equip_som', 'qt_equip_som', 'in_equip_tv', 'qt_equip_tv', 'in_equip_lousa_digital', 'qt_equip_lousa_digital', 'in_equip_multimidia', 'qt_equip_multimidia', 'in_desktop_aluno', 'qt_desktop_aluno', 'in_comp_portatil_aluno', 'qt_comp_portatil_aluno', 'in_tablet_aluno', 'qt_tablet_aluno', 'in_internet', 'in_internet_alunos', 'in_internet_administrativo', 'in_internet_aprendizagem', 'in_internet_comunidade', 'in_acesso_internet_computador', 'in_aces_internet_disp_pessoais', 'tp_rede_local', 'in_banda_larga', 'qt_prof_administrativos', 'qt_prof_servicos_gerais', 'qt_prof_bibliotecario', 'qt_prof_saude', 'qt_prof_coordenador', 'qt_prof_fonaudiologo', 'qt_prof_nutricionista', 'qt_prof_psicologo', 'qt_prof_alimentacao', 'qt_prof_pedagogia', 'qt_prof_secretario', 'qt_prof_seguranca', 'qt_prof_monitores', 'qt_prof_gestao', 'qt_prof_assist_social', 'in_alimentacao', 'in_serie_ano', 'in_periodos_semestrais', 'in_fundamental_ciclos', 'in_grupos_nao_seriados', 'in_modulos', 'in_formacao_alternancia', 'in_material_ped_multimidia', 'in_material_ped_infantil', 'in_material_ped_cientifico', 'in_material_ped_difusao', 'in_material_ped_musical', 'in_material_ped_jogos', 'in_material_ped_artisticas', 'in_material_ped_desportiva', 'in_material_ped_indigena', 'in_material_ped_etnico', 'in_material_ped_campo', 'in_material_ped_nenhum', 'in_educacao_indigena', 'tp_indigena_lingua', 'co_lingua_indigena_1', 'co_lingua_indigena_2', 'co_lingua_indigena_3', 'in_exame_selecao', 'in_reserva_ppi', 'in_reserva_renda', 'in_reserva_publica', 'in_reserva_pcd', 'in_reserva_outros', 'in_reserva_nenhuma', 'in_redes_sociais', 'in_espaco_atividade', 'in_espaco_equipamento', 'in_orgao_ass_pais', 'in_orgao_ass_pais_mestres', 'in_orgao_conselho_escolar', 'in_orgao_gremio_estudantil', 'in_orgao_outros', 'in_orgao_nenhum', 'tp_proposta_pedagogica', 'tp_aee', 'tp_atividade_complementar', 'in_mediacao_presencial', 'in_mediacao_semipresencial', 'in_mediacao_ead', 'in_especial_exclusiva', 'in_regular', 'in_eja', 'in_profissionalizante', 'in_comum_creche', 'in_comum_pre', 'in_comum_fund_ai', 'in_comum_fund_af', 'in_comum_medio_medio', 'in_comum_medio_integrado', 'in_comum_medio_normal', 'in_esp_exclusiva_creche', 'in_esp_exclusiva_pre', 'in_esp_exclusiva_fund_ai', 'in_esp_exclusiva_fund_af', 'in_esp_exclusiva_medio_medio', 'in_esp_exclusiva_medio_integr', 'in_esp_exclusiva_medio_normal', 'in_comum_eja_fund', 'in_comum_eja_medio', 'in_comum_eja_prof', 'in_esp_exclusiva_eja_fund', 'in_esp_exclusiva_eja_medio', 'in_esp_exclusiva_eja_prof', 'in_comum_prof', 'in_esp_exclusiva_prof', 'nu_funcionarios', ],
    'inep_matriculas': ['co_entidade', 'n_alunos', 'n_matriculas', 'ei', 'creche', 'pre', 'fund_total', 'fund_ai', 'fund_af', 'medio_total', 'medio', 'medio_magistrado', 'medio_integrado', 'prof', 'eja_total', 'eja_fund', 'eja_medio', 'eja_prof', 'especial', 'presencial', 'semi', 'ead', 'n_alunos_at_compl', 'n_alunos_aee', 'masculino', 'feminino', 'cor_raca_nd', 'branca', 'preta', 'parda', 'amarela', 'indigena', 'naturalizado', 'estrangeira', 'uf_nasc_dif', 'in_transporte_publico', 'in_necessidade_especial',  ],
}

csv_models = {
    'escolas_geo': 'equipamento.Escola',
    'inep_escolas': 'inep.EscolaPerfilINEP',
    'inep_matriculas': 'inep.MatriculaPerfilINEP',
}

csv_cod_mun_campo = {
    'escolas_geo': 'pk_cod_municipio',
    'inep_escolas': 'co_municipio',
    'inep_matriculas': None,
}

csv_geom_campo = {
    'escolas_geo': 'geom',
    'inep_escolas': None,
    'inep_matriculas': None,
}

csv_escola_campo = {
    'escolas_geo': None,
    'inep_escolas': 'escola',
    'inep_matriculas': 'escola',
}


def unicode_csv_reader(
        unicode_csv_data, dialect=csv.excel, encoding='utf-8', **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(line_encoder(unicode_csv_data, encoding),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [cell for cell in row]


def line_encoder(unicode_csv_data, encoding):
    for line in unicode_csv_data:
        yield line.encode(encoding)


class Command(BaseCommand):
    help = u'''Importa dados de arquivos CSV: escolas INEP.
    Ex:
        - escolas_geo_br_2015.csv
        - inep_escolas_br_2015.csv
        - inep_matriculas_br_2015.csv

    --diretorio
    '''

    def add_arguments(self, parser):
        parser.add_argument('nome_arquivo', type=str, help='Arquivo CSV')
        parser.add_argument('--encoding', type=str, default='utf8')
        parser.add_argument('--diretorio', action="store_true", default=False)
        parser.add_argument('--pdb-on-exception', action="store_true", default=False)

    def detecta_tipo(self, nome_arquivo):
        tipo = os.path.basename(nome_arquivo).split('_br_')[0]
        self.campos = csv_campos.get(tipo)
        self.campo_cod_municipio = csv_cod_mun_campo.get(tipo)
        self.campo_escola = csv_escola_campo.get(tipo)
        self.campo_geom = csv_geom_campo.get(tipo)
        if self.campo_escola:
            self.escola_model = apps.get_model(*csv_models['escolas_geo'].split('.'))
        self.tipo = apps.get_model(*csv_models.get(tipo).split('.'))

    def importar(self, nome_arquivo, pdb=False):
        print(u"Convertendo '{}':".format(nome_arquivo))
        print(u"Encoding: {}".format(self.encoding))
        try:
            self.detecta_tipo(nome_arquivo)
        except Exception:
            raise CommandError(u'''Não foi possível determinar o tipo do dado pelo nome do arquivo "{}"\n\nEx:
                - escolas_geo_br_2015.csv
                - inep_escolas_br_2015.csv
                - inep_matriculas_br_2015.csv'''.format(
                os.path.basename(nome_arquivo)))

        print(u"Tipo: {}".format(self.tipo))

        ct = 0
        with codecs.open(nome_arquivo, encoding=self.encoding) as arquivo:
            for row in unicode_csv_reader(
                    arquivo, encoding=self.encoding, delimiter=';'):
                if ct != 0:
                    instance = self.tipo()
                    zipped_row = zip(self.campos, row)
                    for fieldname, value in zipped_row:
                        try:
                            field = self.tipo._meta.get_field_by_name(fieldname)[0]
                        except Exception:
                            continue

                        if isinstance(field, (FloatField, IntegerField, BigIntegerField)):
                            value = value.replace(',', '.')
                            if value.strip() in ('', ):
                                value = None
                        elif isinstance(field, (NullBooleanField, BooleanField, )):
                            if value.strip() in ('', ):
                                value = None
                            else:
                                value = bool(int(value))

                        setattr(instance, fieldname, value)

                    if self.campo_cod_municipio:
                        cod_municipio = dict(zipped_row)[self.campo_cod_municipio]
                        instance.municipio = Municipio.objects.filter(cd_geocodm=cod_municipio).first()

                    if self.campo_escola and self.escola_model:
                        co_entidade = dict(zipped_row)['co_entidade']
                        escola = self.escola_model.objects.filter(pk=co_entidade).first()
                        setattr(instance, self.campo_escola, escola)

                    if self.campo_geom:
                        geom = Point(float(instance.id_longitude), float(instance.id_latitude), srid=settings.SRID)
                        setattr(instance, self.campo_geom, geom)

                    try:
                        instance.save()
                    except Exception as e:
                        if pdb:
                            import pdb; pdb.set_trace()
                        else:
                            raise e

                ct += 1

                # print('%.2f%%' % (float(ct * 100) / 272996, ))

        print(u"{} registros importados".format(ct - 1))
        return ct - 1

    def handle(self, *args, **options):
        self.nome_arquivo = options['nome_arquivo']
        self.encoding = options['encoding']

        if options['diretorio']:
            if os.path.isdir(self.nome_arquivo):
                print(u"Importando diretório '{}':".format(self.nome_arquivo))

                for root, dirs, files in os.walk(self.nome_arquivo):
                    csv_files = [fn for fn in files
                                 if fnmatch.fnmatch(fn.lower(), '*.csv')]
                    for fn in csv_files:
                        self.importar(os.path.join(root, fn), pdb=options['pdb-on-exception'])

            else:
                raise CommandError(u'Caminho informado não é um diretório!')
        else:
            self.importar(self.nome_arquivo)
