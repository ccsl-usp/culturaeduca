# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import csv
import codecs
import fnmatch

from django.apps import apps
from django.conf import settings
from django.db.models import NullBooleanField, BooleanField, FloatField, IntegerField, BigIntegerField
from django.core.management.base import BaseCommand, CommandError
from ibge.models import Municipio
from django.contrib.gis.geos import Point


csv_campos = {
    'escolas_geo': ['pk_cod_municipio', 'pk_cod_entidade', 'id_longitude', 'id_latitude', 'tipo_geo', 'sigla', 'pk_cod_estado', 'no_municipio_lower', 'no_entidade', 'desc_endereco', 'tipo', 'logradouro', 'num_endereco', 'desc_endereco_complemento', 'desc_endereco_bairro', 'num_cep', 'num_ddd', 'num_telefone', 'num_telefone_publico1', 'num_telefone_publico2', 'num_fax', 'no_email', ],
    'inep_escolas': ['nu_ano_censo', 'co_entidade', 'no_entidade', 'co_orgao_regional', 'tp_situacao_funcionamento', 'dt_ano_letivo_inicio', 'dt_ano_letivo_termino', 'co_regiao', 'co_mesorregiao', 'co_microrregiao', 'co_uf', 'co_municipio', 'co_distrito', 'tp_dependencia', 'tp_localizacao', 'tp_categoria_escola_privada', 'in_conveniada_pp', 'tp_convenio_poder_publico', 'in_mant_escola_privada_emp', 'in_mant_escola_privada_ong', 'in_mant_escola_privada_sind', 'in_mant_escola_privada_sist_s', 'in_mant_escola_privada_s_fins', 'co_escola_sede_vinculada', 'co_ies_ofertante', 'tp_regulamentacao', 'in_local_func_predio_escolar', 'tp_ocupacao_predio_escolar', 'in_local_func_salas_empresa', 'in_local_func_socioeducativo', 'in_local_func_unid_prisional', 'in_local_func_prisional_socio', 'in_local_func_templo_igreja', 'in_local_func_casa_professor', 'in_local_func_galpao', 'tp_ocupacao_galpao', 'in_local_func_salas_outra_esc', 'in_local_func_outros', 'in_predio_compartilhado', 'in_agua_filtrada', 'in_agua_rede_publica', 'in_agua_poco_artesiano', 'in_agua_cacimba', 'in_agua_fonte_rio', 'in_agua_inexistente', 'in_energia_rede_publica', 'in_energia_gerador', 'in_energia_outros', 'in_energia_inexistente', 'in_esgoto_rede_publica', 'in_esgoto_fossa', 'in_esgoto_inexistente', 'in_lixo_coleta_periodica', 'in_lixo_queima', 'in_lixo_joga_outra_area', 'in_lixo_recicla', 'in_lixo_enterra', 'in_lixo_outros', 'in_sala_diretoria', 'in_sala_professor', 'in_laboratorio_informatica', 'in_laboratorio_ciencias', 'in_sala_atendimento_especial', 'in_quadra_esportes_coberta', 'in_quadra_esportes_descoberta', 'in_quadra_esportes', 'in_cozinha', 'in_biblioteca', 'in_sala_leitura', 'in_biblioteca_sala_leitura', 'in_parque_infantil', 'in_bercario', 'in_banheiro_fora_predio', 'in_banheiro_dentro_predio', 'in_banheiro_ei', 'in_banheiro_pne', 'in_dependencias_pne', 'in_secretaria', 'in_banheiro_chuveiro', 'in_refeitorio', 'in_despensa', 'in_almoxarifado', 'in_auditorio', 'in_patio_coberto', 'in_patio_descoberto', 'in_alojam_aluno', 'in_alojam_professor', 'in_area_verde', 'in_lavanderia', 'in_dependencias_outras', 'nu_salas_existentes', 'nu_salas_utilizadas', 'in_equip_tv', 'in_equip_videocassete', 'in_equip_dvd', 'in_equip_parabolica', 'in_equip_copiadora', 'in_equip_retroprojetor', 'in_equip_impressora', 'in_equip_impressora_mult', 'in_equip_som', 'in_equip_multimidia', 'in_equip_fax', 'in_equip_foto', 'in_computador', 'nu_equip_tv', 'nu_equip_videocassete', 'nu_equip_dvd', 'nu_equip_parabolica', 'nu_equip_copiadora', 'nu_equip_retroprojetor', 'nu_equip_impressora', 'nu_equip_impressora_mult', 'nu_equip_som', 'nu_equip_multimidia', 'nu_equip_fax', 'nu_equip_foto', 'nu_computador', 'nu_comp_administrativo', 'nu_comp_aluno', 'in_internet', 'in_banda_larga', 'nu_funcionarios', 'in_alimentacao', 'tp_aee', 'tp_atividade_complementar', 'in_fundamental_ciclos', 'tp_localizacao_diferenciada', 'in_material_esp_quilombola', 'in_material_esp_indigena', 'in_material_esp_nao_utiliza', 'in_educacao_indigena', 'tp_indigena_lingua', 'co_lingua_indigena', 'in_brasil_alfabetizado', 'in_final_semana', 'in_formacao_alternancia', 'in_mediacao_presencial', 'in_mediacao_semipresencial', 'in_mediacao_ead', 'in_especial_exclusiva', 'in_regular', 'in_eja', 'in_profissionalizante', 'in_comum_creche', 'in_comum_pre', 'in_comum_fund_ai', 'in_comum_fund_af', 'in_comum_medio_medio', 'in_comum_medio_integrado', 'in_comum_medio_normal', 'in_esp_exclusiva_creche', 'in_esp_exclusiva_pre', 'in_esp_exclusiva_fund_ai', 'in_esp_exclusiva_fund_af', 'in_esp_exclusiva_medio_medio', 'in_esp_exclusiva_medio_integr', 'in_esp_exclusiva_medio_normal', 'in_comum_eja_fund', 'in_comum_eja_medio', 'in_comum_eja_prof', 'in_esp_exclusiva_eja_fund', 'in_esp_exclusiva_eja_medio', 'in_esp_exclusiva_eja_prof', 'in_comum_prof', 'in_esp_exclusiva_prof', ],
    'inep_matriculas': ['co_entidade', 'n_alunos', 'n_matriculas', 'creche', 'pre', 'fund_ai', 'fund_af', 'medio', 'medio_magistrado', 'medio_integrado', 'prof', 'eja_fund', 'eja_medio', 'eja_prof', 'especial', 'presencial', 'semi', 'ead', 'n_alunos_at_compl', 'n_alunos_aee', 'masculino', 'feminino', 'cor_raca_nd', 'branca', 'preta', 'parda', 'amarela', 'indigena', 'naturalizado', 'estrangeira', 'uf_nasc_dif', 'in_transporte_publico', 'in_necessidade_especial',  ],
}

csv_models = {
    'escolas_geo': 'equipamento.Escola',
    'inep_escolas': 'inep.EscolaPerfilINEP',
    'inep_matriculas': 'inep.MatriculaPerfilINEP',
}

csv_cod_mun_campo = {
    'escolas_geo': 'pk_cod_municipio',
    'inep_escolas': 'co_municipio',
    'inep_matriculas': None,
}

csv_geom_campo = {
    'escolas_geo': 'geom',
    'inep_escolas': None,
    'inep_matriculas': None,
}

csv_escola_campo = {
    'escolas_geo': None,
    'inep_escolas': 'escola',
    'inep_matriculas': 'escola',
}


def unicode_csv_reader(
        unicode_csv_data, dialect=csv.excel, encoding='utf-8', **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(line_encoder(unicode_csv_data, encoding),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [cell for cell in row]


def line_encoder(unicode_csv_data, encoding):
    for line in unicode_csv_data:
        yield line.encode(encoding)


class Command(BaseCommand):
    help = u'''Importa dados de arquivos CSV: escolas INEP.
    Ex:
        - escolas_geo_br_2015.csv
        - inep_escolas_br_2015.csv
        - inep_matriculas_br_2015.csv

    --diretorio
    '''

    def add_arguments(self, parser):
        parser.add_argument('nome_arquivo', type=str, help='Arquivo CSV')
        parser.add_argument('--encoding', type=str, default='utf8')
        parser.add_argument('--diretorio', action="store_true", default=False)
        parser.add_argument('--pdb-on-exception', action="store_true", default=False)

    def detecta_tipo(self, nome_arquivo):
        tipo = os.path.basename(nome_arquivo).split('_br_')[0]
        self.campos = csv_campos.get(tipo)
        self.campo_cod_municipio = csv_cod_mun_campo.get(tipo)
        self.campo_escola = csv_escola_campo.get(tipo)
        self.campo_geom = csv_geom_campo.get(tipo)
        if self.campo_escola:
            self.escola_model = apps.get_model(*csv_models['escolas_geo'].split('.'))
        self.tipo = apps.get_model(*csv_models.get(tipo).split('.'))

    def importar(self, nome_arquivo, pdb=False):
        print(u"Convertendo '{}':".format(nome_arquivo))
        print(u"Encoding: {}".format(self.encoding))
        try:
            self.detecta_tipo(nome_arquivo)
        except Exception:
            raise CommandError(u'''Não foi possível determinar o tipo do dado pelo nome do arquivo "{}"\n\nEx:
                - escolas_geo_br_2015.csv
                - inep_escolas_br_2015.csv
                - inep_matriculas_br_2015.csv'''.format(
                os.path.basename(nome_arquivo)))

        print(u"Tipo: {}".format(self.tipo))

        ct = 0
        with codecs.open(nome_arquivo, encoding=self.encoding) as arquivo:
            for row in unicode_csv_reader(
                    arquivo, encoding=self.encoding, delimiter=';'):
                if ct != 0:
                    instance = self.tipo()
                    zipped_row = zip(self.campos, row)
                    for fieldname, value in zipped_row:
                        try:
                            field = self.tipo._meta.get_field_by_name(fieldname)[0]
                        except Exception:
                            continue

                        if isinstance(field, (FloatField, IntegerField, BigIntegerField)):
                            value = value.replace(',', '.')
                            if value.strip() in ('', ):
                                value = None
                        elif isinstance(field, (NullBooleanField, BooleanField, )):
                            if value.strip() in ('', ):
                                value = None
                            else:
                                value = bool(int(value))

                        setattr(instance, fieldname, value)

                    if self.campo_cod_municipio:
                        cod_municipio = dict(zipped_row)[self.campo_cod_municipio]
                        instance.municipio = Municipio.objects.filter(cd_geocodm=cod_municipio).first()

                    if self.campo_escola and self.escola_model:
                        co_entidade = dict(zipped_row)['co_entidade']
                        escola = self.escola_model.objects.filter(pk=co_entidade).first()
                        setattr(instance, self.campo_escola, escola)

                    if self.campo_geom:
                        geom = Point(float(instance.id_longitude), float(instance.id_latitude), srid=settings.SRID)
                        setattr(instance, self.campo_geom, geom)

                    try:
                        instance.save()
                    except Exception as e:
                        if pdb:
                            import pdb; pdb.set_trace()
                        else:
                            raise e

                ct += 1

                # print('%.2f%%' % (float(ct * 100) / 272996, ))

        print(u"{} registros importados".format(ct - 1))
        return ct - 1

    def handle(self, *args, **options):
        self.nome_arquivo = options['nome_arquivo']
        self.encoding = options['encoding']

        if options['diretorio']:
            if os.path.isdir(self.nome_arquivo):
                print(u"Importando diretório '{}':".format(self.nome_arquivo))

                for root, dirs, files in os.walk(self.nome_arquivo):
                    csv_files = [fn for fn in files
                                 if fnmatch.fnmatch(fn.lower(), '*.csv')]
                    for fn in csv_files:
                        self.importar(os.path.join(root, fn), pdb=options['pdb-on-exception'])

            else:
                raise CommandError(u'Caminho informado não é um diretório!')
        else:
            self.importar(self.nome_arquivo)
