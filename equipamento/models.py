# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.gis.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.core.urlresolvers import reverse
from django.template.loader import get_template
from utils import equipamentos_entorno
from ibge.models import Municipio

TIPO_GEORREF = (
    (0, u'Coordenada da Base Original'),
    (1, u'Geocodificação Automática por Endereço'),
    (2, u'Geocodificação Manual por Endereço'),
    (3, u'Coordenada da Sede Municipal'),
)

ESFERA_ADMINISTRATIVA = (
    (1, u'Pública'),
    (2, u'Privada'),
)

TIPO_ESFERA_ADMNISTRATIVA = (
    (1, u'Federal'),
    (2, u'Estadual'),
    (3, u'Municipal'),
    (4, u'Associação'),
    (5, u'Empresa'),
    (6, u'Fundação'),
    (7, u'OSCIP'),
    (8, u'Particular'),
    (9, u'Religiosa'),
    (10, u'Mista'),
    (11, u'Entidade Sindical'),
    (12, u'Outra'),
)

SIM_NAO = (
    (0, u'Não'),
    (1, u'Sim'),
)

TIPO_SITUACAO_TEATRO = (
    (1, u'Em atividade'),
    (2, u'Desativado'),
    (3, u'Em Recuperação'),
    (4, u'Em construção'),
    (5, u'Não identificado'),
)

TIPO_TEATRO = (
    (1, u'Ar Livre'),
    (2, u'Arena'),
    (3, u'Arquitetônico Alternativo'),
    (4, u'CC'),
    (5, u'E'),
    (6, u'Italiano'),
    (7, u'Múltiplo'),
    (8, u'Ópera'),
)


class Escola(models.Model):

    # raw_csv = models.CharField(u'Linha CSV do arquivo de importação', max_length=512)
    pk_cod_entidade = models.BigIntegerField(u'Código INEP da Escola', primary_key=True)
    longitude = models.FloatField(u'Longitude da localização do equipamento')
    latitude = models.FloatField(u'Latitude da localização do equipamento')
    tipo_geo = models.IntegerField(u'Identificação numérica do Tipo do Georreferenciamento', choices=TIPO_GEORREF)
    uf = models.CharField(u'Código da UF conforme IBGE', max_length=2)
    no_municipio = models.CharField(u'Nome do Município', max_length=80)
    municipio = models.ForeignKey(Municipio, null=True)
    no_entidade = models.CharField(u'Nome da Escola', max_length=100)
    logradouro = models.CharField(u'Logradouro', max_length=100, null=True)
    num = models.CharField(u'Número do Logradouro', max_length=10, null=True)
    compl_bairro = models.CharField(u'Bairro', max_length=80, null=True)
    cep = models.CharField(u'CEP Original', max_length=9, null=True)
    telefone = models.CharField(u'Número do Telefone', max_length=15, null=True)
    email = models.CharField(u'Email da Escola', max_length=80, null=True)

    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'no_entidade',
                'format': False
            },
            'target_url': {
                'field': 'pk_cod_entidade',
                'format': '{url}'
            },
            'local': {
                'field': ['no_municipio', 'uf'],
                'format': '%s - %s'
            },
        },
        # 'filters': False,
        'filters': (
            ('creche', {
                'label': 'Creche',
                'kwargs': {
                    'matriculaperfilinep__creche__gt': 0,
                },
            }),
            ('pre', {
                'label': 'Pré-escola',
                'kwargs': {
                    'matriculaperfilinep__pre__gt': 0,
                },
            }),
            ('fund_ai', {
                'label': 'Ensino Fundamental - Anos Iniciais',
                'kwargs': {
                    'matriculaperfilinep__fund_ai__gt': 0,
                },
            }),
            ('fund_af', {
                'label': 'Ensino Fundamental - Anos Finais',
                'kwargs': {
                    'matriculaperfilinep__fund_af__gt': 0,
                },
            }),
            ('medio', {
                'label': 'Ensino Médio',
                'kwargs': {
                    'matriculaperfilinep__medio__gt': 0,
                },
            }),
            ('medio_integrado', {
                'label': 'Ensino Médio Integrado',
                'kwargs': {
                    'matriculaperfilinep__medio_integrado__gt': 0,
                },
            }),
            ('prof', {
                'label': 'Ensino Profissionalizante',
                'kwargs': {
                    'matriculaperfilinep__prof__gt': 0,
                },
            }),
            ('eja_fund', {
                'label': 'Ensino EJA Fundamental',
                'kwargs': {
                    'matriculaperfilinep__eja_fund__gt': 0,
                },
            }),
            ('eja_medio', {
                'label': 'Ensino EJA Médio',
                'kwargs': {
                    'matriculaperfilinep__eja_medio__gt': 0,
                },
            }),
        )
    }

    def __unicode__(self):
        return self.no_entidade

    def get_absolute_url(self):
        return reverse('escola_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def inep_escola(self):
        return self.escolaperfilinep_set.first()

    @property
    def inep_matriculas(self):
        return self.matriculaperfilinep_set.first()

    @property
    def inep_indicadores(self):
        return self.indicadoresperfilinep_set.first()

    @property
    def name(self):
        return self.no_entidade

    @property
    def id(self):
        return self.pk

    @property
    def local(self):
        return u'{} - {}'.format(self.no_municipio, self.uf)

    @property
    def lista_url(self):
        return reverse('lista_escola')

    def endereco(self):
        res = [self.logradouro or '', self.num or '']
        if self.compl_bairro:
            res.append(self.compl_bairro)
        if self.cep:
            res.append(u'CEP: {}'.format(self.cep))
        return u' - '.join([r for r in res if len(r.strip())])

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    @property
    def tipo(self):
        return u'Escola'


@python_2_unicode_compatible
class Biblioteca(models.Model):
    """
    Bibliotecas Públicas Municipais
    """
    v001 = models.BigIntegerField(u'Código do equipamento', primary_key=True)
    v002 = models.FloatField(u'Longitude da localização do equipamento')
    v003 = models.FloatField(u'Latitude da localização do equipamento')
    v004 = models.IntegerField(
        u'Identificação numérica do Tipo do Georreferenciamento',
        choices=TIPO_GEORREF)
    v005 = models.CharField(u'Código da UF conforme IBGE', max_length=254)
    v006 = models.IntegerField(u'Código IBGE do Município')
    v007 = models.CharField(u'Nome do Município', max_length=254)
    v008 = models.CharField(u'Denominação do equipamento', max_length=254)
    v009 = models.CharField(
        u'Endereço conforme base de dados original', max_length=254, null=True)
    v010 = models.CharField(
        u'CEP conforme base de dados original', max_length=254, null=True)
    v011 = models.CharField(u'Tipo do Logradouro', max_length=254, null=True)
    v012 = models.CharField(u'Nome do Logradouro', max_length=254, null=True)
    v013 = models.CharField(
        u'Número do equipamento no Logradouro', max_length=254, null=True)
    v014 = models.CharField(
        u'Complemento do endereço', max_length=254, null=True)
    v015 = models.CharField(
        u'Nome do bairro onde se localiza o equipamento',
        max_length=254, null=True)
    v016 = models.CharField(
        u'Código do Endereçamento Postal - CEP', max_length=254, null=True)
    v017 = models.CharField(u'Nome do Responsável', max_length=254, null=True)
    v018 = models.CharField(u'Número do DDD', max_length=254, null=True)
    v019 = models.CharField(u'Número do Telefone1', max_length=254, null=True)
    v020 = models.CharField(u'Número do DDD do Fax', max_length=254, null=True)
    v021 = models.CharField(u'Número do Fax', max_length=254, null=True)
    v022 = models.CharField(u'Email do equipamento', max_length=254, null=True)
    v023 = models.CharField(u'Site do equipamento', max_length=254, null=True)
    v024 = models.IntegerField(
        u'Identificação numérica da Esfera Administrava')
    v025 = models.IntegerField(
        u'Identificação numérica do Tipo da Esfera Administrativa')
    v026 = models.CharField(
        u'Número do CNPJ do Estabelecimento', max_length=254, null=True)
    v027 = models.CharField(
        u'Código da Tipologia SNIIC', max_length=254, null=True)
    v028 = models.IntegerField(u'Código do Censo de bibliotecas da FGV')
    v029 = models.CharField(
        u'Código no Sistema Nacional de Bibliotecas Públicas',
        max_length=254, null=True)
    v030 = models.IntegerField(u'Dia de Funcionamento - Segunda-Feira')
    v031 = models.IntegerField(u'Dia de Funcionamento - Terça-Feira')
    v032 = models.IntegerField(u'Dia de Funcionamento - Quarta-Feira')
    v033 = models.IntegerField(u'Dia de Funcionamento - Quinta-Feira')
    v034 = models.IntegerField(u'Dia de Funcionamento - Sexta-Feira')
    v035 = models.IntegerField(u'Dia de Funcionamento - Sábado')
    v036 = models.IntegerField(u'Dia de Funcionamento - Domingo')
    v037 = models.IntegerField(u'Período de Funcionamento - Manhã')
    v038 = models.IntegerField(u'Período de Funcionamento - Tarde')
    v039 = models.IntegerField(u'Período de Funcionamento - Noite')
    v040 = models.IntegerField(u'Acesso de Internet para Usuários')
    v041 = models.IntegerField(u'Quantidade de empréstimos', null=True)
    v042 = models.IntegerField(u'Quantidade de livros', null=True)
    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'v008',
                'format': False
            },
            'target_url': {
                'field': 'v001',
                'format': '{url}'
            },
            'local': {
                'field': ['v007', 'v005'],
                'format': '%s - %s'
            },
        },
        'filters': False,
    }

    def __str__(self):
        return self.v008 or ''

    @property
    def id(self):
        return self.pk

    def get_absolute_url(self):
        return reverse('biblioteca_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def name(self):
        return self.v008

    @property
    def local(self):
        return u'{} - {}'.format(self.v007, self.v005)

    @property
    def lista_url(self):
        return reverse('lista_biblioteca')

    def endereco(self):
        res = [self.v009 or '']
        if self.v016:
            res.append('CEP: {}'.format(self.v016))
        if self.v015:
            res.append(self.v015 or '')
        return u' - '.join(res)

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    @property
    def tipo(self):
        return u'Biblioteca'


@python_2_unicode_compatible
class Museu(models.Model):
    """
    Museus
    """
    v001 = models.BigIntegerField(u'Código do equipamento', primary_key=True)
    v002 = models.FloatField(u'Longitude da localização do equipamento')
    v003 = models.FloatField(u'Latitude da localização do equipamento')
    v004 = models.IntegerField(
        u'Identificação numérica do Tipo do Georreferenciamento',
        choices=TIPO_GEORREF)
    v005 = models.CharField(u'Código da UF conforme IBGE', max_length=254)
    v006 = models.IntegerField(u'Código IBGE do Município')
    v007 = models.CharField(u'Nome do Município', max_length=254)
    v008 = models.CharField(u'Denominação do equipamento', max_length=254)
    v009 = models.CharField(
        u'Endereço conforme base de dados original', max_length=254, null=True)
    v010 = models.CharField(
        u'CEP conforme base de dados original', max_length=254, null=True)
    v011 = models.CharField(u'Tipo do Logradouro', max_length=254, null=True)
    v012 = models.CharField(u'Nome do Logradouro', max_length=254, null=True)
    v013 = models.CharField(
        u'Número do equipamento no Logradouro', max_length=254, null=True)
    v014 = models.CharField(
        u'KM em que se localiza o equipamento', max_length=254, null=True)
    v015 = models.CharField(
        u'Complemento do endereço', max_length=254, null=True)
    v016 = models.CharField(
        u'Nome do bairro onde se localiza o equipamento',
        max_length=254, null=True)
    v017 = models.CharField(
        u'Código do Endereçamento Postal - CEP', max_length=254, null=True)
    v018 = models.CharField(u'Número do DDD', max_length=254, null=True)
    v019 = models.CharField(u'Número do Telefone', max_length=254, null=True)
    v020 = models.CharField(u'Número do Telefone2', max_length=254, null=True)
    v021 = models.CharField(u'Número do Telefone3', max_length=254, null=True)
    v022 = models.CharField(u'Site do equipamento', max_length=254, null=True)
    v023 = models.IntegerField(
        u'Identificação numérica da Esfera Administrava',
        choices=ESFERA_ADMINISTRATIVA)
    v024 = models.IntegerField(
        u'Identificação numérica do Tipo da Esfera Administrativa',
        null=True, choices=TIPO_ESFERA_ADMNISTRATIVA)
    v025 = models.CharField(
        u'Descrição do Tipo da Esfera Administrativa',
        max_length=254, null=True)
    v026 = models.CharField(
        u'Número do CNPJ do Estabelecimento', max_length=254, null=True)
    v027 = models.CharField(
        u'Código da Tipologia SNIIC', max_length=254, null=True)
    v028 = models.CharField(u'Nome do Local', max_length=254, null=True)
    v029 = models.CharField(u'Ano da Criação', max_length=254, null=True)
    v030 = models.CharField(u'Ano da Abertura', max_length=254, null=True)
    v031 = models.IntegerField(u'Situação do Museu')
    v032 = models.IntegerField(u'O ingresso ao Museu é cobrado')
    v033 = models.IntegerField(u'Descrição do valores dos ingressos')
    v034 = models.CharField(u'Possui orçamento próprio', max_length=254)
    v035 = models.IntegerField(
        u'Número total de bens culturais que compõem o acervo')
    v036 = models.IntegerField(
        u'Número total é  aproximado ou exato')
    v037 = models.CharField(
        u'Tipologia - Antropologia e Etnografia', max_length=254, null=True)
    v038 = models.CharField(
        u'Tipologia - Arqueologia', max_length=254, null=True)
    v039 = models.CharField(
        u'Tipologia - Artes Visuais', max_length=254, null=True)
    v040 = models.CharField(
        u'Tipologia - Ciências Naturais e História Natural',
        max_length=254, null=True)
    v041 = models.CharField(
        u'Tipologia - Ciência e Tecnologia', max_length=254, null=True)
    v042 = models.CharField(u'Tipologia - História', max_length=254, null=True)
    v043 = models.CharField(
        u'Tipologia - Imagem e Som', max_length=254, null=True)
    v044 = models.CharField(u'Tipologia - Virtual', max_length=254, null=True)
    v045 = models.CharField(
        u'Tipologia - Arquivístico', max_length=254, null=True)
    v046 = models.CharField(
        u'Tipologia - Biblioteconômico', max_length=254, null=True)
    v047 = models.CharField(
        u'Tipologia - Documental', max_length=254, null=True)
    v048 = models.CharField(u'Tipologia - Outros', max_length=254, null=True)
    v049 = models.IntegerField(
        u'Possui infraestrutura para recebimento de turistas estrangeiros')
    v050 = models.IntegerField(u'Sinalização visual em outros idiomas')
    v051 = models.CharField(u'Especifique o idioma', max_length=254)
    v052 = models.IntegerField(
        u'Etiquetas de objetos/textos explicativos em outros idiomas')
    v053 = models.CharField(u'Especifique o idioma', max_length=254)
    v054 = models.IntegerField(u'Publicações em outros idiomas')
    v055 = models.CharField(u'Idioma das Publicações', max_length=254)
    v056 = models.IntegerField(
        u'Outras infraestruturas para recebimento de turistas estrangeiros')
    v057 = models.CharField(
        u'Idioma das outras infraestruturas para recebimento de turistas estrangeiros',
        max_length=254)
    v058 = models.IntegerField(u'Bebedouro')
    v059 = models.IntegerField(u'Estacionamento')
    v060 = models.IntegerField(u'Lanchonete/ Retaurante')
    v061 = models.IntegerField(u'Livraria')
    v062 = models.IntegerField(u'Loja')
    v063 = models.IntegerField(u'Sanitários')
    v064 = models.IntegerField(u'Telefone público')
    v065 = models.IntegerField(u'Outras instalações')
    v066 = models.CharField(
        u'Especificação outras instalações', max_length=254)
    v067 = models.IntegerField(u'Vagas exclusivas em estacionamento')
    v068 = models.IntegerField(
        u'Elevadores com cabine e portas de entrada acessíveis para pessoa portadora de deficiência ou com mobilidade reduzida')
    v069 = models.IntegerField(u'Rampa de acesso')
    v070 = models.IntegerField(
        u'Sanitários adaptados com equipamentos e acessórios próprios')
    v071 = models.IntegerField(u'Sinalização em braile')
    v072 = models.IntegerField(
        u'Textos/etiquetas em braile com informações sobre os objetos em exposição')
    v073 = models.IntegerField(
        u'Outras instalações destinadas aos portadores de necessidades especiais')
    v074 = models.CharField(
        u'Possui Medidas Preventivas', max_length=254, null=True)
    v075 = models.IntegerField(
        u'Treinamento periódico dos profissionais que trabalham no museu')
    v076 = models.IntegerField(u'Brigada contra incêndio')
    v077 = models.IntegerField(
        u'Revisão periódica  dos extintores de incêndio')
    v078 = models.IntegerField(u'Revisão periódica da rede elétrica do museu')
    v079 = models.IntegerField(u'Outros', null=True)
    v080 = models.CharField(
        u'Promove visitas guiadas', max_length=254, null=True)
    v081 = models.CharField(
        u'Promove visitas guiadas com áudio-guia', max_length=254, null=True)
    v082 = models.CharField(
        u'Promove visitas guiadas com monitores', max_length=254, null=True)
    v083 = models.CharField(
        u'É necessário agendamento prévio', max_length=254, null=True)
    v084 = models.CharField(u'Possui biblioteca', max_length=254, null=True)
    v085 = models.CharField(
        u'A biblioteca tem acesso ao público', max_length=254, null=True)
    v086 = models.CharField(
        u'Tipologia e abrangência do acervo', max_length=254, null=True)
    v087 = models.CharField(
        u'Possui arquivo histórico', max_length=254, null=True)
    v088 = models.CharField(
        u'O arquivo tem acesso ao público', max_length=254, null=True)
    v089 = models.CharField(
        u'Tipologia e abrangência do acervo', max_length=254, null=True)
    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'v008',
                'format': False
            },
            'target_url': {
                'field': 'v001',
                'format': '{url}'
            },
            'local': {
                'field': ['v007', 'v005'],
                'format': '%s - %s'
            },
        },
        'filters': False,
    }

    def __str__(self):
        return self.v008 or ''

    @property
    def id(self):
        return self.pk

    def get_absolute_url(self):
        return reverse('museu_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def name(self):
        return self.v008

    @property
    def local(self):
        return u'{} - {}'.format(self.v007, self.v005)

    @property
    def lista_url(self):
        return reverse('lista_museu')

    def endereco(self):
        res = [self.v009 or '']
        if self.v017:
            res.append('CEP: {}'.format(self.v017))
        if self.v016:
            res.append(self.v016 or '')
        return u' - '.join(res)

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    @property
    def tipo(self):
        return u'Museu'


@python_2_unicode_compatible
class Teatro(models.Model):
    """
    Teatros - Espaços Cénicos
    """
    v001 = models.BigIntegerField(u'Código do equipamento', primary_key=True)
    v002 = models.FloatField(u'Longitude da localização do equipamento')
    v003 = models.FloatField(u'Latitude da localização do equipamento')
    v004 = models.IntegerField(
        u'Identificação numérica do Tipo do Georreferenciamento',
        choices=TIPO_GEORREF)
    v005 = models.CharField(
        u'Código da UF conforme IBGE', max_length=254, null=True)
    v006 = models.IntegerField(u'Código IBGE do Município')
    v007 = models.CharField(u'Nome do Município', max_length=254, null=True)
    v008 = models.CharField(u'Nome do Estabelecimento', max_length=254)
    v009 = models.CharField(
        u'Endereço conforme base de dados original', max_length=254, null=True)
    v010 = models.CharField(
        u'CEP conforme base de dados original', max_length=254, null=True)
    v011 = models.CharField(u'Tipo do Logradouro', max_length=254, null=True)
    v012 = models.CharField(u'Nome do Logradouro', max_length=254, null=True)
    v013 = models.CharField(
        u'Número do equipamento no Logradouro', max_length=254, null=True)
    v014 = models.CharField(
        u'KM em que se localiza o equipamento', max_length=254, null=True)
    v015 = models.CharField(
        u'Complemento do endereço', max_length=254, null=True)
    v016 = models.CharField(
        u'Nome do bairro onde se localiza o equipamento',
        max_length=254, null=True)
    v017 = models.CharField(
        u'Código do Endereçamento Postal - CEP', max_length=254, null=True)
    v018 = models.CharField(u'Número do DDD', max_length=254, null=True)
    v019 = models.CharField(u'Número do Telefone', max_length=254, null=True)
    v020 = models.CharField(u'Número do Telefone2', max_length=254, null=True)
    v021 = models.CharField(u'Número do Fax', max_length=254, null=True)
    v022 = models.CharField(u'Email do equipamento', max_length=254, null=True)
    v023 = models.CharField(u'Site do equipamento', max_length=254, null=True)
    v024 = models.IntegerField(
        u'Identificação numérica da Esfera Administrava',
        null=True, choices=ESFERA_ADMINISTRATIVA)
    v025 = models.IntegerField(
        u'Identificação numérica do Tipo da Esfera Administrativa',
        null=True, choices=TIPO_ESFERA_ADMNISTRATIVA)
    v026 = models.CharField(
        u'Número do CNPJ do Estabelecimento', max_length=254, null=True)
    v027 = models.CharField(
        u'Código da Tipologia SNIIC', max_length=254, null=True)
    v028 = models.IntegerField(u'Código Original')
    v029 = models.IntegerField(
        u'Situação do Teatro',
        choices=TIPO_SITUACAO_TEATRO)
    v030 = models.CharField(u'Tipo do Teatro', max_length=254, null=True)
    v031 = models.CharField(
        u'Quantidade de Lugares', max_length=254, null=True)
    v032 = models.CharField(u'Ano da Inauguração', max_length=254, null=True)
    v033 = models.CharField(
        u'Ano do desaparecimento', max_length=254, null=True)
    v034 = models.CharField(u'Data da informação', max_length=254, null=True)
    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'v008',
                'format': False
            },
            'target_url': {
                'field': 'v001',
                'format': '{url}'
            },
            'local': {
                'field': ['v007', 'v005'],
                'format': '%s - %s'
            },
        },
        'filters': False,
    }

    def __str__(self):
        return self.v008 or ''

    @property
    def id(self):
        return self.pk

    def get_absolute_url(self):
        return reverse('teatro_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def name(self):
        return self.v008

    @property
    def local(self):
        return u'{} - {}'.format(self.v007, self.v005)

    @property
    def lista_url(self):
        return reverse('lista_teatro')

    def endereco(self):
        res = [self.v009 or '']
        if self.v017:
            res.append('CEP: {}'.format(self.v017))
        if self.v016:
            res.append(self.v016 or '')
        return u' - '.join(res)

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    @property
    def tipo(self):
        return u'Teatro'


@python_2_unicode_compatible
class Cinema(models.Model):
    """
    Cinemas
    """
    v001 = models.CharField(
        u'Código do Equipamento', max_length=254, null=True)
    v002 = models.FloatField(u'Longitude da localização do equipamento')
    v003 = models.FloatField(u'Latitude da localização do equipamento')
    v004 = models.IntegerField(
        u'Identificação numérica do Tipo do Georreferenciamento',
        choices=TIPO_GEORREF)
    v005 = models.CharField(u'Código da UF conforme IBGE', max_length=254)
    v006 = models.IntegerField(u'Código IBGE do Município')
    v007 = models.CharField(u'Nome do Município', max_length=254)
    v008 = models.CharField(u'Denominação do equipamento', max_length=254)
    v009 = models.CharField(
        u'Endereço conforme base de dados original', max_length=254, null=True)
    v010 = models.CharField(
        u'CEP conforme base de dados original', max_length=254, null=True)
    v011 = models.CharField(u'Tipo do Logradouro', max_length=254, null=True)
    v012 = models.CharField(u'Nome do Logradouro', max_length=254, null=True)
    v013 = models.CharField(
        u'Número do equipamento no Logradouro', max_length=254, null=True)
    v014 = models.CharField(
        u'KM em que se localiza o equipamento', max_length=254, null=True)
    v015 = models.CharField(
        u'Complemento do endereço', max_length=254, null=True)
    v016 = models.CharField(
        u'Nome do bairro onde se localiza o equipamento',
        max_length=254, null=True)
    v017 = models.CharField(
        u'Código do Endereçamento Postal dos Correios - CEP',
        max_length=254, null=True)
    v018 = models.CharField(u'Número do DDD', max_length=254, null=True)
    v019 = models.CharField(u'Número do Telefone1', max_length=254, null=True)
    v020 = models.CharField(u'Número do Telefone2', max_length=254, null=True)
    v021 = models.CharField(u'Email do equipamento', max_length=254, null=True)
    v022 = models.CharField(u'Site do equipamento', max_length=254, null=True)
    v023 = models.IntegerField(
        u'Identificação numérica da Esfera Administrativa',
        choices=ESFERA_ADMINISTRATIVA)
    v024 = models.IntegerField(
        u'Identificação numérica do Tipo da Esfera Administrativa',
        choices=TIPO_ESFERA_ADMNISTRATIVA)
    v025 = models.CharField(
        u'Número do CNPJ do Estabelecimento', max_length=254, null=True)
    v026 = models.CharField(u'Código da Tipologia SNIIC', max_length=254)
    v027 = models.CharField(u'Nome do Grupo Ancine', max_length=254)
    v028 = models.IntegerField(u'Número de Salas em 2012', null=True)
    v029 = models.IntegerField(u'Número de Salas em 2013', null=True)
    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'v008',
                'format': False
            },
            'target_url': {
                'field': 'id',
                'format': '{url}'
            },
            'local': {
                'field': ['v007', 'v005'],
                'format': '%s - %s'
            },
        },
        'filters': False,
    }

    def __str__(self):
        return self.v008 or ''

    def get_absolute_url(self):
        return reverse('cinema_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def name(self):
        return self.v008

    @property
    def local(self):
        return u'{} - {}'.format(self.v007, self.v005)

    @property
    def lista_url(self):
        return reverse('lista_cinema')

    def endereco(self):
        res = [self.v009 or '']
        if self.v017:
            res.append('CEP: {}'.format(self.v017))
        if self.v016:
            res.append(self.v016 or '')
        return u' - '.join(res)

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    @property
    def tipo(self):
        return u'Cinema'


@python_2_unicode_compatible
class PontoCultura(models.Model):
    """
    Pontos de Cultura
    """
    v001 = models.CharField(
        u'Código do equipamento', max_length=254, null=True)
    v002 = models.FloatField(u'Longitude da localização do equipamento')
    v003 = models.FloatField(u'Latitude da localização do equipamento')
    v004 = models.IntegerField(
        u'Identificação numérica do Tipo do Georreferenciamento',
        choices=TIPO_GEORREF)
    v005 = models.CharField(
        u'Código da UF conforme IBGE', max_length=254, null=True)
    v006 = models.IntegerField(u'Código IBGE do Município')
    v007 = models.CharField(u'Nome do Município', max_length=254, null=True)
    v008 = models.CharField(
        u'Nome da entidade proponente', max_length=254, null=True)
    v009 = models.CharField(u'Denominação do Ponto de Cultura', max_length=254)
    v010 = models.CharField(
        u'Endereço conforme base de dados original', max_length=254, null=True)
    v011 = models.CharField(u'Tipo do Logradouro', max_length=254, null=True)
    v012 = models.CharField(u'Nome do Logradouro', max_length=254, null=True)
    v013 = models.CharField(
        u'Número do equipamento no Logradouro', max_length=254, null=True)
    v014 = models.CharField(
        u'KM em que se localiza o equipamento', max_length=254, null=True)
    v015 = models.CharField(
        u'Complemento do endereço', max_length=254, null=True)
    v016 = models.CharField(
        u'Nome do bairro onde se localiza o equipamento',
        max_length=254, null=True)
    v017 = models.CharField(
        u'Código do Endereçamento Postal - CEP', max_length=254, null=True)
    v018 = models.CharField(
        u'Número do Edital de Seleção', max_length=254, null=True)
    v019 = models.CharField(u'Tipo do Ponto de Cultura',
                            max_length=254, null=True)
    v020 = models.CharField(
        u'Tipo de instrumento', max_length=254, null=True)
    v021 = models.CharField(
        u'Identificação numérica do Tipo da Esfera Administrativa',
        max_length=254, null=True, choices=TIPO_ESFERA_ADMNISTRATIVA)
    v022 = models.CharField(
        u'Número do CNPJ da Entidade Proponente', max_length=254, null=True)
    v023 = models.CharField(u'Código PRONAC', max_length=254, null=True)
    v024 = models.FloatField(u'Código SALIC', null=True)
    v025 = models.FloatField(
        u'Código Secretaria de Cidadania e Diversidade Cultural - SCDC/MinC',
        null=True)
    v026 = models.CharField(u'Número do DDD1', max_length=254, null=True)
    v027 = models.CharField(u'Número do Telefone1', max_length=254, null=True)
    v028 = models.CharField(u'Número do DDD2', max_length=254, null=True)
    v029 = models.CharField(u'Número do Telefone2', max_length=254, null=True)
    v030 = models.CharField(u'Número do DDD3', max_length=254, null=True)
    v031 = models.CharField(u'Número do Telefone3', max_length=254, null=True)
    v032 = models.CharField(
        u'Site do Ponto de Cultura ou da Entidade Proponente ',
        max_length=254, null=True)
    v033 = models.CharField(
        u'Nome do responsável do Ponto de Cultura ou da Entidade Proponente ',
        max_length=254, null=True)
    v034 = models.CharField(
        u'Email do responsável do Ponto de Cultura ou da Entidade Proponente ',
        max_length=254, null=True)
    v035 = models.CharField(
        u'Email 1 do Ponto de Cultura ou da Entidade Proponente ',
        max_length=254, null=True)
    v036 = models.CharField(
        u'Email 1 do Ponto de Cultura ou da Entidade Proponente ',
        max_length=254, null=True)
    v037 = models.CharField(
        u'Email 3 do Ponto de Cultura ou da Entidade Proponente ',
        max_length=254, null=True)
    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'v008',
                'format': False
            },
            'target_url': {
                'field': 'id',
                'format': '{url}'
            },
            'local': {
                'field': ['v007', 'v005'],
                'format': '%s - %s'
            },
        },
        'filters': False,
    }

    def __str__(self):
        return self.v008 or ''

    def get_absolute_url(self):
        return reverse('pontocultura_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def name(self):
        return self.v008

    @property
    def local(self):
        return get_template('mapa/popup/ponto_cultura_popup.html').render(
            {'equipamento': self}
        )

    @property
    def lista_url(self):
        return reverse('lista_pontocultura')

    def endereco(self):
        res = [self.v010 or '']
        if self.v017:
            res.append('CEP: {}'.format(self.v017))
        if self.v016:
            res.append(self.v016 or '')
        return u' - '.join(res)

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    class Meta:
        verbose_name = 'Ponto de Cultura'
        verbose_name_plural = 'Pontos de Cultura'

    @property
    def tipo(self):
        return self._meta.verbose_name


@python_2_unicode_compatible
class CentroCiencia(models.Model):
    """
    Centros e Museus de Ciências
    """
    v001 = models.CharField(
        u'Código do equipamento', max_length=254, null=True)
    v002 = models.FloatField(u'Longitude da localização do equipamento')
    v003 = models.FloatField(u'Latitude da localização do equipamento')
    v004 = models.IntegerField(
        u'Identificação numérica do Tipo do Georreferenciamento',
        choices=TIPO_GEORREF)
    v005 = models.CharField(u'Código da UF conforme IBGE', max_length=254)
    v006 = models.IntegerField(u'Código IBGE do Município')
    v007 = models.CharField(u'Nome do Município', max_length=254)
    v008 = models.CharField(u'Denominação do equipamento', max_length=254)
    v009 = models.CharField(u'Endereço Original', max_length=254)
    v010 = models.CharField(u'Tipo do Logradouro', max_length=254, null=True)
    v011 = models.CharField(u'Nome do Logradouro', max_length=254)
    v012 = models.CharField(
        u'Número do equipamento no Logradouro', max_length=254, null=True)
    v013 = models.CharField(
        u'KM em que se localiza o equipamento', max_length=254, null=True)
    v014 = models.CharField(
        u'Complemento do endereço', max_length=254, null=True)
    v015 = models.CharField(
        u'Nome do bairro onde se localiza o equipamento',
        max_length=254, null=True)
    v016 = models.CharField(
        u'Código do Endereçamento Postal - CEP', max_length=254, null=True)
    v017 = models.IntegerField(
        u'Identificação numérica da Esfera Administrava',
        null=True, choices=ESFERA_ADMINISTRATIVA)
    v018 = models.IntegerField(
        u'Identificação numérica do Tipo da Esfera Administrativa',
        null=True, choices=TIPO_ESFERA_ADMNISTRATIVA)
    v019 = models.CharField(u'Número do DDD', max_length=254, null=True)
    v020 = models.CharField(u'Número do Telefone1', max_length=254, null=True)
    v021 = models.CharField(u'Número do Telefone2', max_length=254, null=True)
    v022 = models.CharField(u'Email do equipamento', max_length=254, null=True)
    v023 = models.CharField(u'Site do equipamento', max_length=254, null=True)
    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'v008',
                'format': False
            },
            'target_url': {
                'field': 'id',
                'format': '{url}'
            },
            'local': {
                'field': ['v007', 'v005'],
                'format': '%s - %s'
            },
        },
        'filters': False,
    }

    def __str__(self):
        return self.v008 or ''

    def get_absolute_url(self):
        return reverse('centrociencia_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def name(self):
        return self.v008

    @property
    def local(self):
        return u'{} - {}'.format(self.v007, self.v005)

    @property
    def lista_url(self):
        return reverse('lista_centrociencia')

    def endereco(self):
        res = [self.v009 or '']
        if self.v016:
            res.append('CEP: {}'.format(self.v016))
        if self.v015:
            res.append(self.v015 or '')
        return u' - '.join(res)

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    class Meta:
        verbose_name = u'Centro de Ciência'
        verbose_name_plural = u'Centros de Ciências'

    @property
    def tipo(self):
        return self._meta.verbose_name


@python_2_unicode_compatible
class SalaVerde(models.Model):
    """
    Salas Verdes
    """
    v001 = models.CharField(
        u'Código do equipamento', max_length=254, null=True)
    v002 = models.FloatField(u'Longitude da localização do equipamento')
    v003 = models.FloatField(u'Latitude da localização do equipamento')
    v004 = models.IntegerField(
        u'Identificação numérica do Tipo do Georreferenciamento',
        choices=TIPO_GEORREF)
    v005 = models.CharField(u'Código da UF conforme IBGE', max_length=254)
    v006 = models.IntegerField(u'Código IBGE do Município')
    v007 = models.CharField(u'Nome do Município', max_length=254)
    v008 = models.CharField(u'Denominação do equipamento', max_length=254)
    v009 = models.CharField(u'Endereço Original', max_length=254, null=True)
    v010 = models.CharField(
        u'Código do Endereçamento Postal - CEP', max_length=254, null=True)
    v011 = models.CharField(u'Número do DDD', max_length=254, null=True)
    v012 = models.CharField(u'Número do Telefone1', max_length=254, null=True)
    v013 = models.CharField(u'Número do DDD2', max_length=254, null=True)
    v014 = models.CharField(u'Número do Telefone2', max_length=254, null=True)
    v015 = models.CharField(
        u'Nome do bairro onde se localiza o equipamento', max_length=254)
    v016 = models.CharField(u'Email do equipamento', max_length=254, null=True)
    v017 = models.CharField(
        u'Email2 do equipamento', max_length=254, null=True)
    v018 = models.CharField(
        u'Email3 do equipamento', max_length=254, null=True)
    v019 = models.CharField(u'Nome da Instituição', max_length=254, null=True)
    v020 = models.CharField(u'Informações do Projeto', max_length=254)
    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'v008',
                'format': False
            },
            'target_url': {
                'field': 'id',
                'format': '{url}'
            },
            'local': {
                'field': ['v007', 'v005'],
                'format': '%s - %s'
            },
        },
        'filters': False,
    }

    def __str__(self):
        return self.v008 or ''

    def get_absolute_url(self):
        return reverse('salaverde_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def name(self):
        return self.v008

    @property
    def local(self):
        return u'{} - {}'.format(self.v007, self.v005)

    @property
    def lista_url(self):
        return reverse('lista_salaverde')

    def endereco(self):
        res = [self.v009 or '']
        if self.v010:
            res.append('CEP: {}'.format(self.v010))
        if self.v015:
            res.append(self.v015 or '')
        return u' - '.join(res)

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    class Meta:
        verbose_name = u'Sala Verde'
        verbose_name_plural = u'Salas Verde'

    @property
    def tipo(self):
        return self._meta.verbose_name


@python_2_unicode_compatible
class CRAS(models.Model):
    """
    Centros de Referência da Assistência Social
    """
    v001 = models.BigIntegerField(u'Código do Equipamento', primary_key=True)
    v002 = models.FloatField(u'Longitude da localização do equipamento')
    v003 = models.FloatField(u'Latitude da localização do equipamento')
    v004 = models.IntegerField(
        u'Identificação numérica do Tipo do Georreferenciamento',
        choices=TIPO_GEORREF)
    v005 = models.CharField(u'Código da UF conforme IBGE', max_length=254)
    v006 = models.IntegerField(u'Código IBGE do Município')
    v007 = models.CharField(u'Nome do Município', max_length=254)
    v008 = models.CharField(
        u'Denominação do equipamento', max_length=254, null=True)
    v009 = models.CharField(u'Tipo do Logradouro', max_length=254, null=True)
    v010 = models.CharField(u'Nome do Logradouro', max_length=254, null=True)
    v011 = models.CharField(
        u'Número do equipamento no Logradouro', max_length=254, null=True)
    v012 = models.CharField(
        u'KM em que se localiza o equipamento', max_length=254, null=True)
    v013 = models.CharField(
        u'Complemento do endereço', max_length=254, null=True)
    v014 = models.CharField(
        u'Referência da localização', max_length=254, null=True)
    v015 = models.CharField(
        u'Nome do bairro onde se localiza o equipamento',
        max_length=254, null=True)
    v016 = models.CharField(
        u'Código do Endereçamento Postal dos Correios - CEP',
        max_length=254, null=True)
    v017 = models.IntegerField(u'Número do DDD', null=True)
    v018 = models.CharField(u'Número do Telefone1', max_length=254, null=True)
    v019 = models.CharField(u'Número do Ramal', max_length=254, null=True)
    v020 = models.CharField(u'Email do equipamento', max_length=254, null=True)
    v021 = models.CharField(u'Número do Fax', max_length=254, null=True)
    v022 = models.IntegerField(u'Ano de implantação do CRAS', null=True)
    v023 = models.IntegerField(u'q16_1_Recepção e acolhida', null=True)
    v024 = models.IntegerField(u'q16_2_Acompanhamento de famílias', null=True)
    v025 = models.IntegerField(
        u'q16_3_Acompanhamento prioritário de famílias em descumprimento de condicionalidades do PBF', null=True)
    v026 = models.IntegerField(
        u'q16_4_Acompanhamento prioritário dos beneficiários do BPC (idosos)',
        null=True)
    v027 = models.IntegerField(
        u'q16_5_Acompanhamento prioritário dos beneficiários do BPC (pessoas com deficiência)',
        null=True)
    v028 = models.IntegerField(
        u'q16_6_Acompanhamento a famílias integrantes do PETI', null=True)
    v029 = models.IntegerField(
        u'q16_7_Acompanhamento a famílias atendidas com benefícios eventuais',
        null=True)
    v030 = models.IntegerField(u'q16_8_Atendimento de indivíduos', null=True)
    v031 = models.IntegerField(
        u'q16_9_Grupo-oficina de convivência e atividades socioeducativas com famílias',
        null=True)
    v032 = models.IntegerField(u'q16_10_Visitas Domiciliares', null=True)
    v033 = models.IntegerField(
        u'q16_11_Deslocamento da Equipe para atendimento e oferta de serviço em localidades distantes',
        null=True)
    v034 = models.IntegerField(u'q16_12_Palestras', null=True)
    v035 = models.IntegerField(
        u'q16_13_Apoio para obtenção de Documentação pessoal', null=True)
    v036 = models.IntegerField(
        u'q16_14_Orientação-acompanhamento para inserção do BPC', null=True)
    v037 = models.IntegerField(
        u'q16_15_Encaminhamento de famílias ou indivíduos para a rede de serviço socioassistencial',
        null=True)
    v038 = models.IntegerField(
        u'q16_16_Encaminhamento de famílias ou indivíduos para outras políticas públicas',
        null=True)
    v039 = models.IntegerField(
        u'q16_17_Encaminhamento para obtenção de Benefícios Eventuais',
        null=True)
    v040 = models.IntegerField(
        u'q16_18_Encaminhamento para inserção de famílias no Cadastro Único',
        null=True)
    v041 = models.IntegerField(
        u'q16_19_Acompanhamento dos encaminhamentos realizados', null=True)
    v042 = models.CharField(
        u'q17_1a_Total de famílias em acompanhamento pelo PAIF em Agosto de 2011',
        max_length=254, null=True)
    v043 = models.CharField(
        u'q17_1b_Quantidade de novas famílias inseridas no acompanhamento do PAIF durante o mês de Agosto de 2011',
        max_length=254, null=True)
    v044 = models.CharField(
        u'q17_2a_Quantidade de Famílias em situação de extrema pobreza',
        max_length=254, null=True)
    v045 = models.CharField(
        u'q17_2b_Quantidade de Famílias beneficiárias do Programa Bolsa Família',
        max_length=254, null=True)
    v046 = models.CharField(
        u'q17_2c_Quantidade de Famílias beneficiárias do Programa Bolsa Família, em descumprimento de condicionalidades',
        max_length=254, null=True)
    v047 = models.CharField(
        u'q17_2d_Quantidade de Famílias com membros beneficiários do BPC',
        max_length=254, null=True)
    v048 = models.CharField(
        u'q17_2e_Quantidade de Famílias com crianças-adolescentes no PETI',
        max_length=254, null=True)
    v049 = models.CharField(
        u'q17_2f_Quantidade de Famílias com adolescentes no Projovem adolescente',
        max_length=254, null=True)
    v050 = models.CharField(
        u'q17_3a_Total de atendimentos individualizados realizados no mês de Agosto de 2011',
        max_length=254, null=True)
    v051 = models.CharField(
        u'q17_3b_Quantidade de Famílias encaminhadas para inclusão no Cadastro Único (ago-2011)',
        max_length=254, null=True)
    v052 = models.CharField(
        u'q17_3c_Quantidade de Famílias encaminhadas para atualização cadastral no Cadastro Único (ago-2011)',
        max_length=254, null=True)
    v053 = models.CharField(
        u'q17_3d_Quantidade de indivíduos encaminhados para acesso ao BPC (ago-2011)',
        max_length=254, null=True)
    v054 = models.CharField(
        u'q17_3e_Quantidade de Famílias encaminhadas para outras políticas (ago-2011)',
        max_length=254, null=True)
    v055 = models.CharField(
        u'q17_3f_Quantidade de famílias encaminhadas para o CREAS (ago-2011)',
        max_length=254, null=True)
    v056 = models.CharField(
        u'q17_4_Total de visitas domiciliares realizadas durante o mês de Agosto de 2011',
        max_length=254, null=True)
    v057 = models.IntegerField(
        u'q18_Este CRAS realiza Serviços de Convivência e Fortalecimento de Vínculos (Grupos-Coletivos)? (Ver Tipificação Nacional de Serviços Socioassistenciais)',
        null=True)
    v058 = models.CharField(
        u'q19_1a_Serviço de Convivência e Fortalecimento de Vínculos para crianças até 6 anos de idade (mês de referência AGOSTO de 2011) (Não considerar os serviços executados fora do CRAS)',
        max_length=254, null=True)
    v059 = models.CharField(
        u'q19_1b_Quantidade total de grupos de crianças de até 06 anos neste CRAS',
        max_length=254, null=True)
    v060 = models.CharField(
        u'q19_1c_Quantidade total de crianças que participam deste(s) grupo(s)',
        max_length=254, null=True)
    v061 = models.CharField(
        u'q19_1d_Quantidade total de crianças com deficiência que participam dos grupos',
        max_length=254, null=True)
    v062 = models.CharField(
        u'q19_1e_Do total de crianças com deficiência, quantas recebem o BPC',
        max_length=254, null=True)
    v063 = models.CharField(
        u'q19_1f_Quantos dias normalmente cada criança participa das atividades com este(s) Grupo(s) neste CRAS',
        max_length=254, null=True)
    v064 = models.CharField(
        u'q19_1g_Total de horas por semana em que, normalmente, cada criança participa dos grupos',
        max_length=254, null=True)
    v065 = models.CharField(
        u'q19_1h_Nestes grupos, com qual freqüência há a participação das famílias destas crianças',
        max_length=254, null=True)
    v066 = models.CharField(
        u'q19_2a_Serviço de Convivência e Fortalecimento de Vínculos para crianças e adolescentes de 6 a 15 anos de idade (mês de referência AGOSTO de 2011) (Não considerar os serviços executados fora do CRAS)',
        max_length=254, null=True)
    v067 = models.CharField(
        u'q19_2b_Quantidade total de grupos de crianças e adolescentes de 6 a 15 anos neste CRAS',
        max_length=254, null=True)
    v068 = models.CharField(
        u'q19_2c_Quantidade total de crianças e adolescentes de 06 a 15 anos que participam deste(s) grupo(s)',
        max_length=254, null=True)
    v069 = models.CharField(
        u'q19_2d_Quantidade total de crianças e adolescentes do PETI que participam deste(s) grupo(s)',
        max_length=254, null=True)
    v070 = models.CharField(
        u'q19_2e_Quantidade total de crianças e adolescentes com deficiência que participam dos grupos',
        max_length=254, null=True)
    v071 = models.CharField(
        u'q19_2f_Do total de crianças e adolescentes com deficiência, quantos recebem o BPC',
        max_length=254, null=True)
    v072 = models.CharField(
        u'q19_2g_Quantos dias normalmente cada criança ou adolescente participa das atividades com este(s) Grupo(s) neste CRAS',
        max_length=254, null=True)
    v073 = models.CharField(
        u'q19_2h_Total de horas por semana em que, normalmente, cada criança e adolescente participa dos grupos',
        max_length=254, null=True)
    v074 = models.CharField(
        u'q19_2i_Estes grupos são formados por crianças e adolescentes',
        max_length=254, null=True)
    v075 = models.CharField(
        u'q19_2j_São desenvolvidas atividades com as famílias dos participantes deste(s) grupos de crianças e adolescentes?',
        max_length=254, null=True)
    v076 = models.CharField(
        u'q19_3a_Serviço de Convivência e Fortalecimento de Vínculos para jovens adolescentes de 15 a 17 anos de idade (mês de referência AGOSTO de 2011) (Não considerar os serviços executados fora do CRAS)',
        max_length=254, null=True)
    v077 = models.CharField(
        u'q19_3b_Quantidade total de grupos-coletivos',
        max_length=254, null=True)
    v078 = models.CharField(
        u'q19_3c_Quantidade total de jovens adolescentes que participam deste(s) grupo-coletivo (s)',
        max_length=254, null=True)
    v079 = models.CharField(
        u'q19_3d_Quantidade total de jovens adolescentes do Projovem que participam deste(s) grupo-coletivo (s)',
        max_length=254, null=True)
    v080 = models.CharField(
        u'q19_3e_Quantidade total de jovens adolescentes com deficiência que participam dos grupos-coletivos',
        max_length=254, null=True)
    v081 = models.CharField(
        u'q19_3f_Do total de jovens adolescentes com deficiência, quantos recebem BPC',
        max_length=254, null=True)
    v082 = models.CharField(
        u'q19_3g_Quantos dias normalmente cada jovem adolescente participa de atividades com este(s) grupo-coletivo (s) neste CRAS',
        max_length=254, null=True)
    v083 = models.CharField(
        u'q19_3h_Total de horas por semana em que, normalmente, cada jovem adolescente participa dos grupos-coletivos',
        max_length=254, null=True)
    v084 = models.CharField(
        u'q19_3i_Estes grupos são formados por adolescentes ou jovens',
        max_length=254, null=True)
    v085 = models.CharField(
        u'q19_3j_São desenvolvidas atividades com as famílias dos participantes deste(s) grupos-coletivos de jovens adolescentes?',
        max_length=254, null=True)
    v086 = models.CharField(
        u'q19_4a_Serviço de Convivência e Fortalecimento de Vínculos para Idosos (mês de referência AGOSTO de 2011) (Não considerar os serviços executados fora do CRAS)',
        max_length=254, null=True)
    v087 = models.CharField(
        u'q19_4b_Quantidade de grupos de idosos neste CRAS',
        max_length=254, null=True)
    v088 = models.CharField(
        u'q19_4c_Quantidade total de idosos que participam deste(s) grupo(s)',
        max_length=254, null=True)
    v089 = models.CharField(
        u'q19_4d_Quantidade total de idosos beneficiários do BPC',
        max_length=254, null=True)
    v090 = models.CharField(
        u'q19_4e_Quantidade total de idosos com deficiência participam dos grupos',
        max_length=254, null=True)
    v091 = models.CharField(
        u'q19_4f_Quantos dias normalmente cada idoso participa de atividades com este(s) Grupo(s) neste CRAS',
        max_length=254, null=True)
    v092 = models.CharField(
        u'q19_4g_Total de horas por semana em que, normalmente, cada idoso participa dos grupos',
        max_length=254, null=True)
    v093 = models.CharField(
        u'q19_4h_São desenvolvidas atividades com as famílias dos participantes deste(s) grupos de idosos?',
        max_length=254, null=True)
    v094 = models.CharField(
        u'q20a_1_Crianças até 6', max_length=254, null=True)
    v095 = models.CharField(
        u'q20a_2_Crianças e Adolescentes de 6 a 15',
        max_length=254, null=True)
    v096 = models.CharField(
        u'q20a_3_Jovens de 15 a 17', max_length=254, null=True)
    v097 = models.CharField(
        u'q20a_4_Idosos', max_length=254, null=True)
    v098 = models.CharField(
        u'q20b_1_Crianças até 6', max_length=254, null=True)
    v099 = models.CharField(
        u'q20b_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v100 = models.CharField(
        u'q20b_3_Jovens de 15 a 17', max_length=254, null=True)
    v101 = models.CharField(
        u'q20b_4_Idosos', max_length=254, null=True)
    v102 = models.CharField(
        u'q20c_1_Crianças até 6', max_length=254, null=True)
    v103 = models.CharField(
        u'q20c_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v104 = models.CharField(
        u'q20c_3_Jovens de 15 a 17', max_length=254, null=True)
    v105 = models.CharField(u'q20c_4_Idosos', max_length=254, null=True)
    v106 = models.CharField(
        u'q20d_1_Crianças até 6', max_length=254, null=True)
    v107 = models.CharField(
        u'q20d_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v108 = models.CharField(
        u'q20d_3_Jovens de 15 a 17', max_length=254, null=True)
    v109 = models.CharField(u'q20d_4_Idosos', max_length=254, null=True)
    v110 = models.CharField(
        u'q20e_1_Crianças até 6', max_length=254, null=True)
    v111 = models.CharField(
        u'q20e_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v112 = models.CharField(
        u'q20e_3_Jovens de 15 a 17', max_length=254, null=True)
    v113 = models.CharField(u'q20e_4_Idosos', max_length=254, null=True)
    v114 = models.CharField(
        u'q20f_1_Crianças até 6', max_length=254, null=True)
    v115 = models.CharField(
        u'q20f_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v116 = models.CharField(
        u'q20f_3_Jovens de 15 a 17', max_length=254, null=True)
    v117 = models.CharField(u'q20f_4_Idosos', max_length=254, null=True)
    v118 = models.CharField(
        u'q20g_1_Crianças até 6', max_length=254, null=True)
    v119 = models.CharField(
        u'q20g_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v120 = models.CharField(
        u'q20g_3_Jovens de 15 a 17', max_length=254, null=True)
    v121 = models.CharField(u'q20g_4_Idosos', max_length=254, null=True)
    v122 = models.CharField(
        u'q20h_1_Crianças até 6', max_length=254, null=True)
    v123 = models.CharField(
        u'q20h_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v124 = models.CharField(
        u'q20h_3_Jovens de 15 a 17', max_length=254, null=True)
    v125 = models.CharField(u'q20h_4_Idosos', max_length=254, null=True)
    v126 = models.CharField(
        u'q20i_1_Crianças até 6', max_length=254, null=True)
    v127 = models.CharField(
        u'q20i_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v128 = models.CharField(
        u'q20i_3_Jovens de 15 a 17', max_length=254, null=True)
    v129 = models.CharField(u'q20i_4_Idosos', max_length=254, null=True)
    v130 = models.CharField(
        u'q20j_1_Crianças até 6', max_length=254, null=True)
    v131 = models.CharField(
        u'q20j_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v132 = models.CharField(
        u'q20j_3_Jovens de 15 a 17', max_length=254, null=True)
    v133 = models.CharField(u'q20j_4_Idosos', max_length=254, null=True)
    v134 = models.CharField(
        u'q20k_1_Crianças até 6', max_length=254, null=True)
    v135 = models.CharField(
        u'q20k_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v136 = models.CharField(
        u'q20k_3_Jovens de 15 a 17', max_length=254, null=True)
    v137 = models.CharField(u'q20k_4_Idosos', max_length=254, null=True)
    v138 = models.CharField(
        u'q20l_1_Crianças até 6', max_length=254, null=True)
    v139 = models.CharField(
        u'q20l_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v140 = models.CharField(
        u'q20l_3_Jovens de 15 a 17', max_length=254, null=True)
    v141 = models.CharField(u'q20l_4_Idosos', max_length=254, null=True)
    v142 = models.CharField(
        u'q20m_1_Crianças até 6', max_length=254, null=True)
    v143 = models.CharField(
        u'q20m_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v144 = models.CharField(
        u'q20m_3_Jovens de 15 a 17', max_length=254, null=True)
    v145 = models.CharField(u'q20m_4_Idosos', max_length=254, null=True)
    v146 = models.CharField(
        u'q20n_1_Crianças até 6', max_length=254, null=True)
    v147 = models.CharField(
        u'q20n_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v148 = models.CharField(
        u'q20n_3_Jovens de 15 a 17', max_length=254, null=True)
    v149 = models.CharField(u'q20n_4_Idosos', max_length=254, null=True)
    v150 = models.CharField(
        u'q20o_1_Crianças até 6', max_length=254, null=True)
    v151 = models.CharField(
        u'q20o_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v152 = models.CharField(
        u'q20o_3_Jovens de 15 a 17', max_length=254, null=True)
    v153 = models.CharField(u'q20o_4_Idosos', max_length=254, null=True)
    v154 = models.CharField(
        u'q20p_1_Crianças até 6', max_length=254, null=True)
    v155 = models.CharField(
        u'q20p_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v156 = models.CharField(
        u'q20p_3_Jovens de 15 a 17', max_length=254, null=True)
    v157 = models.CharField(u'q20p_4_Idosos', max_length=254, null=True)
    v158 = models.CharField(
        u'q20q_1_Crianças até 6', max_length=254, null=True)
    v159 = models.CharField(
        u'q20q_2_Crianças e Adolescentes de 6 a 15', max_length=254, null=True)
    v160 = models.CharField(
        u'q20q_3_Jovens de 15 a 17', max_length=254, null=True)
    v161 = models.CharField(u'q20q_4_Idosos', max_length=254, null=True)
    v162 = models.IntegerField(
        u'q21_Este CRAS realizou no mês de AGOSTO DE 2011 grupos no âmbito do PAIF',
        null=True)
    v163 = models.CharField(
        u'q22a_Quantidade de grupos do PAIF ofertados neste CRAS em Agosto-2011 (não devem ser considerados os grupos de inclusão produtiva e de convivência para crianças, jovens e idosos)',
        max_length=254, null=True)
    v164 = models.CharField(
        u'q22b_Quantidade total de participantes dos grupos em Agosto-2011',
        max_length=254, null=True)
    v165 = models.CharField(
        u'q22c_Quantidade total de mulheres que participaram dos grupos em Agosto-2011',
        max_length=254, null=True)
    v166 = models.CharField(
        u'q22d_Total de horas por semana utilizadas para oferta dos grupos PAIF em Agosto-2011',
        max_length=254, null=True)
    v167 = models.CharField(
        u'q22e_1_Direito à transferência de renda e benefícios assistenciais',
        max_length=254, null=True)
    v168 = models.CharField(
        u'q22e_2_Direito a Documentação Civil Básica (certidão de nascimento,<br> CPF, RG, título eleitoral)',
        max_length=254, null=True)
    v169 = models.CharField(
        u'q22e_3_Direito a cultura e lazer', max_length=254, null=True)
    v170 = models.CharField(
        u'q22e_4_Direito das Mulheres', max_length=254, null=True)
    v171 = models.CharField(
        u'q22e_5_Direitos das pessoas com deficiência', max_length=254, null=True)
    v172 = models.CharField(
        u'q22e_6_Direito à Alimentação', max_length=254, null=True)
    v173 = models.CharField(
        u'q22e_7_Os direitos das famílias', max_length=254, null=True)
    v174 = models.CharField(
        u'q22e_8_As especificidades do ciclo vital dos membros das famílias',
        max_length=254, null=True)
    v175 = models.CharField(
        u'q22e_9_Cuidar de quem cuida', max_length=254, null=True)
    v176 = models.CharField(
        u'q22e_10_O uso de álcool e-ou outras drogas na família',
        max_length=254, null=True)
    v177 = models.CharField(
        u'q22e_11_Problemas e soluções do território',
        max_length=254, null=True)
    v178 = models.CharField(
        u'q23_Este CRAS oferta grupos de famílias do PAIF com temas diferentes dos relacionados nos blocos temáticos acima?',
        max_length=254, null=True)
    v179 = models.CharField(u'q23_quais_Qual(is)?', max_length=254, null=True)
    v180 = models.IntegerField(
        u'q24_É feita concessão de Benefícios Eventuais neste CRAS', null=True)
    v181 = models.CharField(
        u'q25_1_Auxílio Funeral', max_length=254, null=True)
    v182 = models.CharField(
        u'q25_2_Auxílio Natalidade', max_length=254, null=True)
    v183 = models.CharField(
        u'q25_3_Auxílios relacionados à segurança alimentar (cesta básica, leite pó, entre outros)',
        max_length=254, null=True)
    v184 = models.CharField(u'q25_4_Passagens', max_length=254, null=True)
    v185 = models.CharField(u'q25_5_Outros', max_length=254, null=True)
    v186 = models.CharField(u'q25_5_qual_Qual?', max_length=254, null=True)
    v187 = models.IntegerField(
        u'q26_Este CRAS realiza ações ou projetos de Capacitação Profissional e-ou Inclusão Produtiva?',
        null=True)
    v188 = models.CharField(
        u'q27_1_Cursos de capacitação profissional para o mercado de trabalho (organizados pelo CRAS)',
        max_length=254, null=True)
    v189 = models.CharField(
        u'q27_2_Cessão de espaço físico para realização de cursos de capacitação profissional ofertados por outras instituições',
        max_length=254, null=True)
    v190 = models.CharField(
        u'q27_3_Cursos de artesanato (trabalhos manuais como pintura em tecido, bordados, bijuterias, etc)',
        max_length=254, null=True)
    v191 = models.CharField(
        u'q27_4_Cadastramento para participação em programas de qualificação profissional',
        max_length=254, null=True)
    v192 = models.CharField(
        u'q27_5_Encaminhamentos para colocação no mercado de trabalho',
        max_length=254, null=True)
    v193 = models.CharField(
        u'q27_6_Organização ou assessoramento para formação de cooperativas ou associações (unidades produtivas)',
        max_length=254, null=True)
    v194 = models.CharField(
        u'q27_7_Cessão de espaço para funcionamento de unidades produtivas',
        max_length=254, null=True)
    v195 = models.CharField(
        u'q27_8_Cessão de espaço para apoio a atividades de concessão de microcrédito produtivo orientado',
        max_length=254, null=True)
    v196 = models.IntegerField(u'q28_1_Não', null=True)
    v197 = models.IntegerField(u'q28_2_Sim, busca ativa', null=True)
    v198 = models.IntegerField(
        u'q28_3_Sim, ações de divulgação e mobilização', null=True)
    v199 = models.IntegerField(
        u'q28_4_Sim, articulação intersetorial para formação de rede de apoio', null=True)
    v200 = models.IntegerField(
        u'q28_5_Sim, articulação com associações e-ou entidades para a formação de rede de apoio',
        null=True)
    v201 = models.IntegerField(u'q28_6_Sim, outras estratégias', null=True)
    v202 = models.CharField(
        u'D3_Fontes de financiamento', max_length=254, null=True)
    v203 = models.CharField(
        u'Total de crianças, jovens e idosos que participam dos grupos de convivencia no CRAS',
        max_length=254, null=True)
    v204 = models.CharField(
        u'd19_1g_horas por semana CRIANÇA 00 06 anos',
        max_length=254, null=True)
    v205 = models.CharField(
        u'd19_2h_horas por semana CRIANÇA ADOLESCENTE 06 15 anos',
        max_length=254, null=True)
    v206 = models.CharField(
        u'd19_3h_horas por semana JOVENS0 15 17 anos',
        max_length=254, null=True)
    v207 = models.CharField(
        u'd19_4g_horas por semana IDOSO', max_length=254, null=True)
    v208 = models.CharField(
        u'd22d_Total de horas por semana utilizadas para oferta dos grupos PAIF (Agosto/2011)',
        max_length=254, null=True)
    v209 = models.IntegerField(
        u'D9_Total de salas utilizadas para atendimento individual ou coletivo',
        null=True)
    v210 = models.IntegerField(
        u'Total de salas utilizadas para atendimento e para atividades administrativas',
        null=True)
    v211 = models.CharField(
        u'D9_Salas_atend_fx_Número de salas utilizadas para atendimento individual ou coletivo',
        max_length=254, null=True)
    v212 = models.CharField(
        u'D4_horas_por_semana_fx_Total de horas por semana que o CRAS encontra-se em funcionamento',
        max_length=254, null=True)
    v213 = models.CharField(
        u'D13a_Quantidade de computadores no CRAS', max_length=254, null=True)
    v214 = models.CharField(
        u'D13b_Quantidade de computadores conectados à internet no CRAS',
        max_length=254, null=True)
    v215 = models.CharField(
        u'D37_fx_Quantidade de famílias em situação de vulnerabilidade que residem no território de abrangência do CRAS',
        max_length=254, null=True)
    v216 = models.CharField(
        u'Total de salas utilizadas para atendimento e para atividades administrativas_fx',
        max_length=254, null=True)
    v217 = models.CharField(
        u'Número de trabalhadores de Nível fundamental (inclui trabalhadores sem escolaridade, fundamental incompleto e ensino médio incompleto)',
        max_length=254, null=True)
    v218 = models.CharField(
        u'Número de trabalhadores de Nível médio (inclui trabalhadores com ensino superior incompleto)',
        max_length=254, null=True)
    v219 = models.CharField(
        u'Número de trabalhadores de Nível superior (inclui trabalhadores com especialização, mestrado e doutorado)',
        max_length=254, null=True)
    v220 = models.CharField(u'Número de Pedagogos', max_length=254, null=True)
    v221 = models.CharField(
        u'Número de Assistentes Sociais', max_length=254, null=True)
    v222 = models.CharField(
        u'Número de Antropólogos', max_length=254, null=True)
    v223 = models.CharField(u'Número de Advogados', max_length=254, null=True)
    v224 = models.CharField(u'Número de Psicólogos', max_length=254, null=True)
    v225 = models.CharField(
        u'Número de Servidores Estatutários', max_length=254, null=True)
    v226 = models.CharField(
        u'Número de Empregados Públicos(CLT)', max_length=254, null=True)
    v227 = models.CharField(
        u'Número de Comissionado', max_length=254, null=True)
    v228 = models.CharField(
        u'Número de profissionais com outros vínculos não permanentes',
        max_length=254, null=True)
    v229 = models.IntegerField(u'Número de trabalhdores no CRAS', null=True)
    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'v008',
                'format': False
            },
            'target_url': {
                'field': 'v001',
                'format': '{url}'
            },
            'local': {
                'field': ['v007', 'v005'],
                'format': '%s - %s'
            },
        },
        'filters': False,
    }

    def __str__(self):
        return self.v008 or ''

    @property
    def id(self):
        return self.pk

    def get_absolute_url(self):
        return reverse('cras_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def name(self):
        return self.v008

    @property
    def local(self):
        return u'{} - {}'.format(self.v007, self.v005)

    @property
    def lista_url(self):
        return reverse('lista_cras')

    def endereco(self):
        inicial = u'{} {}'.format(self.v009 or '', self.v010 or '')
        if self.v011:
            inicial = u'{}, {}'.format(inicial, self.v011)
        res = [inicial]
        if self.v016:
            res.append('CEP: {}'.format(self.v016))
        if self.v015:
            res.append(self.v015 or '')
        return u' - '.join(res)

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    class Meta:
        verbose_name = 'CRAS'
        verbose_name_plural = 'CRAS'

    @property
    def tipo(self):
        return self._meta.verbose_name


@python_2_unicode_compatible
class Saude(models.Model):
    """
    Saúde
    """
    v001 = models.BigIntegerField(
        u'Código no Cadastro Nacional de Estabelecimentos de Saúde')
    v002 = models.FloatField(u'Longitude da localização do equipamento')
    v003 = models.FloatField(u'Latitude da localização do equipamento')
    v004 = models.IntegerField(
        u'Identificação numérica do Tipo do Georreferenciamento',
        choices=TIPO_GEORREF)
    v005 = models.CharField(u'Código da UF conforme IBGE', max_length=254)
    v006 = models.IntegerField(u'Código IBGE do Município')
    v007 = models.CharField(u'Nome do Município', max_length=254)
    v008 = models.CharField(u'Denominação do equipamento', max_length=254)
    v009 = models.CharField(u'Nome do Estabelecimento', max_length=254)
    v010 = models.CharField(u'Endereço Original', max_length=254, null=True)
    v011 = models.CharField(u'Tipo do Logradouro', max_length=254, null=True)
    v012 = models.CharField(u'Nome do Logradouro', max_length=254, null=True)
    v013 = models.CharField(u'Número do Logradouro', max_length=254, null=True)
    v014 = models.CharField(u'KM do Logradouro', max_length=254, null=True)
    v015 = models.CharField(u'Complemento', max_length=254, null=True)
    v016 = models.CharField(u'Bairro', max_length=254, null=True)
    v017 = models.IntegerField(
        u'Código do Endereçamento Postal - CEP', null=True)
    v018 = models.CharField(u'Número do Telefone', max_length=254, null=True)
    v019 = models.IntegerField(
        u'Identificação numérica da Esfera Administrava',
        choices=ESFERA_ADMINISTRATIVA)
    v020 = models.IntegerField(
        u'Identificação numérica do Tipo da Esfera Administrativa',
        choices=TIPO_ESFERA_ADMNISTRATIVA)
    v021 = models.IntegerField(u'Tipo do Estabelecimento')
    v022 = models.CharField(
        u'Descrição do Tipo do Estabelecimento', max_length=254)
    v023 = models.BigIntegerField(
        u'Código para link com a ficha do estabelecimento no CNESNet',
        null=True)
    geom = models.PointField(srid=settings.SRID)
    objects = models.GeoManager()

    layer_config = {
        'fields': {
            '__geometry__': {
                'field': 'geom',
            },
            'name': {
                'field': 'v009',
                'format': False
            },
            'target_url': {
                'field': 'id',
                'format': '{url}'
            },
            'local': {
                'field': ['v007', 'v005'],
                'format': '%s - %s'
            },
        },
        'filters': False,
    }

    def __str__(self):
        return self.v009 or ''

    def get_absolute_url(self):
        return reverse('saude_detalhe', args=[str(self.pk)])

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def name(self):
        return self.v009

    @property
    def local(self):
        return u'{} - {}'.format(self.v007, self.v005)

    @property
    def lista_url(self):
        return reverse('lista_saude')

    def endereco(self):
        res = [self.v010 or '']
        if self.v017:
            res.append('CEP: {}'.format(self.v017))
        if self.v016:
            res.append(self.v016 or '')
        return u' - '.join(res)

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio)

    class Meta:
        verbose_name = u'Saúde'
        verbose_name_plural = 'Saúde'

    @property
    def tipo(self):
        return self._meta.verbose_name