# -*- coding: utf-8 -*-
from django.views.generic import DetailView
from django.views.decorators.cache import cache_page
from django.views.decorators.gzip import gzip_page

from axis.core.views import (
    GenericGeoJSONListView
)

from equipamento.models import (
    Escola,
    Biblioteca,
    Museu,
    Teatro,
    Cinema,
    PontoCultura,
    CentroCiencia,
    SalaVerde,
    CRAS,
    Saude,
)

from ibge.models import SetorCensitario

CACHE_TIMEOUT = None


class EquipamentoDetailView(DetailView):
    template_name_suffix = '_detalhe'
    context_object_name = 'l'

    def get_context_data(self, **kwargs):
        context = super(EquipamentoDetailView, self).get_context_data(**kwargs)

        tipo_equipamento = self.model._meta.verbose_name
        if not tipo_equipamento.isupper():
            tipo_equipamento = tipo_equipamento.capitalize()

        perfil_setores = SetorCensitario.objects.entorno(
            context['l'].geom.y, context['l'].geom.x).perfil()

        total_equipamentos, equipamentos = context['l'].equipamentos_entorno()

        # filtrando todos os tipos de equipamentos que tenham eq[1] >= 1
        # eq[1] é o número de equipamentos de um determinado tipo (zero é
        # quando não tem nenhum deste tipo no entorno)
        equipamentos_existentes = filter(lambda eq: eq[1] >= 1, equipamentos)

        # retorna apenas um dicionário com o model name do tipo de equipamento, exemplo:
        # {
        #     'escola': True,
        # }
        equipamentos_existentes = dict([(model_name, True, ) for tipo, ct, equips, model_name in equipamentos_existentes])

        context.update({
            'tipo_equipamento': tipo_equipamento,
            'perfil_setores': perfil_setores,
            'equipamentos': equipamentos,
            'equipamentos_existentes': equipamentos_existentes,
            'total_equipamentos': total_equipamentos,
        })

        return context


lista_escola = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=Escola,
            properties=['no_entidade', 'pk', 'target_url', 'name', 'local']
        )
    )
)


lista_biblioteca = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=Biblioteca,
            properties=['v008', 'pk', 'target_url', 'name', 'local']
        )
    )
)


lista_museu = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=Museu,
            properties=['v008', 'pk', 'target_url', 'name', 'local']
        )
    )
)


lista_teatro = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=Teatro,
            properties=['v008', 'pk', 'target_url', 'name', 'local']
        )
    )
)


lista_cinema = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=Cinema,
            properties=['v008', 'pk', 'target_url', 'name', 'local']
        )
    )
)


lista_pontocultura = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=PontoCultura,
            properties=['v009', 'pk', 'target_url', 'name', 'local']
        )
    )
)


lista_centrociencia = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=CentroCiencia,
            properties=['v008', 'pk', 'target_url', 'name', 'local']
        )
    )
)


lista_salaverde = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=SalaVerde,
            properties=['v008', 'pk', 'target_url', 'name', 'local']
        )
    )
)


lista_cras = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=CRAS,
            properties=['v008', 'pk', 'target_url', 'name', 'local']
        )
    )
)


lista_saude = cache_page(CACHE_TIMEOUT)(
    gzip_page(
        GenericGeoJSONListView.as_view(
            model=Saude,
            properties=['v009', 'pk', 'target_url', 'name', 'local']
        )
    )
)


escola_detalhe = EquipamentoDetailView.as_view(
    model=Escola
)


biblioteca_detalhe = EquipamentoDetailView.as_view(
    model=Biblioteca,
)

museu_detalhe = EquipamentoDetailView.as_view(
    model=Museu,
)


teatro_detalhe = EquipamentoDetailView.as_view(
    model=Teatro,
)


cinema_detalhe = EquipamentoDetailView.as_view(
    model=Cinema,
)


pontocultura_detalhe = EquipamentoDetailView.as_view(
    model=PontoCultura,
)

centrociencia_detalhe = EquipamentoDetailView.as_view(
    model=CentroCiencia,
)


salaverde_detalhe = EquipamentoDetailView.as_view(
    model=SalaVerde,
)


cras_detalhe = EquipamentoDetailView.as_view(
    model=CRAS,
)


saude_detalhe = EquipamentoDetailView.as_view(
    model=Saude,
)
