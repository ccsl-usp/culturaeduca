from django.conf.urls import include, url
import equipamento.views

urlpatterns = [
    url(r'^lista_escola.geojson$', equipamento.views.lista_escola, name='lista_escola'),
    url(r'^lista_biblioteca.geojson$', equipamento.views.lista_biblioteca, name='lista_biblioteca'),
    url(r'^lista_museu.geojson$', equipamento.views.lista_museu, name='lista_museu'),
    url(r'^lista_teatro.geojson$', equipamento.views.lista_teatro, name='lista_teatro'),
    url(r'^lista_cinema.geojson$', equipamento.views.lista_cinema, name='lista_cinema'),
    url(r'^lista_pontocultura.geojson$', equipamento.views.lista_pontocultura, name='lista_pontocultura'),
    url(r'^lista_centrociencia.geojson$', equipamento.views.lista_centrociencia, name='lista_centrociencia'),
    url(r'^lista_salaverde.geojson$', equipamento.views.lista_salaverde, name='lista_salaverde'),
    url(r'^lista_cras.geojson$', equipamento.views.lista_cras, name='lista_cras'),
    url(r'^lista_saude.geojson$', equipamento.views.lista_saude, name='lista_saude'),

    url(r'^escola_detalhe/(?P<pk>\d+)/$', equipamento.views.escola_detalhe, name='escola_detalhe'),
    url(r'^biblioteca_detalhe/(?P<pk>\d+)/$', equipamento.views.biblioteca_detalhe, name='biblioteca_detalhe'),
    url(r'^museu_detalhe/(?P<pk>\d+)/$', equipamento.views.museu_detalhe, name='museu_detalhe'),
    url(r'^teatro_detalhe/(?P<pk>\d+)/$', equipamento.views.teatro_detalhe, name='teatro_detalhe'),
    url(r'^cinema_detalhe/(?P<pk>\d+)/$', equipamento.views.cinema_detalhe, name='cinema_detalhe'),
    url(r'^pontocultura_detalhe/(?P<pk>\d+)/$', equipamento.views.pontocultura_detalhe, name='pontocultura_detalhe'),
    url(r'^centrociencia_detalhe/(?P<pk>\d+)/$', equipamento.views.centrociencia_detalhe, name='centrociencia_detalhe'),
    url(r'^salaverde_detalhe/(?P<pk>\d+)/$', equipamento.views.salaverde_detalhe, name='salaverde_detalhe'),
    url(r'^cras_detalhe/(?P<pk>\d+)/$', equipamento.views.cras_detalhe, name='cras_detalhe'),
    url(r'^saude_detalhe/(?P<pk>\d+)/$', equipamento.views.saude_detalhe, name='saude_detalhe'),

    # url(r'^acoes/(?P<pk>[0-9]+)/$', 'mapa.views.acoes_detalhe', name='acoes_detalhe'),
    # url(r'^acoes_detalhe_geojson/(?P<pk>[0-9]+)/$', 'mapa.views.acoes_detalhe_geojson', name='acoes_detalhe_geojson'),
]
