# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Escola',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('v001', models.IntegerField(verbose_name='C\xf3digo INEP da Escola')),
                ('v002', models.FloatField(verbose_name='Longitude da localiza\xe7\xe3o do equipamento')),
                ('v003', models.FloatField(verbose_name='Latitude da localiza\xe7\xe3o do equipamento')),
                ('v004', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento')),
                ('v005', models.CharField(max_length=254, verbose_name='C\xf3digo da UF conforme IBGE')),
                ('v006', models.IntegerField(verbose_name='C\xf3digo IBGE do Munic\xedpio')),
                ('v007', models.CharField(max_length=254, verbose_name='Nome do Munic\xedpio')),
                ('v008', models.CharField(max_length=254, verbose_name='Nome da Escola')),
                ('v009', models.CharField(max_length=254, null=True, verbose_name='Endere\xe7o Original')),
                ('v010', models.CharField(max_length=254, null=True, verbose_name='CEP Original')),
                ('v011', models.CharField(max_length=254, null=True, verbose_name='Tipo do Logradouro')),
                ('v012', models.CharField(max_length=254, null=True, verbose_name='Nome do Logradouro')),
                ('v013', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Logradouro')),
                ('v014', models.CharField(max_length=254, null=True, verbose_name='KM do Logradouro')),
                ('v015', models.CharField(max_length=254, null=True, verbose_name='Complemento')),
                ('v016', models.CharField(max_length=254, null=True, verbose_name='Bairro')),
                ('v017', models.FloatField(verbose_name='C\xf3digo do Endere\xe7amento Postal - CEP')),
                ('v018', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD')),
                ('v019', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone')),
                ('v020', models.CharField(max_length=254, null=True, verbose_name='Email da Escola')),
                ('v021', models.IntegerField(verbose_name='Nome do Diretor da Escola em 2014')),
                ('v022', models.IntegerField(verbose_name='Nome do Coordenador Programa Mais Educa\xe7\xe3o em 2014')),
                ('v023', models.CharField(max_length=254, null=True, verbose_name='Email do Coordenador Programa Mais Educa\xe7\xe3o em 2014')),
                ('v024', models.CharField(max_length=254, null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava')),
                ('v025', models.CharField(max_length=254, null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa')),
                ('v026', models.IntegerField(verbose_name='Programa Mais Cultura nas Escolas')),
                ('v027', models.IntegerField(verbose_name='Programa Ensino M\xe9dio Inovador')),
                ('v028', models.IntegerField(verbose_name='Programa Escola Aberta')),
                ('v029', models.IntegerField(verbose_name='Programa Brasil Alfabetizador')),
                ('v030', models.IntegerField(verbose_name='Programa Sa\xfade nas Escolas')),
                ('v031', models.IntegerField(verbose_name='N\xfamero de Alunos do Programa Sa\xfade nas Escolas 2012')),
                ('v032', models.IntegerField(verbose_name='Plano Brasil Sem Mis\xe9ria')),
                ('v033', models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2008')),
                ('v034', models.IntegerField(verbose_name='N\xfamero de Alunos do Programa Mais Educa\xe7\xe3o em 2008')),
                ('v035', models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2009')),
                ('v036', models.IntegerField(verbose_name='N\xfamero de Alunos do Programa Mais Educa\xe7\xe3o em 2009')),
                ('v037', models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2010')),
                ('v038', models.IntegerField(verbose_name='N\xfamero de Alunos do Programa Mais Educa\xe7\xe3o em 2010')),
                ('v039', models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2011')),
                ('v040', models.IntegerField(verbose_name='N\xfamero de Alunos do Programa Mais Educa\xe7\xe3o em 2011')),
                ('v041', models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2012')),
                ('v042', models.IntegerField(verbose_name='N\xfamero de Alunos do Programa Mais Educa\xe7\xe3o em 2012')),
                ('v043', models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2014')),
                ('v044', models.IntegerField(verbose_name='N\xfamero de Alunos do Programa Mais Educa\xe7\xe3o em 2014')),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4674)),
            ],
        ),
    ]
