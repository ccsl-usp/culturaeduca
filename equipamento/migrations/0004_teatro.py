# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0003_museu'),
    ]

    operations = [
        migrations.CreateModel(
            name='Teatro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('v001', models.IntegerField(verbose_name='C\xf3digo do equipamento')),
                ('v002', models.FloatField(verbose_name='Longitude da localiza\xe7\xe3o do equipamento')),
                ('v003', models.FloatField(verbose_name='Latitude da localiza\xe7\xe3o do equipamento')),
                ('v004', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento')),
                ('v005', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo da UF conforme IBGE')),
                ('v006', models.IntegerField(verbose_name='C\xf3digo IBGE do Munic\xedpio')),
                ('v007', models.CharField(max_length=254, null=True, verbose_name='Nome do Munic\xedpio')),
                ('v008', models.CharField(max_length=254, verbose_name='Nome do Estabelecimento')),
                ('v009', models.CharField(max_length=254, null=True, verbose_name='Endere\xe7o conforme base de dados original')),
                ('v010', models.CharField(max_length=254, null=True, verbose_name='CEP conforme base de dados original')),
                ('v011', models.CharField(max_length=254, null=True, verbose_name='Tipo do Logradouro')),
                ('v012', models.CharField(max_length=254, null=True, verbose_name='Nome do Logradouro')),
                ('v013', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do equipamento no Logradouro')),
                ('v014', models.CharField(max_length=254, null=True, verbose_name='KM em que se localiza o equipamento')),
                ('v015', models.CharField(max_length=254, null=True, verbose_name='Complemento do endere\xe7o')),
                ('v016', models.CharField(max_length=254, null=True, verbose_name='Nome do bairro onde se localiza o equipamento')),
                ('v017', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo do Endere\xe7amento Postal \u2013 CEP')),
                ('v018', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD')),
                ('v019', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone')),
                ('v020', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone2')),
                ('v021', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Fax')),
                ('v022', models.CharField(max_length=254, null=True, verbose_name='Email do equipamento')),
                ('v023', models.CharField(max_length=254, null=True, verbose_name='Site do equipamento')),
                ('v024', models.CharField(max_length=254, null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava')),
                ('v025', models.CharField(max_length=254, null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa')),
                ('v026', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do CNPJ do Estabelecimento')),
                ('v027', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo da Tipologia SNIIC')),
                ('v028', models.IntegerField(verbose_name='C\xf3digo Original')),
                ('v029', models.IntegerField(verbose_name='Situa\xe7\xe3o do Teatro')),
                ('v030', models.CharField(max_length=254, null=True, verbose_name='Tipo do Teatro')),
                ('v031', models.CharField(max_length=254, null=True, verbose_name='Quantidade de Lugares')),
                ('v032', models.CharField(max_length=254, null=True, verbose_name='Ano da Inaugura\xe7\xe3o')),
                ('v033', models.CharField(max_length=254, null=True, verbose_name='Ano do desaparecimento')),
                ('v034', models.CharField(max_length=254, null=True, verbose_name='Data da informa\xe7\xe3o')),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4674)),
            ],
        ),
    ]
