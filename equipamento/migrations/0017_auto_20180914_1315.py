# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0016_auto_20170125_1636'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='escola',
            name='raw_csv',
        ),
        migrations.AddField(
            model_name='escola',
            name='logradouro',
            field=models.CharField(max_length=100, null=True, verbose_name='Logradouro'),
        ),
        migrations.AddField(
            model_name='escola',
            name='num_fax',
            field=models.CharField(max_length=15, null=True, verbose_name='N\xfamero do Fax'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='desc_endereco',
            field=models.CharField(max_length=100, null=True, verbose_name='Nome do Logradouro'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='desc_endereco_bairro',
            field=models.CharField(max_length=80, null=True, verbose_name='Bairro'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='desc_endereco_complemento',
            field=models.CharField(max_length=80, null=True, verbose_name='Complemento'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='no_email',
            field=models.CharField(max_length=80, null=True, verbose_name='Email da Escola'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='no_entidade',
            field=models.CharField(max_length=100, verbose_name='Nome da Escola'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='no_municipio_lower',
            field=models.CharField(max_length=80, verbose_name='Nome do Munic\xedpio'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='num_cep',
            field=models.CharField(max_length=8, null=True, verbose_name='CEP Original'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='num_ddd',
            field=models.CharField(max_length=2, null=True, verbose_name='N\xfamero do DDD'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='num_endereco',
            field=models.CharField(max_length=10, null=True, verbose_name='N\xfamero do Logradouro'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='num_telefone',
            field=models.CharField(max_length=15, null=True, verbose_name='N\xfamero do Telefone'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='tipo',
            field=models.CharField(max_length=15, null=True, verbose_name='Tipo do Logradouro'),
        ),
    ]
