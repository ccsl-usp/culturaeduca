# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0018_auto_20211026_2013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='escola',
            name='cep',
            field=models.CharField(max_length=9, null=True, verbose_name='CEP Original'),
        ),
    ]
