# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0012_auto_20160718_1243'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='salaverde',
            options={'verbose_name': 'Sala Verde', 'verbose_name_plural': 'Salas Verde'},
        ),
    ]
