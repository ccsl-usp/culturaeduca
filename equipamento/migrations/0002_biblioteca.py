# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Biblioteca',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('v001', models.IntegerField(verbose_name='C\xf3digo do equipamento')),
                ('v002', models.FloatField(verbose_name='Longitude da localiza\xe7\xe3o do equipamento')),
                ('v003', models.FloatField(verbose_name='Latitude da localiza\xe7\xe3o do equipamento')),
                ('v004', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento')),
                ('v005', models.CharField(max_length=254, verbose_name='C\xf3digo da UF conforme IBGE')),
                ('v006', models.IntegerField(verbose_name='C\xf3digo IBGE do Munic\xedpio')),
                ('v007', models.CharField(max_length=254, verbose_name='Nome do Munic\xedpio')),
                ('v008', models.CharField(max_length=254, verbose_name='Denomina\xe7\xe3o do equipamento')),
                ('v009', models.CharField(max_length=254, null=True, verbose_name='Endere\xe7o conforme base de dados original')),
                ('v010', models.CharField(max_length=254, null=True, verbose_name='CEP conforme base de dados original')),
                ('v011', models.CharField(max_length=254, null=True, verbose_name='Tipo do Logradouro')),
                ('v012', models.CharField(max_length=254, null=True, verbose_name='Nome do Logradouro')),
                ('v013', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do equipamento no Logradouro')),
                ('v014', models.CharField(max_length=254, null=True, verbose_name='Complemento do endere\xe7o')),
                ('v015', models.CharField(max_length=254, null=True, verbose_name='Nome do bairro onde se localiza o equipamento')),
                ('v016', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo do Endere\xe7amento Postal - CEP')),
                ('v017', models.CharField(max_length=254, null=True, verbose_name='Nome do Respons\xe1vel')),
                ('v018', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD')),
                ('v019', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone1')),
                ('v020', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD do Fax')),
                ('v021', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Fax')),
                ('v022', models.CharField(max_length=254, null=True, verbose_name='Email do equipamento')),
                ('v023', models.CharField(max_length=254, null=True, verbose_name='Site do equipamento')),
                ('v024', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava')),
                ('v025', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa')),
                ('v026', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do CNPJ do Estabelecimento')),
                ('v027', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo da Tipologia SNIIC')),
                ('v028', models.IntegerField(verbose_name='C\xf3digo do Censo de bibliotecas da FGV')),
                ('v029', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo no Sistema Nacional de Bibliotecas P\xfablicas')),
                ('v030', models.IntegerField(verbose_name='Dia de Funcionamento - Segunda-Feira')),
                ('v031', models.IntegerField(verbose_name='Dia de Funcionamento - Ter\xe7a-Feira')),
                ('v032', models.IntegerField(verbose_name='Dia de Funcionamento - Quarta-Feira')),
                ('v033', models.IntegerField(verbose_name='Dia de Funcionamento - Quinta-Feira')),
                ('v034', models.IntegerField(verbose_name='Dia de Funcionamento - Sexta-Feira')),
                ('v035', models.IntegerField(verbose_name='Dia de Funcionamento - S\xe1bado')),
                ('v036', models.IntegerField(verbose_name='Dia de Funcionamento - Domingo')),
                ('v037', models.IntegerField(verbose_name='Per\xedodo de Funcionamento - Manh\xe3')),
                ('v038', models.IntegerField(verbose_name='Per\xedodo de Funcionamento - Tarde')),
                ('v039', models.IntegerField(verbose_name='Per\xedodo de Funcionamento - Noite')),
                ('v040', models.IntegerField(verbose_name='Acesso de Internet para Usu\xe1rios')),
                ('v041', models.IntegerField(null=True, verbose_name='Quantidade de empr\xe9stimos')),
                ('v042', models.IntegerField(null=True, verbose_name='Quantidade de livros')),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4674)),
            ],
        ),
    ]
