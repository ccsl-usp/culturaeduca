# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0005_auto_20160714_1040'),
    ]

    operations = [
        migrations.CreateModel(
            name='PontoCultura',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('v001', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo do equipamento')),
                ('v002', models.FloatField(verbose_name='Longitude da localiza\xe7\xe3o do equipamento')),
                ('v003', models.FloatField(verbose_name='Latitude da localiza\xe7\xe3o do equipamento')),
                ('v004', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento')),
                ('v005', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo da UF conforme IBGE')),
                ('v006', models.IntegerField(verbose_name='C\xf3digo IBGE do Munic\xedpio')),
                ('v007', models.CharField(max_length=254, null=True, verbose_name='Nome do Munic\xedpio')),
                ('v008', models.CharField(max_length=254, null=True, verbose_name='Nome da entidade proponente')),
                ('v009', models.CharField(max_length=254, verbose_name='Denomina\xe7\xe3o do Ponto de Cultura')),
                ('v010', models.CharField(max_length=254, null=True, verbose_name='Endere\xe7o conforme base de dados original')),
                ('v011', models.CharField(max_length=254, null=True, verbose_name='Tipo do Logradouro')),
                ('v012', models.CharField(max_length=254, null=True, verbose_name='Nome do Logradouro')),
                ('v013', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do equipamento no Logradouro')),
                ('v014', models.CharField(max_length=254, null=True, verbose_name='KM em que se localiza o equipamento')),
                ('v015', models.CharField(max_length=254, null=True, verbose_name='Complemento do endere\xe7o')),
                ('v016', models.CharField(max_length=254, null=True, verbose_name='Nome do bairro onde se localiza o equipamento')),
                ('v017', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo do Endere\xe7amento Postal - CEP')),
                ('v018', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Edital de Sele\xe7\xe3o')),
                ('v019', models.CharField(max_length=254, null=True, verbose_name='Tipo de instrumento')),
                ('v020', models.CharField(max_length=254, null=True, verbose_name='Tipo do Ponto de Cultura')),
                ('v021', models.CharField(max_length=254, null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa')),
                ('v022', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do CNPJ da Entidade Proponente')),
                ('v023', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo PRONAC')),
                ('v024', models.FloatField(null=True, verbose_name='C\xf3digo SALIC')),
                ('v025', models.FloatField(null=True, verbose_name='C\xf3digo Secretaria de Cidadania e Diversidade Cultural - SCDC/MinC')),
                ('v026', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD1')),
                ('v027', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone1')),
                ('v028', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD2')),
                ('v029', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone2')),
                ('v030', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD3')),
                ('v031', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone3')),
                ('v032', models.CharField(max_length=254, null=True, verbose_name='Site do Ponto de Cultura ou da Entidade Proponente ')),
                ('v033', models.CharField(max_length=254, null=True, verbose_name='Nome do respons\xe1vel do Ponto de Cultura ou da Entidade Proponente ')),
                ('v034', models.CharField(max_length=254, null=True, verbose_name='Email do respons\xe1vel do Ponto de Cultura ou da Entidade Proponente ')),
                ('v035', models.CharField(max_length=254, null=True, verbose_name='Email 1 do Ponto de Cultura ou da Entidade Proponente ')),
                ('v036', models.CharField(max_length=254, null=True, verbose_name='Email 1 do Ponto de Cultura ou da Entidade Proponente ')),
                ('v037', models.CharField(max_length=254, null=True, verbose_name='Email 3 do Ponto de Cultura ou da Entidade Proponente ')),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4674)),
            ],
        ),
    ]
