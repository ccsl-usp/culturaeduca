# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0015_auto_20170116_1723'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='biblioteca',
            name='id',
        ),
        migrations.RemoveField(
            model_name='cras',
            name='id',
        ),
        migrations.RemoveField(
            model_name='museu',
            name='id',
        ),
        migrations.RemoveField(
            model_name='teatro',
            name='id',
        ),
        migrations.AlterField(
            model_name='biblioteca',
            name='v001',
            field=models.BigIntegerField(serialize=False, verbose_name='C\xf3digo do equipamento', primary_key=True),
        ),
        migrations.AlterField(
            model_name='cras',
            name='v001',
            field=models.BigIntegerField(serialize=False, verbose_name='C\xf3digo do Equipamento', primary_key=True),
        ),
        migrations.AlterField(
            model_name='escola',
            name='pk_cod_entidade',
            field=models.BigIntegerField(serialize=False, verbose_name='C\xf3digo INEP da Escola', primary_key=True),
        ),
        migrations.AlterField(
            model_name='museu',
            name='v001',
            field=models.BigIntegerField(serialize=False, verbose_name='C\xf3digo do equipamento', primary_key=True),
        ),
        migrations.AlterField(
            model_name='saude',
            name='v001',
            field=models.BigIntegerField(verbose_name='C\xf3digo no Cadastro Nacional de Estabelecimentos de Sa\xfade'),
        ),
        migrations.AlterField(
            model_name='teatro',
            name='v001',
            field=models.BigIntegerField(serialize=False, verbose_name='C\xf3digo do equipamento', primary_key=True),
        ),
    ]
