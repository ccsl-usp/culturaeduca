# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('ibge', '0001_initial'),
        ('equipamento', '0013_auto_20160801_1159'),
    ]

    operations = [
        migrations.CreateModel(
            name='EscolaNovo',
            fields=[
                ('raw_csv', models.CharField(max_length=512, verbose_name='Linha CSV do arquivo de importa\xe7\xe3o')),
                ('pk_cod_entidade', models.IntegerField(serialize=False, verbose_name='C\xf3digo INEP da Escola', primary_key=True)),
                ('id_longitude', models.FloatField(verbose_name='Longitude da localiza\xe7\xe3o do equipamento')),
                ('id_latitude', models.FloatField(verbose_name='Latitude da localiza\xe7\xe3o do equipamento')),
                ('tipo_geo', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')])),
                ('sigla', models.CharField(max_length=2, verbose_name='C\xf3digo da UF conforme IBGE')),
                ('pk_cod_estado', models.IntegerField(verbose_name='pk_cod_estado')),
                ('no_municipio_lower', models.CharField(max_length=254, verbose_name='Nome do Munic\xedpio')),
                ('no_entidade', models.CharField(max_length=254, verbose_name='Nome da Escola')),
                ('desc_endereco', models.CharField(max_length=254, null=True, verbose_name='Nome do Logradouro')),
                ('tipo', models.CharField(max_length=254, null=True, verbose_name='Tipo do Logradouro')),
                ('num_endereco', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Logradouro')),
                ('desc_endereco_complemento', models.CharField(max_length=254, null=True, verbose_name='Complemento')),
                ('desc_endereco_bairro', models.CharField(max_length=254, null=True, verbose_name='Bairro')),
                ('num_cep', models.CharField(max_length=254, null=True, verbose_name='CEP Original')),
                ('num_ddd', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD')),
                ('num_telefone', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone')),
                ('no_email', models.CharField(max_length=254, null=True, verbose_name='Email da Escola')),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4674)),
                ('municipio', models.ForeignKey(to='ibge.Municipio', null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Escola',
        ),
    ]
