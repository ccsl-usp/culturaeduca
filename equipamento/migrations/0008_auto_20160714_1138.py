# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0007_centrociencia'),
    ]

    operations = [
        migrations.CreateModel(
            name='SalaVerde',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('v001', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo do equipamento')),
                ('v002', models.FloatField(verbose_name='Longitude da localiza\xe7\xe3o do equipamento')),
                ('v003', models.FloatField(verbose_name='Latitude da localiza\xe7\xe3o do equipamento')),
                ('v004', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento')),
                ('v005', models.CharField(max_length=254, verbose_name='C\xf3digo da UF conforme IBGE')),
                ('v006', models.IntegerField(verbose_name='C\xf3digo IBGE do Munic\xedpio')),
                ('v007', models.CharField(max_length=254, verbose_name='Nome do Munic\xedpio')),
                ('v008', models.CharField(max_length=254, verbose_name='Denomina\xe7\xe3o do equipamento')),
                ('v009', models.CharField(max_length=254, null=True, verbose_name='Endere\xe7o Original')),
                ('v010', models.CharField(max_length=254, null=True, verbose_name='C\xf3digo do Endere\xe7amento Postal - CEP')),
                ('v011', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD')),
                ('v012', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone1')),
                ('v013', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do DDD2')),
                ('v014', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone2')),
                ('v015', models.CharField(max_length=254, verbose_name='Nome do bairro onde se localiza o equipamento')),
                ('v016', models.CharField(max_length=254, null=True, verbose_name='Email do equipamento')),
                ('v017', models.CharField(max_length=254, null=True, verbose_name='Email2 do equipamento')),
                ('v018', models.CharField(max_length=254, null=True, verbose_name='Email3 do equipamento')),
                ('v019', models.CharField(max_length=254, null=True, verbose_name='Nome da Institui\xe7\xe3o')),
                ('v020', models.CharField(max_length=254, verbose_name='Informa\xe7\xf5es do Projeto')),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4674)),
            ],
        ),
        migrations.AlterField(
            model_name='centrociencia',
            name='v016',
            field=models.CharField(max_length=254, null=True, verbose_name='C\xf3digo do Endere\xe7amento Postal - CEP'),
        ),
    ]
