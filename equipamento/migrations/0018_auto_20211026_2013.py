# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0017_auto_20180914_1315'),
    ]

    operations = [
        migrations.RenameField(
            model_name='escola',
            old_name='num_cep',
            new_name='cep',
        ),
        migrations.RenameField(
            model_name='escola',
            old_name='desc_endereco_bairro',
            new_name='compl_bairro',
        ),
        migrations.RenameField(
            model_name='escola',
            old_name='no_email',
            new_name='email',
        ),
        migrations.RenameField(
            model_name='escola',
            old_name='id_latitude',
            new_name='latitude',
        ),
        migrations.RenameField(
            model_name='escola',
            old_name='id_longitude',
            new_name='longitude',
        ),
        migrations.RenameField(
            model_name='escola',
            old_name='no_municipio_lower',
            new_name='no_municipio',
        ),
        migrations.RenameField(
            model_name='escola',
            old_name='num_endereco',
            new_name='num',
        ),
        migrations.RenameField(
            model_name='escola',
            old_name='num_telefone',
            new_name='telefone',
        ),
        migrations.RenameField(
            model_name='escola',
            old_name='sigla',
            new_name='uf',
        ),
        migrations.RemoveField(
            model_name='escola',
            name='desc_endereco',
        ),
        migrations.RemoveField(
            model_name='escola',
            name='desc_endereco_complemento',
        ),
        migrations.RemoveField(
            model_name='escola',
            name='num_ddd',
        ),
        migrations.RemoveField(
            model_name='escola',
            name='num_fax',
        ),
        migrations.RemoveField(
            model_name='escola',
            name='pk_cod_estado',
        ),
        # migrations.RemoveField(
        #     model_name='escola',
        #     name='tipo',
        # ),
    ]
