# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0010_saude'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='centrociencia',
            options={'verbose_name': 'Centro de Ci\xeancia', 'verbose_name_plural': 'Centros de Ci\xeancias'},
        ),
        migrations.AlterModelOptions(
            name='cras',
            options={'verbose_name': 'CRAS', 'verbose_name_plural': 'CRAS'},
        ),
        migrations.AlterModelOptions(
            name='pontocultura',
            options={'verbose_name': 'Ponto de Cultura', 'verbose_name_plural': 'Pontos de Cultura'},
        ),
        migrations.AlterModelOptions(
            name='saude',
            options={'verbose_name': 'Sa\xfade', 'verbose_name_plural': 'Sa\xfade'},
        ),
        migrations.AlterField(
            model_name='biblioteca',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.AlterField(
            model_name='centrociencia',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.RunSQL('ALTER TABLE equipamento_centrociencia ALTER COLUMN v017 TYPE integer USING v017::integer'),
        #migrations.AlterField(
        #    model_name='centrociencia',
        #    name='v017',
        #    field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava', choices=[(1, 'P\xfablica'), (2, 'Privada')]),
        #),
        migrations.RunSQL('ALTER TABLE equipamento_centrociencia ALTER COLUMN v018 TYPE integer USING v018::integer'),
        #migrations.AlterField(
        #    model_name='centrociencia',
        #    name='v018',
        #    field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        #),
        migrations.AlterField(
            model_name='cinema',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.AlterField(
            model_name='cinema',
            name='v023',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrativa', choices=[(1, 'P\xfablica'), (2, 'Privada')]),
        ),
        migrations.RunSQL('ALTER TABLE equipamento_cinema ALTER COLUMN v024 TYPE integer USING v024::integer'),
        #migrations.AlterField(
        #    model_name='cinema',
        #    name='v024',
        #    field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        #),
        migrations.AlterField(
            model_name='cras',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v021',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava', choices=[(1, 'P\xfablica'), (2, 'Privada')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v022',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v023',
            field=models.CharField(max_length=254, null=True, verbose_name='Nome do Diretor da Escola em 2014'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v024',
            field=models.CharField(max_length=254, null=True, verbose_name='Nome do Coordenador Programa Mais Educa\xe7\xe3o em 2014'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v025',
            field=models.CharField(max_length=254, null=True, verbose_name='Email do Coordenador Programa Mais Educa\xe7\xe3o em 2014'),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v026',
            field=models.IntegerField(verbose_name='Programa Mais Cultura nas Escolas', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v027',
            field=models.IntegerField(verbose_name='Programa Ensino M\xe9dio Inovador', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v028',
            field=models.IntegerField(verbose_name='Programa Escola Aberta', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v029',
            field=models.IntegerField(verbose_name='Programa Brasil Alfabetizador', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v030',
            field=models.IntegerField(verbose_name='Programa Sa\xfade nas Escolas', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v032',
            field=models.IntegerField(verbose_name='Plano Brasil Sem Mis\xe9ria', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v033',
            field=models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2008', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v035',
            field=models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2009', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v037',
            field=models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2010', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v039',
            field=models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2011', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v041',
            field=models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2012', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='escola',
            name='v043',
            field=models.IntegerField(verbose_name='Aderiu ao Programa Mais Educa\xe7\xe3o em 2014', choices=[(0, 'N\xe3o'), (1, 'Sim')]),
        ),
        migrations.AlterField(
            model_name='museu',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.AlterField(
            model_name='museu',
            name='v023',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava', choices=[(1, 'P\xfablica'), (2, 'Privada')]),
        ),
        migrations.RunSQL('ALTER TABLE equipamento_museu ALTER COLUMN v024 TYPE integer USING v024::integer'),
        #migrations.AlterField(
        #    model_name='museu',
        #    name='v024',
        #    field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        #),
        migrations.AlterField(
            model_name='pontocultura',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.AlterField(
            model_name='pontocultura',
            name='v019',
            field=models.CharField(max_length=254, null=True, verbose_name='Tipo do Ponto de Cultura'),
        ),
        migrations.AlterField(
            model_name='pontocultura',
            name='v020',
            field=models.CharField(max_length=254, null=True, verbose_name='Tipo de instrumento'),
        ),
        migrations.AlterField(
            model_name='pontocultura',
            name='v021',
            field=models.CharField(max_length=254, null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        ),
        migrations.AlterField(
            model_name='salaverde',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.AlterField(
            model_name='saude',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.AlterField(
            model_name='saude',
            name='v019',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava', choices=[(1, 'P\xfablica'), (2, 'Privada')]),
        ),
        migrations.AlterField(
            model_name='saude',
            name='v020',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        ),
        migrations.AlterField(
            model_name='teatro',
            name='v004',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento', choices=[(0, 'Coordenada da Base Original'), (1, 'Geocodifica\xe7\xe3o Autom\xe1tica por Endere\xe7o'), (2, 'Geocodifica\xe7\xe3o Manual por Endere\xe7o'), (3, 'Coordenada da Sede Municipal')]),
        ),
        migrations.RunSQL('ALTER TABLE equipamento_teatro ALTER COLUMN v024 TYPE integer USING v024::integer'),
        #migrations.AlterField(
        #    model_name='teatro',
        #    name='v024',
        #    field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava', choices=[(1, 'P\xfablica'), (2, 'Privada')]),
        #),
        migrations.RunSQL('ALTER TABLE equipamento_teatro ALTER COLUMN v025 TYPE integer USING v025::integer'),
        # migrations.AlterField(
        #     model_name='teatro',
        #     name='v025',
        #     field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        # ),
        migrations.AlterField(
            model_name='teatro',
            name='v029',
            field=models.IntegerField(verbose_name='Situa\xe7\xe3o do Teatro', choices=[(1, 'Em atividade'), (2, 'Desativado'), (3, 'Em Recupera\xe7\xe3o'), (4, 'Em constru\xe7\xe3o'), (5, 'N\xe3o identificado')]),
        ),
    ]
