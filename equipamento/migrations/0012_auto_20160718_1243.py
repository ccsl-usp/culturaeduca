# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0011_auto_20160718_1119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='centrociencia',
            name='v017',
            field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava', choices=[(1, 'P\xfablica'), (2, 'Privada')]),
        ),
        migrations.AlterField(
            model_name='centrociencia',
            name='v018',
            field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        ),
        migrations.AlterField(
            model_name='cinema',
            name='v024',
            field=models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        ),
        migrations.AlterField(
            model_name='museu',
            name='v024',
            field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        ),
        migrations.AlterField(
            model_name='saude',
            name='v023',
            field=models.BigIntegerField(null=True, verbose_name='C\xf3digo para link com a ficha do estabelecimento no CNESNet'),
        ),
        migrations.AlterField(
            model_name='teatro',
            name='v024',
            field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava', choices=[(1, 'P\xfablica'), (2, 'Privada')]),
        ),
        migrations.AlterField(
            model_name='teatro',
            name='v025',
            field=models.IntegerField(null=True, verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Associa\xe7\xe3o'), (5, 'Empresa'), (6, 'Funda\xe7\xe3o'), (7, 'OSCIP'), (8, 'Particular'), (9, 'Religiosa'), (10, 'Mista'), (11, 'Entidade Sindical'), (12, 'Outra')]),
        ),
    ]
