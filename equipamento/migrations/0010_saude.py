# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipamento', '0009_cras'),
    ]

    operations = [
        migrations.CreateModel(
            name='Saude',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('v001', models.IntegerField(verbose_name='C\xf3digo no Cadastro Nacional de Estabelecimentos de Sa\xfade')),
                ('v002', models.FloatField(verbose_name='Longitude da localiza\xe7\xe3o do equipamento')),
                ('v003', models.FloatField(verbose_name='Latitude da localiza\xe7\xe3o do equipamento')),
                ('v004', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo do Georreferenciamento')),
                ('v005', models.CharField(max_length=254, verbose_name='C\xf3digo da UF conforme IBGE')),
                ('v006', models.IntegerField(verbose_name='C\xf3digo IBGE do Munic\xedpio')),
                ('v007', models.CharField(max_length=254, verbose_name='Nome do Munic\xedpio')),
                ('v008', models.CharField(max_length=254, verbose_name='Denomina\xe7\xe3o do equipamento')),
                ('v009', models.CharField(max_length=254, verbose_name='Nome do Estabelecimento')),
                ('v010', models.CharField(max_length=254, null=True, verbose_name='Endere\xe7o Original')),
                ('v011', models.CharField(max_length=254, null=True, verbose_name='Tipo do Logradouro')),
                ('v012', models.CharField(max_length=254, null=True, verbose_name='Nome do Logradouro')),
                ('v013', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Logradouro')),
                ('v014', models.CharField(max_length=254, null=True, verbose_name='KM do Logradouro')),
                ('v015', models.CharField(max_length=254, null=True, verbose_name='Complemento')),
                ('v016', models.CharField(max_length=254, null=True, verbose_name='Bairro')),
                ('v017', models.IntegerField(null=True, verbose_name='C\xf3digo do Endere\xe7amento Postal - CEP')),
                ('v018', models.CharField(max_length=254, null=True, verbose_name='N\xfamero do Telefone')),
                ('v019', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica da Esfera Administrava')),
                ('v020', models.IntegerField(verbose_name='Identifica\xe7\xe3o num\xe9rica do Tipo da Esfera Administrativa')),
                ('v021', models.IntegerField(verbose_name='Tipo do Estabelecimento')),
                ('v022', models.CharField(max_length=254, verbose_name='Descri\xe7\xe3o do Tipo do Estabelecimento')),
                ('v023', models.FloatField(null=True, verbose_name='C\xf3digo para link com a ficha do estabelecimento no CNESNet')),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4674)),
            ],
        ),
    ]
