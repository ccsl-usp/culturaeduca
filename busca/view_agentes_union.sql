CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP MATERIALIZED VIEW IF EXISTS view_agentes_union;
DROP VIEW IF EXISTS view_agentes_union;
CREATE VIEW view_agentes_union AS (
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, core_user.id::VARCHAR AS id, core_user.nome AS nome, core_profile.municipio_id AS municipio_id, 0 AS tipo FROM core_profile, core_user WHERE core_profile.user_id = core_user.id) AS pessoa_fisica
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, agente_agente.id::VARCHAR AS id, agente_agente.nome AS nome, agente_agente.municipio_id AS municipio_id, 1 AS tipo FROM agente_coletivo, agente_agente WHERE agente_coletivo.agente_ptr_id = agente_agente.id) AS coletivo
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, agente_agente.id::VARCHAR AS id, agente_agente.nome AS nome, agente_agente.municipio_id AS municipio_id, 2 AS tipo FROM agente_instituicao, agente_agente WHERE agente_instituicao.agente_ptr_id = agente_agente.id) AS instituicao
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, agente_agente.id::VARCHAR AS id, agente_agente.nome AS nome, agente_agente.municipio_id AS municipio_id, 3 AS tipo FROM agente_conselho, agente_agente WHERE agente_conselho.agente_ptr_id = agente_agente.id) AS conselho
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, agente_agente.id::VARCHAR AS id, agente_agente.nome AS nome, agente_agente.municipio_id AS municipio_id, 4 AS tipo FROM agente_movimentosocial, agente_agente WHERE agente_movimentosocial.agente_ptr_id = agente_agente.id) AS movimento_social
);