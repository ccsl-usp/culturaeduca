# coding: utf-8
from django.db import models
from municipios.models import Municipio
from django.core.urlresolvers import reverse

class Busca(models.Model):

    class EQUIPAMENTOS_TIPO(object):
        CRAS = 0
        CENTROSCIENCIA = 1
        BIBLIOTECAS = 2
        MUSEUS = 3
        CINEMAS = 4
        TEATROS = 5
        PONTOSCULTURA = 6
        ESCOLAS = 7
        SALASVERDES = 8
        UNIDADESSAUDE = 9

    class AGENTES_TIPO(object):
        PESSOASFISICAS = 0
        COLETIVOS = 1
        INSTITUICOES = 2
        CONSELHOS = 3
        MOVIMENTOSSOCIAIS = 4

    class ACOES_TIPO(object):
        PROJETOS = 0
        ATIVIDADES = 1

    class DIVISOES_URBANAS_TIPO(object):
        ESTADOS = 0
        MUNICIPIOS = 1

    EQUIPAMENTOS = (
        (0, u'CRAS'),
        (1, u'Centros de Ciência'),
        (2, u'Bibliotecas'),
        (3, u'Museus'),
        (4, u'Cinemas'),
        (5, u'Teatros'),
        (6, u'Pontos de Cultura'),
        (7, u'Escolas'),
        (8, u'Salas Verdes'),
        (9, u'Unidades de Saúde'),
    )

    AGENTES = (
        (0, u'Pessoas Físicas'),
        (1, u'Coletivos'),
        (2, u'Instituições'),
        (3, u'Conselhos'),
        (4, u'Movimentos Sociais'),
    )

    ACOES = (
        (0, u'Projetos'),
        (1, u'Atividades'),
    )

    DIVISOES_URBANAS = (
        (0, u'Estados'),
        (1, u'Municípios'),
    )

    equipamentos = models.CharField(
        max_length=254, choices=EQUIPAMENTOS, default=None)
    agentes = models.CharField(max_length=254, choices=AGENTES, default=None)
    acoes = models.CharField(max_length=254, choices=ACOES, default=None)
    divisoes_urbanas = models.CharField(
        max_length=254, choices=DIVISOES_URBANAS, default=None)
    municipio = models.ForeignKey(Municipio)
    nome = models.CharField(max_length=254)


class EquipamentosUnion(models.Model):
    EQUIPAMENTOS = (
        (0, u'CRAS'),
        (1, u'Centro de Ciência'),
        (2, u'Biblioteca'),
        (3, u'Museu'),
        (4, u'Cinema'),
        (5, u'Teatro'),
        (6, u'Ponto de Cultura'),
        (7, u'Escola'),
        (8, u'Sala Verde'),
        (9, u'Saúde'),
    )

    URL_DETALHE = (
        (0, u'cras_detalhe'),
        (1, u'centrociencia_detalhe'),
        (2, u'biblioteca_detalhe'),
        (3, u'museu_detalhe'),
        (4, u'cinema_detalhe'),
        (5, u'teatro_detalhe'),
        (6, u'pontocultura_detalhe'),
        (7, u'escola_detalhe'),
        (8, u'salaverde_detalhe'),
        (9, u'saude_detalhe'),
    )

    uid = models.UUIDField(primary_key=True, editable=False)
    id = models.CharField(max_length=254, editable=False)
    nome = models.CharField(max_length=254, editable=False)
    tipo = models.CharField(
        max_length=254, choices=EQUIPAMENTOS, editable=False)
    municipio = models.ForeignKey(Municipio)

    def __str__(self):
        return self.nome or ''

    def get_absolute_url(self):
        return reverse(self.URL_DETALHE[self.tipo][1], args=[self.id])

    @property
    def tipo_texto(self):
        return self.EQUIPAMENTOS[self.tipo][1]

    @property
    def target_url(self):
        return self.get_absolute_url()

    class Meta:
        managed = False
        db_table = 'view_equipamentos_union'

class AgentesUnion(models.Model):
    AGENTES = (
        (0, u'Pessoa Física'),
        (1, u'Coletivo'),
        (2, u'Instituição'),
        (3, u'Conselho'),
        (4, u'Movimento Social'),
    )

    URL_DETALHE = (
        (0, u'profile_detail'),
        (1, u'coletivo_detail'),
        (2, u'instituicao_detail'),
        (3, u'conselho_detail'),
        (4, u'movimentosocial_detail'),
    )

    uid = models.UUIDField(primary_key=True, editable=False)
    id = models.CharField(max_length=254, editable=False)
    nome = models.CharField(max_length=254, editable=False)
    tipo = models.CharField(
        max_length=254, choices=AGENTES, editable=False)
    municipio = models.ForeignKey(Municipio)

    def __str__(self):
        return self.nome or ''

    def get_absolute_url(self):
        return reverse(self.URL_DETALHE[self.tipo][1], args=[self.id])

    @property
    def tipo_texto(self):
        return self.AGENTES[self.tipo][1]

    @property
    def target_url(self):
        return self.get_absolute_url()
    
    class Meta:
        managed = False
        db_table = 'view_agentes_union'

class AcoesUnion(models.Model):
    ACOES = (
        (0, u'Projeto'),
        (1, u'Atividade'),
    )

    URL_DETALHE = (
        (0, u'projeto_detail'),
        (1, u'atividade_detail'),
    )

    uid = models.UUIDField(primary_key=True, editable=False)
    id = models.CharField(max_length=254, editable=False)
    nome = models.CharField(max_length=254, editable=False)
    tipo = models.CharField(
        max_length=254, choices=ACOES, editable=False)

    def __str__(self):
        return self.nome or ''

    def get_absolute_url(self):
        return reverse(self.URL_DETALHE[self.tipo][1], args=[self.id])

    @property
    def tipo_texto(self):
        return self.ACOES[self.tipo][1]

    @property
    def target_url(self):
        return self.get_absolute_url()
    
    class Meta:
        managed = False
        db_table = 'view_acoes_union'

class DivisoesUrbanasUnion(models.Model):
    DIVISOES_URBANAS = (
        (0, u'Estado'),
        (1, u'Municipio'),
    )

    URL_DETALHE = (
        (0, u'uf_detail'),
        (1, u'municipio_detail'),
    )

    uid = models.UUIDField(primary_key=True, editable=False)
    id = models.CharField(max_length=254, editable=False)
    nome = models.CharField(max_length=254, editable=False)
    tipo = models.CharField(
        max_length=254, choices=DIVISOES_URBANAS, editable=False)
    uf = models.CharField(max_length=2, editable=False)

    def __str__(self):
        return self.nome or ''

    def get_absolute_url(self):
        if self.tipo==0:
            return reverse(self.URL_DETALHE[self.tipo][1], args=[self.uf])
        else:
            return reverse(self.URL_DETALHE[self.tipo][1], args=[self.uf, self.nome, self.id])

    @property
    def tipo_texto(self):
        return self.DIVISOES_URBANAS[self.tipo][1]

    @property
    def target_url(self):
        return self.get_absolute_url()

    class Meta:
        managed = False
        db_table = 'view_divisoesurbanas_union'