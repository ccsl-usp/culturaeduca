# coding: utf-8
from django import forms
from django_select2.forms import ModelSelect2Widget
from municipios.models import Municipio

from .models import (
    Busca
)


class UnicodeModelSelect2Widget(ModelSelect2Widget):
    def label_from_instance(self, obj):
        return unicode(obj)


class BuscaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(BuscaForm, self).__init__(*args, **kwargs)

        # add placeholder for select
        self.fields['nome'].widget.attrs.update({
            'placeholder': u'Ex.: Mariana',
        })
        self.fields['municipio'].widget.attrs.update({
            'data-minimum-input-length': 3,
            'data-placeholder': u'Ex.: Belo Horizonte - MG',
        })
        self.fields['nome'].required = False
        self.fields['municipio'].required = False

    class Meta:
        model = Busca
        fields = (u'divisoes_urbanas', u'equipamentos',
                  u'agentes', u'acoes', u'municipio', u'nome',)
        widgets = {
            'divisoes_urbanas': forms.CheckboxSelectMultiple(),
            'equipamentos': forms.CheckboxSelectMultiple(),
            'agentes': forms.CheckboxSelectMultiple(),
            'acoes': forms.CheckboxSelectMultiple(),
            'municipio': UnicodeModelSelect2Widget(model=Municipio, search_fields=['nome__unaccent__icontains']),
            'nome': forms.TextInput(),
        }
