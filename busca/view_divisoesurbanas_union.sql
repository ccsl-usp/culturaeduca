CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP MATERIALIZED VIEW IF EXISTS view_divisoesurbanas_union;
DROP VIEW IF EXISTS view_divisoesurbanas_union;
CREATE MATERIALIZED VIEW view_divisoesurbanas_union AS (
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, cd_geocuf::VARCHAR AS id, nm_estado as nome, uf, 0 AS tipo FROM ibge_uf) AS uf
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, cd_geocodm::VARCHAR AS id, nm_municip as nome, uf_sigla as uf, 1 AS tipo FROM ibge_municipio) AS municipio
);
