from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^equipamento$', views.equipamento_list, name='equipamento_list'),
    url(r'^agente$', views.agente_list, name='agente_list'),
    url(r'^acao$', views.acao_list, name='acao_list'),
    url(r'^divisao-urbana$', views.divisaourbana_list, name='divisaourbana_list'),
]