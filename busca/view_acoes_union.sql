CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP MATERIALIZED VIEW IF EXISTS view_acoes_union;
DROP VIEW IF EXISTS view_acoes_union;
CREATE VIEW view_acoes_union AS (
    SELECT * FROM (SELECT uuid_generate_v4() AS uid, acao_acao.id::VARCHAR AS id, acao_acao.nome AS nome, 0 AS tipo FROM acao_projeto, acao_acao WHERE acao_projeto.acao_ptr_id = acao_acao.id) AS projeto
    UNION
    SELECT * FROM (SELECT uuid_generate_v4() AS uid, acao_acao.id::VARCHAR AS id, acao_acao.nome AS nome, 1 AS tipo FROM acao_atividade, acao_acao WHERE acao_atividade.acao_ptr_id = acao_acao.id) AS atividade
);