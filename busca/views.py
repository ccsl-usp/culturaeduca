# coding: utf-8
from django.views.generic import ListView

from .models import (
    EquipamentosUnion,
    AgentesUnion,
    AcoesUnion,
    DivisoesUrbanasUnion,
)

from axis.acao.models import Local

from .forms import BuscaForm
from django.db.models import Q
import operator

class EquipamentoView(ListView):
    template_name = 'busca/equipamento_listagem.html'
    paginate_by = 10

    def get_form_kwargs(self):
        kwargs = {}
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super(EquipamentoView, self).get_context_data(
            *args, **kwargs)

        nome = self.request.GET.get('nome') or ''
        municipio = self.request.GET.get('municipio') or ''
        equipamentos = self.request.GET.getlist('equipamentos') or []

        data = {
            'nome': nome,
            'municipio': municipio,
            'equipamentos': equipamentos,
        }

        context['data'] = {
            'nome': nome,
            'municipio': municipio,
            'tipos': equipamentos,
        }

        self.form = BuscaForm(initial=data, **self.get_form_kwargs())
        context['form'] = self.form

        # add parameters to url
        context['page_url'] = ''

        url_single_params = ['nome', 'municipio']
        for param in url_single_params:
            if data[param]:
                if context['page_url']:
                    context['page_url'] += '&'
                context['page_url'] += param + '=' + data[param]

        url_multiple_params = ['equipamentos']
        for param in url_multiple_params:
            if data[param]:
                for param_item in data[param]:
                    if context['page_url']:
                        context['page_url'] += '&'
                    context['page_url'] += param + '=' + param_item

        if context['page_url']:
            context['page_url'] += '&'

        return context

    def get_queryset(self):
        nome_field = self.request.GET.get('nome') or ''
        municipio_field = self.request.GET.get('municipio') or ''
        nome_list = nome_field.split()

        equipamentos = self.request.GET.getlist('equipamentos') or []
        tipos = [int(equipamento) for equipamento in equipamentos]

        object_list = EquipamentosUnion.objects.all()
        if nome_list:
            # filter list using every string in nome_list, ignoring accentuation
            object_list = object_list.filter(
                reduce(operator.and_, (Q(nome__unaccent__icontains=x) for x in nome_list)))
        if tipos:
            object_list = object_list.filter(tipo__in=tipos)
        if municipio_field:
            object_list = object_list.filter(
                municipio__id_ibge=municipio_field)

        return object_list

equipamento_list = EquipamentoView.as_view()

class AgenteView(ListView):
    template_name = 'busca/agente_listagem.html'
    paginate_by = 10

    def get_form_kwargs(self):
        kwargs = {}
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super(AgenteView, self).get_context_data(*args, **kwargs)

        nome = self.request.GET.get('nome') or ''
        municipio = self.request.GET.get('municipio') or ''
        agentes = self.request.GET.getlist('agentes') or []

        data = {
            'nome': nome,
            'municipio': municipio,
            'agentes': agentes,
        }

        context['data'] = {
            'nome': nome,
            'municipio': municipio,
            'tipos': agentes,
        }

        self.form = BuscaForm(initial=data, **self.get_form_kwargs())
        context['form'] = self.form

        # add parameters to url
        context['page_url'] = ''

        url_single_params = ['nome', 'municipio']
        for param in url_single_params:
            if data[param]:
                if context['page_url']:
                    context['page_url'] += '&'
                context['page_url'] += param + '=' + data[param]

        url_multiple_params = ['agentes']
        for param in url_multiple_params:
            if data[param]:
                for param_item in data[param]:
                    if context['page_url']:
                        context['page_url'] += '&'
                    context['page_url'] += param + '=' + param_item

        if context['page_url']:
            context['page_url'] += '&'

        return context

    def get_queryset(self):
        nome_field = self.request.GET.get('nome') or ''
        municipio_field = self.request.GET.get('municipio') or ''
        nome_list = nome_field.split()

        agentes = self.request.GET.getlist('agentes') or []
        tipos = [int(agente) for agente in agentes]

        object_list = AgentesUnion.objects.all()
        if nome_list:
            # filter list using every string in nome_list, ignoring accentuation
            object_list = object_list.filter(
                reduce(operator.and_, (Q(nome__unaccent__icontains=x) for x in nome_list)))
        if tipos:
            object_list = object_list.filter(tipo__in=tipos)
        if municipio_field:
            object_list = object_list.filter(
                municipio__id_ibge=municipio_field)

        return object_list

agente_list = AgenteView.as_view()

class AcaoView(ListView):
    template_name = 'busca/acao_listagem.html'
    paginate_by = 10

    def get_form_kwargs(self):
        kwargs = {}
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super(AcaoView, self).get_context_data(*args, **kwargs)

        nome = self.request.GET.get('nome') or ''
        municipio = self.request.GET.get('municipio') or ''
        acoes = self.request.GET.getlist('acoes') or []

        data = {
            'nome': nome,
            'municipio': municipio,
            'acoes': acoes,
        }

        context['data'] = {
            'nome': nome,
            'municipio': municipio,
            'tipos': acoes,
        }

        self.form = BuscaForm(initial=data, **self.get_form_kwargs())
        context['form'] = self.form

        # add parameters to url
        context['page_url'] = ''

        url_single_params = ['nome', 'municipio']
        for param in url_single_params:
            if data[param]:
                if context['page_url']:
                    context['page_url'] += '&'
                context['page_url'] += param + '=' + data[param]

        url_multiple_params = ['acoes']
        for param in url_multiple_params:
            if data[param]:
                for param_item in data[param]:
                    if context['page_url']:
                        context['page_url'] += '&'
                    context['page_url'] += param + '=' + param_item

        if context['page_url']:
            context['page_url'] += '&'

        return context

    def get_queryset(self):
        nome_field = self.request.GET.get('nome') or ''
        municipio_field = self.request.GET.get('municipio') or ''
        nome_list = nome_field.split()

        acoes = self.request.GET.getlist('acoes') or []
        tipos = [int(acao) for acao in acoes]

        object_list = AcoesUnion.objects.all()
        if nome_list:
            # filter list using every string in nome_list, ignoring accentuation
            object_list = object_list.filter(
                reduce(operator.and_, (Q(nome__unaccent__icontains=x) for x in nome_list)))
        if tipos:
            object_list = object_list.filter(tipo__in=tipos)
        if municipio_field:
            local_list = Local.objects.all().filter(municipio__id_ibge=municipio_field
                                                    ).values_list('acao__pk', flat=True).distinct()
            object_list = object_list.filter(id__in=set(local_list))

        return object_list

acao_list = AcaoView.as_view()

class DivisaoUrbanaView(ListView):
    template_name = 'busca/divisaourbana_listagem.html'
    paginate_by = 10

    def get_form_kwargs(self):
        kwargs = {}
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super(DivisaoUrbanaView, self).get_context_data(
            *args, **kwargs)

        nome = self.request.GET.get('nome') or ''
        divisoes_urbanas = self.request.GET.getlist('divisoes_urbanas') or []

        data = {
            'nome': nome,
            'divisoes_urbanas': divisoes_urbanas
        }

        context['data'] = {
            'nome': nome,
            'tipos': divisoes_urbanas,
        }

        self.form = BuscaForm(initial=data, **self.get_form_kwargs())
        context['form'] = self.form

        # add parameters to url
        context['page_url'] = ''

        url_single_params = ['nome']
        for param in url_single_params:
            if data[param]:
                if context['page_url']:
                    context['page_url'] += '&'
                context['page_url'] += param + '=' + data[param]

        url_multiple_params = ['divisoes_urbanas']
        for param in url_multiple_params:
            if data[param]:
                for param_item in data[param]:
                    if context['page_url']:
                        context['page_url'] += '&'
                    context['page_url'] += param + '=' + param_item

        if context['page_url']:
            context['page_url'] += '&'

        return context

    def get_queryset(self):
        nome_field = self.request.GET.get('nome') or ''
        nome_list = nome_field.split()

        divisoes_urbanas = self.request.GET.getlist('divisoes_urbanas') or []
        tipos = [int(divisao_urbana) for divisao_urbana in divisoes_urbanas]

        object_list = DivisoesUrbanasUnion.objects.all()
        if nome_list:
            # filter list using every string in nome_list, ignoring accentuation
            object_list = object_list.filter(
                reduce(operator.and_, (Q(nome__unaccent__icontains=x) for x in nome_list)))
        if tipos:
            object_list = object_list.filter(tipo__in=tipos)

        return object_list

divisaourbana_list = DivisaoUrbanaView.as_view()