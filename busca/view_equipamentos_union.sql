CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP MATERIALIZED VIEW IF EXISTS view_equipamentos_union;
DROP VIEW IF EXISTS view_equipamentos_union;
CREATE MATERIALIZED VIEW view_equipamentos_union AS (
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, v001::VARCHAR AS id, v008 AS nome, v006 AS municipio_id, 2 AS tipo FROM equipamento_biblioteca) AS biblioteca
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, id::VARCHAR AS id, v008 AS nome, v006 AS municipio_id, 1 AS tipo FROM equipamento_centrociencia) AS centrociencia
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, id::VARCHAR AS id, v008 AS nome, v006 AS municipio_id, 4 AS tipo FROM equipamento_cinema) AS cinema
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, v001::VARCHAR AS id, v008 AS nome, v006 AS municipio_id, 0 AS tipo FROM equipamento_cras) AS cras
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, pk_cod_entidade::VARCHAR as id, no_entidade AS nome, municipio_id AS municipio_id, 7 AS tipo FROM equipamento_escola) AS escola
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, v001::VARCHAR AS id, v008 AS nome, v006 AS municipio_id, 3 AS tipo FROM equipamento_museu) AS museu
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, id::VARCHAR AS id, v008 AS nome, v006 AS municipio_id, 6 AS tipo FROM equipamento_pontocultura) AS pontocultura
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, id::VARCHAR AS id, v008 AS nome, v006 AS municipio_id, 8 AS tipo FROM equipamento_salaverde) AS salaverde
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, id::VARCHAR AS id, v009 AS nome, v006 AS municipio_id, 9 AS tipo FROM equipamento_saude) AS saude
	UNION
	SELECT * FROM (SELECT uuid_generate_v4() AS uid, v001::VARCHAR AS id, v008 AS nome, v006 AS municipio_id, 5 AS tipo FROM equipamento_teatro) AS teatro
);