# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.core.cache import caches
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import render_to_response, get_object_or_404
from django.views.decorators.csrf import csrf_protect
from ibge.models import SetorCensitario
from django.core.serializers import serialize


cache = caches['ibge']


def index(request):
    '''Pagina inicial do mapa'''

    return render(request, 'mapa.html')


def setores_censitarios(request):
    '''Plota raio dos setores censitarios no mapa'''

    if 'raio' in request.GET:
        raio = int(request.GET['raio'])
    else:
        raio = 1

    try:
        longitude = request.GET['longitude']
    except:
        longitude = 0

    try:
        latitude = request.GET['latitude']
    except:
        latitude = 0

    agrupamento_setores = SetorCensitario.objects.entorno(
        latitude, longitude, raio)
    s = serialize('custom_geojson', agrupamento_setores,
                  geometry_field='geom', fields=('geom', 'nm_bairro',))

    return HttpResponse(s, content_type='application/vnd.geo+json')


def setores_censitarios_por_acao(request):

    from axis.acao.models import Acao

    if 'raio' in request.GET:
        raio = int(request.GET['raio'])
    else:
        raio = 1

    pk = request.GET.get('pk', False)

    if not pk:
        raise Http404

    acao = get_object_or_404(Acao, pk=pk)

    agrupamento_setores = acao.get_child().setores_censitarios_entorno(raio=raio)

    s = serialize('custom_geojson', agrupamento_setores,
                  geometry_field='geom', fields=('geom', 'nm_bairro',))

    return HttpResponse(s, content_type='application/vnd.geo+json')


def setores_censitarios_por_uf(request):

    from ibge.models import UF

    pk = request.GET.get('pk', False)

    if not pk:
        raise Http404

    uf_object = get_object_or_404(UF, pk=pk)

    setorescensitarios_uf = cache.get(
        'setorescensitarios_uf_{}'.format(uf_object.uf))
    # setorescensitarios_uf_SP, setorescensitarios_uf_MG, setorescensitarios_uf_SP, ...

    if not setorescensitarios_uf:
        setorescensitarios_uf = SetorCensitario.objects.filter(
            municipio__uf_sigla=uf_object.uf
        )
        cache.set('setorescensitarios_uf_{}'.format(
            uf_object.uf), setorescensitarios_uf, timeout=None)

    setorescensitarios_uf_geojson = cache.get(
        'setorescensitarios_uf_{}_as_geojson'.format(uf_object.uf))
    # setorescensitarios_uf_SP_as_geojson, setorescensitarios_uf_MG_as_geojson, setorescensitarios_uf_SP_as_geojson, ...

    if not setorescensitarios_uf_geojson:
        setorescensitarios_uf_geojson = serialize(
            'custom_geojson', setorescensitarios_uf, geometry_field='geom', fields=('geom', 'nm_bairro',))
        cache.set('setorescensitarios_uf_{}_as_geojson'.format(
            uf_object.uf), setorescensitarios_uf_geojson, timeout=None)

    return HttpResponse(setorescensitarios_uf_geojson, content_type='application/vnd.geo+json')


def setores_censitarios_por_municipio(request):

    from ibge.models import Municipio

    pk = request.GET.get('pk', False)

    if not pk:
        raise Http404

    municipio = get_object_or_404(Municipio, pk=pk)

    setorescensitarios_mun = cache.get(
        'setorescensitarios_mun_{}'.format(municipio.pk))
    if not setorescensitarios_mun:
        setorescensitarios_mun = SetorCensitario.objects.filter(
            municipio__pk=municipio.pk
        )
        cache.set('setorescensitarios_mun_{}'.format(
            municipio.pk), setorescensitarios_mun, timeout=None)

    setorescensitarios_mun_geojson = cache.get(
        'setorescensitarios_mun_{}_as_geojson'.format(municipio.pk))

    if not setorescensitarios_mun_geojson:
        setorescensitarios_mun_geojson = serialize(
            'custom_geojson', setorescensitarios_mun, geometry_field='geom', fields=('geom', 'nm_bairro',))
        cache.set('setorescensitarios_mun_{}_as_geojson'.format(
            municipio.pk), setorescensitarios_mun_geojson, timeout=None)

    return HttpResponse(setorescensitarios_mun_geojson, content_type='application/vnd.geo+json')
