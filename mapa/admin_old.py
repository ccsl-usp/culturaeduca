from django.contrib.gis import admin

from mapa.models import SaoAreaPonderacao
from mapa.models import SaoDistrito
from mapa.models import SaoSetorCensitario
from mapa.models import SaoSubprefeitura
from mapa.models import Regiao5Sp


class SaoSubprefeituraAdmin(admin.OSMGeoAdmin):
    list_display = ('nome','cd_geocodm',)

class SaoDistritoAdmin(admin.OSMGeoAdmin):
    list_display = ('nm_distrit', 'cd_subpref',)

class SaoAreaPonderacaoAdmin(admin.OSMGeoAdmin):
    list_display = ('cd_ap','cd_geocodd','cd_subpref',)

class SaoSetorCensitarioAdmin(admin.OSMGeoAdmin):
    list_display = ('gid','cd_ap','cd_geocodd','cd_subpref',)

class Regiao5SpAdmin(admin.OSMGeoAdmin):
    list_display = ('nm_regiao','cd_regiao',)

admin.site.register(SaoAreaPonderacao, SaoAreaPonderacaoAdmin)
admin.site.register(SaoDistrito, SaoDistritoAdmin)
admin.site.register(SaoSetorCensitario, SaoSetorCensitarioAdmin)
admin.site.register(SaoSubprefeitura, SaoSubprefeituraAdmin)
admin.site.register(Regiao5Sp, Regiao5SpAdmin)
