from django.conf.urls import include, url
import mapa.views

urlpatterns = [
	url(r'^$', mapa.views.index, name='mapa_index'),
    url(r'^setores_censitarios.geojson$', mapa.views.setores_censitarios, name='setores_censitarios'),
    url(r'^setores_censitarios_por_acao.geojson$', mapa.views.setores_censitarios_por_acao, name='setores_censitarios_por_acao'),
    url(r'^setores_censitarios_por_uf.geojson$', mapa.views.setores_censitarios_por_uf, name='setores_censitarios_por_uf'),
    url(r'^setores_censitarios_por_municipio.geojson$', mapa.views.setores_censitarios_por_municipio, name='setores_censitarios_por_municipio'),
]
