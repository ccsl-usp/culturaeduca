#!/bin/bash
set -e

pipenv run python manage.py gerarconfigtilestache
pipenv run gunicorn "TileStache:WSGITileServer('/opt/culturaeduca/tsconfig.json')" \
    --user=django --group=django \
    --bind=0.0.0.0:8181 --workers=4 --worker-class=eventlet