#!/bin/bash
set -e

pipenv run python manage.py collectstatic --settings=axis.settings.production --noinput
pipenv run gunicorn axis.wsgi \
    --user=django --group=django \
    --bind=0.0.0.0:8000 --workers=4 --timeout=300