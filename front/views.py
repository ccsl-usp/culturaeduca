from django.shortcuts import render
from django.template import RequestContext, loader
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import Http404
from django.shortcuts import render_to_response, get_object_or_404

def index(request):
	'''Pagina inicial'''
	return render(request, 'home.html')


def sobre(request):
	'''Pagina institucional'''
	return render(request, 'sobre.html')


def rede(request):
	'''Pagina Rede'''
	return render(request, 'rede/rede.html')
