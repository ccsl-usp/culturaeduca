# -*- coding: utf-8 -*-

from django.db import models

class Domicilio(models.Model):
    '''Amostra IBGE Domicilios'''

    V0001 = models.BigIntegerField(null=True, blank=True)
    V0002 = models.BigIntegerField(null=True, blank=True)
    V0011 = models.BigIntegerField(null=True, blank=True)
    V0300 = models.BigIntegerField(null=True, blank=True)
    V0010 = models.BigIntegerField(null=True, blank=True)
    V1001 = models.BigIntegerField(null=True, blank=True)
    V1002 = models.BigIntegerField(null=True, blank=True)
    V1003 = models.BigIntegerField(null=True, blank=True)
    V1004 = models.BigIntegerField(null=True, blank=True)
    V1006 = models.BigIntegerField(null=True, blank=True)
    V4001 = models.BigIntegerField(null=True, blank=True)
    V4002 = models.BigIntegerField(null=True, blank=True)
    V0201 = models.BigIntegerField(null=True, blank=True)
    V2011 = models.BigIntegerField(null=True, blank=True)
    V2012 = models.BigIntegerField(null=True, blank=True)
    V0202 = models.BigIntegerField(null=True, blank=True)
    V0203 = models.BigIntegerField(null=True, blank=True)
    V6203 = models.BigIntegerField(null=True, blank=True)
    V0204 = models.BigIntegerField(null=True, blank=True)
    V6204 = models.BigIntegerField(null=True, blank=True)
    V0205 = models.BigIntegerField(null=True, blank=True)
    V0206 = models.BigIntegerField(null=True, blank=True)
    V0207 = models.BigIntegerField(null=True, blank=True)
    V0208 = models.BigIntegerField(null=True, blank=True)
    V0209 = models.BigIntegerField(null=True, blank=True)
    V0210 = models.BigIntegerField(null=True, blank=True)
    V0211 = models.BigIntegerField(null=True, blank=True)
    V0212 = models.BigIntegerField(null=True, blank=True)
    V0213 = models.BigIntegerField(null=True, blank=True)
    V0214 = models.BigIntegerField(null=True, blank=True)
    V0215 = models.BigIntegerField(null=True, blank=True)
    V0216 = models.BigIntegerField(null=True, blank=True)
    V0217 = models.BigIntegerField(null=True, blank=True)
    V0218 = models.BigIntegerField(null=True, blank=True)
    V0219 = models.BigIntegerField(null=True, blank=True)
    V0220 = models.BigIntegerField(null=True, blank=True)
    V0221 = models.BigIntegerField(null=True, blank=True)
    V0222 = models.BigIntegerField(null=True, blank=True)
    V0301 = models.BigIntegerField(null=True, blank=True)
    V0401 = models.BigIntegerField(null=True, blank=True)
    V0402 = models.BigIntegerField(null=True, blank=True)
    V0701 = models.BigIntegerField(null=True, blank=True)
    V6529 = models.BigIntegerField(null=True, blank=True)
    V6530 = models.BigIntegerField(null=True, blank=True)
    V6531 = models.BigIntegerField(null=True, blank=True)
    V6532 = models.BigIntegerField(null=True, blank=True)
    V6600 = models.BigIntegerField(null=True, blank=True)
    V6210 = models.BigIntegerField(null=True, blank=True)
    M0201 = models.BigIntegerField(null=True, blank=True)
    M2011 = models.BigIntegerField(null=True, blank=True)
    M0202 = models.BigIntegerField(null=True, blank=True)
    M0203 = models.BigIntegerField(null=True, blank=True)
    M0204 = models.BigIntegerField(null=True, blank=True)
    M0205 = models.BigIntegerField(null=True, blank=True)
    M0206 = models.BigIntegerField(null=True, blank=True)
    M0207 = models.BigIntegerField(null=True, blank=True)
    M0208 = models.BigIntegerField(null=True, blank=True)
    M0209 = models.BigIntegerField(null=True, blank=True)
    M0210 = models.BigIntegerField(null=True, blank=True)
    M0211 = models.BigIntegerField(null=True, blank=True)
    M0212 = models.BigIntegerField(null=True, blank=True)
    M0213 = models.BigIntegerField(null=True, blank=True)
    M0214 = models.BigIntegerField(null=True, blank=True)
    M0215 = models.BigIntegerField(null=True, blank=True)
    M0216 = models.BigIntegerField(null=True, blank=True)
    M0217 = models.BigIntegerField(null=True, blank=True)
    M0218 = models.BigIntegerField(null=True, blank=True)
    M0219 = models.BigIntegerField(null=True, blank=True)
    M0220 = models.BigIntegerField(null=True, blank=True)
    M0221 = models.BigIntegerField(null=True, blank=True)
    M0222 = models.BigIntegerField(null=True, blank=True)
    M0301 = models.BigIntegerField(null=True, blank=True)
    M0401 = models.BigIntegerField(null=True, blank=True)
    M0402 = models.BigIntegerField(null=True, blank=True)
    M0701 = models.BigIntegerField(null=True, blank=True)

    def __unicode__(self):
        return str(self.V0011)

    class Meta:
        verbose_name = 'Amostra Domicílio'
        verbose_name_plural = 'Amostras Domicílios'

class Emigracao(models.Model):
    '''Classe de emigracao'''

    V0001 = models.BigIntegerField(null=True, blank=True)
    V0002 = models.BigIntegerField(null=True, blank=True)
    V0011 = models.BigIntegerField(null=True, blank=True)
    V0300 = models.BigIntegerField(null=True, blank=True)
    V0010 = models.BigIntegerField(null=True, blank=True)
    V1001 = models.BigIntegerField(null=True, blank=True)
    V1002 = models.BigIntegerField(null=True, blank=True)
    V1003 = models.BigIntegerField(null=True, blank=True)
    V1004 = models.BigIntegerField(null=True, blank=True)
    V1006  = models.BigIntegerField(null=True, blank=True)
    V0303 = models.BigIntegerField(null=True, blank=True)
    V0304 = models.BigIntegerField(null=True, blank=True)
    V0305 = models.BigIntegerField(null=True, blank=True)
    V3061 = models.BigIntegerField(null=True, blank=True)
    M0303 = models.BigIntegerField(null=True, blank=True)
    M0304 = models.BigIntegerField(null=True, blank=True)
    M0305 = models.BigIntegerField(null=True, blank=True)
    M3061 = models.BigIntegerField(null=True, blank=True)

    def __unicode__(self):
        return str(self.V0001)

    class Meta:
        verbose_name = 'Emigração'
        verbose_name_plural = 'Emigrações'

class Mortalidade(models.Model):
    '''Mortalidade'''

    V0001 = models.BigIntegerField(null=True, blank=True)
    V0002 = models.BigIntegerField(null=True, blank=True)
    V0011 = models.BigIntegerField(null=True, blank=True)
    V0300 = models.BigIntegerField(null=True, blank=True)
    V0010 = models.BigIntegerField(null=True, blank=True)
    V1001 = models.BigIntegerField(null=True, blank=True)
    V1002 = models.BigIntegerField(null=True, blank=True)
    V1003 = models.BigIntegerField(null=True, blank=True)
    V1004 = models.BigIntegerField(null=True, blank=True)
    V1006  = models.BigIntegerField(null=True, blank=True)
    V0703 = models.BigIntegerField(null=True, blank=True)
    V0704 = models.BigIntegerField(null=True, blank=True)
    V7051 = models.BigIntegerField(null=True, blank=True)
    V7052 = models.BigIntegerField(null=True, blank=True)
    M0703 = models.BigIntegerField(null=True, blank=True)
    M0704 = models.BigIntegerField(null=True, blank=True)
    M7051 = models.BigIntegerField(null=True, blank=True)
    M7052 = models.BigIntegerField(null=True, blank=True)

    def __unicode__(self):
        return str(self.V0001)

    class Meta:
        verbose_name = 'Mortalidade'
        verbose_name_plural = 'Mortalidades'

class Pessoa(models.Model):
    '''Classe de pessoas'''

    V0001 = models.BigIntegerField(null=True, blank=True)
    V0002 = models.BigIntegerField(null=True, blank=True)
    V0011 = models.BigIntegerField(null=True, blank=True)
    V0300 = models.BigIntegerField(null=True, blank=True)
    V0010 = models.BigIntegerField(null=True, blank=True)
    V1001 = models.BigIntegerField(null=True, blank=True)
    V1002 = models.BigIntegerField(null=True, blank=True)
    V1003 = models.BigIntegerField(null=True, blank=True)
    V1004 = models.BigIntegerField(null=True, blank=True)
    V1006 = models.BigIntegerField(null=True, blank=True)
    V0502 = models.BigIntegerField(null=True, blank=True)
    V0504 = models.BigIntegerField(null=True, blank=True)
    V0601 = models.BigIntegerField(null=True, blank=True)
    V6033 = models.BigIntegerField(null=True, blank=True)
    V6036 = models.BigIntegerField(null=True, blank=True)
    V6037 = models.BigIntegerField(null=True, blank=True)
    V6040 = models.BigIntegerField(null=True, blank=True)
    V0606 = models.BigIntegerField(null=True, blank=True)
    V0613 = models.BigIntegerField(null=True, blank=True)
    V0614 = models.BigIntegerField(null=True, blank=True)
    V0615 = models.BigIntegerField(null=True, blank=True)
    V0616 = models.BigIntegerField(null=True, blank=True)
    V0617 = models.BigIntegerField(null=True, blank=True)
    V0618 = models.BigIntegerField(null=True, blank=True)
    V0619 = models.BigIntegerField(null=True, blank=True)
    V0620 = models.BigIntegerField(null=True, blank=True)
    V0621 = models.BigIntegerField(null=True, blank=True)
    V0622 = models.BigIntegerField(null=True, blank=True)
    V6222 = models.BigIntegerField(null=True, blank=True)
    V6224 = models.BigIntegerField(null=True, blank=True)
    V0623 = models.BigIntegerField(null=True, blank=True)
    V0624 = models.BigIntegerField(null=True, blank=True)
    V0625 = models.BigIntegerField(null=True, blank=True)
    V6252 = models.BigIntegerField(null=True, blank=True)
    V6254 = models.BigIntegerField(null=True, blank=True)
    V6256 = models.BigIntegerField(null=True, blank=True)
    V0626 = models.BigIntegerField(null=True, blank=True)
    V6262 = models.BigIntegerField(null=True, blank=True)
    V6264 = models.BigIntegerField(null=True, blank=True)
    V6266 = models.BigIntegerField(null=True, blank=True)
    V0627 = models.BigIntegerField(null=True, blank=True)
    V0628 = models.BigIntegerField(null=True, blank=True)
    V0629 = models.BigIntegerField(null=True, blank=True)
    V0630 = models.BigIntegerField(null=True, blank=True)
    V0631 = models.BigIntegerField(null=True, blank=True)
    V0632 = models.BigIntegerField(null=True, blank=True)
    V0633 = models.BigIntegerField(null=True, blank=True)
    V0634 = models.BigIntegerField(null=True, blank=True)
    V0635 = models.BigIntegerField(null=True, blank=True)
    V6400 = models.BigIntegerField(null=True, blank=True)
    V6352 = models.BigIntegerField(null=True, blank=True)
    V6354 = models.BigIntegerField(null=True, blank=True)
    V6356 = models.BigIntegerField(null=True, blank=True)
    V0636 = models.BigIntegerField(null=True, blank=True)
    V6362 = models.BigIntegerField(null=True, blank=True)
    V6364 = models.BigIntegerField(null=True, blank=True)
    V6366 = models.BigIntegerField(null=True, blank=True)
    V0637 = models.BigIntegerField(null=True, blank=True)
    V0638 = models.BigIntegerField(null=True, blank=True)
    V0639 = models.BigIntegerField(null=True, blank=True)
    V0640 = models.BigIntegerField(null=True, blank=True)
    V0641 = models.BigIntegerField(null=True, blank=True)
    V0642 = models.BigIntegerField(null=True, blank=True)
    V0643 = models.BigIntegerField(null=True, blank=True)
    V0644 = models.BigIntegerField(null=True, blank=True)
    V0645 = models.BigIntegerField(null=True, blank=True)
    V6461 = models.BigIntegerField(null=True, blank=True)
    V6471 = models.BigIntegerField(null=True, blank=True)
    V0648 = models.BigIntegerField(null=True, blank=True)
    V0649 = models.BigIntegerField(null=True, blank=True)
    V0650 = models.BigIntegerField(null=True, blank=True)
    V0651 = models.BigIntegerField(null=True, blank=True)
    V6511 = models.BigIntegerField(null=True, blank=True)
    V6513 = models.BigIntegerField(null=True, blank=True)
    V6514 = models.BigIntegerField(null=True, blank=True)
    V0652 = models.BigIntegerField(null=True, blank=True)
    V6521 = models.BigIntegerField(null=True, blank=True)
    V6524 = models.BigIntegerField(null=True, blank=True)
    V6525 = models.BigIntegerField(null=True, blank=True)
    V6526 = models.BigIntegerField(null=True, blank=True)
    V6527 = models.BigIntegerField(null=True, blank=True)
    V6528 = models.BigIntegerField(null=True, blank=True)
    V6529 = models.BigIntegerField(null=True, blank=True)
    V6530 = models.BigIntegerField(null=True, blank=True)
    V6531 = models.BigIntegerField(null=True, blank=True)
    V6532 = models.BigIntegerField(null=True, blank=True)
    V0653 = models.BigIntegerField(null=True, blank=True)
    V0654 = models.BigIntegerField(null=True, blank=True)
    V0655 = models.BigIntegerField(null=True, blank=True)
    V0656 = models.BigIntegerField(null=True, blank=True)
    V0657 = models.BigIntegerField(null=True, blank=True)
    V0658 = models.BigIntegerField(null=True, blank=True)
    V0659 = models.BigIntegerField(null=True, blank=True)
    V6591 = models.BigIntegerField(null=True, blank=True)
    V0660 = models.BigIntegerField(null=True, blank=True)
    V6602 = models.BigIntegerField(null=True, blank=True)
    V6604 = models.BigIntegerField(null=True, blank=True)
    V6606 = models.BigIntegerField(null=True, blank=True)
    V0661 = models.BigIntegerField(null=True, blank=True)
    V0662 = models.BigIntegerField(null=True, blank=True)
    V0663 = models.BigIntegerField(null=True, blank=True)
    V6631 = models.BigIntegerField(null=True, blank=True)
    V6632 = models.BigIntegerField(null=True, blank=True)
    V6633 = models.BigIntegerField(null=True, blank=True)
    V0664 = models.BigIntegerField(null=True, blank=True)
    V6641 = models.BigIntegerField(null=True, blank=True)
    V6642 = models.BigIntegerField(null=True, blank=True)
    V6643 = models.BigIntegerField(null=True, blank=True)
    V0665 = models.BigIntegerField(null=True, blank=True)
    V6660 = models.BigIntegerField(null=True, blank=True)
    V6664 = models.BigIntegerField(null=True, blank=True)
    V0667 = models.BigIntegerField(null=True, blank=True)
    V0668 = models.BigIntegerField(null=True, blank=True)
    V6681 = models.BigIntegerField(null=True, blank=True)
    V6682 = models.BigIntegerField(null=True, blank=True)
    V0669 = models.BigIntegerField(null=True, blank=True)
    V6691 = models.BigIntegerField(null=True, blank=True)
    V6692 = models.BigIntegerField(null=True, blank=True)
    V6693 = models.BigIntegerField(null=True, blank=True)
    V6800 = models.BigIntegerField(null=True, blank=True)
    V0670 = models.BigIntegerField(null=True, blank=True)
    V0671 = models.BigIntegerField(null=True, blank=True)
    V6900 = models.BigIntegerField(null=True, blank=True)
    V6910 = models.BigIntegerField(null=True, blank=True)
    V6920 = models.BigIntegerField(null=True, blank=True)
    V6930 = models.BigIntegerField(null=True, blank=True)
    V6940 = models.BigIntegerField(null=True, blank=True)
    V6121 = models.BigIntegerField(null=True, blank=True)
    V0604 = models.BigIntegerField(null=True, blank=True)
    V0605 = models.BigIntegerField(null=True, blank=True)
    V5020 = models.BigIntegerField(null=True, blank=True)
    V5060 = models.BigIntegerField(null=True, blank=True)
    V5070 = models.BigIntegerField(null=True, blank=True)
    V5080 = models.BigIntegerField(null=True, blank=True)
    V6462 = models.BigIntegerField(null=True, blank=True)
    V6472 = models.BigIntegerField(null=True, blank=True)
    V5110 = models.BigIntegerField(null=True, blank=True)
    V5120 = models.BigIntegerField(null=True, blank=True)
    V5030 = models.BigIntegerField(null=True, blank=True)
    V5040 = models.BigIntegerField(null=True, blank=True)
    V5090 = models.BigIntegerField(null=True, blank=True)
    V5100 = models.BigIntegerField(null=True, blank=True)
    V5130 = models.BigIntegerField(null=True, blank=True)
    M0502 = models.BigIntegerField(null=True, blank=True)
    M0601 = models.BigIntegerField(null=True, blank=True)
    M6033 = models.BigIntegerField(null=True, blank=True)
    M0606 = models.BigIntegerField(null=True, blank=True)
    M0613 = models.BigIntegerField(null=True, blank=True)
    M0614 = models.BigIntegerField(null=True, blank=True)
    M0615 = models.BigIntegerField(null=True, blank=True)
    M0616 = models.BigIntegerField(null=True, blank=True)
    M0617 = models.BigIntegerField(null=True, blank=True)
    M0618 = models.BigIntegerField(null=True, blank=True)
    M0619 = models.BigIntegerField(null=True, blank=True)
    M0620 = models.BigIntegerField(null=True, blank=True)
    M0621 = models.BigIntegerField(null=True, blank=True)
    M0622 = models.BigIntegerField(null=True, blank=True)
    M6222 = models.BigIntegerField(null=True, blank=True)
    M6224 = models.BigIntegerField(null=True, blank=True)
    M0623 = models.BigIntegerField(null=True, blank=True)
    M0624 = models.BigIntegerField(null=True, blank=True)
    M0625 = models.BigIntegerField(null=True, blank=True)
    M6252 = models.BigIntegerField(null=True, blank=True)
    M6254 = models.BigIntegerField(null=True, blank=True)
    M6256 = models.BigIntegerField(null=True, blank=True)
    M0626 = models.BigIntegerField(null=True, blank=True)
    M6262 = models.BigIntegerField(null=True, blank=True)
    M6264 = models.BigIntegerField(null=True, blank=True)
    M6266 = models.BigIntegerField(null=True, blank=True)
    M0627 = models.BigIntegerField(null=True, blank=True)
    M0628 = models.BigIntegerField(null=True, blank=True)
    M0629 = models.BigIntegerField(null=True, blank=True)
    M0630 = models.BigIntegerField(null=True, blank=True)
    M0631 = models.BigIntegerField(null=True, blank=True)
    M0632 = models.BigIntegerField(null=True, blank=True)
    M0633 = models.BigIntegerField(null=True, blank=True)
    M0634 = models.BigIntegerField(null=True, blank=True)
    M0635 = models.BigIntegerField(null=True, blank=True)
    M6352 = models.BigIntegerField(null=True, blank=True)
    M6354 = models.BigIntegerField(null=True, blank=True)
    M6356 = models.BigIntegerField(null=True, blank=True)
    M0636 = models.BigIntegerField(null=True, blank=True)
    M6362 = models.BigIntegerField(null=True, blank=True)
    M6364 = models.BigIntegerField(null=True, blank=True)
    M6366 = models.BigIntegerField(null=True, blank=True)
    M0637 = models.BigIntegerField(null=True, blank=True)
    M0638 = models.BigIntegerField(null=True, blank=True)
    M0639 = models.BigIntegerField(null=True, blank=True)
    M0640 = models.BigIntegerField(null=True, blank=True)
    M0641 = models.BigIntegerField(null=True, blank=True)
    M0642 = models.BigIntegerField(null=True, blank=True)
    M0643 = models.BigIntegerField(null=True, blank=True)
    M0644 = models.BigIntegerField(null=True, blank=True)
    M0645 = models.BigIntegerField(null=True, blank=True)
    M6461 = models.BigIntegerField(null=True, blank=True)
    M6471 = models.BigIntegerField(null=True, blank=True)
    M0648 = models.BigIntegerField(null=True, blank=True)
    M0649 = models.BigIntegerField(null=True, blank=True)
    M0650 = models.BigIntegerField(null=True, blank=True)
    M0651 = models.BigIntegerField(null=True, blank=True)
    M6511 = models.BigIntegerField(null=True, blank=True)
    M0652 = models.BigIntegerField(null=True, blank=True)
    M6521 = models.BigIntegerField(null=True, blank=True)
    M0653 = models.BigIntegerField(null=True, blank=True)
    M0654 = models.BigIntegerField(null=True, blank=True)
    M0655 = models.BigIntegerField(null=True, blank=True)
    M0656 = models.BigIntegerField(null=True, blank=True)
    M0657 = models.BigIntegerField(null=True, blank=True)
    M0658 = models.BigIntegerField(null=True, blank=True)
    M0659 = models.BigIntegerField(null=True, blank=True)
    M6591 = models.BigIntegerField(null=True, blank=True)
    M0660 = models.BigIntegerField(null=True, blank=True)
    M6602 = models.BigIntegerField(null=True, blank=True)
    M6604 = models.BigIntegerField(null=True, blank=True)
    M6606 = models.BigIntegerField(null=True, blank=True)
    M0661 = models.BigIntegerField(null=True, blank=True)
    M0662 = models.BigIntegerField(null=True, blank=True)
    M0663 = models.BigIntegerField(null=True, blank=True)
    M6631 = models.BigIntegerField(null=True, blank=True)
    M6632 = models.BigIntegerField(null=True, blank=True)
    M6633 = models.BigIntegerField(null=True, blank=True)
    M0664 = models.BigIntegerField(null=True, blank=True)
    M6641 = models.BigIntegerField(null=True, blank=True)
    M6642 = models.BigIntegerField(null=True, blank=True)
    M6643 = models.BigIntegerField(null=True, blank=True)
    M0665 = models.BigIntegerField(null=True, blank=True)
    M6660 = models.BigIntegerField(null=True, blank=True)
    M0667 = models.BigIntegerField(null=True, blank=True)
    M0668 = models.BigIntegerField(null=True, blank=True)
    M6681 = models.BigIntegerField(null=True, blank=True)
    M6682 = models.BigIntegerField(null=True, blank=True)
    M0669 = models.BigIntegerField(null=True, blank=True)
    M6691 = models.BigIntegerField(null=True, blank=True)
    M6692 = models.BigIntegerField(null=True, blank=True)
    M6693 = models.BigIntegerField(null=True, blank=True)
    M0670 = models.BigIntegerField(null=True, blank=True)
    M0671 = models.BigIntegerField(null=True, blank=True)
    M6800 = models.BigIntegerField(null=True, blank=True)
    M6121 = models.BigIntegerField(null=True, blank=True)
    M0604 = models.BigIntegerField(null=True, blank=True)
    M0605 = models.BigIntegerField(null=True, blank=True)
    M6462 = models.BigIntegerField(null=True, blank=True)
    M6472 = models.BigIntegerField(null=True, blank=True)

    def __unicode__(self):
        return str(self.V0001)

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoas'
