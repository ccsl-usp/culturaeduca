# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ibge_amostra', '0002_auto_20150914_0846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domicilio',
            name='V0002',
            field=models.BigIntegerField(null=True, blank=True),
        ),
    ]
