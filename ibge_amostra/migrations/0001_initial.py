# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Domicilio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('V0001', models.IntegerField(null=True, blank=True)),
                ('V0002', models.IntegerField(null=True, blank=True)),
                ('V0011', models.BigIntegerField(null=True, blank=True)),
                ('V0300', models.BigIntegerField(null=True, blank=True)),
                ('V0010', models.IntegerField(null=True, blank=True)),
                ('V1001', models.IntegerField(null=True, blank=True)),
                ('V1002', models.IntegerField(null=True, blank=True)),
                ('V1003', models.IntegerField(null=True, blank=True)),
                ('V1004', models.IntegerField(null=True, blank=True)),
                ('V1006', models.IntegerField(null=True, blank=True)),
                ('V4001', models.IntegerField(null=True, blank=True)),
                ('V4002', models.IntegerField(null=True, blank=True)),
                ('V0201', models.IntegerField(null=True, blank=True)),
                ('V2011', models.IntegerField(null=True, blank=True)),
                ('V2012', models.IntegerField(null=True, blank=True)),
                ('V0202', models.IntegerField(null=True, blank=True)),
                ('V0203', models.IntegerField(null=True, blank=True)),
                ('V6203', models.IntegerField(null=True, blank=True)),
                ('V0204', models.IntegerField(null=True, blank=True)),
                ('V6204', models.IntegerField(null=True, blank=True)),
                ('V0205', models.IntegerField(null=True, blank=True)),
                ('V0206', models.IntegerField(null=True, blank=True)),
                ('V0207', models.IntegerField(null=True, blank=True)),
                ('V0208', models.IntegerField(null=True, blank=True)),
                ('V0209', models.IntegerField(null=True, blank=True)),
                ('V0210', models.IntegerField(null=True, blank=True)),
                ('V0211', models.IntegerField(null=True, blank=True)),
                ('V0212', models.IntegerField(null=True, blank=True)),
                ('V0213', models.IntegerField(null=True, blank=True)),
                ('V0214', models.IntegerField(null=True, blank=True)),
                ('V0215', models.IntegerField(null=True, blank=True)),
                ('V0216', models.IntegerField(null=True, blank=True)),
                ('V0217', models.IntegerField(null=True, blank=True)),
                ('V0218', models.IntegerField(null=True, blank=True)),
                ('V0219', models.IntegerField(null=True, blank=True)),
                ('V0220', models.IntegerField(null=True, blank=True)),
                ('V0221', models.IntegerField(null=True, blank=True)),
                ('V0222', models.IntegerField(null=True, blank=True)),
                ('V0301', models.IntegerField(null=True, blank=True)),
                ('V0401', models.IntegerField(null=True, blank=True)),
                ('V0402', models.IntegerField(null=True, blank=True)),
                ('V0701', models.IntegerField(null=True, blank=True)),
                ('V6529', models.IntegerField(null=True, blank=True)),
                ('V6530', models.IntegerField(null=True, blank=True)),
                ('V6531', models.IntegerField(null=True, blank=True)),
                ('V6532', models.IntegerField(null=True, blank=True)),
                ('V6600', models.IntegerField(null=True, blank=True)),
                ('V6210', models.IntegerField(null=True, blank=True)),
                ('M0201', models.IntegerField(null=True, blank=True)),
                ('M2011', models.IntegerField(null=True, blank=True)),
                ('M0202', models.IntegerField(null=True, blank=True)),
                ('M0203', models.IntegerField(null=True, blank=True)),
                ('M0204', models.IntegerField(null=True, blank=True)),
                ('M0205', models.IntegerField(null=True, blank=True)),
                ('M0206', models.IntegerField(null=True, blank=True)),
                ('M0207', models.IntegerField(null=True, blank=True)),
                ('M0208', models.IntegerField(null=True, blank=True)),
                ('M0209', models.IntegerField(null=True, blank=True)),
                ('M0210', models.IntegerField(null=True, blank=True)),
                ('M0211', models.IntegerField(null=True, blank=True)),
                ('M0212', models.IntegerField(null=True, blank=True)),
                ('M0213', models.IntegerField(null=True, blank=True)),
                ('M0214', models.IntegerField(null=True, blank=True)),
                ('M0215', models.IntegerField(null=True, blank=True)),
                ('M0216', models.IntegerField(null=True, blank=True)),
                ('M0217', models.IntegerField(null=True, blank=True)),
                ('M0218', models.IntegerField(null=True, blank=True)),
                ('M0219', models.IntegerField(null=True, blank=True)),
                ('M0220', models.IntegerField(null=True, blank=True)),
                ('M0221', models.IntegerField(null=True, blank=True)),
                ('M0222', models.IntegerField(null=True, blank=True)),
                ('M0301', models.IntegerField(null=True, blank=True)),
                ('M0401', models.IntegerField(null=True, blank=True)),
                ('M0402', models.IntegerField(null=True, blank=True)),
                ('M0701', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Amostra Domic\xedlio',
                'verbose_name_plural': 'Amostras Domic\xedlios',
            },
        ),
    ]
