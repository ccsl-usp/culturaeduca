# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ibge_amostra', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domicilio',
            name='V2011',
            field=models.BigIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='domicilio',
            name='V6529',
            field=models.BigIntegerField(null=True, blank=True),
        ),
    ]
