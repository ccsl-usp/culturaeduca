# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0053_auto_20171120_1540'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemgaleria',
            name='thumbnail',
            field=models.FileField(upload_to=b'galeria', null=True, verbose_name='Thumbnail', blank=True),
        ),
        migrations.AddField(
            model_name='itemgaleria',
            name='url',
            field=models.CharField(max_length=255, null=True, verbose_name='URL', blank=True),
        ),
        migrations.AlterField(
            model_name='itemgaleria',
            name='arquivo',
            field=models.FileField(upload_to=b'galeria', null=True, verbose_name='Arquivo', blank=True),
        ),
    ]
