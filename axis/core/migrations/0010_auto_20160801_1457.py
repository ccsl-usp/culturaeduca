# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20160801_1204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='areaatuacao',
            name='nome',
            field=models.CharField(max_length=100, verbose_name=b'Nome'),
        ),
        migrations.AlterField(
            model_name='areaestudopesquisa',
            name='nome',
            field=models.CharField(max_length=100, verbose_name=b'Nome'),
        ),
        migrations.AlterField(
            model_name='identidadeetinicocultural',
            name='nome',
            field=models.CharField(max_length=100, verbose_name=b'Identidade \xc3\x89tnico-cultural'),
        ),
        migrations.AlterField(
            model_name='linguagemartistica',
            name='nome',
            field=models.CharField(max_length=100, verbose_name=b'Nome'),
        ),
        migrations.AlterField(
            model_name='nivelescolaridade',
            name='nome',
            field=models.CharField(max_length=100, verbose_name=b'Nome'),
        ),
        migrations.AlterField(
            model_name='ocupacao',
            name='nome',
            field=models.CharField(max_length=100, verbose_name='Ocupa\xe7\xe3o'),
        ),
        migrations.AlterField(
            model_name='publicofocal',
            name='nome',
            field=models.CharField(max_length=100, verbose_name=b'Nome'),
        ),
    ]
