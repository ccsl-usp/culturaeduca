# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def update_user_privacy_fields(apps, schema_editor):
    # atualiza usuarios anteriores a SRP #28 (adiciona perfil público)
    # para adicionar ProfilePrivacyField's
    User = apps.get_model("core", "User")
    ProfilePrivacyField = apps.get_model("core", "ProfilePrivacyField")

    privacy_default_fields = (
        # (nome do attr no Profile, se é publico ou privado),
        ('email', False),
        ('telefone1', False),
        ('telefone2', False),
        ('data_nascimento', False),
        ('sexo', False),
        ('genero', False),
        ('raca_cor_ibge', False),
        ('etinia', False),
        ('ocupacao', False),
        ('escolaridade', False),
        ('area_atuacao', False),
        ('tipo_espaco', False),
        ('abrangencia', False),
        ('manifestacao_cultural', False),
        ('area_pesquisa', False),
        ('publico_focal', False),
        ('linguagens_artisticas', False),
        ('atua_desde', False),
        ('atuacao', False),
        ('cep', False),
        ('logradouro', False),
        ('numero', False),
        ('complemento', False),
        ('bairro', False),
        ('municipio', False),
    )

    for user in User.objects.filter(privacy_fields=None):
        ProfilePrivacyField.objects.bulk_create([ProfilePrivacyField(
            user=user,
            name=name,
            public=public
        ) for name, public in privacy_default_fields])


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_profileprivacyfield'),
    ]

    operations = [
        migrations.RunPython(update_user_privacy_fields),
    ]
