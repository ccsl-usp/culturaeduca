# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0056_itemgaleria_titulo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='itemgaleria',
            name='titulo',
            field=models.CharField(max_length=100, verbose_name='T\xedtulo'),
        ),
    ]
