# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_redesocial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='area_estudo_pesquisa',
            field=models.ManyToManyField(to='core.AreaEstudoPesquisa', null=True, verbose_name='\xc1rea Estudo/Pesquisa', blank=True),
        ),
    ]
