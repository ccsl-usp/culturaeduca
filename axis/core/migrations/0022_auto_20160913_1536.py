# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0021_auto_20160802_1022'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_email_verified',
            field=models.BooleanField(default=False, verbose_name=b'E-mail verificado'),
        ),
        migrations.AddField(
            model_name='user',
            name='last_emailverification',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='profile',
            name='area_pesquisa',
            field=models.ManyToManyField(to='core.AreaEstudoPesquisa', verbose_name='\xc1era de Estudo e Pesquisa', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='linguagens_artisticas',
            field=models.ManyToManyField(to='core.LinguagemArtistica', verbose_name='Linguagens Art\xedtica', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='manifestacao_cultural',
            field=models.ManyToManyField(to='core.ManifestacaoCultural', verbose_name='Manifesta\xe7\xe3o Cultural', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='raca_cor_ibge',
            field=models.SmallIntegerField(choices=[(3, 'amarela'), (4, 'branca'), (5, 'indigena'), (6, 'parda'), (7, 'preta'), (2, 'outros'), (1, 'n\xe3o informar')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_active',
            field=models.BooleanField(default=False, verbose_name=b'ativo'),
        ),
    ]
