# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0029_profile_atua_desde_int'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='linguagens_artisticas',
            field=models.ManyToManyField(to='core.LinguagemArtistica', verbose_name='Linguagens Art\xedsticas', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='manifestacao_cultural',
            field=models.ManyToManyField(to='core.ManifestacaoCultural', verbose_name='Manifesta\xe7\xf5es Culturais', blank=True),
        ),
    ]
