# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0018_auto_20160801_1733'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='abrangencia',
            field=models.SmallIntegerField(default=1, verbose_name='Abrang\xeancia Geogr\xe1fica', choices=[(1, 'Local'), (2, 'Municipal'), (3, 'Estadual'), (4, 'Nacional')]),
            preserve_default=False,
        ),
    ]
