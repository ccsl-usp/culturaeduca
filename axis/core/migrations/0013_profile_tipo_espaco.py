# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20160801_1538'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='tipo_espaco',
            field=models.ManyToManyField(to='core.TipoEspaco', verbose_name='Tipo de Espa\xe7o'),
        ),
    ]
