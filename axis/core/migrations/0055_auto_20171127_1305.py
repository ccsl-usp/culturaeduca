# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0054_auto_20171121_0940'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='itemgaleria',
            options={'ordering': ('pk',), 'verbose_name': 'Item de Galeria', 'verbose_name_plural': 'Itens de Galeria'},
        ),
        migrations.AlterField(
            model_name='itemgaleria',
            name='tipo',
            field=models.SmallIntegerField(blank=True, verbose_name='Tipo de arquivo', choices=[(1, '\xc1udiovisual'), (2, 'Imagem'), (3, 'Documento')]),
        ),
    ]
