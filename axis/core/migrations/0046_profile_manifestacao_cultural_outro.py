# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0045_profile_publico_focal_outro'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='manifestacao_cultural_outro',
            field=models.CharField(max_length=512, verbose_name='Outro Tipo de Manifesta\xe7\xe3o Cultural', blank=True),
        ),
    ]
