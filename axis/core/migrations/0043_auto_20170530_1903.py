# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0042_auto_20170530_1851'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='areaatuacao',
            options={'ordering': ('nome',), 'verbose_name': '\xc1rea de Atua\xe7\xe3o', 'verbose_name_plural': '\xc1reas de Atuac\xe3o'},
        ),
        migrations.AlterModelOptions(
            name='publicofocal',
            options={'ordering': ('nome',), 'verbose_name': 'P\xfablico Focal', 'verbose_name_plural': 'P\xfablicos Focal'},
        ),
        migrations.AlterField(
            model_name='profile',
            name='atua_desde',
            field=models.PositiveSmallIntegerField(help_text='formato: AAAA', null=True, verbose_name='Atua nesta(s) \xe1rea(s) desde o ano:'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='raca_cor_ibge',
            field=models.SmallIntegerField(verbose_name='Ra\xe7a/Cor segundo o IBGE', choices=[(3, 'Amarela'), (4, 'Branca'), (5, 'Ind\xedgena'), (6, 'Parda'), (7, 'Preta'), (2, 'Outra'), (1, 'N\xe3o informar')]),
        ),
    ]
