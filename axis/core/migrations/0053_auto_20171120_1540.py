# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0052_auto_20171120_1532'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='itemgaleria',
            options={'ordering': ('descricao',), 'verbose_name': 'Item de Galeria', 'verbose_name_plural': 'Itens de Galeria'},
        ),
        migrations.RemoveField(
            model_name='itemgaleria',
            name='date_add',
        ),
        migrations.AddField(
            model_name='itemgaleria',
            name='data',
            field=models.DateField(null=True, verbose_name='Data', blank=True),
        ),
        migrations.AddField(
            model_name='itemgaleria',
            name='descricao',
            field=models.TextField(default='auto', verbose_name='Descri\xe7\xe3o'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='itemgaleria',
            name='local',
            field=models.CharField(max_length=150, null=True, verbose_name='Local', blank=True),
        ),
    ]
