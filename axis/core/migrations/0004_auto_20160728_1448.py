# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_remove_profile_nome'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='data_nascimento',
            field=models.DateField(verbose_name='Data de nascimento'),
        ),
    ]
