# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0019_profile_abrangencia'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='abrangencia',
            field=models.SmallIntegerField(verbose_name='Abrang\xeancia Geogr\xe1fica', choices=[(3, 'Estadual'), (1, 'Local'), (2, 'Municipal'), (4, 'Nacional')]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='area_estudo_pesquisa',
            field=models.ManyToManyField(to='core.AreaEstudoPesquisa', null=True, verbose_name='\xc1era de Estudo e Pesquisa', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='genero',
            field=models.SmallIntegerField(verbose_name='G\xeanero', choices=[(4, 'cisgen\xearo'), (3, 'transg\xeanero'), (2, 'outros'), (1, 'n\xe3o informar')]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='raca_cor_ibge',
            field=models.SmallIntegerField(choices=[(3, 'amarela'), (4, 'branca'), (9, 'cisgenero'), (5, 'indigena'), (6, 'parda'), (7, 'preta'), (8, 'transgenero'), (2, 'outros'), (1, 'n\xe3o informar')]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='sexo',
            field=models.SmallIntegerField(choices=[(3, 'feminino'), (4, 'masculino'), (2, 'outros'), (1, 'n\xe3o informar')]),
        ),
        migrations.AlterField(
            model_name='redesocial',
            name='plataforma',
            field=models.SmallIntegerField(verbose_name='Plataforma', choices=[(1, 'Google+'), (2, 'Facebook'), (3, 'Instagram'), (4, 'Pinterest'), (5, 'Snapchat'), (7, 'Skype'), (6, 'Telegram'), (9, 'Tumbrl'), (8, 'Twitter'), (10, 'Youtube')]),
        ),
    ]
