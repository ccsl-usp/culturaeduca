# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        #('municipios', '0001_initial'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(unique=True, max_length=254, verbose_name=b'e-mail')),
                ('nome', models.CharField(max_length=100, verbose_name='Nome ou nome social')),
                ('is_active', models.BooleanField(default=True, verbose_name=b'ativo')),
                ('is_staff', models.BooleanField(default=False, verbose_name=b'administrador')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'data de cadastro')),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'Usu\xe1rio',
                'verbose_name_plural': 'Usu\xe1rios',
            },
        ),
        migrations.CreateModel(
            name='AreaAtuacao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=80, verbose_name=b'Nome')),
            ],
            options={
                'verbose_name': 'Area de Atua\xe7\xe3o',
                'verbose_name_plural': 'Areas de Atuac\xe3o',
            },
        ),
        migrations.CreateModel(
            name='AreaEstudoPesquisa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=80, verbose_name=b'Nome')),
            ],
            options={
                'verbose_name': 'Area de Estudo ou Pesquisa',
                'verbose_name_plural': 'Areas de Estudo ou Pesquisa',
            },
        ),
        migrations.CreateModel(
            name='IdentidadeEtinicoCultural',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=80, verbose_name=b'Identidade \xc3\x89tnico-cultural')),
            ],
            options={
                'verbose_name': 'etinia',
                'verbose_name_plural': 'etinias',
            },
        ),
        migrations.CreateModel(
            name='LinguagemArtistica',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=80, verbose_name=b'Nome')),
            ],
            options={
                'verbose_name': 'Linguagem Artistica',
                'verbose_name_plural': 'Linguagens Art\xedsticas',
            },
        ),
        migrations.CreateModel(
            name='ManifestacaoCultural',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Nome')),
            ],
        ),
        migrations.CreateModel(
            name='NivelEscolaridade',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=80, verbose_name=b'Nome')),
            ],
            options={
                'verbose_name': 'N\xedvel de escolaridade',
                'verbose_name_plural': 'N\xedveis de escolaridade',
            },
        ),
        migrations.CreateModel(
            name='Ocupacao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=80, verbose_name=b'Identidade \xc3\x89tnico-cultural')),
            ],
            options={
                'verbose_name': 'Ocupa\xe7\xe3o',
                'verbose_name_plural': 'Ocupa\xe7\xf5es',
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cep', models.CharField(max_length=8)),
                ('logradouro', models.CharField(max_length=100)),
                ('numero', models.CharField(max_length=50, verbose_name='N\xfamero')),
                ('complemento', models.CharField(max_length=100, null=True, blank=True)),
                ('bairro', models.CharField(max_length=100)),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4674, null=True, blank=True)),
                ('nome', models.CharField(max_length=80, verbose_name=b'Nome')),
                ('data_inicio', models.DateField(auto_now_add=True, verbose_name='Data de in\xedcio')),
                ('telefone1', models.CharField(max_length=25, null=True, verbose_name=b'telefone', blank=True)),
                ('telefone2', models.CharField(max_length=25, null=True, verbose_name=b'telefone', blank=True)),
                ('data_nascimento', models.DateField(verbose_name='Data de nascimento', blank=True)),
                ('sexo', models.SmallIntegerField(choices=[(3, b'feminino'), (4, b'masculino'), (2, b'outros'), (1, b'n\xc3\xa3o informar')])),
                ('genero', models.SmallIntegerField(choices=[(1, b'n\xc3\xa3o informar'), (2, b'outros'), (3, b'transg\xc3\xaanero'), (4, b'cisgen\xc3\xaaro')])),
                ('raca_cor_ibge', models.SmallIntegerField(choices=[(1, b'n\xc3\xa3o informar'), (2, b'outros'), (3, b'amarela'), (4, b'branca'), (5, b'indigena'), (6, b'parda'), (7, b'preta'), (8, b'transgenero'), (9, b'cisgenero')])),
                ('area_atuacao', models.ManyToManyField(to='core.AreaAtuacao', verbose_name='\xe1rea de atua\xe7\xe3o')),
                ('area_estudo_pesquisa', models.ManyToManyField(to='core.AreaEstudoPesquisa')),
                ('escolaridade', models.ForeignKey(verbose_name='Forma\xe7\xe3o/n\xedvel de escolaridade', to='core.NivelEscolaridade')),
                ('etinia', models.ForeignKey(blank=True, to='core.IdentidadeEtinicoCultural', null=True)),
                ('linguagens_artisticas', models.ManyToManyField(to='core.LinguagemArtistica')),
                ('municipio', models.ForeignKey(to='municipios.Municipio')),
                ('ocupacao', models.ForeignKey(verbose_name='Ocupa\xe7\xe3o', to='core.Ocupacao')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Endere\xe7o',
            },
        ),
        migrations.CreateModel(
            name='PublicoFocal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=80, verbose_name=b'Nome')),
            ],
            options={
                'verbose_name': 'Publico Focal',
                'verbose_name_plural': 'Publicos Focal',
            },
        ),
        migrations.AddField(
            model_name='profile',
            name='publico_focal',
            field=models.ManyToManyField(to='core.PublicoFocal'),
        ),
        migrations.AddField(
            model_name='profile',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
