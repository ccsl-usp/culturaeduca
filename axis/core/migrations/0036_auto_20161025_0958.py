# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0035_auto_20161017_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='cep',
            field=models.CharField(help_text='formato: 12232054', max_length=8),
        ),
    ]
