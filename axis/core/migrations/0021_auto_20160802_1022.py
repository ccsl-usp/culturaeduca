# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0020_auto_20160802_1021'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profile',
            old_name='area_estudo_pesquisa',
            new_name='area_pesquisa',
        ),
    ]
