# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20160728_1557'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='areaatuacao',
            options={'ordering': ('nome',), 'verbose_name': 'Area de Atua\xe7\xe3o', 'verbose_name_plural': 'Areas de Atuac\xe3o'},
        ),
        migrations.AlterModelOptions(
            name='areaestudopesquisa',
            options={'ordering': ('nome',), 'verbose_name': 'Area de Estudo ou Pesquisa', 'verbose_name_plural': 'Areas de Estudo ou Pesquisa'},
        ),
        migrations.AlterModelOptions(
            name='identidadeetinicocultural',
            options={'ordering': ('nome',), 'verbose_name': 'etinia', 'verbose_name_plural': 'etinias'},
        ),
        migrations.AlterModelOptions(
            name='linguagemartistica',
            options={'ordering': ('nome',), 'verbose_name': 'Linguagem Artistica', 'verbose_name_plural': 'Linguagens Art\xedsticas'},
        ),
        migrations.AlterModelOptions(
            name='manifestacaocultural',
            options={'ordering': ('name',), 'verbose_name': 'Manifesta\xe7\xe3o Cultural', 'verbose_name_plural': 'Manifesta\xe7\xf5es Culturais'},
        ),
        migrations.AlterModelOptions(
            name='nivelescolaridade',
            options={'ordering': ('nome',), 'verbose_name': 'N\xedvel de escolaridade', 'verbose_name_plural': 'N\xedveis de escolaridade'},
        ),
        migrations.AlterModelOptions(
            name='ocupacao',
            options={'ordering': ('nome',), 'verbose_name': 'Ocupa\xe7\xe3o', 'verbose_name_plural': 'Ocupa\xe7\xf5es'},
        ),
        migrations.AlterModelOptions(
            name='profile',
            options={'ordering': ('user__nome', 'data_inicio'), 'verbose_name': 'Perfil', 'verbose_name_plural': 'Perfis'},
        ),
        migrations.AlterModelOptions(
            name='publicofocal',
            options={'ordering': ('nome',), 'verbose_name': 'Publico Focal', 'verbose_name_plural': 'Publicos Focal'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'ordering': ('nome', 'email'), 'verbose_name': 'Usu\xe1rio', 'verbose_name_plural': 'Usu\xe1rios'},
        ),
    ]
