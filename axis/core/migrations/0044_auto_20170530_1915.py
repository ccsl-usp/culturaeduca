# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0043_auto_20170530_1903'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='abrangencia',
            field=models.SmallIntegerField(verbose_name='Abrang\xeancia Geogr\xe1fica da atua\xe7\xe3o', choices=[(1, 'Local'), (2, 'Municipal'), (5, 'Intermunicipal'), (3, 'Estadual'), (4, 'Nacional')]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='atua_desde',
            field=models.PositiveSmallIntegerField(help_text='formato: AAAA', null=True, verbose_name='Atua nesta(s) \xe1rea(s) desde que ano?'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='publico_focal',
            field=models.ManyToManyField(to='core.PublicoFocal', verbose_name='P\xfablico Focal'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='tipo_espaco',
            field=models.ManyToManyField(to='core.TipoEspaco', verbose_name='Tipo de espa\xe7o/local de atua\xe7\xe3o'),
        ),
    ]
