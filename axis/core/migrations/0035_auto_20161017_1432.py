# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import axis.core.models
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0034_auto_20161017_1059'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(storage=django.core.files.storage.FileSystemStorage(), max_length=1024, upload_to=axis.core.models.UploadToPathAndRename(b'agente/avatar'), blank=True),
        ),
    ]
