# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0027_remove_redesocial_nome'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='atuacao',
            field=models.TextField(null=True, blank=True, verbose_name='Breve Hist\xf3rico de Atua\xe7\xe3o'),
        ),
    ]
