# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0032_auto_20161014_1810'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='identidadeetinicocultural',
            options={'ordering': ('nome',), 'verbose_name': 'Identidade \xc9tnico-cultural', 'verbose_name_plural': 'Identidades \xc9tnico-culturais'},
        ),
        migrations.RenameField(
            model_name='profile',
            old_name='etinia',
            new_name='etnia',
        ),
        migrations.AlterField(
            model_name='profile',
            name='etnia',
            field=models.ForeignKey(verbose_name='Identidade \xc9tnico-cultural', blank=True, to='core.IdentidadeEtinicoCultural', null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='tipo_espaco',
            field=models.ManyToManyField(to='core.TipoEspaco', verbose_name='Tipo de espa\xe7o de atua\xe7\xe3o'),
        ),
    ]
