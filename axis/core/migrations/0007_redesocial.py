# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20160729_1013'),
    ]

    operations = [
        migrations.CreateModel(
            name='RedeSocial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_add', models.DateTimeField(auto_now_add=True)),
                ('date_upd', models.DateTimeField(auto_now=True)),
                ('plataforma', models.SmallIntegerField(verbose_name='Plataforma', choices=[(1, 'Facebook'), (2, 'Twitter'), (3, 'Youtube'), (4, 'Google+'), (5, 'Instagram'), (6, 'Telegram'), (7, 'Snapchat'), (8, 'Skype'), (10, 'Pinterest'), (11, 'Tumbrl')])),
                ('link', models.URLField(null=True, verbose_name='Link', blank=True)),
                ('nome', models.CharField(max_length=150, null=True, verbose_name='Nome', blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('user_add', models.ForeignKey(related_name='core_redesocial_created_by', editable=False, to=settings.AUTH_USER_MODEL)),
                ('user_upd', models.ForeignKey(related_name='core_redesocial_modified_by', editable=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('plataforma', 'date_add'),
                'abstract': False,
                'verbose_name': 'Rede Social',
                'verbose_name_plural': 'Redes Sociais',
            },
        ),
    ]
