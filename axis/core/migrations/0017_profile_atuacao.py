# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_profile_atua_desde'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='atuacao',
            field=models.TextField(default='.', verbose_name='Breve Hist\xf3rico de Atua\xe7\xe3o'),
            preserve_default=False,
        ),
    ]
