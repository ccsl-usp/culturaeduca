# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0048_auto_20170816_1844'),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemGaleria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('arquivo', models.FileField(upload_to=b'galeria', verbose_name='Arquivo')),
                ('data', models.DateTimeField(auto_now_add=True)),
                ('tipo', models.SmallIntegerField(blank=True, choices=[(1, '\xc1udiovisual'), (2, 'Imagem'), (3, 'Documento')])),
            ],
            options={
                'ordering': ('data',),
                'verbose_name': 'Item de Galeria',
                'verbose_name_plural': 'Itens de Galeria',
            },
        ),
    ]
