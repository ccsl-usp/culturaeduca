# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0051_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='itemgaleria',
            options={'ordering': ('-date_add',), 'verbose_name': 'Item de Galeria', 'verbose_name_plural': 'Itens de Galeria'},
        ),
        migrations.RenameField(
            model_name='itemgaleria',
            old_name='data',
            new_name='date_add',
        ),
        migrations.AlterField(
            model_name='profile',
            name='raca_cor_ibge',
            field=models.SmallIntegerField(help_text='As op\xe7\xf5es desse campo s\xe3o definidas pelo IBGE para o Censo Demog\u0155afico. ', verbose_name='Ra\xe7a/Cor segundo o IBGE', choices=[(3, 'Amarela'), (4, 'Branca'), (5, 'Ind\xedgena'), (6, 'Parda'), (7, 'Preta'), (1, 'N\xe3o informar')]),
        ),
    ]
