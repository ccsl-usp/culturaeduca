# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0041_auto_20170530_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='atua_desde',
            field=models.PositiveSmallIntegerField(help_text='formato: AAAA', null=True, verbose_name='Atua nesta(s) \xe1rea(s) desde:'),
        ),
    ]
