# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20160728_1448'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='data_nascimento',
            field=models.DateField(help_text=b'formato: dd/mm/yyyy', verbose_name='Data de nascimento'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='telefone1',
            field=models.CharField(help_text=b'informe o (ddd)!', max_length=25, null=True, verbose_name=b'telefone 1'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='telefone2',
            field=models.CharField(help_text=b'informe o (ddd)!', max_length=25, null=True, verbose_name=b'telefone 2', blank=True),
        ),
    ]
