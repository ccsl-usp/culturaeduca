# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_profile_manifestacao_cultural'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='atua_desde',
            field=models.DateField(null=True, verbose_name='Atua desde', blank=True),
        ),
    ]
