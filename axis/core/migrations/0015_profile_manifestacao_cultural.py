# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20160801_1716'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='manifestacao_cultural',
            field=models.ManyToManyField(to='core.ManifestacaoCultural', verbose_name='Manifesta\xe7\xe3o Cultural'),
        ),
    ]
