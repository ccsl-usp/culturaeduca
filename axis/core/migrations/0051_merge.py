# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0049_auto_20171002_1702'),
        ('core', '0050_user_galeria'),
    ]

    operations = [
    ]
