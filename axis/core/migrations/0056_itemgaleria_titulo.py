# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0055_auto_20171127_1305'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemgaleria',
            name='titulo',
            field=models.CharField(default='Sem t\xedtulo', max_length=54, verbose_name='T\xedtulo'),
            preserve_default=False,
        ),
    ]
