# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0040_auto_20170524_2030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='telefone1',
            field=models.CharField(help_text='informe o (ddd)!', max_length=25, null=True, verbose_name='telefone 1', blank=True),
        ),
    ]
