# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0047_auto_20170716_1547'),
    ]

    operations = [
        migrations.AlterField(
            model_name='redesocial',
            name='plataforma',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Plataforma', choices=[(1, 'Google+'), (2, 'Facebook'), (3, 'Instagram'), (4, 'Pinterest'), (5, 'Snapchat'), (7, 'Skype'), (9, 'Tumbrl'), (8, 'Twitter'), (10, 'Youtube')]),
        ),
    ]
