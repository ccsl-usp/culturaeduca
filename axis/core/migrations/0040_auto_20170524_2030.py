# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0039_auto_20170104_1729'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='atua_desde',
            field=models.PositiveSmallIntegerField(help_text='formato: AAAA', null=True, verbose_name='Atua nesta(s) \xe1rea(s) desde:', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='raca_cor_ibge',
            field=models.SmallIntegerField(verbose_name='Ra\xe7a/Cor IBGE', choices=[(3, 'Amarela'), (4, 'Branca'), (5, 'Ind\xedgena'), (6, 'Parda'), (7, 'Preta'), (2, 'Outra'), (1, 'N\xe3o informar')]),
        ),
    ]
