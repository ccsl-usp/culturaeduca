# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def altera_choice_outros(apps, schema_editor):
    AreaAtuacao = apps.get_model("core", "AreaAtuacao")
    Ocupacao = apps.get_model("core", "Ocupacao")
    TipoEspaco = apps.get_model("core", "TipoEspaco")

    area_atuacao = AreaAtuacao.objects.get(pk=97)
    ocupacao = Ocupacao.objects.get(pk=153)
    tipo_espaco = TipoEspaco.objects.get(pk=27)

    area_atuacao.nome = "Outros"
    ocupacao.nome = "Outros"
    tipo_espaco.nome = "Outros"

    area_atuacao.save()
    ocupacao.save()
    tipo_espaco.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0037_auto_20161026_1500'),
    ]

    operations = [
        migrations.RunPython(altera_choice_outros),
    ]
