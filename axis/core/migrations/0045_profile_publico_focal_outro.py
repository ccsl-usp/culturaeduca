# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0044_auto_20170530_1915'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='publico_focal_outro',
            field=models.CharField(max_length=512, verbose_name='Outro Tipo de P\xfablico Focal', blank=True),
        ),
    ]
