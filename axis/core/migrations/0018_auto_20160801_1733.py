# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_profile_atuacao'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='atua_desde',
            field=models.DateField(help_text='formato: dd/mm/yyyy', null=True, verbose_name='Atua desde', blank=True),
        ),
    ]
