# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20160801_1457'),
    ]

    operations = [
        migrations.CreateModel(
            name='TipoEspaco',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Tipo(s) de Espa\xe7o(s) de Atua\xe7\xe3o')),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'Tipo de espa\xe7o de atua\xe7\xe3o',
                'verbose_name_plural': 'Tipos de espa\xe7os de atua\xe7\xe3o',
            },
        ),
    ]
