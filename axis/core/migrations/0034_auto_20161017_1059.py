# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0033_auto_20161017_0930'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(storage=django.core.files.storage.FileSystemStorage(), max_length=1024, upload_to=b'agente/avatar', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='area_pesquisa',
            field=models.ManyToManyField(to='core.AreaEstudoPesquisa', verbose_name='\xc1rea de Estudo e Pesquisa', blank=True),
        ),
    ]
