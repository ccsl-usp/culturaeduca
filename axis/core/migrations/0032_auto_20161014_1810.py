# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0031_profile_identidade_genero'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='genero',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='sexo',
        ),
    ]
