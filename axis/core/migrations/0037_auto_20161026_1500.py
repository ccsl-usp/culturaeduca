# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0036_auto_20161025_0958'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='etnia_outro',
            field=models.CharField(max_length=200, verbose_name='Outra Etnia', blank=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='ocupacao_outro',
            field=models.CharField(max_length=200, verbose_name='Outra Ocupa\xe7ao', blank=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='raca_cor_ibge_outro',
            field=models.CharField(max_length=200, verbose_name='Outra Ra\xe7a/Cor IBGE', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='raca_cor_ibge',
            field=models.SmallIntegerField(verbose_name='Ra\xe7a/Cor IBGE', choices=[(3, 'amarela'), (4, 'branca'), (5, 'indigena'), (6, 'parda'), (7, 'preta'), (2, 'outros'), (1, 'n\xe3o informar')]),
        ),
        migrations.AddField(
            model_name='profile',
            name='area_atuacao_outro',
            field=models.CharField(max_length=512, verbose_name='Outra \xc1rea de Atua\xe7\xe3o', blank=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='tipo_espaco_outro',
            field=models.CharField(max_length=512, verbose_name='Outro Tipo de espa\xe7o de atua\xe7\xe3o', blank=True),
        ),
    ]
