# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0046_profile_manifestacao_cultural_outro'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='manifestacao_cultural_outro',
            field=models.CharField(max_length=512, null=True, verbose_name='Outro Tipo de Manifesta\xe7\xe3o Cultural', blank=True),
        ),
    ]
