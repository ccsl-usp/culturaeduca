# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ocupacao',
            name='nome',
            field=models.CharField(max_length=80, verbose_name='Ocupa\xe7\xe3o'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='area_estudo_pesquisa',
            field=models.ManyToManyField(to='core.AreaEstudoPesquisa', verbose_name='\xc1rea Estudo/Pesquisa'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='genero',
            field=models.SmallIntegerField(verbose_name=b'G\xc3\xaanero', choices=[(1, b'n\xc3\xa3o informar'), (2, b'outros'), (3, b'transg\xc3\xaanero'), (4, b'cisgen\xc3\xaaro')]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='linguagens_artisticas',
            field=models.ManyToManyField(to='core.LinguagemArtistica', verbose_name='Linguagens Art\xedtica'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='nome',
            field=models.CharField(max_length=80, null=True, verbose_name=b'Nome', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='telefone1',
            field=models.CharField(max_length=25, null=True, verbose_name=b'telefone 1', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='telefone2',
            field=models.CharField(max_length=25, null=True, verbose_name=b'telefone 2', blank=True),
        ),
    ]
