# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0048_auto_20170816_1844'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='raca_cor_ibge_outro',
        ),
        migrations.AlterField(
            model_name='profile',
            name='atua_desde',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Atua nesta(s) \xe1rea(s) desde que ano?'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='cep',
            field=models.CharField(max_length=8),
        ),
        migrations.AlterField(
            model_name='profile',
            name='data_nascimento',
            field=models.DateField(verbose_name='Data de nascimento'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='etnia',
            field=models.ForeignKey(blank=True, to='core.IdentidadeEtinicoCultural', help_text=' ', null=True, verbose_name='Identidade \xc9tnico-cultural'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='identidade_genero',
            field=models.SmallIntegerField(help_text='G\xeanero com  o  qual  uma  pessoa  se  identifica,  que  pode  ou  n\xe3o  concordar com o g\xeanero que lhe foi  atribu\xeddo quando de seu  nascimento. O que determina a identidade de g\xeanero \xe9 a maneira como a pessoa se sente e se percebe, assim como a forma que esta deseja ser reconhecida pelas outras pessoas.', verbose_name='Identidade de g\xeanero', choices=[(1, 'Mulher'), (2, 'Homem'), (3, 'Mulher Trans'), (4, 'Homem Trans'), (5, 'Travesti')]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='ocupacao',
            field=models.ForeignKey(verbose_name='Ocupa\xe7\xe3o', to='core.Ocupacao', help_text='As op\xe7\xf5es oferecidas neste campo fazem parte de uma sele\xe7\xe3o de ocupa\xe7\xf5es da Classifica\xe7\xe3o Brasileira de Ocupa\xe7\xf5es (CBO), de responsabilidade do Minist\xe9rio do Trabalho e Emprego. <b>Caso sua ocupa\xe7\xe3o n\xe3o esteja comtemplada, digite OUTRA e preencha no campo que ir\xe1 abrir abaixo.</b> H\xe1 tamb\xe9m a op\xe7\xe3o SEM OCUPA\xc7\xc3O.'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='raca_cor_ibge',
            field=models.SmallIntegerField(help_text='Categorias definidas pelo IBGE para o Censo Demog\u0155afico', verbose_name='Ra\xe7a/Cor segundo o IBGE', choices=[(3, 'Amarela'), (4, 'Branca'), (5, 'Ind\xedgena'), (6, 'Parda'), (7, 'Preta'), (1, 'N\xe3o informar')]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='telefone1',
            field=models.CharField(max_length=25, null=True, verbose_name='telefone 1', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='telefone2',
            field=models.CharField(max_length=25, null=True, verbose_name='telefone 2', blank=True),
        ),
    ]
