# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_auto_20160913_1536'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='nivelescolaridade',
            options={'ordering': ('indice',), 'verbose_name': 'N\xedvel de escolaridade', 'verbose_name_plural': 'N\xedveis de escolaridade'},
        ),
        migrations.AddField(
            model_name='nivelescolaridade',
            name='indice',
            field=models.SmallIntegerField(default=0, verbose_name='N\xedvel'),
        ),
    ]
