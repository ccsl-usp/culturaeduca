# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_atua_desde_int(apps, schema_editor):
    Profile = apps.get_model('core', 'Profile')
    for profile in Profile.objects.filter(atua_desde__isnull=False):
        profile.atua_desde_int = profile.atua_desde.year
        profile.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0028_auto_20161013_1736'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='atua_desde_int',
            field=models.PositiveSmallIntegerField(help_text='formato: AAAA', null=True, verbose_name='Atua desde', blank=True),
        ),
        migrations.RunPython(set_atua_desde_int),
        migrations.RemoveField(
            model_name='profile',
            name='atua_desde',
        ),
        migrations.RenameField(
            model_name='profile',
            old_name='atua_desde_int',
            new_name='atua_desde',
        ),
    ]
