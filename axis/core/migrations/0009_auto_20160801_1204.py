# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20160801_1159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='linguagens_artisticas',
            field=models.ManyToManyField(to='core.LinguagemArtistica', null=True, verbose_name='Linguagens Art\xedtica'),
        ),
    ]
