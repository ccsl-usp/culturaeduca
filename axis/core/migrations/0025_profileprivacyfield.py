# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_auto_20160914_1839'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfilePrivacyField',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='Profile attribute')),
                ('public', models.BooleanField(default=False, verbose_name='Public')),
                ('user', models.ForeignKey(related_name='privacy_fields', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
