# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_profile_tipo_espaco'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tipoespaco',
            options={'ordering': ('nome',), 'verbose_name': 'Tipo de espa\xe7o de atua\xe7\xe3o', 'verbose_name_plural': 'Tipos de espa\xe7os de atua\xe7\xe3o'},
        ),
        migrations.RenameField(
            model_name='tipoespaco',
            old_name='name',
            new_name='nome',
        ),
    ]
