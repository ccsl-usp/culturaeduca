# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_tipoespaco'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='manifestacaocultural',
            options={'ordering': ('nome',), 'verbose_name': 'Manifesta\xe7\xe3o Cultural', 'verbose_name_plural': 'Manifesta\xe7\xf5es Culturais'},
        ),
        migrations.RenameField(
            model_name='manifestacaocultural',
            old_name='name',
            new_name='nome',
        ),
    ]
