# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0038_auto_20161027_1808'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='etnia_outro',
            field=models.CharField(max_length=200, null=True, verbose_name='Outra Etnia', blank=True),
        ),
    ]
