# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


SEXO = {
    'FEMININO': 3,
    'MASCULINO': 4,
}

GENERO = {
    'TRANSGENERO': 3,
    'CISGENERO': 4,
}

IDENTIDADE_GENERO = {
    'MULHER': 1,
    'HOMEM': 2,
    'MULHER_TRANS': 3,
    'HOMEM_TRANS': 4,
    'TRAVESTI': 5,
}

def get_identidade_genero(sexo, genero, Profile):
    if sexo == SEXO['FEMININO']:
        if genero == GENERO['TRANSGENERO']:
            return IDENTIDADE_GENERO['MULHER_TRANS']
        return IDENTIDADE_GENERO['MULHER']

    if sexo == SEXO['MASCULINO']:
        if genero == GENERO['TRANSGENERO']:
            return IDENTIDADE_GENERO['HOMEM_TRANS']
        return IDENTIDADE_GENERO['HOMEM']


    return None

def set_identidade_genero(apps, schema_editor):
    Profile = apps.get_model('core', 'Profile')
    for profile in Profile.objects.all():
        identidade_genero = get_identidade_genero(sexo=profile.sexo, genero=profile.genero, Profile=Profile)
        if identidade_genero:
            profile.identidade_genero = identidade_genero
            profile.save()

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0030_auto_20161014_1434'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='identidade_genero',
            field=models.SmallIntegerField(default=1, verbose_name='Identidade de g\xeanero', choices=[(1, 'Mulher'), (2, 'Homem'), (3, 'Mulher Trans'), (4, 'Homem Trans'), (5, 'Travesti')]),
            preserve_default=False,
        ),
        migrations.RunPython(set_identidade_genero),
    ]
