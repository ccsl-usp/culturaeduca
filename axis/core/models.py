# -*- coding: utf-8 -*-

import os
from PIL import ImageOps
from PIL.ExifTags import TAGS
from cStringIO import StringIO
from django.core.files.uploadedfile import SimpleUploadedFile

from django.conf import settings
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)
from django.db.models import Case, When, Q, Value
from django.apps import apps
from django.contrib.gis.db import models
from django.contrib.sites.models import Site
from django.core.files.storage import get_storage_class
from django.core.urlresolvers import reverse_lazy, reverse
from django.template.loader import get_template
from django.utils import timezone
from django.utils.deconstruct import deconstructible
from django.utils.http import int_to_base36
from municipios.models import Municipio
from utils import equipamentos_entorno
from uuid import uuid4

from PIL import Image


avatar_storage = get_storage_class(settings.DEFAULT_FILE_STORAGE)()


@deconstructible
class UploadToPathAndRename(object):

    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join(self.sub_path, filename)


class Endereco(models.Model):
    '''
    Modelo ZNC / NECTO padrão para endereço.
    Depende do app  django-municipios.
    **IMPORTANTE** -> personalizar o Manager na classe filha para GeoManager.

    Attributes:

        CEP(string):  gravado sem traço. (sp começa com 0 (zero))
        municipio(integer): refere-se a Código do IBGE,
                            depende da app django-municipios
                            (projeto opensource da ZNC/Necto)
        point(geom Point):

    '''

    cep = models.CharField(max_length=8)  # 12232060
    logradouro = models.CharField(max_length=100)
    numero = models.CharField(verbose_name=u'Número', max_length=50)
    complemento = models.CharField(max_length=100, null=True, blank=True)
    bairro = models.CharField(max_length=100)
    municipio = models.ForeignKey(Municipio)  # como código do IBGE
    point = models.PointField(srid=settings.SRID, null=True, blank=True)

    def __unicode__(self):
        return u"%s - %s" % (self.get_cep(), self.numero)

    @property
    def endereco(self):
        res = [self.logradouro or u'']
        if self.numero:
            res.append(self.numero or u'')
        if self.cep:
            res.append(u'CEP: {}'.format(self.get_cep()))

        return u' - '.join(res)

    def get_cep(self):
        cep_mascarado = self.cep[0:5] + "-" + self.cep[5:]
        return cep_mascarado

    class Meta:
        abstract = True


SEXO_CHOICES = (
    ('M', u'Masculino'),
    ('F', u'Feminino')
)


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError('O email é obrigatório')
        email = UserManager.normalize_email(email)
        user = self.model(email=email, is_active=True, is_superuser=False,
                          last_login=now, date_joined=now, **extra_fields)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        user = self.create_user(email, password, **extra_fields)
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class ItemGaleria(models.Model):
    '''
    Se o item da galeria for do tipo audiovisual então trata-se de um link
    para um video no Youtube/Vimeo/Etc.

    Neste caso os campos preenchidos serão:
    - tipo == 1
    - descricao
    - titulo
    - data
    - local
    - url

    Se o item da galeria for do tipo imagem então trata-se de um arquivo
    visual, salvo no sistema de arquivos.

    Neste caso os campos preenchidos serão:
    - tipo == 2
    - descricao
    - titulo
    - data
    - local
    - arquivo
    - thumbnail         <-- automático

    Se o item da galeria for do tipo documento então trata-se de um arquivo
    não-visual, salvo no sistema de arquivos.

    Neste caso os campos preenchidos serão:
    - tipo == 3
    - descricao
    - titulo
    - data
    - local
    - arquivo
    '''
    class TIPO:
        AUDIOVISUAL = 1
        IMAGEM = 2
        DOCUMENTO = 3
        CHOICES = (
            (AUDIOVISUAL, u'Áudiovisual'),
            (IMAGEM, u'Imagem'),
            (DOCUMENTO, u'Documento'),
        )

    arquivo = models.FileField(
        u'Arquivo',
        upload_to='galeria',
        null=True, blank=True,
    )

    descricao = models.TextField(u'Descrição', null=False, blank=False)
    titulo = models.CharField(u'Título', null=False,
                              blank=False, max_length=100)
    data = models.DateField(u'Data', null=True, blank=True)
    local = models.CharField(u'Local', max_length=150, null=True, blank=True)
    tipo = models.SmallIntegerField(
        u'Tipo de arquivo', null=False, blank=True, choices=TIPO.CHOICES)
    url = models.CharField(u'URL', max_length=255, null=True, blank=True)
    thumbnail = models.FileField(
        u'Thumbnail',
        upload_to='galeria',
        null=True, blank=True,
    )

    def create_thumbnail(self):
        if self.tipo != ItemGaleria.TIPO.IMAGEM:
            return

        fileName, fileExtension = os.path.splitext(self.arquivo.path)

        if fileExtension.lower() == '.jpg' or fileExtension.lower() == '.jpeg':
            DJANGO_TYPE = 'image/jpeg'
        elif fileExtension.lower() == '.png':
            DJANGO_TYPE = 'image/png'
        else:
            return

        if DJANGO_TYPE == 'image/jpeg':
            PIL_TYPE = 'jpeg'
            FILE_EXTENSION = 'jpg'
        elif DJANGO_TYPE == 'image/png':
            PIL_TYPE = 'png'
            FILE_EXTENSION = 'png'

        image = Image.open(StringIO(self.arquivo.read()))

        if FILE_EXTENSION == 'jpg':
            exif = image._getexif()
            if exif is not None:
                for tag, value in exif.items():
                    decoded = TAGS.get(tag, tag)
                    if decoded == 'Orientation':
                        if value == 3:
                            image = image.rotate(180)
                        if value == 6:
                            image = image.rotate(270)
                        if value == 8:
                            image = image.rotate(90)
                        break

        image.save(self.arquivo.path, overwrite=True)

        thumb = ImageOps.fit(
            image.copy(),
            (120, 120,),
            Image.ANTIALIAS,
            0,
            (0.5, 0.5)
        )

        # Save the thumbnail
        temp_handle = StringIO()
        thumb.save(temp_handle, PIL_TYPE, quality=100)
        temp_handle.seek(0)

        # Save image to a SimpleUploadedFile which can be saved into
        # ImageField
        suf = SimpleUploadedFile(
            os.path.split(self.arquivo.name)[-1],
            temp_handle.read(),
            content_type=DJANGO_TYPE
        )

        # Save SimpleUploadedFile into image field
        self.thumbnail.save(
            '{0}_thumbnail_{1}_120x120.png'.format(
                os.path.splitext(self._smart_str(suf.name))[0],
                FILE_EXTENSION
            ),
            suf,
            save=False
        )

    def _smart_str(self, text):
        if isinstance(text, unicode):
            return unicode(text).encode("utf-8")
        elif isinstance(text, int) or isinstance(text, float):
            return str(text)
        return text

    def video_data_type(self):
        if self.tipo != ItemGaleria.TIPO.AUDIOVISUAL:
            return

        if 'vimeo' in self.url:
            return 'vimeo'
        elif 'youtube' in self.url:
            return 'youtube'
        else:
            return 'unsupported'

    def video_data_videoid(self):
        if self.tipo != ItemGaleria.TIPO.AUDIOVISUAL:
            return

        if 'vimeo' in self.url:
            return self.url.replace('https://vimeo.com/', '')
        elif 'youtube' in self.url:
            return self.url.replace('https://www.youtube.com/watch?v=', '')
        else:
            return 'unsupported'

    def full_description(self):
        meta_list = []
        if self.data:
            meta_list.append(self.data.strftime("%d/%m/%Y"))

        if self.local:
            meta_list.append(self.local)

        if meta_list:
            meta_data = '[%s]' % (
                ' - '.join(meta_list)
            )
        else:
            meta_data = ''

        return '%s %s' % (
            self.titulo,
            meta_data
        )

    class Meta:
        verbose_name = u'Item de Galeria'
        verbose_name_plural = u'Itens de Galeria'
        ordering = ('pk', )

    def __unicode__(self):
        return self.get_tipo_display()


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('e-mail', unique=True)

    nome = models.CharField(
        verbose_name=u'Nome ou nome social', max_length=100)

    is_active = models.BooleanField('ativo', default=False,)
    is_email_verified = models.BooleanField(
        'E-mail verificado', default=False,)
    is_staff = models.BooleanField('administrador', default=False,)
    date_joined = models.DateTimeField(
        'data de cadastro', default=timezone.now)

    last_emailverification = models.DateTimeField(default=timezone.now)

    galeria = models.ManyToManyField(ItemGaleria)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('nome', )

    class Meta:
        verbose_name = u'Usuário'
        verbose_name_plural = u'Usuários'
        ordering = ('nome', 'email')

    @property
    def tipo(self):
        return u'Agente Pessoa Física'

    @property
    def projetos(self):
        '''
        Projetos do User, ele é o "dono"
        '''
        return apps.get_model('acao', 'Projeto').objects.filter(agente__isnull=True, user_add=self.pk)

    @property
    def atividades(self):
        '''
        Atividades do User, ele é o "dono"
        '''
        return apps.get_model('acao', 'Atividade').objects.filter(agente__isnull=True, user_add=self.pk)

    @property
    def acoes(self):
        '''
        Ações do User, ele é o "dono"
        '''
        return apps.get_model('acao', 'Acao').objects.filter(agente__isnull=True, user_add=self.pk)

    @property
    def all_acoes(self):
        '''Retorna todas as ações em que o user interage.
        '''
        Acao = apps.get_model("acao", "Acao")
        agentes_pks = self.agentes_vinculados.values_list("pk", flat=True) or [
            0]
        q = Q(acaomembro__user=self) | Q(
            acaoagentevinculo__agente__pk__in=agentes_pks)
        acoes = Acao.objects.filter(q).exclude(
            user_add=self,
            agente__isnull=True
        ).order_by('pk').distinct('pk')

        return acoes.annotate(
            sou_membro=Case(
                When(Q(acaomembro__user=self), then=Value(True)),
                default=Value(False),
                output_field=models.BooleanField()
            ),
            acao_outro_agente=Case(
                When(Q(acaoagentevinculo__agente__pk__in=agentes_pks),
                     then=Value(True)),
                default=Value(False),
                output_field=models.BooleanField()
            )
        )

    @property
    def acoes_projetos(self):
        """Retorna todos os projetos que o user interage.
        """
        return self.all_acoes.filter(projeto__isnull=False)

    @property
    def acoes_atividades(self):
        """Retorna todos as atividades que o user interage.
        """
        return self.all_acoes.filter(atividade__isnull=False)

    @property
    def total_rede(self):
        agentes = self.agentes.count()
        agentes_vinculados = self.agentes_vinculados.count()
        # acoes = self.acoes_projetos.count() + self.acoes_atividades.count()
        return agentes + agentes_vinculados

    @property
    def agentes(self):
        '''
        Agentes que o User é Membro
        '''
        return apps.get_model('agente', 'Agente').objects.filter(pk__in=self.agentemembro_set.values_list('agente__pk', flat=True))

    @property
    def agentes_vinculados(self):
        pks = self.uservinculo_set.all().values_list('agente', flat=True)
        return apps.get_model('agente', 'Agente').objects.filter(pk__in=pks)

    def transformed_vinculos(self):
        """Transforma ponto para o SRID do mapa, utilizado na aba rede."""
        agentes = self.agentes_vinculados
        agentes.transform(3857)
        return agentes

    def get_profile(self):
        if not hasattr(self, '_profile'):
            try:
                self._profile = self.profile_set.get()
            except Profile.DoesNotExist:
                self._profile = None
        return self._profile

    def __unicode__(self):
        return self.email

    def get_full_name(self):
        return self.nome

    def get_short_name(self):
        return self.nome.split(' ')[0]

    def get_username(self):
        return self.email

    def is_owner(self, user):
        return not user.is_anonymous() and self == user

    def can_change_gallery(self, user):
        return any([user.is_superuser, self.is_owner(user)])

    def user_agentes(self):
        return self.agentemembro_set.filter(agente__user_add=self).values_list('agente', flat=True)

    def get_absolute_url(self):
        return reverse_lazy('profile_detail', args=(self.pk, ))

    def get_privacy(self, nocache=False):
        if nocache is True:
            delattr(self, '_privacy_objects')

        if not hasattr(self, '_privacy'):
            self._privacy = dict(
                self.privacy_fields.values_list('name', 'public'))
        return self._privacy

    def get_privacy_objects(self, nocache=False):
        if nocache is True:
            delattr(self, '_privacy_objects')

        if not hasattr(self, '_privacy_objects'):
            self._privacy_objects = dict([(field.name, {
                'pk': field.pk,
                'public': field.public,
            }) for field in self.privacy_fields.all()])

        return self._privacy_objects

    def hidden_email(self, keep_length=False):
        pos = self.email.find('@')
        half = pos / 2
        stars = ('*' * (pos - half)) if keep_length else '*******'
        return self.email[:half] + stars + self.email[pos:]

    def send_verification_mail(self):
        from django.core.mail import send_mail
        from .tokens import email_verification_token_generator

        current_site = Site.objects.get_current()
        subject = u'[Cultura Educa] - Verificação de E-mail'
        template = get_template(
            'registration/email/email_verification_token.html')

        email = template.render({
            'site_name': current_site.name,
            'uid': int_to_base36(self.pk),
            'token': email_verification_token_generator.make_token(self),
            'domain': current_site.domain,
            'protocol': u'https'
        })

        return send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, [self.email], html_message=email)

    def send_new_member_email(self, agente):
        from django.core.mail import send_mail

        current_site = Site.objects.get_current()
        subject = u'[Cultura Educa] - Novo vínculo em agente'
        template = get_template(
            'agente/email/email_novo_agentemembro_user.html')

        email = template.render({
            'site_name': current_site.name,
            'domain': current_site.domain,
            'protocol': u'https',
            'agente': agente
        })

        return send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, [self.email], html_message=email)

    def send_new_acaomember_email(self, acao):
        from django.core.mail import send_mail

        current_site = Site.objects.get_current()
        subject = u'[Cultura Educa] - Novo vínculo em ação'
        template = get_template('acao/email/email_novo_acaomembro_user.html')

        email = template.render({
            'site_name': current_site.name,
            'domain': current_site.domain,
            'protocol': u'https',
            'acao': acao
        })

        return send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, [self.email], html_message=email)

    def create_privacy_fields(self):
        ProfilePrivacyField.objects.bulk_create([ProfilePrivacyField(
            user=self,
            name=field_name,
            public=field_value
        ) for field_name, field_value in Profile.PRIVACY_DEFAULT_FIELDS])

    def update_privacy_fields(self, delete=True, create=True, reset_values=False):
        PRIVACY_DEFAULT_FIELDS = dict(Profile.PRIVACY_DEFAULT_FIELDS)

        # se create for True (criar privacy fields que não existem)
        if create is True:
            PRIVACY_CURRENT_FIELDS = self.get_privacy_objects()

            # privacy fields que deveriam existir (existe nos valores default porém não está associado ao usuário)
            PRIVACY_FIELDS_TO_CREATE = filter(
                lambda field: field[0] not in PRIVACY_CURRENT_FIELDS, Profile.PRIVACY_DEFAULT_FIELDS)

            ProfilePrivacyField.objects.bulk_create([ProfilePrivacyField(
                user=self,
                name=field_name,
                public=field_value
            ) for field_name, field_value in PRIVACY_FIELDS_TO_CREATE])

        # para cada privacy field atualmente no usuário
        for privacy_field in self.privacy_fields.all():
            if privacy_field.name in PRIVACY_DEFAULT_FIELDS:
                # se reset_values for True e valor difere do original: atualiza o valor
                if reset_values is True and privacy_field.public != PRIVACY_DEFAULT_FIELDS[privacy_field.name]:
                    privacy_field.public = PRIVACY_DEFAULT_FIELDS[privacy_field.name]
                    privacy_field.save()
            else:
                # privacy field não existir mais nos defaults e se delete for True: deleta
                if delete is True:
                    privacy_field.delete()


class IdentidadeEtinicoCultural(models.Model):
    '''
        Afrodescendente
        Caiçara
        Cigana
        Imigrante: abrir lista país: Alemanha, Argélia, Armênia, Angola, Bolívia, Bangladesh, Chile, China, Congo, Cuba, Espanha, Gana, Guiné (Cronacri), Guiné-Bissau, Hungria, Haití, Líbano,  Itália, Japão, Coréia do Sul, Coréia do Norte, Nigéria, Palestina,  Peru,Portugal, Rússia, Senegal, Síria, outros_________
        Indígena
        Quilombola
        Pomerana
        Ribeirinha
        Outros
    '''
    nome = models.CharField('Identidade Étnico-cultural', max_length=100)

    class Meta:
        verbose_name = "Identidade Étnico-cultural"
        verbose_name_plural = "Identidades Étnico-culturais"
        ordering = ('nome', )

    def __unicode__(self):
        return self.nome


class Ocupacao(models.Model):
    # TODO: Define fields here
    nome = models.CharField(u'Ocupação', max_length=100)

    class Meta:
        verbose_name = u"Ocupação"
        verbose_name_plural = u"Ocupações"
        ordering = ('nome', )

    def __unicode__(self):
        return self.nome


class NivelEscolaridade(models.Model):
    nome = models.CharField('Nome', max_length=100)
    indice = models.SmallIntegerField(u'Nível', default=0)

    class Meta:
        verbose_name = u"Nível de escolaridade"
        verbose_name_plural = u"Níveis de escolaridade"
        ordering = ('indice', )

    def __unicode__(self):
        return self.nome


class AreaAtuacao(models.Model):
    nome = models.CharField('Nome', max_length=100)

    class Meta:
        verbose_name = u"Área de Atuação"
        verbose_name_plural = u"Áreas de Atuacão"
        ordering = ('nome', )

    def __unicode__(self):
        return self.nome


class AreaEstudoPesquisa(models.Model):
    nome = models.CharField('Nome', max_length=100)

    class Meta:
        verbose_name = u"Area de Estudo ou Pesquisa"
        verbose_name_plural = u"Areas de Estudo ou Pesquisa"
        ordering = ('nome', )

    def __unicode__(self):
        return self.nome


class PublicoFocal(models.Model):
    nome = models.CharField('Nome', max_length=100)

    class Meta:
        verbose_name = u"Público Focal"
        verbose_name_plural = u"Públicos Focal"
        ordering = ('nome', )

    def __str__(self):
        return self.nome

    def __unicode__(self):
        return self.nome


class LinguagemArtistica(models.Model):
    nome = models.CharField('Nome', max_length=100)

    class Meta:
        verbose_name = u"Linguagem Artistica"
        verbose_name_plural = u"Linguagens Artísticas"
        ordering = ('nome', )

    def __str__(self):
        return self.nome

    def __unicode__(self):
        return self.nome


class ManifestacaoCultural(models.Model):
    nome = models.CharField(u'Nome', max_length=255)

    class Meta:
        verbose_name = u'Manifestação Cultural'
        verbose_name_plural = u'Manifestações Culturais'
        ordering = ('nome', )

    def __unicode__(self):
        return self.nome


class TipoEspaco(models.Model):
    nome = models.CharField(u'Tipo(s) de Espaço(s) de Atuação', max_length=255)

    class Meta:
        verbose_name = u'Tipo de espaço de atuação'
        verbose_name_plural = u'Tipos de espaços de atuação'
        ordering = ('nome', )

    def __unicode__(self):
        return self.nome


class Profile(Endereco):

    class ABRANGENCIA(object):
        LOCAL = 1
        MUNCIPAL = 2
        INTERMUNICIPAL = 5
        ESTADUAL = 3
        NACIONAL = 4

        CHOICES = (
            (LOCAL, u'Local'),
            (MUNCIPAL, u'Municipal'),
            (INTERMUNICIPAL, u'Intermunicipal'),
            (ESTADUAL, u'Estadual'),
            (NACIONAL, u'Nacional'),
        )

    class IDENTIDADE_GENERO(object):
        MULHER_CIS = 1
        HOMEM_CIS = 2
        MULHER_TRANS = 3
        HOMEM_TRANS = 4
        # TRAVESTI = 5
        NAO_BINARIO = 5 # 5 ou 6?

        CHOICES = (
            (MULHER_CIS, u'Mulher Cis'),
            (HOMEM_CIS, u'Homem Cis'),
            (MULHER_TRANS, u'Mulher Trans'),
            (HOMEM_TRANS, u'Homem Trans'),
            # (TRAVESTI, u'Travesti'),
            (NAO_BINARIO, u'Não-binário'),
        )

    class RACA_COR_IBGE(object):
        NAO_INFORMAR = 1
        # OUTROS = 2
        AMARELA = 3
        BRANCA = 4
        INDIGENA = 5
        PARDA = 6
        PRETA = 7

        CHOICES = (
            (AMARELA, u'Amarela'),
            (BRANCA, u'Branca'),
            (INDIGENA, u'Indígena'),
            (PARDA, u'Parda'),
            (PRETA, u'Preta'),
            # (OUTROS, u'Outra'),
            (NAO_INFORMAR, u'Não informar'),
        )

    user = models.ForeignKey(User)

    avatar = models.ImageField(max_length=1024, upload_to=UploadToPathAndRename(
        'agente/avatar'), storage=avatar_storage, blank=True)

    data_inicio = models.DateField(u'Data de início', auto_now_add=True)

    telefone1 = models.CharField(
        u'telefone 1', max_length=25, blank=True, null=True)
    telefone2 = models.CharField(
        u'telefone 2', max_length=25, blank=True, null=True)
    # endereco = models.ForeignKey(Endereco, blank=True, null=True)

    data_nascimento = models.DateField(
        u'Data de nascimento', blank=False, null=False)
    identidade_genero = models.SmallIntegerField(choices=IDENTIDADE_GENERO.CHOICES, verbose_name=u"Identidade de gênero", blank=False, null=False,
                                                 help_text=u"Gênero com  o  qual  uma  pessoa  se  identifica,  que  pode  ou  não  concordar com o gênero que lhe foi  atribuído quando de seu  nascimento. O que determina a identidade de gênero é a maneira como a pessoa se sente e se percebe, assim como a forma que esta deseja ser reconhecida pelas outras pessoas.")
    raca_cor_ibge = models.SmallIntegerField(verbose_name=u'Raça/Cor segundo o IBGE', choices=RACA_COR_IBGE.CHOICES,
                                             blank=False, null=False, help_text=u"As opções desse campo são definidas pelo IBGE para o Censo Demogŕafico. ")
    # raca_cor_ibge_outro = models.CharField(u'Outra Raça/Cor IBGE', max_length=200, blank=True)
    etnia = models.ForeignKey(IdentidadeEtinicoCultural, blank=True, null=True,
                              verbose_name=u'Identidade Étnico-cultural', help_text=u" ")
    etnia_outro = models.CharField(
        verbose_name=u"Outra Etnia", max_length=200, null=True, blank=True)
    ocupacao = models.ForeignKey(Ocupacao, verbose_name=u'Ocupação', blank=False, null=False,
                                 help_text=u"As opções oferecidas neste campo fazem parte de uma seleção de ocupações da Classificação Brasileira de Ocupações (CBO), de responsabilidade do Ministério do Trabalho e Emprego. <b>Caso sua ocupação não esteja comtemplada, digite OUTRA e preencha no campo que irá abrir abaixo.</b> Há também a opção SEM OCUPAÇÃO.")
    ocupacao_outro = models.CharField(
        verbose_name=u"Outra Ocupaçao", max_length=200, blank=True)
    escolaridade = models.ForeignKey(
        NivelEscolaridade, verbose_name=u'Formação/nível de escolaridade', null=False, blank=False)
    area_atuacao = models.ManyToManyField(
        AreaAtuacao, verbose_name=u'área de atuação')
    area_atuacao_outro = models.CharField(
        verbose_name=u'Outra Área de Atuação', max_length=512, blank=True)
    tipo_espaco = models.ManyToManyField(
        TipoEspaco, verbose_name=u'Tipo de espaço/local de atuação')
    tipo_espaco_outro = models.CharField(
        verbose_name=u'Outro Tipo de espaço de atuação', max_length=512, blank=True)
    abrangencia = models.SmallIntegerField(
        verbose_name=u'Abrangência Geográfica da atuação', choices=ABRANGENCIA.CHOICES)
    manifestacao_cultural = models.ManyToManyField(
        ManifestacaoCultural, verbose_name=u'Manifestações Culturais', blank=True)
    manifestacao_cultural_outro = models.CharField(
        verbose_name=u'Outro Tipo de Manifestação Cultural', max_length=512, null=True, blank=True)
    area_pesquisa = models.ManyToManyField(
        AreaEstudoPesquisa, verbose_name=u'Área de Estudo e Pesquisa', blank=True)
    publico_focal = models.ManyToManyField(
        PublicoFocal, verbose_name=u'Público Focal')
    publico_focal_outro = models.CharField(
        verbose_name=u'Outro Tipo de Público Focal', max_length=512, blank=True)
    linguagens_artisticas = models.ManyToManyField(
        LinguagemArtistica, verbose_name=u'Linguagens Artísticas', blank=True)

    atua_desde = models.PositiveSmallIntegerField(
        u'Atua nesta(s) área(s) desde que ano?', null=True, blank=False)

    atuacao = models.TextField(
        null=True, blank=True, verbose_name=u'Breve Histórico de Atuação')

    PRIVACY_DEFAULT_FIELDS = (
        # (profile attribute name, is public default value)
        ('email', True),
        ('telefone1', True),
        ('telefone2', True),
        ('data_nascimento', True),
        ('identidade_genero', True),
        ('raca_cor_ibge', True),
        ('etnia', True),
        ('ocupacao', True),
        ('escolaridade', True),
        ('area_atuacao', True),
        ('tipo_espaco', True),
        ('abrangencia', True),
        ('manifestacao_cultural', True),
        ('area_pesquisa', True),
        ('publico_focal', True),
        ('linguagens_artisticas', True),
        ('atua_desde', True),
        ('atuacao', True),
        ('endereco', False),
    )

    objects = models.GeoManager()

    class Meta:
        verbose_name = u'Perfil'
        verbose_name_plural = u'Perfis'
        ordering = ('user__nome', 'data_inicio')

    def __unicode__(self):
        return u"%s %s - User PK: %s" % (self.user.nome, self.pk, self.user.pk, )

    @classmethod
    def _get_create_url(cls):
        return reverse('user_add')

    @classmethod
    def _tipo(cls):
        return u'Pessoa Física'

    @property
    def nome(self):
        return self.user.nome

    @property
    def email(self):
        return self.user.email

    def get_absolute_url(self):
        return reverse('profile_detail', args=(self.user.pk, ))

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def lista_url(self):
        return reverse('lista_pessoa_fisica')

    @property
    def name(self):
        return self.user.nome

    def get_field_or_other(self, field_name, field_other_name):
        field = getattr(self, field_name, False)
        model = type(field)
        if field and field.pk == model.objects.get(nome="Outros").pk:
            field = getattr(self, field_other_name)
        return field

    def privacy_cep(self):
        return self.cep if self.user.get_privacy()[u'endereco'] else None

    def privacy_raca_cor_ibge(self):
        raca_cor_ibge = self.get_raca_cor_ibge_display()
        # if self.raca_cor_ibge == self.RACA_COR_IBGE.OUTROS:
        #     raca_cor_ibge = self.raca_cor_ibge_outro
        return raca_cor_ibge if self.user.get_privacy()[u'raca_cor_ibge'] else None

    def privacy_etnia(self):
        return self.get_field_or_other(
            'etnia',
            'etnia_outro',
        ) if self.user.get_privacy()[u'etnia'] else None

    def privacy_ocupacao(self):
        return self.get_field_or_other(
            'ocupacao',
            'ocupacao_outro',
        ) if self.user.get_privacy()[u'ocupacao'] else None

    def privacy_area_atuacao(self):
        return self.area_atuacao if self.user.get_privacy()[u'area_atuacao'] else None

    def privacy_logradouro(self):
        return self.logradouro if self.user.get_privacy()[u'endereco'] else None

    def privacy_numero(self):
        return self.numero if self.user.get_privacy()[u'endereco'] else None

    def privacy_bairro(self):
        return self.bairro if self.user.get_privacy()[u'endereco'] else None

    def privacy_municipio(self):
        return self.municipio if self.user.get_privacy()[u'endereco'] else None

    @property
    def local(self):
        return u'%s' % self.municipio if self.user.get_privacy()[u'endereco'] else u''

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio, geom_field='point')

    def __getattr__(self, attr):

        # se o acesso ao attribute tiver o prefixo 'privacy_'
        if attr.startswith('privacy_'):

            # remove o prefixo 'privacy_' em privacy_attr
            privacy_attr = attr.replace('privacy_', '')

            if privacy_attr not in self.user.get_privacy():
                raise Exception(u'Não existe ProfilePrivacyField com o nome "%s" para <User %s>. Opções válidas: %s' % (
                    privacy_attr,
                    self.user,
                    self.user.get_privacy().keys(),))

            if self.user.get_privacy()[privacy_attr]:
                # se user.get_privacy() tiver o privacy_attr sendo True,
                # ou seja, se o usuario permitiu mostrar o attribute (está como publico)
                # retorna o valor do attribute
                return getattr(self, privacy_attr)
            else:
                return None

        # caso nao tenha o prefixo privacy_ retorna o padrão
        return super(Profile, self).__getattr__(attr)

    @property
    def tipo(self):
        return u'Pessoa Física'


class ProfilePrivacyField(models.Model):
    user = models.ForeignKey(User, related_name=u'privacy_fields')
    name = models.CharField(u'Profile attribute',
                            max_length=50, blank=False, null=False)
    public = models.BooleanField(u'Public', default=False)


class UserAddUpd(models.Model):
    user_add = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="%(app_label)s_%(class)s_created_by", editable=False)
    date_add = models.DateTimeField(auto_now_add=True, editable=False)

    user_upd = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="%(app_label)s_%(class)s_modified_by", editable=False)
    date_upd = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class RedeSocialAbstract(UserAddUpd):
    '''É utilizado também pelo modelo de RedeSocial do app Coletivo
    '''
    class PLATAFORMA(object):
        GOOGLE_PLUS = 1
        FACEBOOK = 2
        INSTAGRAM = 3
        PINTEREST = 4
        SNAPCHAT = 5
        # TELEGRAM = 6
        SKYPE = 7
        TWITTER = 8
        TUMBRL = 9
        YOUTUBE = 10

        choices = [
            (GOOGLE_PLUS, u'Google+'),
            (FACEBOOK, u'Facebook'),
            (INSTAGRAM, u'Instagram'),
            (PINTEREST, u'Pinterest'),
            (SNAPCHAT, u'Snapchat'),
            (SKYPE, u'Skype'),
            # (TELEGRAM, u'Telegram'),
            (TUMBRL, u'Tumbrl'),
            (TWITTER, u'Twitter'),
            (YOUTUBE, u'Youtube'),
        ]

    plataforma = models.SmallIntegerField(
        u'Plataforma', choices=PLATAFORMA.choices, null=True, blank=True)
    link = models.URLField(u'Link', null=True, blank=True)

    def __unicode__(self):
        return u"%s" % (self.get_plataforma_display())

    class Meta:
        abstract = True
        verbose_name = u'Rede Social'
        verbose_name_plural = u'Redes Sociais'
        ordering = ('plataforma', 'date_add')


class RedeSocial(RedeSocialAbstract):
    user = models.ForeignKey(User)
