# coding: utf-8
from django import forms
from django.conf import settings
from django.db.models import Q
from django.db.models.query import QuerySet
from django.utils.text import smart_split
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import (
    AuthenticationForm as DjangoAuthenticationForm,
    PasswordResetForm as DjangoPasswordResetForm,
)
from django_select2.forms import ModelSelect2Widget, ModelSelect2MultipleWidget

from axis.agente.models import Agente
from municipios.widgets import SelectMunicipioWidget
from municipios.models import Municipio
from utils import clean_tilestache_cache

from .models import (
    AreaEstudoPesquisa,
    LinguagemArtistica,
    User,
    Profile,
    Endereco,
    RedeSocial,
    Ocupacao,
    AreaAtuacao,
    TipoEspaco,
    IdentidadeEtinicoCultural,
    PublicoFocal,
    ManifestacaoCultural,
    ItemGaleria,
)


class CustomInlineFormSet(forms.BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        super(CustomInlineFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, *args, **kwargs):
        kwargs['request_user'] = self.request_user
        return super(CustomInlineFormSet, self)._construct_form(*args, **kwargs)


class UnicodeModelSelect2MultipleWidget(ModelSelect2MultipleWidget):
    def label_from_instance(self, obj):
        return unicode(obj)


class UnicodeModelSelect2Widget(ModelSelect2Widget):
    def label_from_instance(self, obj):
        return unicode(obj)


class AgenteModelSelect2Widget(ModelSelect2Widget):
    model = Agente
    search_fields = ['nome__icontains', ]

    def label_from_instance(self, obj):
        return u'%s (%s - %s)' % (obj.nome, obj.tipo, obj.municipio.uf, )

    def build_attrs(self, base_attrs, extra_attrs=None):
        attrs = super(AgenteModelSelect2Widget, self).build_attrs(
            base_attrs, extra_attrs=extra_attrs)

        attrs.update({
            'data-minimum-input-length': 3,
        })

        return attrs


class AgenteBaseSearchForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.tipo = kwargs.pop('tipo', u'')
        super(AgenteBaseSearchForm, self).__init__(*args, **kwargs)

        if u'nome' not in self.fields:
            self.fields['nome'] = forms.CharField()

        self.fields['nome'].widget.attrs.update({
            'placeholder': u'Procure pelo nome',
        })
        self.fields['municipio'].widget.attrs.update({
            'data-minimum-input-length': 3,
            'data-placeholder': u'Procure pelo munícipio',
        })

    class Meta:
        fields = (u'nome', u'municipio', )
        widgets = {
            'municipio': UnicodeModelSelect2Widget(model=Municipio, search_fields=['nome__unaccent__icontains']),
            'nome': forms.TextInput(),
        }


DATABASE_ENGINE = settings.DATABASES[settings.DATABASES.keys()[
    0]]['ENGINE'].split('.')[-1]


DEFAULT_STOPWORDS = ('de,o,a,os,as')


class BaseSearchForm(forms.Form):
    """See http://gregbrown.co.nz/code/django-simple-search/ for details"""

    STOPWORD_LIST = DEFAULT_STOPWORDS.split(',')
    DEFAULT_OPERATOR = Q.__and__

    q = forms.CharField(label=_('Search'), required=False)

    def clean_q(self):
        return self.cleaned_data['q'].strip()

    order_by = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )

    class Meta:
        abstract = True

        base_qs = None

        # example: ['name', 'category__name', '@description', '=id']
        search_fields = None

        # should be a list of  pairs, of the form ('field1,field2', WEIGHTING)
        # where WEIGHTING is an integer. Assumes the relevant indexes have been
        # created

        fulltext_indexes = None

    def __init__(self, *args, **kwargs):
        super(BaseSearchForm, self).__init__(*args, **kwargs)
        self.fields['q'].widget.attrs = {
            'placeholder': self.get_q_placeholder()}

    def get_advanced_search_fields(self):
        return [field for idx, field in enumerate(self) if idx not in (0, 1)]

    def get_q_placeholder(self):
        return ''

    def construct_search(self, field_name, first):
        if field_name.startswith('^'):
            if first:
                return "%s__istartswith" % field_name[1:]
            else:
                return "%s__icontains" % field_name[1:]
        elif field_name.startswith('='):
            return "%s__iexact" % field_name[1:]
        elif field_name.startswith('@'):
            if DATABASE_ENGINE == 'mysql':
                return "%s__search" % field_name[1:]
            else:
                return "%s__icontains" % field_name[1:]
        else:
            return "%s__icontains" % field_name

    def get_text_query_bits(self, query_string):
        """filter stopwords but only if there are useful words"""

        split_q = list(smart_split(query_string))
        filtered_q = []

        for bit in split_q:
            if bit not in self.STOPWORD_LIST:
                filtered_q.append(bit)

        if len(filtered_q):
            return filtered_q

        else:
            return split_q

    def get_text_search_query(self, query_string):
        filters = []
        first = True

        for bit in self.get_text_query_bits(query_string):
            or_queries = [
                Q(**{self.construct_search(str(field_name), first): bit})
                for field_name in self.Meta.search_fields
            ]
            filters.append(reduce(Q.__or__, or_queries))
            first = False

        if len(filters):
            return reduce(self.DEFAULT_OPERATOR, filters)
        else:
            return False

    def construct_filter_args(self, cleaned_data):
        args = []

        # if its an instance of Q, append to filter args
        # otherwise assume an exact match lookup
        for field in cleaned_data:

            if hasattr(self, 'prepare_%s' % field):
                q_obj = getattr(self, 'prepare_%s' % field)()
                if q_obj:
                    args.append(q_obj)
            elif isinstance(cleaned_data[field], Q):
                args.append(cleaned_data[field])
            elif field == 'order_by':
                pass  # special case - ordering handled in get_result_queryset
            elif cleaned_data[field]:
                if isinstance(cleaned_data[field], list) or isinstance(cleaned_data[field], QuerySet):
                    args.append(Q(**{field + '__in': cleaned_data[field]}))
                else:
                    args.append(Q(**{field: cleaned_data[field]}))

        return args

    def get_result_queryset(self):
        qs = self.Meta.base_qs

        cleaned_data = self.cleaned_data.copy()
        query_text = cleaned_data.pop('q', None)

        qs = qs.filter(*self.construct_filter_args(cleaned_data))

        if query_text:
            fulltext_indexes = getattr(self.Meta, 'fulltext_indexes', None)
            if DATABASE_ENGINE == 'mysql' and fulltext_indexes:
                # cross-column fulltext search if db is mysql, otherwise use default behaviour.
                # We're assuming the appropriate fulltext index has been created
                match_bits = []
                params = []
                for index in fulltext_indexes:
                    match_bits.append('MATCH(%s) AGAINST (%%s) * %s' % index)
                    params.append(query_text)

                match_expr = ' + '.join(match_bits)

                qs = qs.extra(
                    select={'relevance': match_expr},
                    where=(match_expr,),
                    params=params,
                    select_params=params,
                    order_by=('-relevance',)
                )

            else:
                # construct text search for sqlite, or for when no fulltext indexes are defined
                text_q = self.get_text_search_query(query_text)
                if text_q:
                    qs = qs.filter(text_q)
                else:
                    qs = qs.none()

        if self.cleaned_data['order_by']:
            qs = qs.order_by(*self.cleaned_data['order_by'].split(','))

        return qs

    def get_result_queryset_by_user(self, user):
        return self.get_result_queryset().by_user(user)


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Confirmação', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('nome', 'email',)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords diferentes")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password2"])
        new = not user.pk
        if commit:
            user.save()
            if new:
                if settings.REQUIRE_VERIFICATION_EMAIL:
                    user.send_verification_mail()  # sends verification email with token
                else:
                    user.is_active = True
                    user.is_email_verified = True
                    user.save()
                user.create_privacy_fields()  # create ProfilePrivacyField default objects
        return user


class ProfileSearchForm(AgenteBaseSearchForm):
    class Meta(AgenteBaseSearchForm.Meta):
        model = Profile
        fields = (u'municipio', )


class ResendEmailVerificationForm(forms.Form):
    email = forms.EmailField(label='E-mail')

    def clean_email(self):
        email = self.cleaned_data.get("email")
        user = User.objects.filter(email=email).first()
        if user is not None and user.is_active is False:
            self.user = user
        else:
            raise forms.ValidationError("E-mail inválido.")
        return email

    def send_email(self):
        return self.user.send_verification_mail()


class EnderecoForm(forms.ModelForm):

    class Meta:
        model = Endereco
        exclude = []
        widgets = {'municipio': SelectMunicipioWidget}


class ProfileForm(forms.ModelForm):
    cep = forms.CharField(label='CEP', max_length=9)

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['point'].initial = self.instance.point.wkt if self.instance.point else ''
        self.fields['point'].srid = self.instance.point.srid if self.instance.point else ''
        self.fields['point'].required = True
        self.fields['point'].error_messages = {
            'required': 'Ponto no mapa é obrigatório. Por favor, marque o ponto clicando no mapa ou preenchendo o formulário acima e clicando em Mostrar no Mapa.'}
        self.fields['point'].widget = forms.HiddenInput()

        self.fields['cep'].widget.attrs.update(
            {'class': 'geo_coding', 'placeholder': u'00000-000'})
        self.fields['logradouro'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['numero'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['complemento'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['bairro'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['municipio'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['data_nascimento'].widget.attrs.update(
            {'class': 'date', 'placeholder': u'dd/mm/aaaa'})
        self.fields['telefone1'].widget.attrs.update(
            {'class': 'phone', 'autocomplete': 'off', 'placeholder': u'Informe o DDD'})
        self.fields['telefone2'].widget.attrs.update(
            {'class': 'phone', 'autocomplete': 'off', 'placeholder': u'Informe o DDD'})
        # self.fields['raca_cor_ibge'].widget.attrs.update({
        #     'class': 'has-other-choice',
        #     'data-other-choice-field-id': 'id_raca_cor_ibge_outro',
        #     'data-other-choice-id': Profile.RACA_COR_IBGE.OUTROS,
        # })
        # self.fields['raca_cor_ibge_outro'].widget.attrs.update({
        #     'style': 'margin-top:17px;'
        # })
        self.fields['etnia'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_etnia_outro',
            'data-other-choice-id': Profile.etnia.get_queryset().get(
                nome='Outros'
            ).pk,
        })
        self.fields['etnia_outro'].widget.attrs.update({
            'style': 'margin-top:17px;',
            'data-dont-require': True,
        })
        self.fields['ocupacao'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_ocupacao_outro',
            'data-other-choice-id': Ocupacao.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['ocupacao_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })
        self.fields['area_atuacao'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_area_atuacao_outro',
            'data-other-choice-id': AreaAtuacao.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['area_atuacao_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })
        self.fields['tipo_espaco'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_tipo_espaco_outro',
            'data-other-choice-id': TipoEspaco.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['tipo_espaco_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })
        self.fields['publico_focal'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_publico_focal_outro',
            'data-other-choice-id': PublicoFocal.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['publico_focal_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })
        self.fields['manifestacao_cultural'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_manifestacao_cultural_outro',
            'data-other-choice-id': ManifestacaoCultural.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['manifestacao_cultural_outro'].widget.attrs.update({
            'style': 'margin-top:17px;',
            'data-dont-require': True,
        })
        # privacy_fields são todos os ProfilePrivacyField's vinculados a esse user
        # se for o primeiro cadastro do profile desse user os ProfilePrivacyField's
        # vinculados ao user serão os descritos em Profile.PRIVACY_DEFAULT_FIELDS
        self.fields['privacy_fields'] = forms.MultipleChoiceField(
            choices=self.request_user.privacy_fields.values_list('pk', 'name'),
            initial=self.request_user.privacy_fields.filter(
                public=True).values_list('pk', flat=True),
            required=False
        )

        # para cada opção de privacidade é setado privacy, privacy_pk, privacy_checked
        # no field relacionado (e.g.: <ProfilePrivacyField sexo> : <Profile>.sexo )
        # Isso é feito para uso em field_visible.html
        for pk, field_name in self.fields['privacy_fields'].choices:

            try:
                field = self.fields[field_name]
            except KeyError:
                continue

            field.privacy = True
            field.privacy_pk = pk
            field.privacy_checked = pk in self.fields['privacy_fields'].initial

    def clean(self):
        # ref #117 item 4 / necto
        cleaned_data = self.cleaned_data
        cep = cleaned_data.get('cep')
        if cep and '-' in cep:
            cleaned_data['cep'] = cep.replace('-', '')

        return cleaned_data

    def clean_atua_desde(self):
        atua_desde = self.cleaned_data['atua_desde']
        from django.utils import timezone
        if atua_desde > timezone.now().year or atua_desde < 1000:
            raise forms.ValidationError(u'Data inválida.')
        return atua_desde

    # def clean_raca_cor_ibge_outro(self):
    #     raca_cor_ibge_outro = self.cleaned_data['raca_cor_ibge_outro']
    #     if self.cleaned_data['raca_cor_ibge'] != Profile.RACA_COR_IBGE.OUTROS:
    #         raca_cor_ibge_outro = ''

    #     return raca_cor_ibge_outro

    def clean_etnia_outro(self):
        etnia_outro = self.cleaned_data['etnia_outro']
        etnia = self.cleaned_data['etnia']
        if etnia and etnia.pk != IdentidadeEtinicoCultural.objects.get(nome='Outros').pk:
            etnia_outro = ''

        return etnia_outro

    def clean_ocupacao_outro(self):
        ocupacao_outro = self.cleaned_data['ocupacao_outro']
        ocupacao = self.cleaned_data['ocupacao']
        if ocupacao and ocupacao.pk != Ocupacao.objects.get(nome='Outros').pk:
            ocupacao_outro = ''

        return ocupacao_outro

    def clean_area_atuacao_outro(self):
        area_atuacao_outro = self.cleaned_data['area_atuacao_outro']
        areas_pks = [
            area.pk for area in self.cleaned_data['area_atuacao'].all()]
        if AreaAtuacao.objects.get(nome='Outros').pk not in areas_pks:
            area_atuacao_outro = ''

        return area_atuacao_outro

    def clean_tipo_espaco_outro(self):
        tipo_espaco_outro = self.cleaned_data['tipo_espaco_outro']
        tipo_espaco_pks = [
            area.pk for area in self.cleaned_data['tipo_espaco'].all()]
        if TipoEspaco.objects.get(nome='Outros').pk not in tipo_espaco_pks:
            tipo_espaco_outro = ''

        return tipo_espaco_outro

    def clean_publico_focal_outro(self):
        publico_focal_outro = self.cleaned_data['publico_focal_outro']
        publico_focal_pks = [
            area.pk for area in self.cleaned_data['publico_focal'].all()]
        if PublicoFocal.objects.get(nome='Outros').pk not in publico_focal_pks:
            publico_focal_outro = ''

        return publico_focal_outro

    def clean_manifestacao_cultural_outro(self):
        manifestacao_cultural_outro = self.cleaned_data['manifestacao_cultural_outro']
        manifestacao_cultural_pks = [
            area.pk for area in self.cleaned_data['manifestacao_cultural'].all()]
        if ManifestacaoCultural.objects.get(nome='Outros').pk not in manifestacao_cultural_pks:
            manifestacao_cultural_outro = ''

        return manifestacao_cultural_outro

    def save(self, commit=True):
        instance = super(ProfileForm, self).save(commit=False)
        instance.user = self.request_user
        if commit:
            if bool(instance.pk):
                old_point = Profile.objects.get(
                    pk=instance.pk).point.transform(3857, clone=True)
                old_privacy_endereco = Profile.objects.get(
                    pk=instance.pk).user.privacy_fields.filter(name=u'endereco').first()
                if old_point != instance.point:
                    # se estiver editando o local de um registro existente: limpar cache da antiga e da nova área
                    clean_tilestache_cache(point=old_point, layer=u'core_user')
                    clean_tilestache_cache(
                        point=instance.point, layer=u'core_user')
                elif old_privacy_endereco and (
                        (old_privacy_endereco.public is False and str(old_privacy_endereco.id) in self.cleaned_data['privacy_fields']) or
                        (old_privacy_endereco.public is True and str(old_privacy_endereco.id) not in self.cleaned_data['privacy_fields'])):
                    # se estiver editando a privacidade do endereco de um registro existente: limpar cache da área
                    clean_tilestache_cache(
                        point=instance.point, layer=u'core_user')
            else:
                # se estiver criando um novo registro limpar cache apenas da área do novo ponto
                clean_tilestache_cache(
                    point=instance.point, layer=u'core_user')

            instance.save()
            # marca todos os ProfilePrivacyField desse user como privado
            # para remarcar como público apenas os enviados via POST (:checked)
            instance.user.privacy_fields.update(public=False)
            instance.user.privacy_fields.filter(
                pk__in=self.cleaned_data['privacy_fields']).update(public=True)
            self.save_m2m()
        return instance

    class Meta:
        model = Profile

        fields = [
            'avatar',
            'telefone1',
            'telefone2',
            'data_nascimento',
            'identidade_genero',
            'raca_cor_ibge',
            # 'raca_cor_ibge_outro',
            'etnia',
            'etnia_outro',
            'ocupacao',
            'ocupacao_outro',
            'atua_desde',
            'atuacao',
            'tipo_espaco',
            'tipo_espaco_outro',
            'manifestacao_cultural',
            'manifestacao_cultural_outro',
            'escolaridade',
            'area_atuacao',
            'area_atuacao_outro',
            'area_pesquisa',
            'publico_focal',
            'publico_focal_outro',
            'abrangencia',
            'linguagens_artisticas',
            'cep',
            'logradouro',
            'numero',
            'complemento',
            'bairro',
            'municipio',
            'point',
        ]

        widgets = {
            'municipio': SelectMunicipioWidget,
            'ocupacao': ModelSelect2Widget(model=Ocupacao, search_fields=['nome__unaccent__icontains', ]),
            'area_atuacao': UnicodeModelSelect2MultipleWidget(model=AreaAtuacao, search_fields=['nome__unaccent__icontains', ]),
            'area_pesquisa': UnicodeModelSelect2MultipleWidget(model=AreaEstudoPesquisa, search_fields=['nome__unaccent__icontains']),
            'publico_focal': UnicodeModelSelect2MultipleWidget(model=PublicoFocal, search_fields=['nome__unaccent__icontains', ]),
            'linguagens_artisticas': UnicodeModelSelect2MultipleWidget(model=LinguagemArtistica, search_fields=['nome__unaccent__icontains', ]),
            'tipo_espaco': UnicodeModelSelect2MultipleWidget(model=TipoEspaco, search_fields=['nome__unaccent__icontains', ]),
            'manifestacao_cultural': UnicodeModelSelect2MultipleWidget(model=ManifestacaoCultural, search_fields=['nome__unaccent__icontains', ]),
        }


class RedeSocialForm(forms.ModelForm):

    class Meta:
        model = RedeSocial
        fields = (
            'plataforma',
            'link',
        )

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        super(RedeSocialForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(RedeSocialForm, self).save(commit=False)
        instance.user_upd = instance.user
        new = not instance.pk
        if new:
            instance.user_add = self.request_user

        instance.save()

        return instance


class BaseRedeSocialFormSet(forms.BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        super(BaseRedeSocialFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, *args, **kwargs):
        kwargs['request_user'] = self.request_user
        return super(BaseRedeSocialFormSet, self)._construct_form(*args, **kwargs)


RedeSocialFormSet = forms.inlineformset_factory(
    User,
    RedeSocial,
    RedeSocialForm,
    BaseRedeSocialFormSet,
    fk_name='user',
    min_num=1,
    extra=0
)


class AuthenticationForm(DjangoAuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(AuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'required': True})
        self.fields['password'].widget.attrs.update({'required': True})
        self.error_messages['inactive'] = u'''
            Seu cadastro ainda não foi confirmado. Verifique sua caixa de e-mail ou
             clique no link abaixo para reenviar o e-mail de confirmação.
        '''


class PasswordResetForm(DjangoPasswordResetForm):
    def clean_email(self):
        email = self.cleaned_data.get("email")
        user = User.objects.filter(email=email).first()
        if user is not None and user.is_active is False:
            raise forms.ValidationError(u'''
                Seu cadastro ainda não foi confirmado. Verifique sua caixa de e-mail ou
                 clique no link abaixo para reenviar o e-mail de confirmação.
                ''')
        return email


class UserAgenteVinculoForm(forms.Form):

    empty_help_text = u'''
        <p>Você participa de algum <strong>Coletivo</strong>, <strong>Instituição</strong>, <strong>Fórum, Conselho ou Comitê</strong> ou mesmo <strong>Movimento Social</strong>?
        Utilize o espaço abaixo para vincular-se (caso já houver sido cadastrado) ou o botão abaixo para cadastrar um novo agente.</p>
    '''

    class Meta:
        widgets = {
            'agente': UnicodeModelSelect2Widget(model=Agente, search_fields=['nome__unaccent__icontains'])
        }

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        super(UserAgenteVinculoForm, self).__init__(*args, **kwargs)

        agente_qs = Agente.objects.all()

        if not self.request_user.is_anonymous():
            agente_qs = agente_qs.exclude(
                pk__in=self.request_user.agentes_vinculados.values_list('pk', flat=True))

        self.fields['agente'] = forms.ModelChoiceField(queryset=agente_qs)
        self.fields['agente'].widget = AgenteModelSelect2Widget(queryset=agente_qs, attrs={'data-placeholder': u'Digite o nome do agente que deseja se vincular'}, search_fields=['nome__unaccent__icontains'])
        self.fields['agente'].widget.attrs.update(
            {'style': 'width: 100%;', 'class': 'agente_vinculado'})


class UserAgenteMembroForm(forms.Form):

    empty_help_text = u'''
        <p>Você pertence a algum <strong>Coletivo</strong>, <strong>Instituição</strong>, <strong>Fórum, Conselho ou Comitê</strong> ou mesmo <strong>Movimento Social</strong>?
        Utilize o espaço abaixo para tornar-se membro (caso já houver sido cadastrado) </p>
    '''

    class Meta:
        widgets = {
            'agente': UnicodeModelSelect2Widget(model=Agente, search_fields=['nome__unaccent__icontains'])
        }

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        super(UserAgenteMembroForm, self).__init__(*args, **kwargs)

        agente_qs = Agente.objects.all()

        if not self.request_user.is_anonymous():
            agente_qs = agente_qs.exclude(
                pk__in=self.request_user.agentes.values_list('pk', flat=True))

        self.fields['agente'] = forms.ModelChoiceField(queryset=agente_qs)
        self.fields['agente'].widget = AgenteModelSelect2Widget(queryset=agente_qs, attrs={'data-placeholder': u'Digite o nome do agente que deseja se tornar membro'}, search_fields=['nome__unaccent__icontains'])
        self.fields['agente'].widget.attrs.update(
            {'style': 'width: 100%;', 'class': 'agente'})


class ItemGaleriaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.parent = kwargs.pop('parent')
        super(ItemGaleriaForm, self).__init__(*args, **kwargs)

        self._initialize_tipo()
        self._initialize_data()

    def _initialize_data(self):
        self.fields['data'].widget.attrs.update({
            'class': 'date',
            'placeholder': '00/00/0000',
        })

    def _initialize_tipo(self):
        self.fields['tipo'] = forms.ChoiceField(label=u'Tipo', required=True)

        choices = [(0, '---')]
        for item in ItemGaleria.TIPO.CHOICES:
            choices.append(item)
        self.fields['tipo'].choices = choices

        if hasattr(self.instance, 'pk'):
            self.fields['tipo'].initial = self.instance.tipo

    def process_imagem(self, instance):
        instance.url = None
        instance.create_thumbnail()

    def process_audiovisual(self, instance):
        instance.arquivo = None

    def process_documento(self, instance):
        instance.url = None

    def save(self, *args, **kwargs):
        instance = super(ItemGaleriaForm, self).save(
            commit=False, *args, **kwargs)

        if instance.tipo == ItemGaleria.TIPO.AUDIOVISUAL:
            self.process_audiovisual(instance)
        elif instance.tipo == ItemGaleria.TIPO.DOCUMENTO:
            self.process_documento(instance)
        elif instance.tipo == ItemGaleria.TIPO.IMAGEM:
            self.process_imagem(instance)
        else:
            raise Exception(u'Tipo não suportado!')

        instance.save()
        self.parent.galeria.add(instance)
        return instance

    def format_vimeo(self, url):
        # exemplos encontratos
        # https://vimeo.com/channels/movie3/107548383
        # https://vimeo.com/groups/freehd/videos/230521325

        # formato aceito:
        # https://vimeo.com/107548383
        # https://vimeo.com/230521325
        return url[:18] + url.split('/')[-1]

    def clean_url(self):
        '''
        url é obrigatório quando o tipo for audiovisual
        '''
        url = self.cleaned_data['url'] or ''
        tipo = int(self.cleaned_data['tipo'])
        if tipo == ItemGaleria.TIPO.AUDIOVISUAL and not url:
            raise forms.ValidationError(
                u'URL é obrigatória quando o tipo for audiovisual')

        if 'youtube' in url:
            return url
        if 'vimeo' in url:
            return self.format_vimeo(url)
        if not 'vimeo' or 'youtube' in url:
            raise forms.ValidationError(u'Origens suportadas: YouTube, Vimeo')
        return url

    def clean_arquivo(self):
        '''
        arquivo é obrigatório quando o tipo for documento ou imagem
        '''
        arquivo = self.cleaned_data['arquivo']
        tipo = int(self.cleaned_data['tipo'])

        if tipo in [ItemGaleria.TIPO.DOCUMENTO, ItemGaleria.TIPO.IMAGEM] and not arquivo:
            raise forms.ValidationError(
                u'Arquivo é obrigatório quando o tipo for imagem ou documento')

        if tipo == ItemGaleria.TIPO.IMAGEM and arquivo.name.split('.')[-1].lower() not in ['png', 'jpg', 'jpeg']:
            raise forms.ValidationError(
                u"A imagem de ter um dos formatos: 'png', 'jpg', 'jpeg'")

        return arquivo

    class Meta:
        model = ItemGaleria
        fields = [
            'descricao', 'tipo', 'url', 'data', 'local', 'arquivo', 'titulo'
        ]
