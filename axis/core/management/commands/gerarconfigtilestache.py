# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.apps import apps
from django.conf import settings
import json

# from django.contrib.gis.db.models.aggregates import Collect
from django.contrib.gis.db.models import Count, QuerySet, Case, Value, When, CharField


resolutions = (
    4696291,
    2348146,
    1174073,
    587036,
    293518,
    146759,
    73379,
    36690,
    18345,
    9173,
    4586,
    2293,
    1147,
    573,
    287,
    143,
    72,
    36,
)


class Command(BaseCommand):
    help = u'''Gera configuração para TileStache (serviço de tiles de mapas)'''

    def add_arguments(self, parser):
        parser.add_argument('--arquivo', dest='arquivo', default='tsconfig.json', help=u'Nome do arquivo de configuração de saída')

    def get_absolute_url_to_format(self, model):
        temp_instance = model()
        temp_instance.pk = 99999
        return temp_instance.get_absolute_url().replace('99999', '%s')

    def get_queries(self, model):
        # create base QuerySet for model
        base_qs = QuerySet(model=model)
        base_qs_nocluster = QuerySet(model=model)

        # remove every col (default model cols) in select clause
        base_qs.query.default_cols = False
        base_qs_nocluster.query.default_cols = False

        __geometry__ = model.layer_config['fields'].pop('__geometry__')

        # add `count(geom_field) as count` in select clause
        base_qs = base_qs.annotate(count=Count(__geometry__['field']))
        base_qs_nocluster = base_qs_nocluster.extra(select={'count': 1})

        # add __geometry__ in select clause
        base_qs = base_qs.extra(select={
            '__geometry__': 'ST_Transform(ST_Centroid(ST_Collect(%s)), 900913)' % __geometry__['field'],
        })
        base_qs_nocluster = base_qs_nocluster.extra(select={
            '__geometry__': 'ST_Transform(%s, 900913)' % __geometry__['field'],
        })

        for alias, f in model.layer_config['fields'].items():

            then_value = 'array_to_string(array_agg(%s), \'\')'
            then_value_nocluster = '%s'

            if hasattr(f['field'], '__iter__'):
                then_value = ', '.join([then_value % _f for _f in f['field']])
                then_value_nocluster = ', '.join([_f for _f in f['field']])
            else:
                then_value = then_value % f['field']
                then_value_nocluster = then_value_nocluster % f['field']

            if f['format'] is not False:
                then_value = 'format(\'%s\', %s)' % (f['format'], then_value)
                then_value_nocluster = 'format(\'%s\', %s)' % (f['format'], then_value_nocluster)

            base_qs = base_qs.annotate(**{
                alias: Case(
                    When(count=1, then=Value(then_value)),
                    default=Value('\'\''),
                    output_field=CharField(),
                ),
            })

            base_qs_nocluster = base_qs_nocluster.extra(select={
                alias: then_value_nocluster.replace('%', '%%'),
            })

        base_qs = base_qs.extra(where=['ST_Intersects(ST_Transform(%s, 900913), !bbox!)' % __geometry__['field']])

        return (base_qs,
                base_qs_nocluster)

    def get_sql_from_queryset(self, queryset, cluster=True):
        sql = str(queryset.query)
        if cluster:
            sql = sql.split('GROUP BY')[0] + 'GROUP BY ST_SnapToGrid(ST_Transform(geom, 900913), {resolution})'
        return sql

    def handle(self, *args, **options):
        config = {
            "cache": settings.TILESTACHE_CACHE,
            "layers": {}
        }

        for model_name, model in apps.get_app_config('equipamento').models.items():

            query_cluster, query_nocluster = self.get_queries(model=model)

            layer_name = model._meta.db_table
            url = self.get_absolute_url_to_format(model=model)

            if model.layer_config['filters'] is False:
                queries = {}
                query_cluster_sql = self.get_sql_from_queryset(query_cluster)
                query_nocluster_sql = self.get_sql_from_queryset(query_nocluster, cluster=False)
                for idx, res in enumerate(resolutions):
                    queries[idx] = query_cluster_sql.format(url=url, resolution=res)

                queries[idx + 1] = query_nocluster_sql.format(url=url)

                config['layers'][layer_name] = {
                    "allowed origin": "*",
                    "provider": {
                        "class": "TileStache.Goodies.VecTiles:Provider",
                        "kwargs": {
                            "dbinfo": {
                                "user": settings.DATABASES['default']['USER'],
                                "password": settings.DATABASES['default']['PASSWORD'],
                                "database": settings.DATABASES['default']['NAME'],
                                "port": settings.DATABASES['default']['PORT'],
                                "host": settings.DATABASES['default']['HOST'],
                            },
                            "clip": False,
                            "padding": 30,
                            "simplify": 0.0,
                            "queries": queries
                        }
                    }
                }
            else:
                for layer_suffix, filter_dict in model.layer_config['filters']:

                    queries = {}
                    query_cluster_ = query_cluster.filter(**filter_dict['kwargs'])
                    query_nocluster_ = query_nocluster.filter(**filter_dict['kwargs'])
                    query_cluster_sql = self.get_sql_from_queryset(query_cluster_)
                    query_nocluster_sql = self.get_sql_from_queryset(query_nocluster_, cluster=False)
                    for idx, res in enumerate(resolutions):
                        queries[idx] = query_cluster_sql.format(url=url, resolution=res)

                    queries[idx + 1] = query_nocluster_sql.format(url=url)

                    layer_name_ = '%s_%s' % (layer_name, layer_suffix)

                    config['layers'][layer_name_] = {
                        "allowed origin": "*",
                        "provider": {
                            "class": "TileStache.Goodies.VecTiles:Provider",
                            "kwargs": {
                                "dbinfo": {
                                    "user": settings.DATABASES['default']['USER'],
                                    "password": settings.DATABASES['default']['PASSWORD'],
                                    "database": settings.DATABASES['default']['NAME'],
                                    "port": settings.DATABASES['default']['PORT'],
                                    "host": settings.DATABASES['default']['HOST'],
                                },
                                "clip": False,
                                "padding": 30,
                                "simplify": 0.0,
                                "queries": queries
                            }
                        }
                    }

        uf_absolute_url = '/uf/%s/'
        query_ibge_uf = "SELECT cd_geocuf AS __id__, ST_Transform(geom, 900913) AS __geometry__, uf AS name, format('{url}', uf) AS target_url FROM ibge_uf WHERE ST_Intersects(ST_Transform(geom, 900913), !bbox!)"
        query_ibge_uf = query_ibge_uf.format(url=uf_absolute_url)
        config['layers']['ibge_uf'] = {
            "allowed origin": "*",
            "provider": {
                "class": "TileStache.Goodies.VecTiles:Provider",
                "kwargs": {
                    "dbinfo": {
                        "user": settings.DATABASES['default']['USER'],
                        "password": settings.DATABASES['default']['PASSWORD'],
                        "database": settings.DATABASES['default']['NAME'],
                        "port": settings.DATABASES['default']['PORT'],
                        "host": settings.DATABASES['default']['HOST'],
                    },
                    "clip": False,
                    "queries": [
                        query_ibge_uf
                    ]
                }
            }
        }

        municipio_absolute_url = '/uf/%s/municipio/%s-%s/'
        query_ibge_municipio = "SELECT cd_geocodm AS __id__, ST_Transform(geom, 900913) AS __geometry__, nm_municip || ' - ' || uf_sigla AS name, format('{url}', uf_sigla, lower(nm_municip), cd_geocodm) AS target_url FROM ibge_municipio WHERE ST_Intersects(ST_Transform(geom, 900913), !bbox!)"
        query_ibge_municipio = query_ibge_municipio.format(url=municipio_absolute_url)
        config['layers']['ibge_municipio'] = {
            "allowed origin": "*",
            "provider": {
                "class": "TileStache.Goodies.VecTiles:Provider",
                "kwargs": {
                    "dbinfo": {
                        "user": settings.DATABASES['default']['USER'],
                        "password": settings.DATABASES['default']['PASSWORD'],
                        "database": settings.DATABASES['default']['NAME'],
                        "port": settings.DATABASES['default']['PORT'],
                        "host": settings.DATABASES['default']['HOST'],
                    },
                    "clip": False,
                    "queries": [
                        query_ibge_municipio
                    ]
                }
            }
        }

        agente_models = dict(apps.get_app_config('agente').models)
        agente_models_list = [
            ('coletivo', agente_models['coletivo']),
            ('instituicao', agente_models['instituicao']),
            ('conselho', agente_models['conselho']),
            ('movimentosocial', agente_models['movimentosocial']),
        ]

        for model_name, model in agente_models_list:
            table_name = model._meta.db_table
            temp_instance = model()
            temp_instance.pk = 0
            url = temp_instance.get_absolute_url().replace('0', '%s')
            queries = {}
            query = "SELECT ST_Transform(ST_Collect(point), 900913) AS __geometry__, count(point) AS count, CASE WHEN count(point)=1 THEN array_to_string(array_agg(nome), '') ELSE '' END AS name, CASE WHEN count(point)=1 THEN array_to_string(array_agg(ibge_municipio.nm_municip), '') || ' - ' || array_to_string(array_agg(ibge_municipio.uf_sigla), '') ELSE '' END AS local, CASE WHEN COUNT(point)=1 THEN format('{url}', array_to_string(array_agg(agente_ptr_id), '')) ELSE '' END AS target_url FROM {table_name} INNER JOIN agente_agente ON (agente_agente.id = {table_name}.agente_ptr_id) INNER JOIN ibge_municipio on (agente_agente.municipio_id = ibge_municipio.cd_geocodm) WHERE ST_Intersects(ST_Transform(point, 900913), !bbox!) GROUP BY ST_SnapToGrid(ST_Transform(point, 900913), {resolution})"
            for idx, res in enumerate(resolutions):
                queries[idx] = query.format(table_name=table_name, url=url, resolution=res)

            no_cluster_query = "SELECT ST_Transform(point, 900913) AS __geometry__, 1 AS count, ibge_municipio.nm_municip || ' - ' || ibge_municipio.uf_sigla AS local, nome AS name, format('{url}', agente_ptr_id) as target_url FROM {table_name} INNER JOIN agente_agente ON (agente_agente.id = {table_name}.agente_ptr_id) INNER JOIN ibge_municipio on (agente_agente.municipio_id = ibge_municipio.cd_geocodm) WHERE ST_Intersects(ST_Transform(point, 900913), !bbox!)"
            queries[idx + 1] = no_cluster_query.format(table_name=table_name, url=url)

            config['layers'][table_name] = {
                "allowed origin": "*",
                "provider": {
                    "class": "TileStache.Goodies.VecTiles:Provider",
                    "kwargs": {
                        "dbinfo": {
                            "user": settings.DATABASES['default']['USER'],
                            "password": settings.DATABASES['default']['PASSWORD'],
                            "database": settings.DATABASES['default']['NAME'],
                            "port": settings.DATABASES['default']['PORT'],
                            "host": settings.DATABASES['default']['HOST'],
                        },
                        "clip": False,
                        "padding": 30,
                        "simplify": 0.0,
                        "queries": queries
                    }
                }
            }

        user = apps.get_app_config('core').models['user']
        profile = apps.get_app_config('core').models['profile']
        temp_instance = profile()
        temp_instance.user = user(pk=0)
        agente_pessoafisica_absolute_url = temp_instance.get_absolute_url().replace('0', '%s')
        query_agente_pessoafisica = "SELECT ST_Transform(ST_Collect(core_profile.point), 900913) AS __geometry__, count(point) AS count, CASE WHEN count(point)=1 THEN concat(array_to_string(array_agg(ibge_municipio.nm_municip), ''), ' - ', array_to_string(array_agg(ibge_municipio.uf_sigla), '')) ELSE '' END AS local, CASE WHEN count(point)=1 THEN array_to_string(array_agg(core_user.nome), '') ELSE '' END as name, CASE WHEN count(point)=1 THEN format('{url}', array_to_string(array_agg(core_user.id), '')) ELSE '' END AS target_url FROM core_profile INNER JOIN core_user ON (core_profile.user_id = core_user.id) INNER JOIN ibge_municipio on (core_profile.municipio_id = ibge_municipio.cd_geocodm) INNER JOIN core_profileprivacyfield as core_priv ON (core_priv.user_id = core_profile.user_id and core_priv.name = 'endereco' and core_priv.public = true) WHERE ST_Intersects(ST_Transform(core_profile.point, 900913), !bbox!) GROUP BY ST_SnapToGrid(ST_Transform(core_profile.point, 900913), {resolution})"
        queries = {}
        for idx, res in enumerate(resolutions):
            queries[idx] = query_agente_pessoafisica.format(url=agente_pessoafisica_absolute_url, resolution=res)

        no_cluster_query = "SELECT ST_Transform(core_profile.point, 900913) AS __geometry__, 1 AS count, ibge_municipio.nm_municip || ' - ' || ibge_municipio.uf_sigla AS local, core_user.nome as name, format('{url}', core_user.id) AS target_url FROM core_profile INNER JOIN core_user ON (core_profile.user_id = core_user.id) INNER JOIN ibge_municipio on (core_profile.municipio_id = ibge_municipio.cd_geocodm) INNER JOIN core_profileprivacyfield as core_priv ON (core_priv.user_id = core_profile.user_id and core_priv.name = 'endereco' and core_priv.public = true) WHERE ST_Intersects(ST_Transform(core_profile.point, 900913), !bbox!)"
        queries[idx + 1] = no_cluster_query.format(table_name=table_name, url=agente_pessoafisica_absolute_url)

        config['layers']['core_user'] = {
            "allowed origin": "*",
            "provider": {
                "class": "TileStache.Goodies.VecTiles:Provider",
                "kwargs": {
                    "dbinfo": {
                        "user": settings.DATABASES['default']['USER'],
                        "password": settings.DATABASES['default']['PASSWORD'],
                        "database": settings.DATABASES['default']['NAME'],
                        "port": settings.DATABASES['default']['PORT'],
                        "host": settings.DATABASES['default']['HOST'],
                    },
                    "clip": False,
                    "queries": queries
                }
            }
        }

        acao_models = dict(apps.get_app_config('acao').models)
        acao_models_list = [
            ('projeto', acao_models['projeto']),
            ('atividade', acao_models['atividade']),
        ]

        for model_name, model in acao_models_list:
            table_name = model._meta.db_table
            temp_instance = model()
            temp_instance.pk = 0
            url = temp_instance.get_absolute_url().replace('0', '%s')
            join_atividade_projeto = u''
            join_atividade_projeto_nome = u''
            join_atividade_projeto_nome_aggr = u''

            if table_name == u'acao_atividade':
                temp_proj_instance = acao_models['projeto']()
                temp_proj_instance.pk = 0
                url_proj = temp_proj_instance.get_absolute_url().replace('0', '%s')
                join_atividade_projeto = u"LEFT JOIN acao_projeto ON (acao_projeto.acao_ptr_id = acao_atividade.vinculo_projeto_id) LEFT JOIN acao_acao AS acao_acao_projeto ON (acao_projeto.acao_ptr_id = acao_acao_projeto.id)"
                join_atividade_projeto_nome_aggr = u", CASE WHEN count(acao_local.point)=1 THEN array_to_string(array_agg(acao_acao_projeto.nome), '') ELSE '' END AS project_name, CASE WHEN count(acao_local.point)=1 THEN format('{url}', array_to_string(array_agg(acao_acao_projeto.id), '')) ELSE '' END AS project_target_url".format(url=url_proj)
                join_atividade_projeto_nome = u", acao_acao_projeto.nome AS project_name, format('{url}', acao_acao_projeto.id) AS project_target_url".format(url=url_proj)

            queries = {}
            query = "SELECT ST_Transform(ST_Collect(acao_local.point), 900913) AS __geometry__, count(acao_local.point) AS count, CASE WHEN count(acao_local.point)=1 THEN array_to_string(array_agg(acao_acao.nome), '') ELSE '' END AS name, CASE WHEN count(acao_local.point)=1 THEN array_to_string(array_agg(ibge_municipio.nm_municip), '') || ' - ' || array_to_string(array_agg(ibge_municipio.uf_sigla), '') ELSE '' END AS local, CASE WHEN COUNT(acao_local.point)=1 THEN format('{url}', array_to_string(array_agg({table_name}.acao_ptr_id), '')) ELSE '' END AS target_url{join_atividade_projeto_nome} FROM acao_localacao INNER JOIN acao_local ON (acao_local.id = acao_localacao.local_id) INNER JOIN ibge_municipio ON (acao_local.municipio_id = ibge_municipio.cd_geocodm) INNER JOIN acao_acao ON (acao_acao.id = acao_localacao.acao_id) INNER JOIN {table_name} ON (acao_acao.id = {table_name}.acao_ptr_id) {join_atividade_projeto} WHERE ST_Intersects(ST_Transform(acao_local.point, 900913), !bbox!) GROUP BY ST_SnapToGrid(ST_Transform(acao_local.point, 900913), {resolution})"
            for idx, res in enumerate(resolutions):
                queries[idx] = query.format(table_name=table_name, url=url, resolution=res, join_atividade_projeto_nome=join_atividade_projeto_nome_aggr, join_atividade_projeto=join_atividade_projeto)

            no_cluster_query = "SELECT ST_Transform(acao_local.point, 900913) AS __geometry__, 1 AS count, acao_acao.nome AS name, ibge_municipio.nm_municip || ' - ' || ibge_municipio.uf_sigla AS local, format('{url}', {table_name}.acao_ptr_id) AS target_url{join_atividade_projeto_nome} FROM acao_localacao INNER JOIN acao_local ON (acao_local.id = acao_localacao.local_id) INNER JOIN ibge_municipio ON (acao_local.municipio_id = ibge_municipio.cd_geocodm) INNER JOIN acao_acao ON (acao_acao.id = acao_localacao.acao_id) INNER JOIN {table_name} ON (acao_acao.id = {table_name}.acao_ptr_id) {join_atividade_projeto} WHERE ST_Intersects(ST_Transform(acao_local.point, 900913), !bbox!)"
            queries[idx + 1] = no_cluster_query.format(table_name=table_name, url=url, join_atividade_projeto_nome=join_atividade_projeto_nome, join_atividade_projeto=join_atividade_projeto)

            config['layers'][table_name] = {
                "allowed origin": "*",
                "provider": {
                    "class": "TileStache.Goodies.VecTiles:Provider",
                    "kwargs": {
                        "dbinfo": {
                            "user": settings.DATABASES['default']['USER'],
                            "password": settings.DATABASES['default']['PASSWORD'],
                            "database": settings.DATABASES['default']['NAME'],
                            "port": settings.DATABASES['default']['PORT'],
                            "host": settings.DATABASES['default']['HOST'],
                        },
                        "clip": False,
                        "padding": 30,
                        "simplify": 0.0,
                        "queries": queries
                    }
                }
            }

        arquivo = open(options['arquivo'], 'w')
        json.dump(config, arquivo, indent=2)
        arquivo.close()
        self.stdout.write(u'Arquivo "{}" criado!'.format(arquivo.name))
