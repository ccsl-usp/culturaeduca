# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.core.cache import caches
from django.apps import apps
import time


cache_ibge = caches['ibge']


class Command(BaseCommand):
    help = u'''Gera cache de perfil dos setores censitários de UFs e Municípios para serem mostrados nas telas de detalhe. '''

    def add_arguments(self, parser):
        parser.add_argument('--uf', dest='uf', default=False, help=u'Cria (ou recria) somente UFs filtrados por esse valor, utiliza --uf-kwarg-name (default: uf).')
        parser.add_argument('--uf-kwarg-name', dest='uf_kwarg_name', default=u'uf', help=u'Nome do argumento usado para filtar UF. Opções: cd_geocuf, municipio, nm_estado, nm_regiao, uf.')
        parser.add_argument('--municipio', dest='municipio', default=False, help=u'Cria (ou recria) somente Municipios filtrados por esse valor, utiliza --municipio-kwarg-name (default: cd_geocodm).')
        parser.add_argument('--municipio-kwarg-name', dest='municipio_kwarg_name', default=u'cd_geocodm', help=u'Nome do argumento usado para filtar Municipios. Opções: cd_geocodm, nm_municip, setorcensitario, uf_id, uf_sigla.')
        parser.add_argument('--reset', action='store_true', dest='reset', default=False, help=u'Cria cache novamente mesmo que já exista.')
        parser.add_argument('--timeout', dest='timeout', default=None, help=u'Kwarg timeout de cache.set para UFs e Municipios (default: None - não expira).')

    def handle(self, *args, **options):
        ibge_models = dict(apps.get_app_config('ibge').models)
        reset = options['reset']
        timeout = options['timeout']

        start_time = time.time()

        SetorCensitario = ibge_models['setorcensitario']

        if options['verbosity'] >= 2:
            self.stdout.write(u'Carregando UFs...', ending='')

        UF = ibge_models['uf']
        ufs = UF.objects

        if options['uf']:
            ufs = ufs.filter(**{options['uf_kwarg_name']: options['uf']})
        else:
            ufs = ufs.all()

        if options['verbosity'] >= 2:
            self.stdout.write(u'OK')

        for uf in ufs:
            key = u'perfil_uf_{}'.format(uf.uf)

            if options['verbosity'] >= 2:
                self.stdout.write(u'UF %s (%s) - verificando cache...' % (uf.nm_estado, uf.uf))

            cache_exist = cache_ibge.get(key) is not None

            if not cache_exist or reset:
                if options['verbosity'] >= 2:
                    self.stdout.write(u'\n  [já existente: %s, reset: %s] UF %s (%s) - atualizando cache...' % (cache_exist, reset, uf.nm_estado, uf.uf), ending='')

                _start_time = time.time()

                cache_ibge.set(key, SetorCensitario.objects.filter(
                    municipio__uf_sigla=uf.uf
                ).perfil(), timeout=timeout)

                _end_time = time.time()

                if options['verbosity'] >= 2:
                    if cache_ibge.get(key) is not None:
                        self.stdout.write(u'criado.', ending='\n\n')
                    else:
                        self.stdout.write(u'NÃO criado.', ending='\n\n')

                if options['verbosity'] >= 2:
                    _total_time = round(_end_time - _start_time, 3)
                    self.stdout.write(u'  Tempo de Execução %sm (%ss)' % (round(_total_time / 60, 3), _total_time, ), ending='\n\n')
            else:
                if options['verbosity'] >= 2:
                    self.stdout.write(u'  [já existente: %s, reset: %s] UF %s (%s)' % (cache_exist, reset, uf.nm_estado, uf.uf), ending='\n\n')

        if options['verbosity'] >= 2:
            self.stdout.write(u'Carregando Municípios...', ending='')

        Municipio = ibge_models['municipio']
        municipios = Municipio.objects

        if options['municipio']:
            municipios = municipios.filter(**{options['municipio_kwarg_name']: options['municipio']})
        else:
            municipios = municipios.all()

        if options['verbosity'] >= 2:
            self.stdout.write(u'OK')

        for municipio in municipios:
            key = u'perfil_mun_{}'.format(municipio.cd_geocodm)

            if options['verbosity'] >= 2:
                self.stdout.write(u'Municipio %s - %s - verificando cache...' % (municipio.nm_municip, municipio.uf_sigla))

            cache_exist = cache_ibge.get(key) is not None

            if not cache_exist or reset:
                if options['verbosity'] >= 2:
                    self.stdout.write(u'  [já existente: %s, reset: %s] Municipio %s - %s - atualizando cache...' % (cache_exist, reset, municipio.nm_municip, municipio.uf_sigla), ending='')

                _start_time = time.time()

                cache_ibge.set(key, SetorCensitario.objects.filter(
                    municipio__pk=municipio.pk
                ).perfil(), timeout=timeout)

                _end_time = time.time()

                if options['verbosity'] >= 2:
                    if cache_ibge.get(key) is not None:
                        self.stdout.write(u'criado.', ending='\n\n')
                    else:
                        self.stdout.write(u'NÃO criado.', ending='\n\n')

                if options['verbosity'] >= 2:
                    _total_time = round(_end_time - _start_time, 3)
                    self.stdout.write(u'  Tempo de Execução %sm (%ss)' % (_total_time / 60, _total_time, ), ending='\n\n')
            else:
                if options['verbosity'] >= 2:
                    self.stdout.write(u'  [já existente: %s, reset: %s] Municipio %s - %s' % (cache_exist, reset, municipio.nm_municip, municipio.uf_sigla), ending='\n\n')

        end_time = time.time()
        if options['verbosity'] >= 2:
            total_time = round(end_time - start_time, 3)
            self.stdout.write(u'Tempo Total de Execução %sm (%ss)' % (round(total_time / 60, 3), total_time, ), ending='\n\n')
