# coding: utf-8
from __future__ import print_function

from axis.core.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = u'''Atualiza campos de privacidade do usuário USER ou todos (se não for definido).'''

    def add_arguments(self, parser):
        parser.add_argument('--user', dest='user', default=False, help=u'Atualiza somente usuário informado.')
        parser.add_argument('--dont-create', action='store_false', dest='create', default=True, help=u'Não cria campos que foram criados após cadastro do usuário.')
        parser.add_argument('--dont-delete', action='store_false', dest='delete', default=True, help=u'Não remove campos que existiam após cadastro do usuário porém não existem mais para novos usuários.')
        parser.add_argument('--reset-values', action='store_true', dest='reset_values', default=False, help=u'Redefine privacidade com valores padrão.')

    def handle(self, *args, **options):
        users = User.objects.all()

        user = options['user']
        create = options['create']
        delete = options['delete']
        reset_values = options['reset_values']

        if user:
            users = users.filter(pk=user)

        for user in users:
            if options['verbosity'] >= 2:
                self.stdout.write(u'User %s - %s - atualizando...' % (user.pk, user.nome), ending='')

            user.update_privacy_fields(delete=delete, create=create, reset_values=reset_values)

            campos_privados_novos = user.privacy_fields.filter(public=False).values_list('name', flat=True)
            campos_publicos_novos = user.privacy_fields.filter(public=True).values_list('name', flat=True)

            if options['verbosity'] >= 2:
                self.stdout.write(u'...OK')

            if options['verbosity'] >= 2:
                self.stdout.write(u' Campos Privados:')
                self.stdout.write(u'    %s.' % campos_privados_novos, ending=u'\n')
                self.stdout.write(u' Campos Públicos:')
                self.stdout.write(u'    %s.' % campos_publicos_novos, ending=u'\n\n\n')

        if options['verbosity'] >= 1:
                self.stdout.write(u'%s atualizado(s).' % (users.count(),))
