from django.contrib import admin
from .models import Profile, IdentidadeEtinicoCultural, Ocupacao, NivelEscolaridade, AreaAtuacao, User, AreaEstudoPesquisa, PublicoFocal, LinguagemArtistica, TipoEspaco, ManifestacaoCultural


class UserAdmin(admin.ModelAdmin):
    list_display = ('nome','pk','email')
    search_fields = ('pk', 'nome','email')

class ProfileAdmin(admin.ModelAdmin):
	list_display = ('nome','pk','email','municipio','identidade_genero','raca_cor_ibge','escolaridade','ocupacao')
	search_fields = ('nome',)
	list_filter = ('escolaridade','ocupacao','raca_cor_ibge','identidade_genero','area_atuacao','publico_focal','tipo_espaco')

class AreaEstudoPesquisaAdmin(admin.ModelAdmin):
    search_fields = ('nome','pk')

class ManifestacaoCulturalAdmin(admin.ModelAdmin):
    search_fields = ('nome','pk')

class PublicoFocalAdmin(admin.ModelAdmin):
    list_display = ('nome','pk')
    search_fields = ('nome',)

class LinguagemArtisticaAdmin(admin.ModelAdmin):
    search_fields = ('nome','pk')

class TipoEspacoAdmin(admin.ModelAdmin):
    search_fields = ('nome','pk')
    search_fields = ('nome',)

class AreaAtuacaoAdmin(admin.ModelAdmin):
    search_fields = ('nome','pk')
    search_fields = ('nome',)


admin.site.register(User, UserAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(AreaEstudoPesquisa, AreaEstudoPesquisaAdmin)
admin.site.register(ManifestacaoCultural, ManifestacaoCulturalAdmin)
admin.site.register(PublicoFocal, PublicoFocalAdmin)
admin.site.register(LinguagemArtistica, LinguagemArtisticaAdmin)
admin.site.register(TipoEspaco, TipoEspacoAdmin)

admin.site.register(IdentidadeEtinicoCultural)
admin.site.register(Ocupacao)
admin.site.register(NivelEscolaridade)
admin.site.register(AreaAtuacao, AreaAtuacaoAdmin)




