from django.conf.urls import url
from django.contrib.auth import views as django_views
from . import views
from .forms import PasswordResetForm

urlpatterns = [
    url(r'^cadastro/$', views.user_create, name='user_add'),
    url(r'^profile/edit/$', views.profile_form, name='profile_edit'),
    url(r'^profile/detail/$', views.profile_detail, name='profile_detail'),
    url(r'^profile/detail/(?P<pk>\d+)/$', views.profile_detail, name='profile_detail'),
    url(r'^cadastro/confirmacao/(?P<uid>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', views.user_email_verification_confirm, name='email_verification_confirm'),
    url(r'^cadastro/reenviar-email-confirmacao/$', views.user_email_verification_resend, name='email_verification_resend'),

    url(r'^(?P<pk>\d+)/rede_social/cadastro/$', views.rede_social_create, name='rede_social_core_create'),
    url(r'^(?P<pk>\d+)/rede_social/(?P<rede_social_pk>\d+)/$', views.rede_social_update, name='rede_social_core_update'),

    url(r'^lista_pessoa_fisica.geojson$', views.lista_pessoa_fisica, name='lista_pessoa_fisica'),

    url(
        r'^galeria/list/app/(?P<src_app>\w+)/model/(?P<src_model>\w+)/(?P<src_pk>\d+)/$',
        views.galeria_list,
        name='galeria_list'
    ),
    url(
        r'^galeria/dashboard/app/(?P<src_app>\w+)/model/(?P<src_model>\w+)/(?P<src_pk>\d+)/$',
        views.galeria_dashboard,
        name='galeria_dashboard'
    ),

    url(
        r'^galeria/counter/app/(?P<src_app>\w+)/model/(?P<src_model>\w+)/(?P<src_pk>\d+)/$',
        views.galeria_counter,
        name='galeria_counter'
    ),

    url(
        r'^galeria/form/app/(?P<src_app>\w+)/model/(?P<src_model>\w+)/(?P<src_pk>\d+)/create/$',
        views.galeria_create,
        name='galeria_create'
    ),

    url(
        r'^galeria/form/app/(?P<src_app>\w+)/model/(?P<src_model>\w+)/(?P<src_pk>\d+)/update/(?P<pk>\d+)/$',
        views.galeria_update,
        name='galeria_update'
    ),

    # password reset views

    url(
        r'^cadastro/senha/reset/$',
        django_views.password_reset,
        {
            'post_reset_redirect': '/usuario/cadastro/senha/reset/done/',
            'email_template_name': 'registration/email/password_reset_email.html',
            'password_reset_form': PasswordResetForm
        },
        name='password_reset'
    ),
    url(
        r'^cadastro/senha/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        django_views.password_reset_confirm,
        {
            'post_reset_redirect': '/usuario/cadastro/senha/done/'
        },
        name='password_reset_confirm'
    ),
    url(
        r'^cadastro/senha/reset/done/$',
        django_views.password_reset_done,
        name='password_reset_done'
    ),
    url(
        r'^cadastro/senha/done/$',
        django_views.password_reset_complete,
        name='password_reset_complete'
    ),
]
