// Variável global do mapa (ol.Map)
var znc_ola_map;
var lista_dict_enderecos = [];
var estados;

$.getJSON('/static/js/estados.json', {}, function(data) { 
  estados = data;
});


// Dicionário de valores default de inicialização
var znc_ola_opt_default = {
    elementGeocoding: $('#id_reverse_geocoding'),
    elementPointWKT: $('#id_ponto'),
    elementGeocodingTrigger: $('#reverse_geocoding_button'),
    setCenterToCurrentPosition: false, // centraliza na posição geográfica em que o user está
    elementGeocodingCallback: function(){},
    elementReverseGeocodingCallback: function(){},
    canSetPoint: false,
    initialPointWKT: false,
    center: [0, 0],
    zoom: 5,
    target: 'map',
    projection: 'EPSG:3857',
    messages: {
        addressNotFound: 'Não foi encontrado um endereço. Clique no mapa para marcá-lo.'
    },
    elementGeocodingResponse: $('#id_geocoding_response')
};

// Dicionário de valores de inicialização
var znc_ola_opt;

// Dicionário de funções
var znc_ola_functions = {};

// Adição do método getLayer em ol.Map
// para retornar apenas UMA layer pelo nome
if (ol.Map.prototype.getLayer === undefined) {
    ol.Map.prototype.getLayer = function (name) {
        var layer;  
        this.getLayers().forEach(function (l) {
            if (name == l.get('name')) layer = l;
        });
        return layer;
    }
}

$.fn.changeVal = function (v) {
    return $(this).val(v).trigger("change");
}

// Função de inicialização do mapa
function initZncMap(options) {

    // Resultado final para dicionário de valores
    znc_ola_opt = $.extend(true, znc_ola_opt_default, options);

    if (typeof znc_ola_opt.elementGeocoding == "string")
        // Se elementGeocoding for uma string, transformar para obj antes de continuar
        znc_ola_opt.elementGeocoding = $(znc_ola_opt.elementGeocoding);

    if (typeof znc_ola_opt.elementPointWKT == "string")
        // Se elementPointWKT for uma string, transformar para obj antes de continuar
        znc_ola_opt.elementPointWKT = $(znc_ola_opt.elementPointWKT);

    if (typeof znc_ola_opt.elementGeocodingTrigger == "string")
        // Se elementGeocodingTrigger for uma string, transformar para obj antes de continuar
        znc_ola_opt.elementGeocodingTrigger = $(znc_ola_opt.elementGeocodingTrigger);

    // Inicialização do mapa
    znc_ola_map = new ol.Map({
        target: znc_ola_opt.target,
        controls: ol.control.defaults({
          attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
            collapsible: false
          })
        }),
        layers: [
            new ol.layer.Tile({
                name: 'basemap',
                source: new ol.source.OSM(),
                opacity: 0.7
            }),
        ],
        view: new ol.View({
            center: znc_ola_opt.center,
            projection: znc_ola_opt.projection,
            zoom: znc_ola_opt.zoom
        })
    });

    if (znc_ola_opt.setCenterToCurrentPosition) {
        // Se setCenterToCurrentPosition for true, verificar se existe geolocation no navegador
        // e chama função que muda center do mapa para a localização do usuário
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(znc_ola_functions.currentPosition);
        }
    }

    $(znc_ola_opt.elementGeocoding).on('change', function() {
        // Ao mudar valor do input elementGeocoding chamar função de geocoding
        znc_ola_functions.geocoding( $(this).val(), znc_ola_opt.elementGeocodingCallback );
    });

    $(znc_ola_opt.elementGeocodingTrigger).on('click', function() {
        // Ao clicar no elementGeocodingTrigger chamar função de geocoding
        znc_ola_functions.geocoding( $(znc_ola_opt.elementGeocoding).val(), znc_ola_opt.elementGeocodingCallback );
    });

    // Adicionando layer para novos pontos no mapa
    znc_ola_map.addLayer(new ol.layer.Vector({
        name: 'znc_ola_novos_pontos',
        source: new ol.source.Vector()
    }));

    // Ao clicar no mapa chamar função de click
    znc_ola_map.on('click', znc_ola_functions.onClick);
    if (znc_ola_opt.initialPointWKT) {
        ponto_geom = (new ol.format.WKT()).readGeometry(znc_ola_opt.initialPointWKT, {
            dataProjection: znc_ola_opt.projection
        });
        znc_ola_functions.onClick({
            coordinate: ponto_geom.getCoordinates()
        }, false);
        znc_ola_map.getView().setCenter(ponto_geom.getCoordinates());
    }

}

znc_ola_functions.onClick = function(evt, geocode_reverse=true) {
    // Se usuário pode colocar pontos no mapa
    if (znc_ola_opt.canSetPoint) {

        // Recupera a layer de novos pontos
        var layer = znc_ola_map.getLayer('znc_ola_novos_pontos');

        // Cria nova feature para mostrar no mapa o ponto clicado
        var feature = new ol.Feature({
            geometry: new ol.geom.Point(evt.coordinate)
        });

        // ref #117 customiza o marker
        var style = new ol.style.Style({
          image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 32], // POSICAO DA BASE DO MARKER
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            opacity: 1,
            // src: 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/32/map-marker-icon.png'
            src: '/static/img/map-marker-icon.png'
          }))
        });

        feature.setStyle(style);

        // Se já houver alguma feature na layer recebe true
        var replacing = layer.getSource().getFeatures().length == 1;

        // Limpa o source da layer de novos pontos
        layer.getSource().clear();

        // Adiciona a feature recém criada
        layer.getSource().addFeature(feature);

        // Recupera o SRID pela configuração de inicialização
        var SRID = znc_ola_opt.projection.split(':')[1];

        // Atualizar o valor de elementPointWKT para o WKT do ponto
        znc_ola_opt.elementPointWKT.changeVal('SRID='+SRID+';' + new ol.format.WKT().writeFeature(feature));

        if (!replacing)
            // Se for o primeiro ponto marcado no mapa pelo click
            // muda o center do mapa para as coordenadas do novo ponto
            znc_ola_map.getView().setCenter(evt.coordinate);

        if (geocode_reverse)
            // Se geocode_reverse for true faz o geocoding reverso pelas coordenadas
            znc_ola_functions.reverse_geocoding(evt.coordinate, znc_ola_opt.elementReverseGeocodingCallback);

    }
};

znc_ola_functions.currentPosition = function(position) {

    // Callback de navigator.geolocation.getCurrentPosition

    znc_ola_map.getView().setCenter( ol.proj.transform([
        position.coords.longitude,
        position.coords.latitude
    ], 'EPSG:4326', znc_ola_opt.projection) );

};

znc_ola_functions.setGeocodingResponseText = function(content) {

    // Define o valor/conteúdo de elementGeocoding para o content
    // sendo elementGeocoding um input, textarea ou div
    if (znc_ola_opt.elementGeocodingResponse.is(":hidden")){
        znc_ola_opt.elementGeocodingResponse.show();
    }
    if (znc_ola_opt.elementGeocodingResponse.is('input, textarea')) {
        if (content==znc_ola_opt.messages.addressNotFound) {
            znc_ola_opt.elementGeocodingResponse.prop('placeholder', content);
            znc_ola_opt.elementGeocodingResponse.val('');
        } else znc_ola_opt.elementGeocodingResponse.val(content);
    } else znc_ola_opt.elementGeocodingResponse.text(content);

    znc_ola_opt.elementGeocodingResponse.delay(3000).fadeOut(2000)

    return znc_ola_opt.setGeocodingResponse;
};

znc_ola_functions.setGeocodingFieldText = function(content) {

    // Define o valor/conteúdo de elementGeocoding para o content
    // sendo elementGeocoding um input, textarea ou div
    if (znc_ola_opt.elementGeocoding.is('input, textarea')) {
        if (content==znc_ola_opt.messages.addressNotFound) {
            znc_ola_opt.elementGeocoding.prop('placeholder', content);
            znc_ola_opt.elementGeocoding.val('');
        } else znc_ola_opt.elementGeocoding.val(content);
    } else znc_ola_opt.elementGeocoding.text(content);

    return znc_ola_opt.elementGeocoding;
};

znc_ola_functions.getResponseCoordinates = function (response) {
    return ol.proj.transform([
        parseFloat(response.lon),
        parseFloat(response.lat)
    ], 'EPSG:4326', znc_ola_opt.projection);
}

znc_ola_functions.setMapPin = function (response, coordinates){
    var pinInserted = false;
    var zoom_by_type = {
        // limite de cidades, encontradas com esse nome de rua
        "city": 12,
        "suburb": 15,
        "station": 15,
        "road": 18,
        "residential": 18,
        "house": 18,
        "pedestrian": 18,
    };
    // Define o center do mapa com coords
    znc_ola_map.getView().setCenter(coordinates);
    if (response.raw_json.address.road != undefined) {
        // Chama znc_ola_functions.onClick passando a coordenada
        // para colocar um ponto no mapa (sem resetGeocodingResponseTextverse geocoding)
        znc_ola_functions.onClick({coordinate:coordinates}, false);
        pinInserted = true;
    } else {
        // Recupera a layer de novos pontos
        var layer = znc_ola_map.getLayer('znc_ola_novos_pontos');
        // Limpa o source da layer de novos pontos
        layer.getSource().clear();
        znc_ola_functions.setGeocodingResponseText(znc_ola_opt.messages.addressNotFound);
    }
    // se houver rua já cria ponto e salva coords no input znc_ola_opt.elementPointWKT
    znc_ola_map.getView().setZoom(zoom_by_type[response.raw_json.type] || 12);
    return pinInserted
}
znc_ola_functions.setCenterOnCity = function (address) {
    var url = 'https://nominatim.openstreetmap.org/search';

    var data = {};
    data.limit = 1;
    data.city = address.city;
    data.state = address.state;
    data.country = address.country;
    data.format = 'json';
    data.addressdetails = 1;
    data.polygon = 1;

    response = $.ajax({
        dataType: "json",
        url: url,
        data: data,
        error: znc_ola_functions.nominatimCallback,
        success:  function (response) {
            var coords = znc_ola_functions.getResponseCoordinates(response[0])
            znc_ola_map.getView().setCenter(coords);
            znc_ola_map.getView().setZoom(12);
        }
    });

    return true
}

znc_ola_functions.setCenterOnDistrict = function (address) {
    var url = 'https://nominatim.openstreetmap.org/search';
    var data = {};
    data.limit = 1;
    // Para encontrar bairro no nominatim, este deve ser inserido como 'city' e a cidade como 'county'
    data.city = address.district;
    data.county = address.city
    data.state = address.state;
    data.country = address.country;
    data.format = 'json';
    data.addressdetails = 1;
    data.polygon = 1;

    response = $.ajax({
        dataType: "json",
        url: url,
        data: data,
        error: znc_ola_functions.nominatimCallback,
        success:  function (response) {
            try {
                var coords = znc_ola_functions.getResponseCoordinates(response[0])
                if (data.county!=response[0].address.city) throw e;
            } catch(e) {
                return znc_ola_functions.setCenterOnCity(address);
            }
            znc_ola_map.getView().setCenter(coords);
            znc_ola_map.getView().setZoom(15);
        }
    });

    return true
}

znc_ola_functions.geocoding = function (address) {

    if (!address) return {}; // Se não existir address retorna um objeto vazio.

    var url = 'https://nominatim.openstreetmap.org/search'; // URL de Geocoding do Nominatim
    // dados a serem enviados pela URL (parametros de pesquisa)
    var data = {};
    data.limit = 50;
    data.street = address.house_number + ' ' + address.street;
    // Tem Bairro que é válido para os endereços dos correios, mas não existe no openstreetmap
    // data.county = address.district;
    data.city = address.city;
    data.state = address.state;
    data.country = address.country;
    data.format = 'json';
    data.addressdetails = 1;
    data.polygon = 1;

    $.ajax({
        dataType: "json",
        url: url,
        data: data,
        error: znc_ola_functions.nominatimCallback,
        success: znc_ola_functions.getNominatimResponse,
        complete: znc_ola_functions.processNominatimResponse
    })
    // Retorna lista de objetos (endereços)
    return lista_dict_enderecos
};

znc_ola_functions.processNominatimResponse = function (jqXHR, textStatus) {
        // Chama função callback SE existir
    var failMsg = znc_ola_opt.messages.addressNotFound;
    var callBack = znc_ola_opt.elementGeocodingCallback;
    var hasCallBack = callBack ? true : false;

    var callCallback = hasCallBack &&
        textStatus === "success" &&
        lista_dict_enderecos.length > 1;

    if (callCallback) callBack.call(null, lista_dict_enderecos);
    else if (lista_dict_enderecos.length === 1) return false;
    else {
        if (failMsg) znc_ola_functions.setGeocodingResponseText(failMsg);
        var end = get_endereco_json();
        SetMapCenterOn(end);
        var layer = znc_ola_map.getLayer('znc_ola_novos_pontos');
        layer.getSource().clear();  // Limpa o source da layer de novos pontos
    }

}

znc_ola_functions.nominatimCallback = function (jqXHR, textStatus, errorThrown) {
    console.log(errorThrown);
}

znc_ola_functions.getNominatimResponse = function(response) {
    lista_dict_enderecos = [];
    var _response;
    var municipio = $('#id_municipio option:selected').text();

    znc_ola_opt.elementPointWKT.changeVal(''); // Define o valor de elementPointWKT para vazio
    // Se houver resposta do Nominatim
    response = _.uniqWith(response, _.isEqual); // Recupera as coordenadas transformando para znc_ola_opt.projection

    if (response.length == 1) {
        var response_address = response[0]
        var response_coordinates = znc_ola_functions.getResponseCoordinates(response_address);
        _response = znc_ola_functions.response_data(response_address, response_coordinates);
        znc_ola_functions.setMapPin(_response, response_coordinates);
        lista_dict_enderecos.push(_response);
        // Define _response com o primeiro resultado da pesquisa
    } else if (response.length > 1){

        for (i = 0; i < response.length; i++) {
            if(response[i].address.city === municipio && lista_dict_enderecos.length < 10){
                var coordinates = znc_ola_functions.getResponseCoordinates(response[i]);
                lista_dict_enderecos.push(znc_ola_functions.response_data(response[i], coordinates));
            }
        }

        lista_dict_enderecos = _.uniqWith(lista_dict_enderecos, function(a, b) {
            return a.street == b.street &&
                a.region == b.region &&
                a.suburb == b.suburb &&
                a.city_cod == b.city_cod &&
                a.state_cod == b.state_cod &&
                a.postcode == b.postcode;
            }
        );

        if (lista_dict_enderecos.length == 1) {
            var response_address = response[0];
            var response_coordinates = znc_ola_functions.getResponseCoordinates(response_address);
            _response = znc_ola_functions.response_data(response_address, response_coordinates);
            znc_ola_functions.setMapPin(_response, response_coordinates);
        }
        
        return lista_dict_enderecos;  // Devolve uma lista de endereços para o callback do geocoding
    }
}

znc_ola_functions.reverse_geocoding = function(coordinates, callback=false) {

    // Recebe cordenada e retorna os dados do endereço

    // URL de Reverse geocoding do Nominatim
    var url = 'https://nominatim.openstreetmap.org/reverse';

    // Coordenadas transformadas de znc_ola_opt.projection para EPSG:4326 (Nominatim)
    var _coordinates = ol.proj.transform([
        parseFloat(coordinates[0]),
        parseFloat(coordinates[1])
    ], znc_ola_opt.projection, 'EPSG:4326');

    // Data a ser enviada pela URL (parametros de pesquisa)
    var data = {
        limit:1,
        lon:_coordinates[0],
        lat:_coordinates[1],
        format:'json',
        addressdetails:1
    };

    // Define o valor/conteúdo de elementGeocoding para
    // "Aguarde..." enquanto não terminou a chamada
    znc_ola_functions.setGeocodingFieldText("Aguarde...");

    var _response;

    // Ajax Request
    $.getJSON(url, data, function(response) {

        // Se houver address e address.road em response
        if ('address' in response && 'road' in response.address) {
            znc_ola_functions.setGeocodingFieldText(response.address.road);
            // Define o valor/conteúdo de elementGeocoding para rua do resultado
        } else {
            znc_ola_functions.setGeocodingFieldText(znc_ola_opt.messages.addressNotFound);
            // Define o valor/conteúdo de elementGeocoding para mensagem de erro 'addressNotFound'
        }

        // Define _response com o resultado da pesquisa
        _response = response;
    }).then(() => {
        // Se NÃO houve resposta do Nominatim
        if (!_response) {
            // Retorna um objeto vazio
            _response = {};
        } else {
            // Se HOUVE resposta do Nominatim
            _response = znc_ola_functions.response_data(_response, coordinates);   
        }

        $("#id_cep").val(_response.postcode);
        $("#id_logradouro").val(_response.street);
        $("#id_numero").val(null);
        $("#id_complemento").val(null);
        $("#id_bairro").val(_response.suburb);
        $("#id_municipio").data("municipio-selected", _response.city_cod);
        $("#municipio_uf").val(_response.state_cod);
        $("#municipio_uf").trigger('onchange');

        // Chama função callback SE existir
        if (callback)
            callback.call(null, _response);

        // Retorna objeto _response
        return _response;
    });
};


znc_ola_functions.getBBOX = function(separator=false) {
    var bbox = znc_ola_map.getView().calculateExtent(znc_ola_map.getSize());
    var response = bbox;

    if (bbox.length != 4) return false;

    if (isNaN(bbox[0]) || isNaN(bbox[1]) || isNaN(bbox[2]) || isNaN(bbox[3])) return false;

    if (separator) {
        response = "";
        bbox.forEach(function(b) {
            response += b + separator;
        });
    }

    return response;
};

znc_ola_functions.response_data = function(_response, coordinates) {

    // Recupera o SRID pela configuração de inicialização
    var SRID = znc_ola_opt.projection.split(':')[1];

    // Recupera o WKT da coordenada passada
    var wkt = new ol.format.WKT().writeGeometry(new ol.geom.Point(coordinates));

    if (_response.type == "station") {
        _response.address.suburb = _response.address.station;
    }

    var estado_dict = Object.keys(estados).reduce(function (filtered, key) {
        if (estados[key]['nome'] == _response.address.state) filtered[key] = estados[key];
        return filtered;
    }, {});

    _response.address.state_cod = Object.keys(estado_dict)[0];
    _response.address.city_cod = undefined;
    if (_response.address.state_cod) {
        _response.address.city_cod = estados[_response.address.state_cod]['municipios'][_response.address.city]
    }

    return {
        street: _response.address.road,
        region: _response.address.state_district,
        suburb: _response.address.suburb,
        city: _response.address.city,
        city_cod: _response.address.city_cod,
        state: _response.address.state,
        state_cod: _response.address.state_cod,
        postcode: _response.address.postcode,
        polygon: _response.polygonpoints,
        ewkt: 'SRID='+SRID+';' + wkt,
        coordinates: coordinates,
        raw_json: _response
    };
};

var Geocoding = function(address, callback, callback_fail, set_response_text) {
    // Função Geocoding() retorna znc_ola_functions.geocoding
    return znc_ola_functions.geocoding( address, callback, callback_fail, set_response_text );
};
var SetMapCenterOn = function(address) {
    // Função Geocoding() retorna znc_ola_functions.geocoding
    return znc_ola_functions.setCenterOnDistrict(address);    
};

var ReverseGeocoding = function(coords, callback=false) {
    // Função ReverseGeocoding() retorna znc_ola_functions.reverse_geocoding
    return znc_ola_functions.reverse_geocoding( coords, callback );
};

String.prototype.Geocoding = function( callback=false ){
    /* String prototype added function
    *
    * USO:
    *   "St. Address".Geocoding(callback);
    */ 
    return Geocoding( this, callback );
};

$.fn.Geocoding = function(callback) {
    /* Object prototype added function
    *
    * USO:
    *   $("#reverse_field").Geocoding(callback);
    */ 
    return this.each(function(){
        if (typeof this == 'object' && $(this).is('input, textarea') )
            Geocoding( $(this).val(), callback );
    });
};
