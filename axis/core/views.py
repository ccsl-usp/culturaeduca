# coding: utf-8
# from django.views.decorators.cache import cache_page
import json
import os
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response

from django.apps import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.messages.views import SuccessMessageMixin
from django.core.cache import caches
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse_lazy
from django.forms import models as model_forms, ModelChoiceField
from django.shortcuts import redirect, render, get_object_or_404
from django.utils import timezone
from django.utils.http import base36_to_int
from django.views.decorators.gzip import gzip_page
from django.views.generic.base import View, TemplateView
from django.views.generic.detail import DetailView, SingleObjectMixin, SingleObjectTemplateResponseMixin
from django.views.generic.edit import FormView, UpdateView, BaseUpdateView, FormMixin, CreateView
from django.views.generic.list import ListView
from django.http import HttpResponse, HttpResponseRedirect, Http404
from .forms import (
    ProfileForm,
    RedeSocialForm,
    RedeSocialFormSet,
    ResendEmailVerificationForm,
    UserAgenteMembroForm,
    UserAgenteVinculoForm,
    UserCreationForm,
    ItemGaleriaForm,
)
from .models import User, Profile, RedeSocial, ItemGaleria
from ibge.models import SetorCensitario, UF, Municipio
from django.core.serializers import serialize

from axis.acao.forms import AcaoVinculoForm

CACHE_TIMEOUT = None
get_model = apps.get_model


cache_ibge = caches['ibge']
cache_geojson = caches['geojson']


class GenericGeoJSONListView(ListView):
    content_type = 'application/vnd.geo+json'
    http_method_names = [u'get', ]
    geom_field = 'geom'
    properties = []
    srid_out = None

    def get_queryset(self):
        queryset = super(GenericGeoJSONListView, self).get_queryset()
        if 'pk' in self.request.GET:
            pk = self.request.GET['pk']
            queryset = queryset.filter(pk=int(pk))
        else:
            filter_kw = {'{}__isnull'.format(self.geom_field): False}
            queryset = queryset.filter(**filter_kw)

        if self.srid_out:
            queryset = queryset.transform(
                self.srid_out, field_name=self.geom_field)

        return queryset

    def render_to_response(self, context, **response_kwargs):
        s = serialize('custom_geojson', self.get_queryset(),
                      geometry_field=self.geom_field,
                      fields=[self.geom_field] + self.properties)
        return HttpResponse(s, content_type=self.content_type)


lista_pessoa_fisica = gzip_page(
    GenericGeoJSONListView.as_view(
        model=Profile,
        geom_field='point',
        properties=['pk', 'target_url', 'name', 'local'],
        srid_out=4326
    )
)


class AddRequestUserMixin(object):
    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(AddRequestUserMixin,
                       self).get_form_kwargs(*args, **kwargs)
        kwargs['request_user'] = self.request.user
        return kwargs


class ProfileDetail(DetailView):
    model = User
    template_name = 'core/profile_detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ProfileDetail, self).get_context_data(*args, **kwargs)
        context['edit_mode'] = self.object.pk and self.request.user.pk == self.object.pk

        context['form_membro'] = UserAgenteMembroForm(
            request_user=self.request.user)

        context['form_vinculo'] = UserAgenteVinculoForm(
            request_user=self.request.user)
        context['form_acao_vinculo'] = AcaoVinculoForm(
            request_user=self.request.user, agente=self.object)
        context['user_agentes'] = self.object.user_agentes()
        if self.object and self.object.pk:
            context['total_galeria'] = self.object.galeria.all().count()
            kw = {
                'src_app': 'core',
                'src_model': self.model.__name__,
                'src_pk': self.object.pk,
            }
            context['galeria_list_url'] = reverse_lazy(
                'galeria_list', kwargs=kw)
            context['galeria_dashboard_url'] = reverse_lazy(
                'galeria_dashboard', kwargs=kw)
            context['galeria_counter_url'] = reverse_lazy(
                'galeria_counter', kwargs=kw)
            context['galeria_create_url'] = reverse_lazy(
                'galeria_create', kwargs=kw)

            kw_extra = {'pk': 0}
            kw_extra.update(kw)
            context['galeria_update_url'] = reverse_lazy(
                'galeria_update', kwargs=kw_extra)
        else:
            context['total_galeria'] = 0

        profile = context['object'].get_profile()
        context["tipo_agente"] = self.model.__name__.lower()
        if profile and profile.point:
            perfil_setores = SetorCensitario.objects.entorno(
                profile.point.y, profile.point.x).perfil()

            total_equipamentos, equipamentos = profile.equipamentos_entorno()

            # filtrando todos os tipos de equipamentos que tenham eq[1] >= 1
            # eq[1] é o número de equipamentos de um determinado tipo (zero é
            # quando não tem nenhum deste tipo no entorno)
            equipamentos_existentes = filter(
                lambda eq: eq[1] >= 1, equipamentos)

            # retorna apenas um dicionário com o model name do tipo de equipamento, exemplo:
            # {
            #     'escola': True,
            # }
            equipamentos_existentes = dict(
                [(model_name, True, ) for tipo, ct, equips, model_name in equipamentos_existentes])

            context.update({
                'perfil_setores': perfil_setores,
                'equipamentos': equipamentos,
                'equipamentos_existentes': equipamentos_existentes,
                'total_equipamentos': total_equipamentos,
            })

        context['membership_add_remove_urlname'] = 'agente_membership_add_remove_me'
        context['vinculo_add_remove'] = 'user_agente_vinculo_add_remove_me'
        context['acao_agente_vinculo_url'] = 'acao_agente_vinculo_add_remove_me'

        return context

    def get_object(self):
        request_user = self.request.user
        if 'pk' not in self.kwargs:
            if request_user.is_anonymous():
                raise Http404
            return request_user
        return super(ProfileDetail, self).get_object()


profile_detail = ProfileDetail.as_view()


class RemoveM2MView(SingleObjectMixin, View):
    """Class Based View q remove um item do m2m de um modelo
        Exemplo:
            View:
                area_atuacao_remove = RemoveM2MView.as_view(model=Coletivo, m2m_field='area_atuacao')
            url:
                url(
                    r'^(?P<pk>\d+)/area_atuacao_remove/(?P<area_atuacao_pk>\d+)/$',
                    views.area_atuacao_remove,
                    name='area_atuacao_remove'
                ),
            Uso:
                <a class="btn" href="{% url 'area_atuacao_remove' coletivo.pk area_atuacao.pk %}" style="color:red">X</a>
    """
    m2m_field = None
    m2m_pk = None

    def get_m2m_pk(self):
        kw_pk = self.m2m_pk
        if self.m2m_pk is None:
            kw_pk = self.m2m_field + '_pk'
        return self.kwargs[kw_pk]

    def remove(self, request, *args, **kwargs):
        related_obj = getattr(
            self.model,
            self.m2m_field
        ).field.related_model.objects.get(pk=self.get_m2m_pk())
        getattr(self.get_object(), self.m2m_field).remove(related_obj)
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, *args, **kwargs):
        return self.remove(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.remove(request, *args, **kwargs)


class AddM2MView(SingleObjectTemplateResponseMixin, BaseUpdateView):
    """Class Based View q adiciona um item do m2m de um modelo
        Exemplo:
            View:
                area_atuacao_add = AddM2MView.as_view(model=Coletivo, m2m_field='area_atuacao')
            url:
                # a pk é a pk do modelo pai aqui ele é o Coletivo
                url(r'^(?P<pk>\d+)/area_atuacao/add/$', views.area_atuacao_add, name='area_atuacao_add'),
            Uso:
                <a class="btn" href="{% url 'area_atuacao_add' coletivo.pk %}" style="color:red">Nova area de atuação para o coletivo</a>
    """
    m2m_field = None
    form_class = None
    template_name_suffix = '_add'

    def get_template_names(self):
        return "%s/%s%s.html" % (
            self.model._meta.app_label,
            self.m2m_field,
            self.template_name_suffix
        )

    def get_form_class(self):
        m2m_field = self.m2m_field
        if m2m_field is None:
            raise ImproperlyConfigured(
                u"Você precisa especificar o campo m2m_field"
            )
        if self.form_class is not None:
            return self.form_class

        model_form = model_forms.modelform_factory(
            self.model, fields=[m2m_field])
        return type(model_form.__name__, (model_form, ), {
            'save': lambda sf, commit=True: (sf.instance, getattr(sf.instance, m2m_field).add(sf.cleaned_data[m2m_field]))[0],
            m2m_field: ModelChoiceField(
                queryset=getattr(
                    self.model,
                    self.m2m_field
                ).field.related_model.objects.exclude(
                    pk__in=getattr(self.object, m2m_field).values_list(
                        'pk', flat=True)
                )
            )
        })


class SearchFormListView(FormMixin, ListView):
    http_method_names = ['get']
    filter_by_user = False

    def get_form_kwargs(self):
        return {'initial': self.get_initial(), 'data': self.request.GET}

    def get_context_data(self, *args, **kwargs):
        context = super(SearchFormListView, self).get_context_data(
            *args, **kwargs)
        query_string = self.request.GET.copy()
        query_string.pop('page', '1')
        context['query_string'] = query_string.urlencode()
        return context

    def get(self, request, *args, **kwargs):
        self.form = self.get_form(self.get_form_class())
        if self.form.is_valid():
            self.object_list = self.form.get_result_queryset()
        else:
            self.object_list = self.get_queryset()

        if self.filter_by_user:
            self.object_list = self.object_list.by_user(self.request.user)

        context = self.get_context_data(
            object_list=self.object_list,
            form=self.form,
            url_params=request.GET.urlencode()
        )

        return self.render_to_response(context)


def is_anonymous(user):
    return user.is_anonymous()


class UserCreateView(CreateView):
    model = User
    form_class = UserCreationForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        if not form.instance.pk and settings.REQUIRE_VERIFICATION_EMAIL:
            messages.success(
                self.request,
                u'Um e-mail de verificação foi enviado para <strong>%s</strong>. Para entrar, acesse o link no email.' % (
                    form.instance.email
                )
            )
        elif not form.instance.pk:
            messages.success(
                self.request,
                u'Seu cadastro foi concluído com sucesso.'
            )
        return super(UserCreateView, self).form_valid(form)


user_create = user_passes_test(
    is_anonymous,
    login_url=reverse_lazy('profile_detail')
)(UserCreateView.as_view())


class UserEmailVerificationConfirm(View):
    def get(self, *args, **kwargs):
        uid = kwargs.pop('uid')
        token = kwargs.pop('token')

        uid_int = base36_to_int(uid)
        user = User.objects.filter(pk=uid_int).first()

        if user.is_active:
            messages.warning(
                self.request,
                u'''Sua conta já está verificada. Deseja
                <a style="text-decoration:none;color:#3a75b1;" href="%s">recuperar</a> sua senha?''' % (
                    reverse_lazy('password_reset')
                )
            )
        elif self._token_verification(user, token):
            # TODO: implementar backend para facilitar login automático
            user.backend = settings.AUTHENTICATION_BACKENDS[0]
            login(self.request, user)  # authenticate user
        else:
            messages.warning(
                self.request,
                u'''Houve um erro. Sua conta não foi verificada. Deseja
                <a style="text-decoration:none;color:#3a75b1;" href="%s">reenviar</a> e-mail de verificação?''' % (
                    reverse_lazy('email_verification_resend')
                )
            )

        return HttpResponseRedirect(reverse_lazy('login'))

    def _token_verification(self, user, token):
        from .tokens import email_verification_token_generator as token_generator

        token_verified = user is not None and token_generator.check_token(
            user, token)

        if token_verified:
            user.is_active = True  # activate user
            user.last_emailverification = timezone.now()  # invalidate token
            user.save()

        return token_verified


user_email_verification_confirm = UserEmailVerificationConfirm.as_view()


class UserEmailVerificationResend(FormView):

    template_name = 'registration/email_verification_resend.html'
    form_class = ResendEmailVerificationForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        if form.send_email():
            messages.success(
                self.request,
                u'O e-mail de verificação da conta foi reenviado para <strong>%s</strong>.' % (
                    form.user.email
                )
            )
        else:
            messages.warning(
                self.request, u'Não foi possível reenviar o email. Verifique o email e tente novamente.')
        return super(UserEmailVerificationResend, self).form_valid(form)


user_email_verification_resend = UserEmailVerificationResend.as_view()


@login_required
def profile_form(request):

    try:
        profile = Profile.objects.get(user=request.user)
    except Profile.DoesNotExist:
        messages.error(request, u'Você deve completar o seu Perfil!')
        profile = None

    if profile and profile.point:
        profile.point.transform(3857)  # ponto do mapa vem em mercator.

    form = ProfileForm(
        instance=profile,
        data=request.POST or None,
        files=request.FILES or None,
        request_user=request.user
    )
    redesocial_formset = RedeSocialFormSet(
        data=request.POST or None,
        instance=request.user,
        request_user=request.user,
    )

    if request.POST:
        if form.is_valid() and redesocial_formset.is_valid():
            form.save()
            redesocial_formset.save()
            messages.success(request, u'Seu perfil foi atualizado com sucesso')
            # se vier um parâmetro no get NEXT
            # o usuário foi redirecionado para completar o perfil
            # então deve ser redirecionado de volta para a
            # url que o redirecionou para completar o perfil
            # _next = request.GET.get('next', False)
            # if _next:
            #     return redirect(_next)
            return HttpResponseRedirect(reverse_lazy('profile_detail'))

        else:
            messages.warning(
                request,
                u'Houve um erro no formulário. Seus dados não foram salvos.'
            )

    return render(
        request,
        'core/profile_form.html',
        {
            'form': form,
            'redesocial_formset': redesocial_formset,
        }
    )


profile_form_tesre = CreateView.as_view(
    model=Profile,
    form_class=ProfileForm,
    success_url=reverse_lazy('login')
)


def usuario_profile(request):

    render(request, "teste_form.html")


@login_required
def generic_delete_from_model(request, app_model=None, object_id=None):
    next = request.GET.get('next', 'home')
    app_name, model_name = app_model.split('.', 1)

    model = get_model(app_name, model_name)
    obj = get_object_or_404(model, pk=object_id)
    can_delete = True

    if hasattr(obj, 'user_can_delete'):
        if not obj.user_can_delete(request.user):
            messages.success(request, u"Não foi possível deletar")
            can_delete = False

    if can_delete:
        obj.delete()
        messages.success(request, u"Deletado com sucesso")

    return redirect(next)


class RedeSocialMixin(object):
    form_class = RedeSocialForm
    model = RedeSocial

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(RedeSocialMixin, self).get_form_kwargs(*args, **kwargs)
        kwargs['user'] = get_object_or_404(User, pk=self.kwargs['pk'])
        if kwargs['user'].pk != self.request.user.pk:
            raise Http404
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super(RedeSocialMixin, self).get_context_data(
            *args, **kwargs)
        context['user'] = context['form'].user
        return context

    def get_success_url(self):
        return reverse_lazy('profile_detail')


class RedeSocialCreateView(RedeSocialMixin, SuccessMessageMixin, AddRequestUserMixin, CreateView):
    success_message = u'Rede Social Adicionada com sucesso!'


class RedeSocialUpdateView(RedeSocialMixin, SuccessMessageMixin, AddRequestUserMixin, UpdateView):
    pk_url_kwarg = 'rede_social_pk'
    success_message = u'Rede Social Atualizada com sucesso'


rede_social_create = RedeSocialCreateView.as_view()
rede_social_update = RedeSocialUpdateView.as_view()


class UFDetailView(DetailView):
    model = UF
    template_name = 'uf_detail.html'

    def get_object(self):
        uf_sigla = self.kwargs.get('uf_sigla')
        obj = self.model.objects.get(uf=uf_sigla)
        if obj.geom:
            obj.geom.transform(3857)
        return obj

    def get_context_data(self, **kwargs):
        context = super(UFDetailView, self).get_context_data(**kwargs)

        perfil_setores = cache_ibge.get('perfil_uf_{}'.format(self.object.uf))

        if not perfil_setores:
            perfil_setores = SetorCensitario.objects.filter(
                municipio__uf_sigla=self.object.uf
            ).perfil()
            cache_ibge.set('perfil_uf_{}'.format(
                self.object.uf), perfil_setores, timeout=None)

        object_geojson = cache_geojson.get('geom_uf_{}'.format(self.object.uf))
        
        if not object_geojson:
            object_geojson = serialize(
                'custom_geojson', [self.object],
                srid=3857, geometry_field='geom', fields=['geom']
            )

        context.update({
            'perfil_setores': perfil_setores,
            'object_geojson': object_geojson
        })

        return context


class MunicipioDetailView(DetailView):
    model = Municipio
    pk_url_kwarg = 'mun_cod'
    template_name = 'municipio_detail.html'

    def get_context_data(self, **kwargs):
        context = super(MunicipioDetailView, self).get_context_data(**kwargs)

        perfil_setores = cache_ibge.get('perfil_mun_{}'.format(self.object.pk))

        if not perfil_setores:
            perfil_setores = SetorCensitario.objects.filter(
                municipio__pk=self.object.pk
            ).perfil()
            cache_ibge.set('perfil_mun_{}'.format(
                self.object.pk), perfil_setores, timeout=None)

        object_geojson = cache_geojson.get('geom_mun_{}'.format(self.object.pk))
        
        if not object_geojson:
            object_geojson = serialize(
                'custom_geojson', [self.object],
                srid=3857, geometry_field='geom', fields=['geom']
            )        

        context.update({
            'perfil_setores': perfil_setores,
            'object_geojson': object_geojson,
        })

        return context

    def get_object(self):
        obj = super(MunicipioDetailView, self).get_object()

        if obj.geom:
            obj.geom.transform(3857)

        return obj


uf_detail = UFDetailView.as_view()
municipio_detail = MunicipioDetailView.as_view()


class GaleriaList(ListView):
    template_name = 'galeria/list.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(GaleriaList, self).get_context_data(*args, **kwargs)
        if self.kwargs.get('src_model') == 'User':
            if self.instance and self.instance.pk and self.request.user.is_authenticated() and self.request.user.pk == self.instance.pk:
                ctx['can_change_gallery'] = True
        else:
            ctx['can_change_gallery'] = self.instance.can_change_gallery(
                self.request.user)

        kw = {
            'src_app': self.kwargs.get('src_app'),
            'src_model': self.kwargs.get('src_model'),
            'src_pk': self.kwargs.get('src_pk'),
        }
        ctx['galeria_list_url'] = reverse_lazy('galeria_list', kwargs=kw)

        return ctx

    def get_queryset(self):
        src_pk = self.kwargs.get('src_pk')
        src_app = self.kwargs.get('src_app')
        src_model = self.kwargs.get('src_model')

        model = apps.get_model(src_app, src_model)
        self.instance = get_object_or_404(model, pk=src_pk)
        qs = self.instance.galeria.all()

        return qs


galeria_list = GaleriaList.as_view()


class GaleriaDashboard(TemplateView):
    template_name = 'galeria/dashboard.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(GaleriaDashboard, self).get_context_data(*args, **kwargs)
        src_pk = self.kwargs.get('src_pk')
        src_app = self.kwargs.get('src_app')
        src_model = self.kwargs.get('src_model')
        model = apps.get_model(src_app, src_model)
        self.instance = get_object_or_404(model, pk=src_pk)
        galeria_imagem = self.instance.galeria.filter(
            tipo=ItemGaleria.TIPO.IMAGEM)
        galeria_audiovisual = self.instance.galeria.filter(
            tipo=ItemGaleria.TIPO.AUDIOVISUAL)
        galeria_documento = self.instance.galeria.filter(
            tipo=ItemGaleria.TIPO.DOCUMENTO)
        ctx['galeria_imagem'] = galeria_imagem
        ctx['galeria_audiovisual'] = galeria_audiovisual
        ctx['galeria_documento'] = galeria_documento
        ctx['can_change_gallery'] = self.instance.can_change_gallery(
            self.request.user)
        return ctx


galeria_dashboard = GaleriaDashboard.as_view()


class GaleriaCounter(View):
    def get(self, *args, **kwargs):
        src_pk = self.kwargs.get('src_pk')
        src_app = self.kwargs.get('src_app')
        src_model = self.kwargs.get('src_model')

        model = apps.get_model(src_app, src_model)
        self.instance = get_object_or_404(model, pk=src_pk)
        galeria = self.instance.galeria.all()

        return HttpResponse(str(galeria.count()))


galeria_counter = GaleriaCounter.as_view()


class GaleriaCreate(CreateView):
    template_name = 'galeria/form.html'
    model = ItemGaleria
    form_class = ItemGaleriaForm
    success_url = '/'

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(GaleriaCreate, self).get_form_kwargs(*args, **kwargs)

        src_pk = self.kwargs.get('src_pk')
        src_app = self.kwargs.get('src_app')
        src_model = self.kwargs.get('src_model')

        model = apps.get_model(src_app, src_model)
        parent_instance = get_object_or_404(model, pk=src_pk)

        kwargs['user'] = self.request.user
        kwargs['parent'] = parent_instance
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(GaleriaCreate, self).get_context_data(*args, **kwargs)
        src_pk = self.kwargs.get('src_pk')
        src_app = self.kwargs.get('src_app')
        src_model = self.kwargs.get('src_model')
        model = apps.get_model(src_app, src_model)
        self.instance = get_object_or_404(model, pk=src_pk)
        ctx['can_change_gallery'] = self.instance.can_change_gallery(
            self.request.user)
        return ctx

    def form_valid(self, form):
        super(GaleriaCreate, self).form_valid(form)
        return HttpResponse("ok")


galeria_create = GaleriaCreate.as_view()


class GaleriaUpdate(UpdateView):
    template_name = 'galeria/form.html'
    model = ItemGaleria
    form_class = ItemGaleriaForm
    success_url = '/'

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(GaleriaUpdate, self).get_form_kwargs(*args, **kwargs)

        src_pk = self.kwargs.get('src_pk')
        src_app = self.kwargs.get('src_app')
        src_model = self.kwargs.get('src_model')

        model = apps.get_model(src_app, src_model)
        parent_instance = get_object_or_404(model, pk=src_pk)

        kwargs['user'] = self.request.user
        kwargs['parent'] = parent_instance
        return kwargs

    def get_context_data(self, *args, **kwargs):
        ctx = super(GaleriaUpdate, self).get_context_data(*args, **kwargs)
        src_pk = self.kwargs.get('src_pk')
        src_app = self.kwargs.get('src_app')
        src_model = self.kwargs.get('src_model')
        model = apps.get_model(src_app, src_model)
        self.instance = get_object_or_404(model, pk=src_pk)
        ctx['can_change_gallery'] = self.instance.can_change_gallery(
            self.request.user)
        return ctx

    def form_valid(self, form):
        super(GaleriaUpdate, self).form_valid(form)
        return HttpResponse("ok")


galeria_update = GaleriaUpdate.as_view()
