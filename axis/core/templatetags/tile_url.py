from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def tile_url(model_name, extension='pbf'):
    return '%s%s/{z}/{x}/{y}.%s' % (
        settings.TILESTACHE_BASE_URL, model_name, extension)
