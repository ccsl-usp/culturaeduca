# coding: utf-8
from django.contrib import messages
from django.shortcuts import redirect


class RedirecionaParaFormProfileMiddleware(object):
    PROFILE_KEY = 'profile'
    '''
    Redireciona para o Form de Profile
        se o user não tiver um profile
        após o login
    '''
    def process_request(self, request):
        if request.user.pk and self.PROFILE_KEY not in request.session:
            profile = request.user.get_profile()
            request.session[self.PROFILE_KEY] = 'empty' if not profile else profile.pk
            if request.session[self.PROFILE_KEY] == 'empty':
                # messages.error(request, u'Você deve completar o seu Perfil!')
                return redirect('profile_edit')
        return None
