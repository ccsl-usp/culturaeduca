# -*- coding: utf-8 -*-
from axis.core.models import (
    AreaAtuacao,
    AreaEstudoPesquisa,
    Endereco as EnderecoBase,
    LinguagemArtistica,
    ManifestacaoCultural,
    PublicoFocal,
    RedeSocialAbstract,
    UploadToPathAndRename,
    UserAddUpd,
    ItemGaleria,
)
from django.apps import apps
from django.conf import settings
from django.db.models import Case, When, Q, Value
from django.contrib.gis.db import models
from django.contrib.sites.models import Site
from django.core.files.storage import get_storage_class
from django.core.urlresolvers import reverse_lazy, reverse
from django.template.loader import get_template
from utils import equipamentos_entorno

avatar_storage = get_storage_class(settings.DEFAULT_FILE_STORAGE)()


class NaturezaJuridica(models.Model):
    class TIPO(object):
        ADMINISTRACAO_PUBLICA = 1
        ENTIDADES_EMPRESARIAIS = 2
        ENTIDADES_SEM_FINS_LUCRATIVOS = 3
        PESSOAS_FISICAS = 4
        ORGANIZACOES_INTERNACIONAIS = 5
        CHOICES = (
            (ADMINISTRACAO_PUBLICA, u'Administração Pública'),
            (ENTIDADES_EMPRESARIAIS, u'Entidades Empresariais'),
            (ENTIDADES_SEM_FINS_LUCRATIVOS, u'Entidades sem Fins Lucrativos'),
            (PESSOAS_FISICAS, u'Pessoas Físicas'),
            (ORGANIZACOES_INTERNACIONAIS,
             u'Organizações Internacionais e Outras Instituições Extraterritoriais'),
        )

    tipo = models.SmallIntegerField(u'Tipo', choices=TIPO.CHOICES)
    codigo = models.CharField(u'Código', max_length=20)
    nome = models.CharField(u'Descrição', max_length=255)

    @property
    def display_name(self):
        if self.tipo != self.TIPO.PESSOAS_FISICAS:
            return u'%s - %s' % (self.codigo, self.nome, )
        return u'%s' % self.nome

    def __unicode__(self):
        return self.nome


class AgenteRedeSocial(RedeSocialAbstract):
    agente = models.ForeignKey('Agente')


class Agente(EnderecoBase):

    class ABRANGENCIA(object):
        NACIONAL = 1
        ESTADUAL = 2
        MUNICIPAL = 3
        LOCAL = 4
        CHOICES = (
            (ESTADUAL, u'Estadual'),
            (LOCAL, u'Local'),
            (MUNICIPAL, u'Municipal'),
            (NACIONAL, u'Nacional'),
        )

    nome = models.CharField(u'Nome', max_length=200)
    email = models.EmailField(u'Email')
    avatar = models.ImageField(max_length=1024, upload_to=UploadToPathAndRename(
        'agente/avatar'), storage=avatar_storage, blank=True)
    telefone1 = models.CharField(
        u'Telefone 1', max_length=20, null=True, blank=True)
    telefone2 = models.CharField(
        u'Telefone 2', max_length=20, null=True, blank=True)
    site = models.URLField(null=True, blank=True)
    publico_focal = models.ManyToManyField(PublicoFocal)
    publico_focal_outro = models.CharField(
        verbose_name=u'Outro Tipo de Público Focal', max_length=512, blank=True)
    atuacao = models.TextField(
        u'Breve Histórico de Atuação', null=True, blank=True)
    area_atuacao = models.ManyToManyField(AreaAtuacao)
    area_atuacao_outro = models.CharField(
        verbose_name=u'Outras areas de atuacao', max_length=512, blank=True)
    data_abertura = models.PositiveSmallIntegerField(
        u'Data de Abertura/Atua desde', null=True, help_text='formato: AAAA')
    objetivo = models.TextField(u'Objetivo/Fim', null=True, blank=True)
    numero_integrantes = models.IntegerField(
        u'Número de Integrantes', null=True, blank=True)

    user_add = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="agentes_created_by", editable=False)
    date_add = models.DateTimeField(auto_now_add=True, editable=False)

    user_upd = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="agentes_modified_by", editable=False)
    date_upd = models.DateTimeField(auto_now=True, editable=False)

    galeria = models.ManyToManyField(ItemGaleria)

    objects = models.GeoManager()

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio, geom_field='point')

    def is_member(self, user):
        return not user.is_anonymous() and self.membros_ativos().filter(user=user).count() >= 1

    def is_blocked_member(self, user):
        return not user.is_anonymous() and self.agentemembrobloqueio_set.filter(user=user).count() >= 1

    def is_administrador(self, user):
        return not user.is_anonymous() and self.membros_ativos().filter(user=user, is_administrador=True).count() >= 1

    def is_owner(self, user):
        return not user.is_anonymous() and self.user_add == user

    def can_edit(self, user):
        user_has_perm = self.is_administrador(user) or self.is_owner
        return not user.is_anonymous() and user_has_perm

    def can_change_gallery(self, user):
        return any([self.is_member(user), self.is_administrador(user), self.is_owner(user)])

    def add_member(self, user, is_administrador=False, user_add=None):
        if AgenteMembroBloqueio.objects.filter(user=user, agente=self).count():
            return False

        agentemembro = AgenteMembro(
            user=user, is_administrador=is_administrador)

        if user_add:
            agentemembro.user_add = user_add
            agentemembro.user_upd = user_add
        else:
            agentemembro.user_add = user
            agentemembro.user_upd = user

        self.agentemembro_set.add(agentemembro, bulk=False)

        return True

    def add_vinculo(self, agente_to, user_add):
        AgenteVinculo = apps.get_model('agente', 'AgenteVinculo')
        agentevinculo = AgenteVinculo(
            agente_to=agente_to, user_add=user_add, user_upd=user_add)
        self.agentevinculo_set.add(agentevinculo, bulk=False)

        return True

    def remove_vinculo(self, agente_to):
        self.agentevinculo_set.filter(agente_to=agente_to).delete()

    def has_vinculo(self, agente_to):
        return self.agentevinculo_set.filter(agente_to=agente_to).count() >= 1

    def remove_member(self, user):
        if self.agentemembro_set.filter(is_administrador=True).exclude(user=user).count() >= 1:
            self.agentemembro_set.filter(user=user).delete()

    def membros_ativos(self):
        return self.agentemembro_set.filter(user__is_active=True)

    def send_new_member_email(self, user):
        from django.core.mail import send_mail

        current_site = Site.objects.get_current()
        subject = u'[Cultura Educa] - Novo membro em agente'
        template = get_template('agente/email/email_novo_agentemembro.html')

        email = template.render({
            'site_name': current_site.name,
            'domain': current_site.domain,
            'protocol': u'https',
            'agente': self.get_child(),
            'user': user,
        })

        send_emails = self.agentemembro_set.filter(
            is_administrador=True).values_list('user__email', flat=True)

        return send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, send_emails, html_message=email)

    def send_new_agente_email(self, user):
        from django.core.mail import send_mail

        current_site = Site.objects.get_current()
        subject = u'[Cultura Educa] - Novo agente vinculado'
        template = get_template('agente/email/email_novo_agentevinculo.html')

        email = template.render({
            'site_name': current_site.name,
            'domain': current_site.domain,
            'protocol': u'https',
            'agente': self.get_child(),
            'user': user,
        })

        send_emails = self.agentemembro_set.filter(
            is_administrador=True).values_list('user__email', flat=True)

        return send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, send_emails, html_message=email)

    def get_child(self):
        # pega a url do agente (coletivo, instituicao, conselho ou movimentosocial)
        for tipo in [u'coletivo', u'instituicao', u'conselho', u'movimentosocial', ]:
            try:
                child = getattr(self, tipo)
                return child
            except AttributeError:
                pass

    def get_absolute_url(self):
        return self.get_child().get_absolute_url()

    @property
    def app_model(self):
        return 'acao.{}'.format(self.get_child().__class__.__name__)

    @property
    def target_url(self):
        return self.get_absolute_url()

    @property
    def local(self):
        return get_template('mapa/popup/coletivo_popup.html').render({'agente': self})

    @property
    def tipo(self):
        return self.get_child().__class__._tipo()

    @property
    def projetos(self):
        return apps.get_model('acao', 'Projeto').objects.filter(agente=self.pk)

    @property
    def atividades(self):
        return apps.get_model('acao', 'Atividade').objects.filter(agente=self.pk)

    @property
    def acoes(self):
        return apps.get_model('acao', 'Acao').objects.filter(agente=self.pk)

    @property
    def agentes(self):
        pks = self.agentevinculo_set.values_list('agente_to__pk', flat=True)
        return apps.get_model('agente', 'Agente').objects.filter(pk__in=pks)

    @property
    def usuarios_vinculados(self):
        pks = self.user_vinculo.values_list('user', flat=True)
        return apps.get_model('core', 'User').objects.filter(pk__in=pks)

    @property
    def transformed_vinculos(self):
        """Transforma ponto para o SRID do mapa, utilizado na aba rede."""
        agentes = self.agentes
        agentes.transform(3857)
        return agentes

    @property
    def all_acoes(self):
        Acao = apps.get_model('acao', 'Acao')
        pks_agentes = list(self.agentevinculo_set.values_list(
            'agente_to__pk', flat=True)) or [0]
        pks_acoes = list(self.acaoagentevinculo_set.values_list(
            'acao__pk', flat=True)) or [0]
        acoes = Acao.objects.filter(
            Q(agente__pk__in=pks_agentes) | Q(pk__in=pks_acoes)
        ).order_by('pk').distinct('pk')
        return acoes.annotate(
            sou_vinculado=Case(
                When(Q(pk__in=pks_acoes), then=Value(True)),
                default=Value(False),
                output_field=models.BooleanField()
            ),
            acao_outro_agente=Case(
                When(Q(agente__pk__in=pks_agentes), then=Value(True)),
                default=Value(False),
                output_field=models.BooleanField()
            )
        )

    @property
    def acoes_projetos(self):
        """Retorna todos os projetos que o user interage.
        """
        return self.all_acoes.filter(projeto__isnull=False)

    @property
    def acoes_atividades(self):
        """Retorna todos as atividades que o user interage.
        """
        return self.all_acoes.filter(atividade__isnull=False)

    @property
    def total_rede(self):
        agentes = self.agentes.count()
        usuarios = self.user_vinculo.count()
        # acoes = self.acoes_projetos.count() + self.acoes_atividades.count()
        return agentes + usuarios


class AgenteMembro(UserAddUpd):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Usuário')
    agente = models.ForeignKey(Agente)
    is_administrador = models.BooleanField(u'Membro Administrador')

    def user_can_delete(self, user):
        return self.user != user and self.agente.is_administrador(user)

    def __unicode__(self):
        return u"%s -> %s" % (self.user.nome, self.agente.nome)


class AgenteVinculo(UserAddUpd):
    agente_from = models.ForeignKey(Agente)
    agente_to = models.ForeignKey(Agente, related_name='agente_vinculado')

    def user_can_delete(self, user):
        return self.agente_from.is_administrador(user)

    def __unicode__(self):
        return u"%s -> %s" % (self.agente_from.nome, self.agente_to.nome)


class UserVinculo(UserAddUpd):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Usuário')
    agente = models.ForeignKey(Agente, related_name='user_vinculo')

    def user_can_delete(self, user):
        return self.agente.is_administrador(user)

    def __unicode__(self):
        return u"%s -> %s" % (self.user.nome, self.agente.nome)

    class Meta:
        unique_together = (('user', 'agente'),)


class AgenteMembroBloqueio(UserAddUpd):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Usuário')
    agente = models.ForeignKey(Agente)

    def user_can_delete(self, user):
        return self.user != user and self.agente.is_administrador(user)


class Coletivo(Agente):
    ''' Coletivo ou Grupo '''
    ABRANGENCIA = Agente.ABRANGENCIA

    abrangencia = models.SmallIntegerField(
        u'Abrangência geográfica', choices=ABRANGENCIA.CHOICES, null=True, blank=True)
    area_pesquisa = models.ManyToManyField(
        AreaEstudoPesquisa, verbose_name=u'Área de Estudo e Pesquisa', blank=True, related_name=u'coletivos')
    nome_contato = models.CharField(
        u'Nome do Contato de Referência', max_length=255)
    email_contato = models.EmailField(
        u'E-mail Contato de Referência', null=True, blank=True)
    linguagem_artistica = models.ManyToManyField(
        LinguagemArtistica, verbose_name=u'Linguagens Artísticas', blank=True, related_name=u'coletivos')
    manifestacao_cultural = models.ManyToManyField(
        ManifestacaoCultural, verbose_name=u'Manifestações Culturais', blank=True, related_name=u'coletivos')
    manifestacao_cultural_outro = models.CharField(
        verbose_name=u'Outro Tipo de Manifestação Cultural', max_length=512, null=True, blank=True)

    objects = models.GeoManager()

    def get_absolute_url(self):
        return reverse('coletivo_detail', args=(self.pk, ))

    def get_edit_url(self):
        return reverse('coletivo_update', args=(self.pk, ))

    def get_create_url(self):
        return self.__class__._get_create_url()

    @classmethod
    def _get_create_url(cls):
        return reverse('coletivo_create')

    @classmethod
    def _tipo(cls):
        return u'%s' % cls._meta.verbose_name

    @property
    def lista_url(self):
        return reverse_lazy('lista_coletivo')

    class Meta:
        verbose_name = u'Coletivo'

    @property
    def tipo(self):
        return self._meta.verbose_name


class Instituicao(Agente):
    ''' Instituição '''
    ABRANGENCIA = Agente.ABRANGENCIA

    class NATUREZA_ADMINISTRATIVA(object):
        PUBLICA = 1
        PRIVADA = 2
        MISTA = 3
        CHOICES = (
            (PUBLICA, u'Pública'),
            (PRIVADA, u'Privada'),
            (MISTA, u'Mista'),
        )

    abrangencia = models.SmallIntegerField(
        u'Abrangência geográfica', choices=ABRANGENCIA.CHOICES, null=True, blank=True)
    area_pesquisa = models.ManyToManyField(
        AreaEstudoPesquisa, verbose_name=u'Área de Estudo e Pesquisa', blank=True)
    linguagem_artistica = models.ManyToManyField(
        LinguagemArtistica, verbose_name=u'Linguagens Artísticas', blank=True)
    manifestacao_cultural = models.ManyToManyField(
        ManifestacaoCultural, verbose_name=u'Manifestações Culturais', blank=True)
    manifestacao_cultural_outro = models.CharField(
        verbose_name=u'Outro Tipo de Manifestação Cultural', max_length=512, null=True, blank=True)
    responsavel_nome = models.CharField(u'Nome do Responsável', max_length=255)
    responsavel_email = models.EmailField(
        u'E-mail do Responsável', null=True, blank=True)
    cnpj = models.CharField('CNPJ', max_length=20)
    natureza_administrativa = models.SmallIntegerField(
        u'Natureza Administrativa', choices=NATUREZA_ADMINISTRATIVA.CHOICES)
    natureza_juridica = models.ForeignKey(
        NaturezaJuridica, null=True, blank=True)
    natureza_juridica_outro = models.CharField(
        'Outras instituições territoriais', max_length=512, blank=True)

    objects = models.GeoManager()

    def get_absolute_url(self):
        return reverse('instituicao_detail', args=(self.pk, ))

    def get_edit_url(self):
        return reverse('instituicao_update', args=(self.pk, ))

    def get_create_url(self):
        return self.__class__._get_create_url()

    @classmethod
    def _get_create_url(cls):
        return reverse('instituicao_create')

    @classmethod
    def _tipo(cls):
        return u'%s' % cls._meta.verbose_name

    @property
    def lista_url(self):
        return reverse_lazy('lista_instituicao')

    def natureza_juridica_other(self):
        if not self.natureza_juridica:
            return False

        return self.natureza_juridica.pk == NaturezaJuridica.objects.get(nome__icontains="Outras Instituições").pk

    class Meta:
        verbose_name = u'Instituição'

    @property
    def tipo(self):
        return self._meta.verbose_name


class ConselhoComposicaoMembro(models.Model):
    nome = models.CharField(u'Nome', max_length=255, blank=True)
    cargo = models.CharField(u'Cargo', max_length=255, blank=True)
    conselho = models.ForeignKey('Conselho', default='')


class Conselho(Agente):
    ''' Fórum, Conselho ou Comitê '''
    class ABRANGENCIA(Agente.ABRANGENCIA):
        DISTRITAL = 5
        CHOICES = Agente.ABRANGENCIA.CHOICES + (
            (DISTRITAL, u'Distrital'),
        )

    class ORGAO_PUBLICO_ESFERA(object):
        FEDERAL = 1
        ESTADUAL = 2
        MUNICIPAL = 3
        DISTRITAL = 4
        CHOICES = (
            (FEDERAL, u'Federal'),
            (ESTADUAL, u'Estadual'),
            (MUNICIPAL, u'Municipal'),
            (DISTRITAL, u'Distrital'),
        )

    abrangencia = models.SmallIntegerField(
        u'Abrangência geográfica', choices=ABRANGENCIA.CHOICES, null=True, blank=True)
    vinculo_orgao_publico = models.BooleanField(u'Vínculo com órgão público')
    vinculo_orgao_publico_nome = models.CharField(
        u'Orgão público vinculado', max_length=255, null=True, blank=True)
    vinculo_orgao_publico_esfera = models.SmallIntegerField(
        u'Esfera do órgão público', choices=ORGAO_PUBLICO_ESFERA.CHOICES, null=True, blank=True)
    mandato_inicio = models.DateField(
        u'Data início do mandato', null=True, blank=True)
    mandato_fim = models.DateField(
        u'Data fim do mandato', null=True, blank=True)
    periodicidade_reunioes = models.CharField(
        u'Periodicidade das reuniões', max_length=255, null=True, blank=True)
    local_sede = models.CharField(
        u'Local da sede', max_length=255, null=True, blank=True)

    objects = models.GeoManager()

    def get_absolute_url(self):
        return reverse('conselho_detail', args=(self.pk, ))

    def get_edit_url(self):
        return reverse('conselho_update', args=(self.pk, ))

    def get_create_url(self):
        return self.__class__._get_create_url()

    @classmethod
    def _get_create_url(cls):
        return reverse('conselho_create')

    @classmethod
    def _tipo(cls):
        return u'%s' % cls._meta.verbose_name

    @property
    def lista_url(self):
        return reverse_lazy('lista_conselho')

    class Meta:
        verbose_name = u'Fórum, Conselho ou Comitê'

    @property
    def tipo(self):
        return self._meta.verbose_name


class MovimentoSocial(Agente):
    ''' Movimento Social '''
    ABRANGENCIA = Agente.ABRANGENCIA

    abrangencia = models.SmallIntegerField(
        u'Abrangência geográfica', choices=ABRANGENCIA.CHOICES, null=True, blank=True)
    area_pesquisa = models.ManyToManyField(
        AreaEstudoPesquisa, verbose_name=u'Área de Estudo e Pesquisa', blank=True)
    nome_contato = models.CharField(
        u'Nome do Contato de Referência', null=True, blank=True, max_length=255)
    email_contato = models.EmailField(
        u'E-mail do Contato de Referência', null=True, blank=True)

    objects = models.GeoManager()

    def get_absolute_url(self):
        return reverse('movimentosocial_detail', args=(self.pk, ))

    def get_edit_url(self):
        return reverse('movimentosocial_update', args=(self.pk, ))

    def get_create_url(self):
        return self.__class__._get_create_url()

    @classmethod
    def _get_create_url(cls):
        return reverse('movimentosocial_create')

    @classmethod
    def _tipo(cls):
        return u'%s' % cls._meta.verbose_name

    @property
    def lista_url(self):
        return reverse_lazy('lista_movimentosocial')

    class Meta:
        verbose_name = u'Movimento Social'

    @property
    def tipo(self):
        return self._meta.verbose_name
