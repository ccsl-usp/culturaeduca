from django.conf.urls import url

from . import views
from axis.acao import views as acao_views


urlpatterns = [
    # url(r'^$', views.agente_create, name='agente_create'),
    url(r'^rede/pessoa-fisica$', views.pessoafisica_list, name='pessoafisica_list'),

    url(r'^coletivo/cadastro/$', views.coletivo_create, name='coletivo_create'),
    url(r'^coletivo/editar/(?P<pk>\d+)/$', views.coletivo_update, name='coletivo_update'),
    url(r'^coletivo/(?P<pk>\d+)/$', views.coletivo_detail, name='coletivo_detail'),
    url(r'^rede/coletivo$', views.coletivo_list, name='coletivo_list'),
    url(r'^coletivo/lista_coletivo.geojson$', views.lista_coletivo, name='lista_coletivo'),

    url(r'^conselho/cadastro/$', views.conselho_create, name='conselho_create'),
    url(r'^conselho/editar/(?P<pk>\d+)/$', views.conselho_update, name='conselho_update'),
    url(r'^conselho/(?P<pk>\d+)/$', views.conselho_detail, name='conselho_detail'),
    url(r'^rede/conselho$', views.conselho_list, name='conselho_list'),
    url(r'^conselho/lista_conselho.geojson$', views.lista_conselho, name='lista_conselho'),

    url(r'^instituicao/cadastro/$', views.instituicao_create, name='instituicao_create'),
    url(r'^instituicao/editar/(?P<pk>\d+)/$', views.instituicao_update, name='instituicao_update'),
    url(r'^instituicao/(?P<pk>\d+)/$', views.instituicao_detail, name='instituicao_detail'),
    url(r'^rede/instituicao$', views.instituicao_list, name='instituicao_list'),
    url(r'^instituicao/lista_instituicao.geojson$', views.lista_instituicao, name='lista_instituicao'),

    url(r'^movimento-social/cadastro/$', views.movimentosocial_create, name='movimentosocial_create'),
    url(r'^movimento-social/editar/(?P<pk>\d+)/$', views.movimentosocial_update, name='movimentosocial_update'),
    url(r'^movimento-social/(?P<pk>\d+)/$', views.movimentosocial_detail, name='movimentosocial_detail'),
    url(r'^rede/movimento-social$', views.movimentosocial_list, name='movimentosocial_list'),
    url(r'^movimento-social/lista_movimentosocial.geojson$', views.lista_movimentosocial, name='lista_movimentosocial'),

    url(r'^(?P<tipo>(adiciona|remove))-membro/(?P<pk>\d+)/me/$', views.agente_membership_add_remove_me, name='agente_membership_add_remove_me'),
    # url(r'^(?P<tipo>(adiciona|remove))-vinculo-acao/(?P<pk>\d+)/me/$', acao_views.acao_agente_vinculo_add_remove_me, name='acao_agente_vinculo_add_remove_me'),
    url(r'^adiciona-membro/(?P<pk>\d+)/$', views.agente_membership_add, name='agente_membership_add'),
    url(r'^(?P<tipo>(make|undo))-administrador/(?P<pk>\d+)/$', views.agente_membership_make_undo_admin, name='agente_membership_make_undo_admin'),
    url(r'^block/(?P<pk>\d+)/$', views.agente_membership_block, name='agente_membership_block'),

    url(r'^(?P<tipo>(adiciona|remove))-agente/(?P<agente_from_pk>\d+)/(?P<agente_to_pk>\d+)/$', views.agente_vinculo_membership_add_remove, name='agente_vinculo_membership_add_remove'),
    url(r'^(?P<operacao>(adiciona|remove))-agente/(?P<agente_pk>\d+)/$', views.user_agente_vinculo_add_remove_me, name='user_agente_vinculo_add_remove_me'),
    url(r'^(?P<agente_pk>\d+)/projeto/cadastro/$', acao_views.projeto_create, name='projeto_create'),
    url(r'^(?P<agente_pk>\d+)/atividade/cadastro/$', acao_views.atividade_create, name='atividade_create'),
]

'''
    url(r'^(?P<pk>\d+)/$', views.coletivo_update, name='coletivo_update'),
    url(r'^(?P<pk>\d+)/detalhes/$', views.coletivo_detail, name='coletivo_detail'),
    url(r'^(?P<pk>\d+)/acoes/cadastro/$', views.acao_create, name='acao_create'),
    url(r'^(?P<pk>\d+)/acoes/(?P<acao_pk>\d+)/$', views.acao_update, name='acao_update'),
    url(r'^(?P<pk>\d+)/membros/cadastro/$', views.membro_create, name='membro_create'),
    url(r'^(?P<pk>\d+)/membros/(?P<membro_pk>\d+)/$', views.membro_update, name='membro_update'),

    url(r'^(?P<pk>\d+)/rede_social/cadastro/$', views.rede_social_create, name='rede_social_create'),
    url(r'^(?P<pk>\d+)/rede_social/(?P<rede_social_pk>\d+)/$', views.rede_social_update, name='rede_social_update'),

    url(r'^lista_coletivo.geojson$', views.lista_coletivo, name='lista_coletivo'),

    url(
        r'^(?P<pk>\d+)/area_atuacao_remove/(?P<area_atuacao_pk>\d+)/$',
        views.area_atuacao_remove,
        name='area_atuacao_remove'
    ),
    url(
        r'^(?P<pk>\d+)/publico_focal_remove/(?P<publico_focal_pk>\d+)/$',
        views.publico_focal_remove,
        name='publico_focal_remove'
    ),
    url(
        r'^(?P<pk>\d+)/manifestacao_cultural_remove/(?P<manifestacoes_culturais_pk>\d+)/$',
        views.manifestacao_cultural_remove,
        name='manifestacao_cultural_remove'
    ),
    url(
        r'^(?P<pk>\d+)/linguagens_artisticas_remove/(?P<linguagens_artisticas_pk>\d+)/$',
        views.linguagens_artisticas_remove,
        name='linguagens_artisticas_remove'
    ),
'''
