# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0008_auto_20160930_1601'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conselho',
            name='composicao',
        ),
        migrations.AddField(
            model_name='conselhocomposicaomembro',
            name='conselho',
            field=models.ForeignKey(default=b'', to='agente.Conselho'),
        ),
        migrations.AlterField(
            model_name='agenteredesocial',
            name='agente',
            field=models.ForeignKey(to='agente.Agente'),
        ),
    ]
