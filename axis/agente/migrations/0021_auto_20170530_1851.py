# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0020_auto_20161102_1520'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agente',
            name='data_abertura',
            field=models.PositiveSmallIntegerField(help_text=b'formato: AAAA', null=True, verbose_name='Data de Abertura/Atua desde'),
        ),
    ]
