# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0012_auto_20161013_1343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agente',
            name='site',
            field=models.URLField(null=True, blank=True),
        ),
    ]
