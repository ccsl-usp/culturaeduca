# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0018_auto_20161024_1338'),
    ]

    operations = [
        migrations.AddField(
            model_name='agente',
            name='area_atuacao_outro',
            field=models.CharField(max_length=512, verbose_name='Outras areas de atuacao', blank=True),
        ),
        migrations.AddField(
            model_name='instituicao',
            name='natureza_juridica_outro',
            field=models.CharField(max_length=512, verbose_name=b'Outras institui\xc3\xa7\xc3\xb5es territoriais', blank=True),
        ),
    ]
