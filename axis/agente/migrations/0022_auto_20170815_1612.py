# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0021_auto_20170530_1851'),
    ]

    operations = [
        migrations.AddField(
            model_name='agente',
            name='publico_focal_outro',
            field=models.CharField(max_length=512, verbose_name='Outro Tipo de P\xfablico Focal', blank=True),
        ),
        migrations.AddField(
            model_name='coletivo',
            name='manifestacao_cultural_outro',
            field=models.CharField(max_length=512, null=True, verbose_name='Outro Tipo de Manifesta\xe7\xe3o Cultural', blank=True),
        ),
        migrations.AddField(
            model_name='instituicao',
            name='manifestacao_cultural_outro',
            field=models.CharField(max_length=512, null=True, verbose_name='Outro Tipo de Manifesta\xe7\xe3o Cultural', blank=True),
        ),
    ]
