# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0024_auto_20170816_1844'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agente',
            name='cep',
            field=models.CharField(max_length=8),
        ),
    ]
