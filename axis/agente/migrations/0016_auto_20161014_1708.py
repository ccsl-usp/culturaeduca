# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0015_auto_20161014_1434'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='localsede',
            name='municipio',
        ),
        migrations.AlterField(
            model_name='conselho',
            name='local_sede',
            field=models.CharField(max_length=255, null=True, verbose_name='Local da sede', blank=True),
        ),
        migrations.DeleteModel(
            name='LocalSede',
        ),
    ]
