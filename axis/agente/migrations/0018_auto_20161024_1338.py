# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0017_auto_20161017_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agente',
            name='cep',
            field=models.CharField(help_text='formato: 12232054', max_length=8),
        ),
        migrations.AlterField(
            model_name='agente',
            name='data_abertura',
            field=models.PositiveSmallIntegerField(help_text=b'formato: AAAA', null=True, verbose_name='Data de Abertura/Atua desde', blank=True),
        ),
    ]
