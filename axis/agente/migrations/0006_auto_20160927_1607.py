# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0005_auto_20160926_1455'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='agente',
            options={},
        ),
        migrations.AlterModelOptions(
            name='coletivo',
            options={'verbose_name': 'Coletivo'},
        ),
        migrations.AlterModelOptions(
            name='conselho',
            options={'verbose_name': 'F\xf3rum, Conselho ou Comit\xea'},
        ),
        migrations.AlterModelOptions(
            name='instituicao',
            options={'verbose_name': 'Institui\xe7\xe3o'},
        ),
        migrations.AlterModelOptions(
            name='localsede',
            options={},
        ),
        migrations.AlterModelOptions(
            name='movimentosocial',
            options={'verbose_name': 'Movimento Social'},
        ),
    ]
