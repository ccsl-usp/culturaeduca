# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0031_auto_20180126_1457'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uservinculo',
            name='agente',
            field=models.ForeignKey(related_name='user_vinculo', to='agente.Agente'),
        ),
    ]
