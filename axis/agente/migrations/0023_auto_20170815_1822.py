# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0022_auto_20170815_1612'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coletivo',
            name='email_contato',
            field=models.EmailField(max_length=254, null=True, verbose_name='E-mail Contato de Refer\xeancia', blank=True),
        ),
        migrations.AlterField(
            model_name='instituicao',
            name='responsavel_nome',
            field=models.CharField(max_length=255, verbose_name='Nome do Respons\xe1vel'),
        ),
        migrations.AlterField(
            model_name='movimentosocial',
            name='email_contato',
            field=models.EmailField(max_length=254, null=True, verbose_name='E-mail do Contato de Refer\xeancia', blank=True),
        ),
    ]
