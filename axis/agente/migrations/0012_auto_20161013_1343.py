# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0011_auto_20161013_1211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agente',
            name='user_add',
            field=models.ForeignKey(related_name='agentes_created_by', editable=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='agente',
            name='user_upd',
            field=models.ForeignKey(related_name='agentes_modified_by', editable=False, to=settings.AUTH_USER_MODEL),
        ),
    ]
