# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0050_user_galeria'),
        ('agente', '0024_auto_20170816_1844'),
    ]

    operations = [
        migrations.AddField(
            model_name='agente',
            name='galeria',
            field=models.ManyToManyField(to='core.ItemGaleria'),
        ),
    ]
