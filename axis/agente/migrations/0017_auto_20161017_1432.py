# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import axis.core.models
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0016_auto_20161014_1708'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agente',
            name='avatar',
            field=models.ImageField(storage=django.core.files.storage.FileSystemStorage(), max_length=1024, upload_to=axis.core.models.UploadToPathAndRename(b'agente/avatar'), blank=True),
        ),
    ]
