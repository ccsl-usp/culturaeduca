# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0030_uservinculo'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='uservinculo',
            unique_together=set([('user', 'agente')]),
        ),
    ]
