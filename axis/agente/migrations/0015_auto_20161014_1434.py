# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0014_agente_data_abertura_int'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coletivo',
            name='manifestacao_cultural',
            field=models.ManyToManyField(related_name='coletivos', verbose_name='Manifesta\xe7\xf5es Culturais', to='core.ManifestacaoCultural', blank=True),
        ),
        migrations.AlterField(
            model_name='instituicao',
            name='manifestacao_cultural',
            field=models.ManyToManyField(to='core.ManifestacaoCultural', verbose_name='Manifesta\xe7\xf5es Culturais', blank=True),
        ),
    ]
