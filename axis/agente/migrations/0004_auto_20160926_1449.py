# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0003_auto_20160923_1554'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agenteredesocial',
            name='nome',
        ),
        migrations.AlterField(
            model_name='agente',
            name='atuacao',
            field=models.TextField(null=True, verbose_name='Breve Hist\xf3rico de Atua\xe7\xe3o', blank=True),
        ),
        migrations.AlterField(
            model_name='agente',
            name='email',
            field=models.EmailField(default='teste@teste.com', max_length=254, verbose_name='Email'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='instituicao',
            name='cnpj',
            field=models.CharField(default='0001444555/0001-90', max_length=20, verbose_name=b'CNPJ'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='instituicao',
            name='natureza_administrativa',
            field=models.SmallIntegerField(verbose_name='Natureza Administrativa', choices=[(1, 'P\xfablica'), (2, 'Privada'), (3, 'Mista')]),
        ),
        migrations.AlterField(
            model_name='naturezajuridica',
            name='tipo',
            field=models.SmallIntegerField(verbose_name='Tipo', choices=[(1, 'Administra\xe7\xe3o P\xfablica'), (2, 'Entidades Empresariais'), (3, 'Entidades sem Fins Lucrativos'), (4, 'Pessoas F\xedsicas'), (5, 'Organiza\xe7\xf5es Internacionais e Outras Institui\xe7\xf5es Extraterritoriais')]),
        ),
    ]
