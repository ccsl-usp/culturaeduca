# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agente', '0009_auto_20161003_1127'),
    ]

    operations = [
        migrations.AddField(
            model_name='agente',
            name='date_add',
            field=models.DateTimeField(default=datetime.datetime(2016, 10, 13, 15, 9, 6, 547817, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='agente',
            name='date_upd',
            field=models.DateTimeField(default=datetime.datetime(2016, 10, 13, 15, 9, 11, 363530, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='agente',
            name='user_add',
            field=models.ForeignKey(related_name='agentes_created_by', null=True, editable=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='agente',
            name='user_upd',
            field=models.ForeignKey(related_name='agentes_modified_by', null=True, editable=False, to=settings.AUTH_USER_MODEL),
        ),
    ]
