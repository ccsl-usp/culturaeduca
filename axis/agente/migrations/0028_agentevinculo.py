# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agente', '0027_agentemembrobloqueio'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgenteVinculo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_add', models.DateTimeField(auto_now_add=True)),
                ('date_upd', models.DateTimeField(auto_now=True)),
                ('agente_from', models.ForeignKey(related_name='agentes', to='agente.Agente')),
                ('agente_to', models.ForeignKey(related_name='vinculos', to='agente.Agente')),
                ('user_add', models.ForeignKey(related_name='agente_agentevinculo_created_by', editable=False, to=settings.AUTH_USER_MODEL)),
                ('user_upd', models.ForeignKey(related_name='agente_agentevinculo_modified_by', editable=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
