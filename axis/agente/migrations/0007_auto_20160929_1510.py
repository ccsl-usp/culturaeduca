# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0006_auto_20160927_1607'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agente',
            name='redesocial',
        ),
        migrations.AddField(
            model_name='agenteredesocial',
            name='agente',
            field=models.ForeignKey(default=b'', to='agente.Agente'),
        ),
    ]
