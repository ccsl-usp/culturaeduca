# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agente', '0002_auto_20160923_1524'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgenteRedeSocial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_add', models.DateTimeField(auto_now_add=True)),
                ('date_upd', models.DateTimeField(auto_now=True)),
                ('plataforma', models.SmallIntegerField(blank=True, null=True, verbose_name='Plataforma', choices=[(1, 'Google+'), (2, 'Facebook'), (3, 'Instagram'), (4, 'Pinterest'), (5, 'Snapchat'), (7, 'Skype'), (6, 'Telegram'), (9, 'Tumbrl'), (8, 'Twitter'), (10, 'Youtube')])),
                ('link', models.URLField(null=True, verbose_name='Link', blank=True)),
                ('nome', models.CharField(max_length=150, null=True, verbose_name='Nome', blank=True)),
                ('user_add', models.ForeignKey(related_name='agente_agenteredesocial_created_by', editable=False, to=settings.AUTH_USER_MODEL)),
                ('user_upd', models.ForeignKey(related_name='agente_agenteredesocial_modified_by', editable=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('plataforma', 'date_add'),
                'abstract': False,
                'verbose_name': 'Rede Social',
                'verbose_name_plural': 'Redes Sociais',
            },
        ),
        migrations.RenameModel(
            old_name='ConselhoMembro',
            new_name='ConselhoComposicaoMembro',
        ),
        migrations.RemoveField(
            model_name='redesocial',
            name='user_add',
        ),
        migrations.RemoveField(
            model_name='redesocial',
            name='user_upd',
        ),
        migrations.AlterField(
            model_name='agente',
            name='redesocial',
            field=models.ManyToManyField(to='agente.AgenteRedeSocial'),
        ),
        migrations.DeleteModel(
            name='RedeSocial',
        ),
    ]
