# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConselhoMembro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=255, verbose_name='Nome')),
                ('cargo', models.CharField(max_length=255, verbose_name='Cargo')),
            ],
        ),
        migrations.CreateModel(
            name='NaturezaJuridica',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.SmallIntegerField(verbose_name='Tipo')),
                ('codigo', models.CharField(max_length=20, verbose_name='C\xf3digo')),
                ('nome', models.CharField(max_length=255, verbose_name='Descri\xe7\xe3o')),
            ],
        ),
        migrations.AlterField(
            model_name='agentemembro',
            name='user',
            field=models.ForeignKey(verbose_name='Usu\xe1rio', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='conselho',
            name='composicao',
            field=models.ManyToManyField(to='agente.ConselhoMembro'),
        ),
        migrations.AddField(
            model_name='instituicao',
            name='natureza_juridica',
            field=models.ForeignKey(blank=True, to='agente.NaturezaJuridica', null=True),
        ),
    ]
