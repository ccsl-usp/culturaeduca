# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0007_auto_20160929_1510'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movimentosocial',
            name='nome_contato',
            field=models.CharField(max_length=255, null=True, verbose_name='Nome do Contato de Refer\xeancia', blank=True),
        ),
    ]
