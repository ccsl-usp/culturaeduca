# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0028_agentevinculo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agentevinculo',
            name='agente_from',
            field=models.ForeignKey(to='agente.Agente'),
        ),
        migrations.AlterField(
            model_name='agentevinculo',
            name='agente_to',
            field=models.ForeignKey(related_name='agente_vinculado', to='agente.Agente'),
        ),
    ]
