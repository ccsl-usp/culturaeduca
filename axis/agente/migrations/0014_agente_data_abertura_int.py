# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_data_abertura_int(apps, schema_editor):
    Agente = apps.get_model('agente', 'Agente')
    for agente in Agente.objects.filter(data_abertura__isnull=False):
        agente.data_abertura_int = agente.data_abertura.year
        agente.save()


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0013_auto_20161013_1736'),
    ]

    operations = [
        migrations.AddField(
            model_name='agente',
            name='data_abertura_int',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Data de Abertura/Atua desde', blank=True),
        ),
        migrations.RunPython(set_data_abertura_int),
        migrations.RemoveField(
            model_name='agente',
            name='data_abertura',
        ),
        migrations.RenameField(
            model_name='agente',
            old_name='data_abertura_int',
            new_name='data_abertura',
        ),
    ]
