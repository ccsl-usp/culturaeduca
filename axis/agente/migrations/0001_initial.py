# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.files.storage
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0026_auto_20160916_1032'),
        #('municipios', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Agente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cep', models.CharField(max_length=8)),
                ('logradouro', models.CharField(max_length=100)),
                ('numero', models.CharField(max_length=50, verbose_name='N\xfamero')),
                ('complemento', models.CharField(max_length=100, null=True, blank=True)),
                ('bairro', models.CharField(max_length=100)),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4674, null=True, blank=True)),
                ('nome', models.CharField(max_length=200, verbose_name='Nome')),
                ('email', models.EmailField(max_length=254, null=True, verbose_name='Email', blank=True)),
                ('avatar', models.ImageField(storage=django.core.files.storage.FileSystemStorage(), max_length=1024, upload_to=b'agente/avatar', blank=True)),
                ('telefone1', models.CharField(max_length=20, null=True, verbose_name='Telefone 1', blank=True)),
                ('telefone2', models.CharField(max_length=20, null=True, verbose_name='Telefone 2', blank=True)),
                ('site', models.URLField()),
                ('atuacao', models.TextField(verbose_name='Breve Hist\xf3rico de Atua\xe7\xe3o')),
                ('data_abertura', models.DateField(null=True, verbose_name='Data de Abertura/Atua desde', blank=True)),
                ('objetivo', models.TextField(null=True, verbose_name='Objetivo/Fim', blank=True)),
                ('numero_integrantes', models.IntegerField(null=True, verbose_name='N\xfamero de Integrantes', blank=True)),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Endere\xe7o',
            },
        ),
        migrations.CreateModel(
            name='AgenteMembro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_add', models.DateTimeField(auto_now_add=True)),
                ('date_upd', models.DateTimeField(auto_now=True)),
                ('is_administrador', models.BooleanField(verbose_name='Membro Administrador')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='LocalSede',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cep', models.CharField(max_length=8)),
                ('logradouro', models.CharField(max_length=100)),
                ('numero', models.CharField(max_length=50, verbose_name='N\xfamero')),
                ('complemento', models.CharField(max_length=100, null=True, blank=True)),
                ('bairro', models.CharField(max_length=100)),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4674, null=True, blank=True)),
                ('municipio', models.ForeignKey(to='municipios.Municipio')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Endere\xe7o',
            },
        ),
        migrations.CreateModel(
            name='RedeSocial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_add', models.DateTimeField(auto_now_add=True)),
                ('date_upd', models.DateTimeField(auto_now=True)),
                ('plataforma', models.SmallIntegerField(blank=True, null=True, verbose_name='Plataforma', choices=[(1, 'Google+'), (2, 'Facebook'), (3, 'Instagram'), (4, 'Pinterest'), (5, 'Snapchat'), (7, 'Skype'), (6, 'Telegram'), (9, 'Tumbrl'), (8, 'Twitter'), (10, 'Youtube')])),
                ('link', models.URLField(null=True, verbose_name='Link', blank=True)),
                ('nome', models.CharField(max_length=150, null=True, verbose_name='Nome', blank=True)),
                ('user_add', models.ForeignKey(related_name='agente_redesocial_created_by', editable=False, to=settings.AUTH_USER_MODEL)),
                ('user_upd', models.ForeignKey(related_name='agente_redesocial_modified_by', editable=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('plataforma', 'date_add'),
                'abstract': False,
                'verbose_name': 'Rede Social',
                'verbose_name_plural': 'Redes Sociais',
            },
        ),
        migrations.CreateModel(
            name='Coletivo',
            fields=[
                ('agente_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agente.Agente')),
                ('abrangencia', models.SmallIntegerField(blank=True, null=True, verbose_name='Abrang\xeancia geogr\xe1fica', choices=[(2, 'Estadual'), (4, 'Local'), (3, 'Municipal'), (1, 'Nacional')])),
                ('nome_contato', models.CharField(max_length=255, verbose_name='Nome do Contato de Refer\xeancia')),
                ('email_contato', models.EmailField(max_length=254, null=True, verbose_name='E-mail do Respons\xe1vel', blank=True)),
                ('area_pesquisa', models.ManyToManyField(related_name='coletivos', verbose_name='\xc1era de Estudo e Pesquisa', to='core.AreaEstudoPesquisa', blank=True)),
                ('linguagem_artistica', models.ManyToManyField(related_name='coletivos', verbose_name='Linguagens Art\xedsticas', to='core.LinguagemArtistica', blank=True)),
                ('manifestacao_cultural', models.ManyToManyField(related_name='coletivos', verbose_name='Manifesta\xe7\xe3o Cultural', to='core.ManifestacaoCultural', blank=True)),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Endere\xe7o',
            },
            bases=('agente.agente',),
        ),
        migrations.CreateModel(
            name='Conselho',
            fields=[
                ('agente_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agente.Agente')),
                ('abrangencia', models.SmallIntegerField(blank=True, null=True, verbose_name='Abrang\xeancia geogr\xe1fica', choices=[(2, 'Estadual'), (4, 'Local'), (3, 'Municipal'), (1, 'Nacional'), (5, 'Distrital')])),
                ('vinculo_orgao_publico', models.BooleanField(verbose_name='V\xednculo com \xf3rg\xe3o p\xfablico')),
                ('vinculo_orgao_publico_nome', models.CharField(max_length=255, null=True, verbose_name='Org\xe3o p\xfablico vinculado', blank=True)),
                ('vinculo_orgao_publico_esfera', models.SmallIntegerField(blank=True, null=True, verbose_name='Esfera do \xf3rg\xe3o p\xfablico', choices=[(1, 'Federal'), (2, 'Estadual'), (3, 'Municipal'), (4, 'Distrital')])),
                ('mandato_inicio', models.DateField(null=True, verbose_name='Data in\xedcio do mandato', blank=True)),
                ('mandato_fim', models.DateField(null=True, verbose_name='Data fim do mandato', blank=True)),
                ('periodicidade_reunioes', models.CharField(max_length=255, null=True, verbose_name='Periodicidade das reuni\xf5es', blank=True)),
                ('local_sede', models.ForeignKey(to='agente.LocalSede')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Endere\xe7o',
            },
            bases=('agente.agente',),
        ),
        migrations.CreateModel(
            name='Instituicao',
            fields=[
                ('agente_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agente.Agente')),
                ('abrangencia', models.SmallIntegerField(blank=True, null=True, verbose_name='Abrang\xeancia geogr\xe1fica', choices=[(2, 'Estadual'), (4, 'Local'), (3, 'Municipal'), (1, 'Nacional')])),
                ('responsavel_nome', models.CharField(max_length=255, verbose_name='Nome do Contato de Refer\xeancia')),
                ('responsavel_email', models.EmailField(max_length=254, null=True, verbose_name='E-mail do Respons\xe1vel', blank=True)),
                ('cnpj', models.CharField(max_length=20, null=True, verbose_name=b'CNPJ', blank=True)),
                ('natureza_administrativa', models.SmallIntegerField(blank=True, null=True, verbose_name='Natureza Administrativa', choices=[(1, 'P\xfablica'), (2, 'Privada'), (3, 'Mista')])),
                ('area_pesquisa', models.ManyToManyField(to='core.AreaEstudoPesquisa', verbose_name='\xc1era de Estudo e Pesquisa', blank=True)),
                ('linguagem_artistica', models.ManyToManyField(to='core.LinguagemArtistica', verbose_name='Linguagens Art\xedsticas', blank=True)),
                ('manifestacao_cultural', models.ManyToManyField(to='core.ManifestacaoCultural', verbose_name='Manifesta\xe7\xe3o Cultural', blank=True)),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Endere\xe7o',
            },
            bases=('agente.agente',),
        ),
        migrations.CreateModel(
            name='MovimentoSocial',
            fields=[
                ('agente_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='agente.Agente')),
                ('abrangencia', models.SmallIntegerField(blank=True, null=True, verbose_name='Abrang\xeancia geogr\xe1fica', choices=[(2, 'Estadual'), (4, 'Local'), (3, 'Municipal'), (1, 'Nacional')])),
                ('nome_contato', models.CharField(max_length=255, verbose_name='Nome do Contato de Refer\xeancia')),
                ('email_contato', models.EmailField(max_length=254, null=True, verbose_name='E-mail do Respons\xe1vel', blank=True)),
                ('area_pesquisa', models.ManyToManyField(to='core.AreaEstudoPesquisa', verbose_name='\xc1era de Estudo e Pesquisa', blank=True)),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Endere\xe7o',
            },
            bases=('agente.agente',),
        ),
        migrations.AddField(
            model_name='agentemembro',
            name='agente',
            field=models.ForeignKey(to='agente.Agente'),
        ),
        migrations.AddField(
            model_name='agentemembro',
            name='user',
            field=models.ForeignKey(related_name='agente_agentemembro_membro', verbose_name='Usu\xe1rio', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='agentemembro',
            name='user_add',
            field=models.ForeignKey(related_name='agente_agentemembro_created_by', editable=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='agentemembro',
            name='user_upd',
            field=models.ForeignKey(related_name='agente_agentemembro_modified_by', editable=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='agente',
            name='area_atuacao',
            field=models.ManyToManyField(to='core.AreaAtuacao'),
        ),
        migrations.AddField(
            model_name='agente',
            name='municipio',
            field=models.ForeignKey(to='municipios.Municipio'),
        ),
        migrations.AddField(
            model_name='agente',
            name='publico_focal',
            field=models.ManyToManyField(to='core.PublicoFocal'),
        ),
        migrations.AddField(
            model_name='agente',
            name='redesocial',
            field=models.ManyToManyField(to='agente.RedeSocial'),
        ),
    ]
