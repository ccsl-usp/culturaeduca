# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def set_user_add_upd(apps, schema_editor):
    Agente = apps.get_model('agente', 'Agente')
    for agente in Agente.objects.all():
        admins = agente.agentemembro_set.filter(user__is_active=True, is_administrador=True).order_by('-id')
        if admins.count() >= 1:
            agente.date_add = admins.first().date_add
            agente.user_add = admins.first().user
            agente.date_upd = admins.first().date_upd
            agente.user_upd = admins.first().user
            agente.save()


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0010_auto_20161013_1210'),
    ]

    operations = [
        migrations.RunPython(set_user_add_upd),
    ]
