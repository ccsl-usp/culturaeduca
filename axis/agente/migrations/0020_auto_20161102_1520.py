# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0019_auto_20161031_1622'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conselhocomposicaomembro',
            name='cargo',
            field=models.CharField(max_length=255, verbose_name='Cargo', blank=True),
        ),
        migrations.AlterField(
            model_name='conselhocomposicaomembro',
            name='nome',
            field=models.CharField(max_length=255, verbose_name='Nome', blank=True),
        ),
    ]
