# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0004_auto_20160926_1449'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conselho',
            name='local_sede',
            field=models.ForeignKey(to='agente.LocalSede', null=True),
        ),
    ]
