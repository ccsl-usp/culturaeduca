# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agente', '0029_auto_20171204_1102'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserVinculo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_add', models.DateTimeField(auto_now_add=True)),
                ('date_upd', models.DateTimeField(auto_now=True)),
                ('agente', models.ForeignKey(related_name='agente_vinculo', to='agente.Agente')),
                ('user', models.ForeignKey(verbose_name='Usu\xe1rio', to=settings.AUTH_USER_MODEL)),
                ('user_add', models.ForeignKey(related_name='agente_uservinculo_created_by', editable=False, to=settings.AUTH_USER_MODEL)),
                ('user_upd', models.ForeignKey(related_name='agente_uservinculo_modified_by', editable=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
