# coding: utf-8
from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, HttpResponseRedirect
from django.views.decorators.gzip import gzip_page
from django.views.generic import ListView, RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.db.models.functions import Lower

from axis.core.models import (
    Profile
)

from axis.agente.models import (
    Agente,
    AgenteMembro,
    AgenteMembroBloqueio,
    Coletivo,
    Conselho,
    Instituicao,
    MovimentoSocial,
    UserVinculo,
)

from axis.core.views import AddRequestUserMixin, GenericGeoJSONListView
from axis.core.forms import ProfileSearchForm, CustomInlineFormSet

from .forms import (
    AgenteMembroForm,
    ColetivoForm,
    ColetivoSearchForm,
    ConselhoComposicaoMembroForm,
    ConselhoForm,
    ConselhoSearchForm,
    InstituicaoForm,
    InstituicaoSearchForm,
    MovimentoSocialForm,
    MovimentoSocialSearchForm,
    RedeSocialForm,
    AgenteVinculoForm,
)

from axis.acao.forms import AcaoVinculoForm

from ibge.models import SetorCensitario


class ExistProfileMixin(object):

    def get(self, request, *args, **kwargs):
        profile = request.user.get_profile()
        if not profile:
            messages.warning(request, u'Você precisa completar seu Perfil')
            return redirect(reverse('profile_edit') + '?next=' + request.path)
        return super(ExistProfileMixin, self).get(request, *args, **kwargs)


class FormsetsMixin(object):
    """Mixin for rendering multiple formsets with foreign keys (inline formsets)

        Attributes
            formset_classes(list(ModelForm)): ModelForm Classes to produce
               inline formsets.
    """
    formset_classes = []

    def get(self, request, *args, **kwargs):
        if 'pk' in kwargs:
            self.object = self.get_object()
        else:
            self.object = None

        form = self.get_form()
        context = {'form': form}
        for formset_class in self.formset_classes:
            RelatedFormSet = forms.inlineformset_factory(
                self.model,
                formset_class.Meta.model,
                formset_class,
                CustomInlineFormSet,
                min_num=1,
                extra=0,
            )
            modelformset = RelatedFormSet(
                **self.get_form_kwargs()
            )
            context.update({
                formset_class.__name__.lower() + 'set': modelformset
            })

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        if 'pk' in kwargs:
            self.object = self.get_object()
        else:
            self.object = None

        form = self.get_form()
        context = {}
        for formset_class in self.formset_classes:
            RelatedFormSet = forms.inlineformset_factory(
                self.model,
                formset_class.Meta.model,
                formset_class,
                CustomInlineFormSet,
                min_num=1,
                extra=0,
            )
            related_formset = RelatedFormSet(
                **self.get_form_kwargs()
            )
            context.update({
                formset_class.__name__.lower() + 'set': related_formset
            })
        if (form.is_valid() and all([True if formset.is_valid() else False
           for formset in context.values()])):

            return self.form_valid(form, context)
        else:
            context.update({'form': form})
            return self.form_invalid(context)

    def form_valid(self, form, context):
        coletivo = form.save()
        for formset in context.values():
            formset.instance = coletivo
            formset.save()
        return HttpResponseRedirect(coletivo.get_absolute_url())

    def form_invalid(self, context):
        messages.warning(
            self.request,
            u'Houve um erro no formulário. Seus dados não foram salvos.'
        )
        return self.render_to_response(context)


class AgenteEditMixin(AddRequestUserMixin, FormsetsMixin, ExistProfileMixin):

    def get_object(self):
        obj = super(AgenteEditMixin, self).get_object()

        if not obj.is_administrador(self.request.user):
            raise Http404

        if obj and obj.point:
            obj.point.transform(3857)

        return obj


class AgenteListMixin(AddRequestUserMixin, SuccessMessageMixin):

    nome_field = 'nome'
    order_by = '-date_add'

    def get_agente_object(self):
        _agente = None
        _agente = {
            'tipo': self.model._tipo(),
            'get_create_url': self.model._get_create_url(),
        }
        return _agente

    def get_form_kwargs(self):
        kwargs = {
            'tipo': self.get_agente_object()['tipo']
        }
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super(AgenteListMixin, self).get_context_data(
            *args, **kwargs)
        context['agente'] = self.get_agente_object()
        self.form = self.form_class(
            initial=self.request.GET.dict(), **self.get_form_kwargs())
        context['form'] = self.form
        return context

    def get_queryset(self):
        data = self.request.GET.dict()
        queryset = super(AgenteListMixin, self).get_queryset()
        agentes_by_municipios_pks, agentes_by_nome_pks = [], []

        if data.get('municipio', False):
            agentes_by_municipios_pks = queryset.filter(
                municipio__id_ibge=data['municipio']
            ).values_list('pk', flat=True)

        if data.get('nome', False):
            agentes_by_nome_pks = queryset.filter(**{
                self.nome_field + '__icontains': data['nome']
            }).values_list('pk', flat=True)

        if agentes_by_nome_pks or agentes_by_municipios_pks:
            queryset = queryset.filter(pk__in=set(
                list(agentes_by_municipios_pks) + list(agentes_by_nome_pks))
            )

        return queryset.order_by(self.order_by)


class AgenteDetailMixin(object):

    def get_context_data(self, *args, **kwargs):
        context = super(AgenteDetailMixin, self).get_context_data(
            *args, **kwargs)

        tipo_equipamento = self.model._meta.verbose_name
        if not tipo_equipamento.isupper():
            tipo_equipamento = tipo_equipamento.capitalize()

        context['is_administrador'] = self.object.is_administrador(
            self.request.user)
        context['is_member'] = self.object.is_member(self.request.user)
        context['form_vinculo'] = AgenteVinculoForm(
            request_user=self.request.user, agente=self.object)
        context['form_acao_vinculo'] = AcaoVinculoForm(
            request_user=self.request.user, agente=self.object)

        if context['is_administrador']:
            context['form_agentemembro'] = AgenteMembroForm(
                request_user=self.request.user, agente=self.object)

        context['is_member'] = self.object.is_member(self.request.user)
        context['is_blocked'] = self.object.is_blocked_member(
            self.request.user)
        context['membros'] = self.object.membros_ativos().order_by(
            Lower('user__nome'))
        context['total_membros'] = context['membros'].count()

        if self.object and self.object.pk:
            context['total_galeria'] = self.object.galeria.all().count()
            kw = {
                'src_app': 'agente',
                'src_model': self.model.__name__,
                'src_pk': self.object.pk,
            }
            context['galeria_list_url'] = reverse_lazy(
                'galeria_list', kwargs=kw)
            context['galeria_dashboard_url'] = reverse_lazy(
                'galeria_dashboard', kwargs=kw)
            context['galeria_counter_url'] = reverse_lazy(
                'galeria_counter', kwargs=kw)
            context['galeria_create_url'] = reverse_lazy(
                'galeria_create', kwargs=kw)

            kw_extra = {'pk': 0}
            kw_extra.update(kw)
            context['galeria_update_url'] = reverse_lazy(
                'galeria_update', kwargs=kw_extra)
        else:
            context['total_galeria'] = 0

        if self.object.point:
            perfil_setores = SetorCensitario.objects.entorno(
                self.object.point.y, self.object.point.x).perfil()

            total_equipamentos, equipamentos = self.object.equipamentos_entorno()

            # filtrando todos os tipos de equipamentos que tenham eq[1] >= 1
            # eq[1] é o número de equipamentos de um determinado tipo (zero é
            # quando não tem nenhum deste tipo no entorno)
            equipamentos_existentes = filter(
                lambda eq: eq[1] >= 1, equipamentos)

            # retorna apenas um dicionário com o model name do tipo de equipamento, exemplo:
            # {
            #     'escola': True,
            # }
            equipamentos_existentes = dict(
                [(model_name, True, ) for tipo, ct, equips, model_name in equipamentos_existentes])

            context.update({
                'tipo_equipamento': tipo_equipamento,
                'perfil_setores': perfil_setores,
                'equipamentos': equipamentos,
                'equipamentos_existentes': equipamentos_existentes,
                'total_equipamentos': total_equipamentos,
            })

        context['agente'] = self.object
        context['vinculo_add_remove'] = 'agente_vinculo_membership_add_remove'
        context['acao_agente_vinculo_url'] = 'acao_agente_vinculo_add_remove'

        return context


class PessoaFisicaListView(AgenteListMixin, ListView):
    model = Profile
    form_class = ProfileSearchForm
    nome_field = 'user__nome'
    order_by = '-user__id'
    template_name = 'agente/base_listagem.html'


pessoafisica_list = PessoaFisicaListView.as_view()


class ColetivoCreateView(AgenteEditMixin, CreateView):
    model = Coletivo
    form_class = ColetivoForm
    template_name = 'coletivo/coletivo_form.html'
    formset_classes = [RedeSocialForm, ]


class ColetivoDetailView(AgenteDetailMixin, DetailView):
    model = Coletivo
    form_class = ColetivoForm
    template_name = 'agente/coletivo_detalhe.html'


class ColetivoUpdateView(AgenteEditMixin, UpdateView):
    model = Coletivo
    form_class = ColetivoForm
    template_name = 'coletivo/coletivo_form.html'
    formset_classes = [RedeSocialForm, ]


class ColetivoListView(AgenteListMixin, ListView):
    model = Coletivo
    form_class = ColetivoSearchForm
    template_name = 'agente/base_listagem.html'


coletivo_create = login_required(ColetivoCreateView.as_view())
coletivo_update = login_required(ColetivoUpdateView.as_view())
coletivo_detail = ColetivoDetailView.as_view()
coletivo_list = ColetivoListView.as_view()
lista_coletivo = gzip_page(
    GenericGeoJSONListView.as_view(
        model=Agente,
        geom_field='point',
        properties=['pk', 'target_url', 'local'],
        srid_out=4326
    )
)


class InstituicaoCreateView(AgenteEditMixin, CreateView):
    model = Instituicao
    form_class = InstituicaoForm
    template_name = 'instituicao/instituicao_form.html'
    formset_classes = [RedeSocialForm, ]


class InstituicaoDetailView(AgenteDetailMixin, DetailView):
    model = Instituicao
    form_class = InstituicaoForm
    template_name = 'agente/instituicao_detalhe.html'


class InstituicaoUpdateView(AgenteEditMixin, UpdateView):
    model = Instituicao
    form_class = InstituicaoForm
    template_name = 'instituicao/instituicao_form.html'
    formset_classes = [RedeSocialForm, ]


class InstituicaoListView(AgenteListMixin, ListView):
    model = Instituicao
    form_class = InstituicaoSearchForm
    template_name = 'agente/base_listagem.html'


instituicao_create = login_required(InstituicaoCreateView.as_view())
instituicao_update = login_required(InstituicaoUpdateView.as_view())
instituicao_detail = InstituicaoDetailView.as_view()
instituicao_list = InstituicaoListView.as_view()
lista_instituicao = gzip_page(
    GenericGeoJSONListView.as_view(
        model=Agente,
        geom_field='point',
        properties=['pk', 'target_url', 'local'],
        srid_out=4326
    )
)


class ConselhoCreateView(AgenteEditMixin, CreateView):
    model = Conselho
    form_class = ConselhoForm
    template_name = 'conselho/conselho_form.html'
    formset_classes = [RedeSocialForm, ConselhoComposicaoMembroForm]


class ConselhoDetailView(AgenteDetailMixin, DetailView):
    model = Conselho
    form_class = ConselhoForm
    template_name = 'agente/conselho_detalhe.html'


class ConselhoUpdateView(AgenteEditMixin, UpdateView):
    model = Conselho
    form_class = ConselhoForm
    template_name = 'conselho/conselho_form.html'
    formset_classes = [RedeSocialForm, ConselhoComposicaoMembroForm]


class ConselhoListView(AgenteListMixin, ListView):
    model = Conselho
    form_class = ConselhoSearchForm
    template_name = 'agente/base_listagem.html'


conselho_create = login_required(ConselhoCreateView.as_view())
conselho_update = login_required(ConselhoUpdateView.as_view())
conselho_detail = ConselhoDetailView.as_view()
conselho_list = ConselhoListView.as_view()
lista_conselho = gzip_page(
    GenericGeoJSONListView.as_view(
        model=Agente,
        geom_field='point',
        properties=['pk', 'target_url', 'local'],
        srid_out=4326
    )
)


class MovimentoSocialCreateView(AgenteEditMixin, CreateView):
    model = MovimentoSocial
    form_class = MovimentoSocialForm
    template_name = 'movimentosocial/movimentosocial_form.html'
    formset_classes = [RedeSocialForm, ]


class MovimentoSocialDetailView(AgenteDetailMixin, DetailView):
    model = MovimentoSocial
    form_class = MovimentoSocialForm
    template_name = 'agente/movimentosocial_detalhe.html'


class MovimentoSocialUpdateView(AgenteEditMixin, UpdateView):
    model = MovimentoSocial
    form_class = MovimentoSocialForm
    template_name = 'movimentosocial/movimentosocial_form.html'
    formset_classes = [RedeSocialForm, ]


class MovimentoSocialListView(AgenteListMixin, ListView):
    model = MovimentoSocial
    form_class = MovimentoSocialSearchForm
    template_name = 'agente/base_listagem.html'


movimentosocial_create = login_required(MovimentoSocialCreateView.as_view())
movimentosocial_update = login_required(MovimentoSocialUpdateView.as_view())
movimentosocial_detail = MovimentoSocialDetailView.as_view()
movimentosocial_list = MovimentoSocialListView.as_view()
lista_movimentosocial = gzip_page(
    GenericGeoJSONListView.as_view(
        model=Agente,
        geom_field='point',
        properties=['pk', 'target_url', 'local'],
        srid_out=4326
    )
)


class AgenteVinculoAddRemoveMe(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        agente_from = get_object_or_404(Agente, pk=kwargs['agente_from_pk'])
        agente_to = get_object_or_404(Agente, pk=kwargs['agente_to_pk'])
        user = self.request.user

        if not agente_from.is_administrador(user):
            messages.warning(
                self.request, u'Não foi possível vincular agente. Por favor, peça para que um administrador vincule.')
        else:
            if kwargs['tipo'] == 'adiciona':
                agente_from.add_vinculo(agente_to, user)
                agente_to.add_vinculo(agente_from, user)

                if agente_from.has_vinculo(agente_to):
                    messages.success(self.request, u'Agente foi vinculado a esse(a) %s <strong>%s</strong>.' % (
                        agente_to.tipo.lower(), agente_to.nome, ))
                    agente_to.send_new_agente_email(agente_from)
                else:
                    messages.warning(
                        self.request, u'Não foi possível vincular agente. Por favor, peça para que um administrador vincule.')

            elif kwargs['tipo'] == 'remove':
                agente_from.remove_vinculo(agente_to)
                agente_to.remove_vinculo(agente_from)

                if agente_from.has_vinculo(agente_to):
                    messages.warning(
                        self.request, u'Não foi possível desvincular. Por favor, peça para um administrador do agente realizar a operação.')
                else:
                    messages.success(
                        self.request, u'Agente foi desvinculado desse(a) %s com <strong>sucesso</strong>.' % agente_to.tipo.lower())

        self.url = self.request.GET.get('next') or u'{url}{aba_hash}'.format(
            agente_from.get_child().get_absolute_url(), '#panel-rede')

        return super(AgenteVinculoAddRemoveMe, self).get_redirect_url(*args, **kwargs)


agente_vinculo_membership_add_remove = login_required(
    AgenteVinculoAddRemoveMe.as_view())


class AgenteMembroAddRemoveMe(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        agente = get_object_or_404(Agente, pk=kwargs['pk'])
        member = self.request.user

        if kwargs['tipo'] == 'adiciona':
            agente.add_member(member)

            if agente.is_member(member):
                messages.success(self.request, u'Você está vinculado a esse(a) %s <strong>%s</strong>.' %
                                 (agente.tipo.lower(), agente.nome, ))
                agente.send_new_member_email(member)
            else:
                messages.warning(
                    self.request, u'Não foi possível vincular agente. Por favor, peça para que um administrador te vincule.')
        elif kwargs['tipo'] == 'remove':
            agente.remove_member(member)

            if agente.is_member(member):
                messages.warning(
                    self.request, u'Não foi possível sair. Por favor, peça para um administrador do agente te remover.')
            else:
                messages.success(
                    self.request, u'Você não está mais vinculado a esse(a) %s.' % agente.tipo.lower())

        self.url = self.request.GET.get(
            'next') or agente.get_child().get_absolute_url()

        return super(AgenteMembroAddRemoveMe, self).get_redirect_url(*args, **kwargs)


agente_membership_add_remove_me = login_required(
    AgenteMembroAddRemoveMe.as_view())


class UserAgenteVinculoAddRemoveMe(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        agente = get_object_or_404(Agente, pk=kwargs['agente_pk'])
        user = self.request.user
        user_vinculo, create = UserVinculo.objects.get_or_create(
            user=user, agente=agente, user_add=user, user_upd=user)

        if create and kwargs['operacao'] == 'adiciona':
            messages.success(self.request, u'Você está vinculado a esse(a) %s <strong>%s</strong>.' %
                             (agente.tipo.lower(), agente.nome, ))
        elif not create and kwargs['operacao'] == 'remove':
            user_vinculo.delete()
            messages.success(
                self.request, u'Você não está mais vinculado a esse(a) %s.' % agente.tipo.lower())

        self.url = self.request.GET.get(
            'next') or agente.get_child().get_absolute_url()

        return super(UserAgenteVinculoAddRemoveMe, self).get_redirect_url(*args, **kwargs)


user_agente_vinculo_add_remove_me = login_required(
    UserAgenteVinculoAddRemoveMe.as_view())


class AgenteMembershipMakeUndoAdmin(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        agente_membro = get_object_or_404(AgenteMembro, pk=kwargs['pk'])
        agente = agente_membro.agente

        if agente.is_administrador(self.request.user):

            if kwargs['tipo'] == 'make':
                agente_membro.is_administrador = True
            elif kwargs['tipo'] == 'undo':
                agente_membro.is_administrador = False

            agente_membro.save()

            membro = agente_membro.user
            messages.success(self.request, u'%s agora e um administrador desse(a) %s.' % (
                membro.nome, agente.tipo.lower(), ))
        else:
            messages.warning(
                self.request, u'Não foi possível concluir. Por favor, peça para um administrador realizar essa operação.')

        self.url = self.request.GET.get(
            'next') or agente.get_child().get_absolute_url()

        return super(AgenteMembershipMakeUndoAdmin, self).get_redirect_url(*args, **kwargs)


agente_membership_make_undo_admin = login_required(
    AgenteMembershipMakeUndoAdmin.as_view())


class AgenteMembershipBlock(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        agente_membro = get_object_or_404(AgenteMembro, pk=kwargs['pk'])
        agente = agente_membro.agente

        if agente.is_administrador(self.request.user):
            membro = agente_membro.user

            AgenteMembroBloqueio.objects.create(**{
                'user': membro,
                'agente': agente,
                'user_add': self.request.user,
                'user_upd': self.request.user,
            })

            agente_membro.delete()

            messages.success(self.request, u'Membro "%s" foi removido e bloqueado desse(a) %s.' % (
                membro.nome, agente.tipo.lower(), ))
        else:
            messages.warning(
                self.request, u'Não foi possível concluir. Por favor, peça para um administrador realizar essa operação.')

        self.url = self.request.GET.get(
            'next') or agente.get_child().get_absolute_url()

        return super(AgenteMembershipBlock, self).get_redirect_url(*args, **kwargs)


agente_membership_block = login_required(AgenteMembershipBlock.as_view())


class AgenteMembroAdd(SuccessMessageMixin, CreateView):
    model = AgenteMembro
    form_class = AgenteMembroForm
    http_method_names = [u'post']

    def get_form_kwargs(self):
        kwargs = super(AgenteMembroAdd, self).get_form_kwargs()
        self.agente = get_object_or_404(Agente, pk=self.kwargs['pk'])
        kwargs.update({
            'request_user': self.request.user,
            'agente': self.agente
        })
        return kwargs

    def form_valid(self, form):
        self.object = form.save()
        messages.success(self.request, u'Novo membro adicionado com sucesso! <strong>%s</strong> está vinculado a esse(a) %s e receberá uma notificação via e-mail.' %
                         (self.object.user.nome, self.object.agente.tipo.lower(), ))
        self.object.user.send_new_member_email(self.object.agente)
        return super(AgenteMembroAdd, self).form_valid(form)

    def form_invalid(self, form):
        messages.warning(
            self.request, u'Falha ao vincular novo membro. Por favor, tente novamente.')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return self.agente.get_child().get_absolute_url()


agente_membership_add = login_required(AgenteMembroAdd.as_view())
