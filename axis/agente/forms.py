# coding: utf-8
from axis.core.models import (
    AreaEstudoPesquisa,
    LinguagemArtistica,
    User
)
from axis.agente.models import (
    Agente,
    AgenteMembro,
    Conselho,
    Coletivo,
    Instituicao,
    NaturezaJuridica,
    MovimentoSocial,
    AgenteRedeSocial,
    ConselhoComposicaoMembro,
    AreaAtuacao,
    PublicoFocal,
    ManifestacaoCultural
)
from axis.core.forms import (
    UnicodeModelSelect2Widget,
    UnicodeModelSelect2MultipleWidget,
    AgenteBaseSearchForm,
    AgenteModelSelect2Widget,
)
from django import forms
from municipios.widgets import SelectMunicipioWidget
from django_select2.forms import ModelSelect2Widget
from django.contrib.gis.geos import GEOSGeometry
from utils import clean_tilestache_cache


class MembroUserModelSelect2Widget(ModelSelect2Widget):
    model = User
    search_fields = ['nome__icontains', ]

    def label_from_instance(self, obj):
        if obj.get_profile():
            email = obj.get_profile().privacy_email or u'e-mail privado'
        else:
            email = obj.email

        return u'%s (%s)' % (obj.nome, email, )

    def build_attrs(self, base_attrs, extra_attrs=None):
        attrs = super(MembroUserModelSelect2Widget, self).build_attrs(
            base_attrs, extra_attrs=extra_attrs
        )

        attrs.update({
            'data-minimum-input-length': 3,
            'data-placeholder': u'Digite o nome do usuário',
        })

        return attrs


class AgenteFormBase(forms.ModelForm):
    cep = forms.CharField(label='CEP', max_length=9)

    class Meta:
        widgets = {
            'municipio': SelectMunicipioWidget,
            'linguagem_artistica': UnicodeModelSelect2MultipleWidget(model=LinguagemArtistica, search_fields=['nome__unaccent__icontains', ]),
            'area_atuacao': UnicodeModelSelect2MultipleWidget(model=AreaAtuacao, search_fields=['nome__unaccent__icontains']),
            'area_pesquisa': UnicodeModelSelect2MultipleWidget(model=AreaEstudoPesquisa, search_fields=['nome__unaccent__icontains']),
            'publico_focal': UnicodeModelSelect2MultipleWidget(model=PublicoFocal, search_fields=['nome__unaccent__icontains', ]),
            'manifestacao_cultural': UnicodeModelSelect2MultipleWidget(model=ManifestacaoCultural, search_fields=['nome__unaccent__icontains', ])
        }
        fields = (
            'nome',
            'email',
            'avatar',
            'telefone1',
            'telefone2',
            'site',
            'publico_focal',
            'publico_focal_outro',
            'atuacao',
            'area_atuacao',
            'area_atuacao_outro',
            'data_abertura',
            'objetivo',
            'numero_integrantes',
            'abrangencia',
            'cep',
            'logradouro',
            'numero',
            'complemento',
            'bairro',
            'municipio',
            'point',
        )

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        self.tipo = self._meta.model._tipo()
        super(AgenteFormBase, self).__init__(*args, **kwargs)

        self.fields['point'].initial = self.instance.point.wkt if self.instance.point else ''
        self.fields['point'].required = True
        self.fields['point'].error_messages = {
            'required': 'Ponto no mapa é obrigatório. Por favor, marque o ponto e tente novamente.'}
        self.fields['point'].widget = forms.HiddenInput()

        self.fields['cep'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['logradouro'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['numero'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['complemento'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['bairro'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['municipio'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['telefone1'].widget.attrs.update({
            'class': 'phone',
            'autocomplete': 'off'
        })
        self.fields['telefone2'].widget.attrs.update({
            'class': 'phone',
            'autocomplete': 'off'
        })
        self.fields['area_atuacao'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_area_atuacao_outro',
            'data-other-choice-id': AreaAtuacao.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['area_atuacao_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })

        self.fields['publico_focal'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_publico_focal_outro',
            'data-other-choice-id': PublicoFocal.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['publico_focal_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })

    def clean(self):
        # ref #132 e 117 item 4 / necto
        cleaned_data = self.cleaned_data
        cep = cleaned_data.get('cep')
        if cep and '-' in cep:
            cleaned_data['cep'] = cep.replace('-', '')

        return cleaned_data

    def clean_point(self):
        point = self.cleaned_data['point']
        try:
            self._point = GEOSGeometry(point)
        except:
            raise forms.ValidationError(
                u'Ponto no mapa está inválido. Por favor, marque o ponto no mapa e tente novamente.')
        return point

    def clean_data_abertura(self):
        data_abertura = self.cleaned_data['data_abertura']
        from django.utils import timezone
        if data_abertura > timezone.now().year or data_abertura < 1000:
            raise forms.ValidationError(u'Data inválida.')
        return data_abertura

    def clean_area_atuacao_outro(self):
        area_atuacao_outro = self.cleaned_data['area_atuacao_outro']
        areas_pks = [
            area.pk for area in self.cleaned_data['area_atuacao'].all()]
        if AreaAtuacao.objects.get(nome='Outros').pk not in areas_pks:
            area_atuacao_outro = ''

        return area_atuacao_outro

    def clean_publico_focal_outro(self):
        publico_focal_outro = self.cleaned_data['publico_focal_outro']
        publicos_pks = [
            publico.pk for publico in self.cleaned_data['publico_focal'].all()]
        if PublicoFocal.objects.get(nome='Outros').pk not in publicos_pks:
            publico_focal_outro = ''

        return publico_focal_outro

    def clean_manifestacao_cultural_outro(self):
        manifestacao_cultural_outro = self.cleaned_data['manifestacao_cultural_outro']
        manifestacoes_pks = [
            manifestacao.pk for manifestacao in self.cleaned_data['manifestacao_cultural'].all()]
        if ManifestacaoCultural.objects.get(nome='Outros').pk not in manifestacoes_pks:
            manifestacao_cultural_outro = ''

        return manifestacao_cultural_outro

    def save(self, commit=True):
        instance = super(AgenteFormBase, self).save(commit=False)
        instance.user_upd = self.request_user
        new = not bool(instance.pk)
        if new:
            instance.user_add = self.request_user

        if commit:
            model = self.Meta.model
            if bool(instance.pk):
                # se estiver editando um registro existente: limpar cache da antiga e da nova área
                old_point = model.objects.get(pk=instance.pk).point
                if old_point:
                    old_point = old_point.transform(3857, clone=True)
                if old_point and old_point != instance.point:
                    clean_tilestache_cache(
                        point=old_point,
                        layer=model._meta.db_table
                    )
                    clean_tilestache_cache(
                        point=instance.point,
                        layer=model._meta.db_table
                    )
            else:
                # se estiver criando um novo registro limpar cache apenas da área do novo ponto
                clean_tilestache_cache(
                    point=instance.point,
                    layer=model._meta.db_table
                )

            instance.save()
            self.save_m2m()
        if new:
            AgenteMembro.objects.create(
                user_add=self.request_user,
                user_upd=self.request_user,
                user=self.request_user,
                agente=instance.agente_ptr,
                is_administrador=True
            )

        return instance


class ConselhoSearchForm(AgenteBaseSearchForm):
    class Meta(AgenteBaseSearchForm.Meta):
        model = Conselho


class ConselhoForm(AgenteFormBase):

    def __init__(self, *args, **kwargs):
        super(ConselhoForm, self).__init__(*args, **kwargs)
        self.fields['vinculo_orgao_publico'].initial = True
        self.fields['vinculo_orgao_publico'].widget.attrs.update(
            {'checked': True})
        self.fields['numero_integrantes'].required = True
        self.fields['vinculo_orgao_publico_nome'].required = True
        self.fields['vinculo_orgao_publico_esfera'].required = True
        self.fields['mandato_inicio'].widget.attrs.update({'class': 'date'})
        self.fields['mandato_fim'].widget.attrs.update({'class': 'date'})

    def clean(self):
        cleaned_data = super(ConselhoForm, self).clean()

        vinculo_orgao_publico = cleaned_data.get("vinculo_orgao_publico")
        vinculo_orgao_publico_nome = cleaned_data.get(
            "vinculo_orgao_publico_nome")

        if vinculo_orgao_publico and not vinculo_orgao_publico_nome:
            raise forms.ValidationError(
                "Orgão público vinculado é um obrigátorio.")

        vinculo_orgao_publico_esfera = cleaned_data.get(
            "vinculo_orgao_publico_esfera")

        if vinculo_orgao_publico and not vinculo_orgao_publico_esfera:
            raise forms.ValidationError(
                "Esfera do órgão público é um obrigátorio.")

        return cleaned_data

    class Meta(AgenteFormBase.Meta):
        model = Conselho
        fields = AgenteFormBase.Meta.fields + (
            'abrangencia',
            'vinculo_orgao_publico',
            'vinculo_orgao_publico_nome',
            'vinculo_orgao_publico_esfera',
            'mandato_inicio',
            'mandato_fim',
            'periodicidade_reunioes',
            'local_sede',
        )


class ColetivoSearchForm(AgenteBaseSearchForm):
    class Meta(AgenteBaseSearchForm.Meta):
        model = Coletivo


class ColetivoForm(AgenteFormBase):

    def __init__(self, *args, **kwargs):
        super(ColetivoForm, self).__init__(*args, **kwargs)
        self.fields['nome_contato'].initial = self.request_user.nome
        self.fields['email_contato'].initial = self.request_user.email

        self.fields['manifestacao_cultural'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_manifestacao_cultural_outro',
            'data-other-choice-id': ManifestacaoCultural.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['manifestacao_cultural_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })

    class Meta(AgenteFormBase.Meta):
        model = Coletivo
        fields = AgenteFormBase.Meta.fields + (
            'abrangencia',
            'area_pesquisa',
            'nome_contato',
            'email_contato',
            'linguagem_artistica',
            'manifestacao_cultural',
            'manifestacao_cultural_outro',
        )


class InstituicaoSearchForm(AgenteBaseSearchForm):
    class Meta(AgenteBaseSearchForm.Meta):
        model = Instituicao


class InstituicaoForm(AgenteFormBase):

    def __init__(self, *args, **kwargs):
        super(InstituicaoForm, self).__init__(*args, **kwargs)
        self.fields['responsavel_nome'].initial = self.request_user.nome
        self.fields['responsavel_email'].initial = self.request_user.email
        self.fields['email'].label = u'E-mail institucional'
        self.fields['cnpj'].widget.attrs.update({'class': 'cnpj'})

        NaturezaJuridica_CHOICES = [(u'', u'---------')]

        for NaturezaJuridica_TIPO, NaturezaJuridica_DISPLAY_TIPO in NaturezaJuridica.TIPO.CHOICES:
            NaturezaJuridica_CHOICES += [(NaturezaJuridica_DISPLAY_TIPO, [
                (naturezajuridica.pk, naturezajuridica.display_name, ) for naturezajuridica in NaturezaJuridica.objects.filter(tipo=NaturezaJuridica_TIPO)]
            )]

        self.fields['natureza_juridica'] = forms.ChoiceField(
            choices=NaturezaJuridica_CHOICES, required=False)

        self.fields['natureza_juridica'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_natureza_juridica_outro',
            'data-other-choice-id': NaturezaJuridica.objects.get(
                nome__icontains='Outras Instituições'
            ).pk,
        })
        self.fields['natureza_juridica_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })

        self.fields['manifestacao_cultural'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_manifestacao_cultural_outro',
            'data-other-choice-id': ManifestacaoCultural.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['manifestacao_cultural_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })

    def clean_natureza_juridica(self):
        natureza_juridica = self.cleaned_data['natureza_juridica']

        if natureza_juridica == u'':
            return None

        try:
            natureza_juridica = NaturezaJuridica.objects.get(
                pk=natureza_juridica)
        except NaturezaJuridica.DoesNotExist:
            raise forms.ValidationError(
                u'Natureza juridica inválida, por favor selecione uma das opções.')

        return natureza_juridica

    def clean_natureza_juridica_outro(self):
        natureza_juridica_outro = self.cleaned_data['natureza_juridica_outro']
        natureza_juridica = self.cleaned_data['natureza_juridica']
        if natureza_juridica and natureza_juridica.pk != NaturezaJuridica.objects.get(nome__icontains='Outras Instituições').pk:
            natureza_juridica_outro = ''

        return natureza_juridica_outro

    class Meta(AgenteFormBase.Meta):
        model = Instituicao
        widgets = AgenteFormBase.Meta.widgets.copy()
        widgets.update({
            'natureza_juridica': UnicodeModelSelect2Widget(model=NaturezaJuridica, search_fields=['nome__unaccent__icontains', ])
        })
        fields = AgenteFormBase.Meta.fields + (
            'cnpj',
            'abrangencia',
            'area_pesquisa',
            'linguagem_artistica',
            'manifestacao_cultural',
            'manifestacao_cultural_outro',
            'responsavel_nome',
            'responsavel_email',
            'natureza_administrativa',
            'natureza_juridica',
            'natureza_juridica_outro',
        )


class MovimentoSocialSearchForm(AgenteBaseSearchForm):
    class Meta(AgenteBaseSearchForm.Meta):
        model = MovimentoSocial


class MovimentoSocialForm(AgenteFormBase):

    def __init__(self, *args, **kwargs):
        super(MovimentoSocialForm, self).__init__(*args, **kwargs)
        self.fields['nome_contato'].initial = self.request_user.nome
        self.fields['nome_contato'].label = u'Contato de Referência'
        self.fields['email_contato'].initial = self.request_user.email
        self.fields['email_contato'].label = u'E-mail do Contato de Referência'

    class Meta(AgenteFormBase.Meta):
        model = MovimentoSocial
        fields = AgenteFormBase.Meta.fields + (
            'abrangencia',
            'area_pesquisa',
            'nome_contato',
            'email_contato',
        )


class AgenteMembroForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        self.agente = kwargs.pop('agente')
        super(AgenteMembroForm, self).__init__(*args, **kwargs)

        self.fields['user'].queryset = self.fields['user'].queryset.filter(
            is_active=True)
        self.fields['user'].widget.attrs.update({'style': 'width: 100%;'})

        if self.instance.pk:
            # se for edição não pode editar o USER
            self.fields['user'].widget.attrs.update({'readonly': True})
            self.fields['user'].queryset = self.fields['user'].queryset.filter(
                pk=self.instance.user.pk)
        else:
            # se for um update, só adiciona usuários que não são membros ainda!
            self.fields['user'].queryset = self.fields['user'].queryset.exclude(
                pk__in=self.agente.membros_ativos().values_list('user__pk', flat=True)
            )

        self.is_administrador = self.agente.is_administrador(self.request_user)
        if not self.is_administrador:
            # se não for is_administrador não edita o campo is_administrador
            self.fields['is_administrador'].widget.attrs.update(
                {'readonly': True})

    def clean_is_administrador(self):
        # se não for is_administrador não edita o campo is_administrador
        if self.is_administrador:
            # se for o unico is_administrador Ativo não pode se desmarcar como is_administrador
            if self.instance.pk and \
               self.agente.membros_ativos().filter(is_administrador=True).count() >= 1 and \
               self.cleaned_data['is_administrador'] != self.instance.is_administrador:
                raise forms.ValidationError(
                    u'Você não pode editar o administrador para este agente, este usuário é o unico administrador!')
            return self.cleaned_data['is_administrador']
        raise forms.ValidationError(u'Você não pode editar o administrador.')

    def clean_user(self):
        # se for um update não edita o user
        if self.instance.pk and self.cleaned_data['user'] != self.instance.user:
            raise forms.ValidationError(u'Você não pode editar o usuário.')
        return self.cleaned_data['user']

    def save(self, commit=True):
        instance = super(AgenteMembroForm, self).save(commit=False)
        instance.user_upd = self.request_user
        instance.agente = self.agente
        if not instance.pk:
            instance.user_add = self.request_user
        instance.save()
        return instance

    class Meta:
        model = AgenteMembro
        fields = (
            'user',
            'is_administrador',
        )
        widgets = {
            'user': MembroUserModelSelect2Widget(),
        }


class ConselhoComposicaoMembroForm(forms.ModelForm):

    class Meta:
        model = ConselhoComposicaoMembro
        fields = ('nome', 'cargo')

    def __init__(self, *args, **kwargs):
        if kwargs['request_user']:
            # remove o request_user do kwargs porque
            # especificamente este form não usa
            kwargs.pop('request_user')
        super(ConselhoComposicaoMembroForm, self).__init__(*args, **kwargs)


class RedeSocialForm(forms.ModelForm):

    class Meta:
        model = AgenteRedeSocial
        fields = (
            'plataforma',
            'link',
        )

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        super(RedeSocialForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(RedeSocialForm, self).save(commit=False)
        instance.user_upd = self.request_user
        new = not instance.pk
        if new:
            instance.user_add = self.request_user

        instance.save()

        return instance


class AgenteVinculoForm(forms.Form):

    empty_help_text = u'''
        <p>Esse agente faz parte de algum <strong>Coletivo</strong>, <strong>Instituição</strong>, <strong>Fórum, Conselho ou Comitê</strong> ou mesmo <strong>Movimento Social</strong>?
        Utilize o espaço abaixo para vincular esses agentes.</p>
    '''

    class Meta:
        widgets = {
            'agente': UnicodeModelSelect2Widget(model=Agente, search_fields=['nome__unaccent__icontains', ])
        }

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        self.agente = kwargs.pop('agente')
        super(AgenteVinculoForm, self).__init__(*args, **kwargs)

        agente_qs = Agente.objects.all()

        if not self.request_user.is_anonymous():
            pks = list(self.agente.agentes.values_list('pk', flat=True))
            pks += [self.agente.pk, ]
            agente_qs = agente_qs.exclude(pk__in=pks)

        self.fields['agente'] = forms.ModelChoiceField(queryset=agente_qs)
        self.fields['agente'].widget = AgenteModelSelect2Widget(queryset=agente_qs, attrs={
                                                                'data-placeholder': u'Digite o nome do agente que deseja vincular a esse agente'})
        self.fields['agente'].widget.attrs.update(
            {'style': 'width: 100%;', 'class': 'agente_vinculado'})
