from django.contrib import admin
from .models import Agente, AgenteMembro



class AgenteAdmin(admin.ModelAdmin):
	list_display = ('nome',)

class AgenteMembroAdmin(admin.ModelAdmin):
	list_display = ('user', 'agente')
	search_fields = ('agente','is_administrador')


admin.site.register(Agente, AgenteAdmin)
admin.site.register(AgenteMembro, AgenteMembroAdmin)





