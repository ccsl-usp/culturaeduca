import os 

def export_vars(request):
    data = {}
    data['MATOMO_URL'] = os.environ['MATOMO_URL']
    data['MATOMO_SITE_ID'] = os.environ['MATOMO_SITE_ID']
    return data