# coding: utf-8
from axis.core.models import LinguagemArtistica
from django_select2.forms import ModelSelect2Widget

from axis.core.forms import AgenteModelSelect2Widget
from axis.agente.models import (
    AreaAtuacao as Tema,
    PublicoFocal,
    ManifestacaoCultural,
    Agente,
)
from axis.acao.models import (
    AcaoFinanciamento,
    LocalAcao,
    Local,
    Acao,
    AcaoMembro,
    Atividade,
    Projeto,
)
from axis.agente.forms import MembroUserModelSelect2Widget
from axis.core.forms import (
    UnicodeModelSelect2Widget,
    UnicodeModelSelect2MultipleWidget,
    CustomInlineFormSet,
)
from django import forms
from utils import clean_tilestache_cache

from municipios.models import Municipio
from municipios.widgets import SelectMunicipioWidget


class AcaoCustomInlineFormSet(CustomInlineFormSet):
    def __init__(self, *args, **kwargs):
        self.agente = kwargs.pop('agente', False)
        self.projeto = kwargs.pop('projeto', False)
        super(AcaoCustomInlineFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, *args, **kwargs):
        kwargs.update({
            'request_user': self.agente
        })
        return super(AcaoCustomInlineFormSet, self)._construct_form(*args, **kwargs)


class FinanciamentoForm(forms.ModelForm):

    class Meta:
        model = AcaoFinanciamento
        fields = (
            'tipo',
            'outro',
            'empresa',
        )

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        super(FinanciamentoForm, self).__init__(*args, **kwargs)

        # adiciona placeholder em campos outro e empresa
        self.fields['outro'].widget.attrs.update({
            'placeholder': 'Digite aqui o nome do outro tipo de financiamento',
        })
        self.fields['empresa'].widget.attrs.update({
            'placeholder': 'Digite aqui o nome da empresa',
        })

    def clean_outro(self):
        outro = self.cleaned_data['outro']
        if 'tipo' not in self.cleaned_data or self.cleaned_data['tipo'] != AcaoFinanciamento.TIPO.OUTROS:
            outro = u''
        return outro

    def clean_empresa(self):
        empresa = self.cleaned_data['empresa']
        if 'tipo' not in self.cleaned_data or self.cleaned_data['tipo'] != AcaoFinanciamento.TIPO.PATROCINIO_EMPRESARIAL:
            empresa = u''
        return empresa

    def save(self, commit=True):
        instance = super(FinanciamentoForm, self).save(commit=False)
        if commit:
            instance.save()
        return instance


class AcaoFormBase(forms.ModelForm):

    class Meta:
        widgets = {
            'linguagem_artistica': UnicodeModelSelect2MultipleWidget(model=LinguagemArtistica, search_fields=['nome__unaccent__icontains', ]),
            'temas': UnicodeModelSelect2MultipleWidget(model=Tema, search_fields=['nome__unaccent__icontains']),
            'publico_focal': UnicodeModelSelect2MultipleWidget(model=PublicoFocal, search_fields=['nome__unaccent__icontains', ]),
            'manifestacao_cultural': UnicodeModelSelect2MultipleWidget(model=ManifestacaoCultural, search_fields=['nome__unaccent__icontains', ])
        }
        fields = (
            'nome',
            'temas',
            'temas_outro',
            'publico_focal',
            'publico_focal_outro',
            'linguagem_artistica',
            'manifestacao_cultural',
            'manifestacao_cultural_outro',
            'inicio',
            'fim',
            'financiado',
            # 'agente',
        )

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        self.agente = kwargs.pop('agente', False)
        super(AcaoFormBase, self).__init__(*args, **kwargs)
        self.fields['nome'].widget.attrs.update({'autofocus': True})

        self.fields['acaomembro_vinculo'] = forms.ChoiceField(
            label=u'Vínculo na Ação',
            help_text=u'Escolha o seu vínculo com essa ação.',
            choices=AcaoMembro.TIPO.CHOICES
        )

        self.fields['acaomembro_vinculo_outro'] = forms.CharField(
            label=u'Outro Tipo de Vínculo',
            help_text=u'Ao escolher o tipo de vínculo \'Outro\' este campo estará habilitado.',
            required=False
        )

        self.fields['acaomembro_vinculo_outro'].widget.attrs.update({
            'disabled': True
        })

        # adiciona mascara e placeholder em campos data
        for field_name in ('inicio', 'fim', ):
            self.fields[field_name].widget.attrs.update({
                'class': 'date',
                'placeholder': '00/00/0000',
            })

        self.fields['temas'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_temas_outro',
            'data-other-choice-id': Tema.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['temas_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })

        self.fields['publico_focal'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_publico_focal_outro',
            'data-other-choice-id': PublicoFocal.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['publico_focal_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })

        self.fields['manifestacao_cultural'].widget.attrs.update({
            'class': 'has-other-choice',
            'data-other-choice-field-id': 'id_manifestacao_cultural_outro',
            'data-other-choice-id': ManifestacaoCultural.objects.get(
                nome='Outros'
            ).pk,
        })
        self.fields['manifestacao_cultural_outro'].widget.attrs.update({
            'style': 'margin-top:17px;'
        })

    def clean_acaomembro_vinculo_outro(self):
        acaomembro_vinculo_outro = self.cleaned_data['acaomembro_vinculo_outro']
        if 'acaomembro_vinculo' not in self.cleaned_data or self.cleaned_data['acaomembro_vinculo'] != AcaoMembro.TIPO.OUTRO:
            acaomembro_vinculo_outro = u''
        return acaomembro_vinculo_outro

    def clean_temas_outro(self):
        temas_outro = self.cleaned_data['temas_outro']
        temas_pks = [tema.pk for tema in self.cleaned_data['temas'].all()]
        if Tema.objects.get(nome='Outros').pk not in temas_pks:
            temas_outro = ''

        return temas_outro

    def clean_publico_focal_outro(self):
        publico_focal_outro = self.cleaned_data['publico_focal_outro']
        publicos_pks = [
            publico.pk for publico in self.cleaned_data['publico_focal'].all()]
        if PublicoFocal.objects.get(nome='Outros').pk not in publicos_pks:
            publico_focal_outro = ''

        return publico_focal_outro

    def clean_manifestacao_cultural_outro(self):
        manifestacao_cultural_outro = self.cleaned_data['manifestacao_cultural_outro']
        manifestacoes_pks = [
            manifestacao.pk for manifestacao in self.cleaned_data['manifestacao_cultural'].all()]
        if ManifestacaoCultural.objects.get(nome='Outros').pk not in manifestacoes_pks:
            manifestacao_cultural_outro = ''

        return manifestacao_cultural_outro

    def save(self, commit=True):
        instance = super(AcaoFormBase, self).save(commit=False)
        instance.user_upd = self.request_user
        new = not bool(instance.pk)

        if new:
            instance.user_add = self.request_user
            if self.agente:
                instance.agente = self.agente

        instance.save()

        if new:
            AcaoMembro.objects.create(
                user_add=self.request_user,
                user_upd=self.request_user,
                vinculo_tipo=self.cleaned_data['acaomembro_vinculo'],
                vinculo_outro=self.cleaned_data['acaomembro_vinculo_outro'],
                user=self.request_user,
                acao=instance.acao_ptr,
                is_administrador=True
            )

        self.save_m2m()

        return instance


class AcaoBaseSearchForm(forms.ModelForm):

    municipio = forms.ModelChoiceField(
        queryset=Municipio.objects.all(),
        widget=UnicodeModelSelect2Widget(
            model=Municipio, search_fields=['nome__unaccent__icontains'])
    )

    def __init__(self, *args, **kwargs):
        self.tipo = kwargs.pop('tipo', u'')
        super(AcaoBaseSearchForm, self).__init__(*args, **kwargs)

        self.fields['nome'].widget.attrs.update({
            'placeholder': u'Procure um(a) %s pelo nome' % self.tipo.lower(),
        })

        self.fields['municipio'].widget.attrs.update({
            'data-minimum-input-length': 3,
            'data-placeholder': u'Procure um(a) %s pelo munícipio' % self.tipo.lower(),
        })

    class Meta:
        fields = (u'nome', )
        widgets = {
            'nome': forms.TextInput(),
        }


class ProjetoSearchForm(AcaoBaseSearchForm):
    class Meta(AcaoBaseSearchForm.Meta):
        model = Projeto


class AtividadeSearchForm(AcaoBaseSearchForm):
    class Meta(AcaoBaseSearchForm.Meta):
        model = Atividade


class ProjetoForm(AcaoFormBase):

    propositor = forms.CharField(label=u'Propositor', required=False)

    def __init__(self, *args, **kwargs):
        self.tipo = 'Projeto'
        super(ProjetoForm, self).__init__(*args, **kwargs)

        self.fields['propositor_tipo'].initial = Projeto.PROPOSITOR_TIPO.PROPRIO_AGENTE
        self.fields['propositor'].initial = self.agente.nome if self.agente else self.request_user.nome
        self.fields['propositor'].widget.attrs.update({
            'disabled': True,
            'data-agente': self.fields['propositor'].initial
        })
        self.fields['valor'].widget = forms.TextInput(attrs={
            'class': 'money',
            'placeholder': 'R$',
        })

    class Meta(AcaoFormBase.Meta):
        model = Projeto
        fields = AcaoFormBase.Meta.fields + (
            'propositor_tipo',
            'propositor_outro',
            'objetivo',
            'resumo',
            'vinculo',
            'valor',
        )


class AtividadeForm(AcaoFormBase):

    projeto = forms.CharField(label=u'Vínculo com', required=False)

    def __init__(self, *args, **kwargs):
        self.tipo = 'Atividade'
        self.projeto = kwargs.pop('projeto', False)
        super(AtividadeForm, self).__init__(*args, **kwargs)
        if self.projeto:
            self.fields['projeto'].initial = u'Projeto: %s por %s' % (
                self.projeto.nome, self.projeto.agente_add.nome, )
            self.fields['projeto'].widget.attrs.update({
                'disabled': True
            })

    def save(self, commit=True):
        instance = super(AtividadeForm, self).save(commit=False)
        if self.projeto:
            instance.vinculo_projeto = self.projeto
        instance.save()
        return instance

    class Meta(AcaoFormBase.Meta):
        model = Atividade
        fields = AcaoFormBase.Meta.fields + (
            'formato_tipo',
            'formato_outro',
            'descricao'
            # 'projeto',
        )


class LocalForm(forms.ModelForm):
    cep = forms.CharField(label='CEP', max_length=9)

    def __init__(self, *args, **kwargs):
        self.acao = kwargs.pop('acao', False)
        self.request_user = kwargs.pop('request_user')
        super(LocalForm, self).__init__(*args, **kwargs)
        self.fields['point'].initial = self.instance.point.wkt if self.instance.point else ''
        self.fields['point'].required = True
        self.fields['point'].error_messages = {
            'required': 'Ponto no mapa é obrigatório. Por favor, marque o ponto clicando no mapa ou preenchendo o formulário acima e clicando em Mostrar no Mapa.'}
        self.fields['point'].widget = forms.HiddenInput()

        self.fields['cep'].widget.attrs.update({
            'class': 'geo_coding',
        })
        self.fields['logradouro'].widget.attrs.update({
            'class': 'geo_coding',
        })
        self.fields['numero'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['complemento'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['bairro'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['municipio'].widget.attrs.update({'class': 'geo_coding'})
        self.fields['local_nome'].widget.attrs.update({
            'autocomplete': 'off',
            'data-search-url': self.instance.get_search_url(),
        })

    def clean(self):
        # ref #132 e 117 item 4 / necto
        cleaned_data = self.cleaned_data
        cep = cleaned_data.get('cep')
        if cep and '-' in cep:
            cleaned_data['cep'] = cep.replace('-', '')

        return cleaned_data

    def save(self, commit=True):
        acao = self.acao
        new = False
        if 'local_pk' not in self.data:
            instance = super(LocalForm, self).save(commit=False)
            instance.user_upd = self.request_user
            new = not instance.pk
            if new:
                instance.user_add = self.request_user
            instance.save()
        else:
            instance = Local.objects.get(pk=self.data['local_pk'])

        if new or 'local_pk' in self.data:
            LocalAcao.objects.create(acao=acao, local=instance)

        layer_name = u'acao_atividade' if isinstance(
            acao, Atividade) else u'acao_projeto'

        clean_tilestache_cache(point=instance.point, layer=layer_name)

        return instance

    class Meta:
        model = Local
        widgets = {
            'municipio': SelectMunicipioWidget,
        }
        fields = (
            'local_nome',
            'cep',
            'logradouro',
            'numero',
            'complemento',
            'bairro',
            'municipio',
            'point',
        )


class AcaoMembroForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        self.acao = kwargs.pop('acao')
        super(AcaoMembroForm, self).__init__(*args, **kwargs)

        self.fields['user'].queryset = self.fields['user'].queryset.filter(
            is_active=True)
        self.fields['user'].widget.attrs.update({'style': 'width: 100%;'})

        self.fields['vinculo_outro'].widget.attrs.update({'disabled': True})

        if self.instance.pk:
            # se for edição não pode editar o USER
            self.fields['user'].widget.attrs.update({'readonly': True})
            self.fields['user'].queryset = self.fields['user'].queryset.filter(
                pk=self.instance.user.pk)
        else:
            # se for um update, só adiciona usuários que não são membros ainda!
            self.fields['user'].queryset = self.fields['user'].queryset.exclude(
                pk__in=self.acao.membros_ativos().values_list('user__pk', flat=True)
            )

        self.is_administrador = self.acao.is_administrador(self.request_user)
        if not self.is_administrador:
            # se não for is_administrador não edita o campo is_administrador
            self.fields['is_administrador'].widget.attrs.update(
                {'readonly': True})

    def clean_is_administrador(self):
        # se não for is_administrador não edita o campo is_administrador
        if self.is_administrador:
            # se for o unico is_administrador Ativo não pode se desmarcar como is_administrador
            if self.instance.pk and \
               self.acao.membros_ativos().filter(is_administrador=True).count() >= 1 and \
               self.cleaned_data['is_administrador'] != self.instance.is_administrador:
                raise forms.ValidationError(
                    u'Você não pode editar o administrador para este agente, este usuário é o unico administrador!')
            return self.cleaned_data['is_administrador']
        raise forms.ValidationError(u'Você não pode editar o administrador.')

    def clean_user(self):
        # se for um update não edita o user
        if self.instance.pk and self.cleaned_data['user'] != self.instance.user:
            raise forms.ValidationError(u'Você não pode editar o usuário.')
        return self.cleaned_data['user']

    def clean_vinculo_outro(self):
        vinculo_outro = self.cleaned_data['vinculo_outro']
        if 'vinculo_tipo' not in self.cleaned_data or self.cleaned_data['vinculo_tipo'] != AcaoMembro.TIPO.OUTRO:
            vinculo_outro = u''
        return vinculo_outro

    def save(self, commit=True):
        instance = super(AcaoMembroForm, self).save(commit=False)
        instance.user_upd = self.request_user
        instance.acao = self.acao
        if not instance.pk:
            instance.user_add = self.request_user
        instance.save()
        return instance

    class Meta:
        model = AcaoMembro
        fields = (
            'user',
            'vinculo_tipo',
            'vinculo_outro',
            'is_administrador',
        )
        widgets = {
            'user': MembroUserModelSelect2Widget(),
        }


class AcaoAgenteVinculoForm(forms.Form):

    empty_help_text = u'''
        <p>Essa ação faz parte de algum <strong>Coletivo</strong>, <strong>Instituição</strong>, <strong>Fórum, Conselho ou Comitê</strong> ou mesmo <strong>Movimento Social</strong>?
        Utilize o espaço abaixo para vincular um agente à essa ação.</p>
    '''

    class Meta:
        widgets = {
            'agente': UnicodeModelSelect2Widget(model=Agente, search_fields=['nome__unaccent__icontains', ])
        }

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        self.acao = kwargs.pop('acao')
        super(AcaoAgenteVinculoForm, self).__init__(*args, **kwargs)

        agente_qs = Agente.objects.all()

        if not self.request_user.is_anonymous():
            pks = list(self.acao.agentes.values_list('pk', flat=True))

            if self.acao.agente:
                pks += [str(self.acao.agente.pk), ]

            agente_qs = agente_qs.exclude(pk__in=pks)

        self.fields['agente'] = forms.ModelChoiceField(queryset=agente_qs)
        self.fields['agente'].widget = AgenteModelSelect2Widget(queryset=agente_qs, attrs={
                                                                'data-placeholder': u'Digite o nome do agente que deseja vincular à essa ação'})
        self.fields['agente'].widget.attrs.update(
            {'style': 'width: 100%;', 'class': 'agente_vinculado'})


class AcaoModelSelect2Widget(ModelSelect2Widget):
    model = Acao
    search_fields = ['nome__icontains', ]

    def label_from_instance(self, obj):
        return u'%s (%s)' % (obj.nome, obj.tipo, )

    def build_attrs(self, base_attrs, extra_attrs=None):
        attrs = super(AcaoModelSelect2Widget, self).build_attrs(
            base_attrs, extra_attrs=extra_attrs)

        attrs.update({
            'data-minimum-input-length': 3,
        })

        return attrs


class AcaoVinculoForm(forms.Form):

    empty_help_text = u'''
        <p>Esse agente faz parte de algum <strong>Projeto</strong>, ou <strong>Atividade</strong>?
        Utilize o espaço abaixo para vincular este agente a uma ação.</p>
    '''

    class Meta:
        widgets = {
            'acao': UnicodeModelSelect2Widget(
                model=Acao,
                search_fields=['nome__unaccent__icontains', ]
            )
        }

    def __init__(self, *args, **kwargs):
        self.request_user = kwargs.pop('request_user')
        self.agente = kwargs.pop('agente')
        super(AcaoVinculoForm, self).__init__(*args, **kwargs)

        acao_qs = Acao.objects.all()

        if not self.request_user.is_anonymous():
            pks = self.agente.all_acoes.values_list('pk', flat=True)
            acao_qs = acao_qs.exclude(pk__in=pks)

        self.fields['acao'] = forms.ModelChoiceField(queryset=acao_qs)
        self.fields['acao'].widget = AcaoModelSelect2Widget(queryset=acao_qs, attrs={
                                                            'data-placeholder': u'Digite o nome da ação que deseja vincular a esse agente'})
        self.fields['acao'].widget.attrs.update(
            {'style': 'width: 100%;', 'class': 'acao-vinculo'})
