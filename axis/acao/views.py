# coding: utf-8
# from django.contrib.messages.views import SuccessMessageMixin
from axis.acao.models import (
    Acao,
    AcaoFinanciamento,
    LocalAcao,
    Local,
    AcaoMembro,
    Atividade,
    Projeto,
)
from django import forms
from django.apps import apps
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, redirect, HttpResponseRedirect
from django.views.decorators.gzip import gzip_page
from django.views.generic import ListView, RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.db.models.functions import Lower
from .forms import (
    AcaoCustomInlineFormSet,
    LocalForm,
    AcaoMembroForm,
    AcaoAgenteVinculoForm,
    AtividadeForm,
    FinanciamentoForm,
    ProjetoForm,
    ProjetoSearchForm,
    AtividadeSearchForm
)
from axis.agente.models import Agente
from axis.core.views import AddRequestUserMixin, GenericGeoJSONListView
from ibge.models import SetorCensitario
import json


class AcaoGenericGeoJSONListView(GenericGeoJSONListView):
    content_type = 'application/vnd.geo+json'
    http_method_names = [u'get', ]
    geom_field = 'point'
    properties = ['pk', 'target_url', 'local']
    srid_out = 4326

    def get_queryset(self):
        acoes = super(GenericGeoJSONListView, self).get_queryset()

        if 'pk' in self.request.GET:
            pk = self.request.GET['pk']
            acoes = acoes.filter(pk=int(pk))

        from itertools import chain

        filter_kw = {'{}__isnull'.format(self.geom_field): False}

        queryset = list(chain(*[
            acao.locais_acoes.filter(**filter_kw).transform(self.srid_out, field_name=self.geom_field) for acao in acoes
        ]))

        return queryset


class JSONResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """

    def render_to_json_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        data = json.dumps(self.get_queryset())
        return HttpResponse(data, content_type='application/json')


class LocalSearchView(JSONResponseMixin, ListView):
    model = Local

    def get_queryset(self):
        local_nome = self.request.GET.get('local_nome', False)
        cep = self.request.GET.get('cep', False)
        logradouro = self.request.GET.get('logradouro', False)
        numero = self.request.GET.get('numero', False)
        acao_pk = self.request.GET.get('acao', False)
        acao = False

        if acao_pk:
            acao = get_object_or_404(Acao, pk=acao_pk)

        object_list = []
        object_list = self.model.objects

        from django.db.models import Q

        filtro = Q(cep=cep) | Q(logradouro=logradouro, numero=numero)

        if local_nome and len(local_nome) >= 3:
            filtro = filtro | Q(local_nome__icontains=local_nome)

        object_list = object_list.filter(filtro)
        # Q(local_nome__icontains=local_nome) | Q(cep=cep) | Q(logradouro__icontains=logradouro, numero=numero)

        if acao:
            object_list = object_list.exclude(
                pk__in=acao.locais.values_list('pk', flat=True)
            )

        object_list = object_list.select_related('municipio').extra(select={
            'municipio_nome': '"municipios_municipio"."nome"',
            'municipio_uf_id': '"municipios_municipio"."uf_id"',
            'municipio_uf_sigla': '"municipios_municipio"."uf_sigla"',
        })

        object_list = [{
            'id': local.id,
            'fields': {
                'bairro': local.bairro,
                'complemento': local.complemento,
                'point': local.point.wkt,
                'logradouro': local.logradouro,
                'numero': local.numero,
                'municipio_id': local.municipio_id,
                'cep': local.cep,
                'local_nome': local.local_nome,
                'municipio_nome': local.municipio_nome,
                'municipio_uf_id': local.municipio_uf_id,
                'municipio_uf_sigla': local.municipio_uf_sigla,
            }
        } for local in object_list]

        return object_list

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)


local_search = LocalSearchView.as_view()


class ExistProfileMixin(object):

    def get(self, request, *args, **kwargs):
        profile = request.user.get_profile()
        if not profile:
            messages.warning(request, u'Você precisa completar seu Perfil')
            return redirect(reverse('profile_edit') + '?next=' + request.path)
        return super(ExistProfileMixin, self).get(request, *args, **kwargs)


class AddAgenteMixin(object):
    def get_form_kwargs(self):
        kwargs = super(AddAgenteMixin, self).get_form_kwargs()

        if 'agente_pk' in self.kwargs:
            kwargs['agente'] = get_object_or_404(Agente, pk=self.kwargs['agente_pk'])
        elif 'pk' in self.kwargs and self.get_object():
            kwargs['agente'] = self.get_object().agente

        return kwargs


class AddAcaoMixin(object):
    def get_acao(self):
        acao = getattr(self, 'acao', False)
        if not acao:
            acao = get_object_or_404(Acao, pk=self.kwargs['acao_pk'])
            setattr(self, 'acao', acao)
        return acao

    def get_form_kwargs(self):
        kwargs = super(AddAcaoMixin, self).get_form_kwargs()

        if 'acao_pk' in self.kwargs:
            kwargs['acao'] = self.get_acao().get_child()

        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddAcaoMixin, self).get_context_data(**kwargs)
        context['acao'] = self.get_acao().get_child()
        return context


class FormsetsMixin(object):
    """Mixin for rendering multiple formsets with foreign keys (inline formsets)

        Attributes
            formset_classes(list(ModelForm)): ModelForm Classes to produce
               inline formsets.
    """
    formset_classes = []

    def get(self, request, *args, **kwargs):
        if 'pk' in kwargs:
            self.object = self.get_object()
        else:
            self.object = None

        form = self.get_form()
        context = {'form': form}
        context.update(self.get_context_data(*args, **kwargs))
        for formset_class in self.formset_classes:
            RelatedFormSet = forms.inlineformset_factory(
                self.model,
                formset_class.Meta.model,
                formset_class,
                AcaoCustomInlineFormSet,
                min_num=1,
                extra=0,
            )
            modelformset = RelatedFormSet(
                **self.get_form_kwargs()
            )
            context.update({
                formset_class.__name__.lower() + 'set': modelformset
            })

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        if 'pk' in kwargs:
            self.object = self.get_object()
        else:
            self.object = None

        form = self.get_form()
        context = self.get_context_data(*args, **kwargs)
        formsets = []
        for formset_class in self.formset_classes:
            RelatedFormSet = forms.inlineformset_factory(
                self.model,
                formset_class.Meta.model,
                formset_class,
                AcaoCustomInlineFormSet,
                min_num=1,
                extra=0,
            )
            related_formset = RelatedFormSet(
                **self.get_form_kwargs()
            )
            formsets.append(related_formset)
            context.update({
                formset_class.__name__.lower() + 'set': related_formset
            })

        if (form.is_valid() and all([True if formset.is_valid() else False
           for formset in formsets])):
            return self.form_valid(form, context)
        else:
            context.update({'form': form})
            return self.form_invalid(context)

    def form_valid(self, form, context):
        self.object = form.save()

        for formset in filter(lambda f: isinstance(f, AcaoCustomInlineFormSet), context.values()):
            formset.instance = self.object
            formset.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, context):
        messages.warning(
            self.request,
            u'Houve um erro no formulário. Seus dados não foram salvos.'
        )
        return self.render_to_response(context)


class AcaoEditMixin(AddRequestUserMixin, AddAgenteMixin, FormsetsMixin, ExistProfileMixin):

    def get_context_data(self, **kwargs):
        context = super(AcaoEditMixin, self).get_context_data(**kwargs)

        context['FINACIAMENTO_OUTRO_VALUE'] = AcaoFinanciamento.TIPO.OUTROS
        context['FINACIAMENTO_EMPRESA_VALUE'] = AcaoFinanciamento.TIPO.PATROCINIO_EMPRESARIAL
        context['VINCULO_OUTRO_VALUE'] = AcaoMembro.TIPO.OUTRO

        return context

    def get_object(self):
        obj = super(AcaoEditMixin, self).get_object()

        if not obj.can_edit(self.request.user):
            raise Http404

        # transformar ponto(s) aqui ;)
        # point.transform(3857)

        return obj


class ProjetoEditMixin(AcaoEditMixin):

    def get_context_data(self, **kwargs):
        context = super(ProjetoEditMixin, self).get_context_data(**kwargs)

        context['PROPOSITOR_OUTRO_VALUE'] = Projeto.PROPOSITOR_TIPO.OUTROS
        context['PROPOSITOR_ND_VALUE'] = Projeto.PROPOSITOR_TIPO.NAO_DECLARAR

        return context


class AtividadeEditMixin(AcaoEditMixin):

    def get_vinculo_projeto(self):
        if not getattr(self, '_get_vinculo_projeto', False):
            projeto = None
            if 'projeto_pk' in self.kwargs:
                projeto = get_object_or_404(Projeto, pk=self.kwargs['projeto_pk'])
            elif 'pk' in self.kwargs and self.get_object():
                projeto = self.get_object().vinculo_projeto
            setattr(self, '_get_vinculo_projeto', projeto)
        return getattr(self, '_get_vinculo_projeto')

    def get_context_data(self, **kwargs):
        context = super(AtividadeEditMixin, self).get_context_data(**kwargs)
        context['FORMATO_OUTRO_VALUE'] = Atividade.FORMATO_TIPO.OUTRO
        if self.get_vinculo_projeto():
            context['projeto'] = self.get_vinculo_projeto()
        return context

    def get_form_kwargs(self):
        kwargs = super(AtividadeEditMixin, self).get_form_kwargs()

        if self.get_vinculo_projeto():
            kwargs['projeto'] = self.get_vinculo_projeto()

        return kwargs


class AcaoListMixin(object):

    nome_field = 'nome'
    order_by = '-date_add'

    def get_acao_object(self):
        _acao = None
        _acao = {
            'tipo': self.model._tipo(),
            'get_create_url': self.model._get_create_url(),
        }
        return _acao

    def get_form_kwargs(self):
        kwargs = {}
        kwargs.update({
            'tipo': self.get_acao_object()['tipo']
        })
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super(AcaoListMixin, self).get_context_data(*args, **kwargs)
        # context['agente'] = self.get_agente_object()
        self.form = self.form_class(initial=self.request.GET.dict(), **self.get_form_kwargs())
        context['form'] = self.form
        context.update(self.get_acao_object())
        return context

    def get_queryset(self):
        data = self.request.GET.dict()
        queryset = super(AcaoListMixin, self).get_queryset()
        locais = Local.objects.all()
        acoes_by_municipios_pks, acoes_by_nome_pks = [], []

        if data.get('municipio', False):
            acoes_by_municipios_pks = locais.filter(
                municipio__id_ibge=data['municipio']
            ).values_list('acao__pk', flat=True).distinct()

        if data.get('nome', False):
            acoes_by_nome_pks = queryset.filter(**{
                self.nome_field + '__icontains': data['nome']
            }).values_list('pk', flat=True)

        if data.get('municipio', False) or data.get('nome', False):
            queryset = queryset.filter(pk__in=set(
                list(acoes_by_municipios_pks) + list(acoes_by_nome_pks))
            )

        return queryset.order_by(self.order_by)


class AcaoDetailView(object):

    def get_context_data(self, *args, **kwargs):
        context = super(AcaoDetailView, self).get_context_data(*args, **kwargs)
        context['is_administrador'] = self.object.is_administrador(self.request.user)
        context['form_acaomembro'] = AcaoMembroForm(request_user=self.request.user, acao=self.object)
        context['is_member'] = self.object.is_member(self.request.user)
        context['is_only_member'] = self.object.is_member(self.request.user) and self.object.acaomembro_set.count() == 1
        context['membros'] = self.object.membros_ativos().order_by(Lower('user__nome'))
        context['total_membros'] = context['membros'].count()
        context['acao'] = self.object
        context['VINCULO_OUTRO_VALUE'] = AcaoMembro.TIPO.OUTRO
        context['form_vinculo'] = AcaoAgenteVinculoForm(request_user=self.request.user, acao=self.object)

        if self.object and self.object.pk:
            context['total_galeria'] = self.object.galeria.all().count()
            kw = {
                'src_app': 'acao',
                'src_model': self.model.__name__,
                'src_pk': self.object.pk,
            }
            context['galeria_list_url'] = reverse_lazy('galeria_list', kwargs=kw)
            context['galeria_dashboard_url'] = reverse_lazy('galeria_dashboard', kwargs=kw)
            context['galeria_counter_url'] = reverse_lazy('galeria_counter', kwargs=kw)
            context['galeria_create_url'] = reverse_lazy('galeria_create', kwargs=kw)
            kw_extra = {'pk': 0}
            kw_extra.update(kw)
            context['galeria_update_url'] = reverse_lazy('galeria_update', kwargs=kw_extra)
        else:
            context['total_galeria'] = 0

        return context


class ProjetoCreateView(ProjetoEditMixin, CreateView):
    model = Projeto
    form_class = ProjetoForm
    template_name = 'projeto_form.html'
    formset_classes = [FinanciamentoForm, ]


class ProjetoDetailView(AcaoDetailView, DetailView):
    model = Projeto
    form_class = ProjetoForm
    template_name = 'projeto_detalhe.html'


class ProjetoUpdateView(ProjetoEditMixin, UpdateView):
    model = Projeto
    form_class = ProjetoForm
    template_name = 'projeto_form.html'
    formset_classes = [FinanciamentoForm, ]


class ProjetoListView(AcaoListMixin, ListView):
    model = Projeto
    form_class = ProjetoSearchForm
    template_name = 'acao/base_listagem.html'


projeto_create = login_required(ProjetoCreateView.as_view())
projeto_update = login_required(ProjetoUpdateView.as_view())
projeto_detail = ProjetoDetailView.as_view()
projeto_list = ProjetoListView.as_view()
lista_projeto = gzip_page(
    AcaoGenericGeoJSONListView.as_view(
        model=Projeto
    )
)


class AtividadeCreateView(AtividadeEditMixin, CreateView):
    model = Atividade
    form_class = AtividadeForm
    template_name = 'atividade_form.html'
    formset_classes = [FinanciamentoForm, ]


class AtividadeDetailView(AcaoDetailView, DetailView):
    model = Atividade
    form_class = AtividadeForm
    template_name = 'atividade_detalhe.html'


class AtividadeUpdateView(AtividadeEditMixin, UpdateView):
    model = Atividade
    form_class = AtividadeForm
    template_name = 'atividade_form.html'
    formset_classes = [FinanciamentoForm, ]


class AtividadeListView(AcaoListMixin, ListView):
    model = Atividade
    form_class = AtividadeSearchForm
    template_name = 'acao/base_listagem.html'


atividade_create = login_required(AtividadeCreateView.as_view())
atividade_update = login_required(AtividadeUpdateView.as_view())
atividade_detail = AtividadeDetailView.as_view()
atividade_list = AtividadeListView.as_view()
lista_atividade = gzip_page(
    AcaoGenericGeoJSONListView.as_view(
        model=Atividade,
    )
)


class LocalManageView(AddAcaoMixin, ListView):
    model = LocalAcao
    template_name = 'acaolocal_manage_view.html'

    def get(self, *args, **kwargs):

        if 'acao_pk' not in self.kwargs:
            raise Http404

        if not self.get_acao().can_edit(self.request.user):
            raise Http404

        return super(LocalManageView, self).get(*args, **kwargs)

    def get_queryset(self):
        qs = super(LocalManageView, self).get_queryset()
        qs = qs.filter(acao__pk=self.kwargs['acao_pk'])
        return qs


acaolocal_manage_view = login_required(LocalManageView.as_view())


class LocalCreateView(AddRequestUserMixin, AddAcaoMixin, CreateView):
    model = Local
    form_class = LocalForm
    template_name = 'acaolocal_form.html'

    def get_success_url(self):
        acao = get_object_or_404(Acao, pk=(self.kwargs['acao_pk']))
        return acao.get_child().get_absolute_url()


local_create = login_required(LocalCreateView.as_view())


class LocalUpdateView(AddRequestUserMixin, AddAcaoMixin, UpdateView):
    model = Local
    form_class = LocalForm
    template_name = 'acaolocal_form.html'

    def get_success_url(self):
        return reverse('acaolocal_manage_view', args=(self.kwargs['acao_pk'], ))

    def get_object(self):
        obj = super(LocalUpdateView, self).get_object()

        if not self.get_acao().can_edit(self.request.user):
            raise Http404

        if obj.point:
            obj.point.transform(3857)

        return obj


local_update = login_required(LocalUpdateView.as_view())

class LocalRemoveView(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        local_acao = get_object_or_404(LocalAcao, local_id=kwargs['pk'], acao_id=kwargs['acao_pk'])
        acao = get_object_or_404(Acao, pk=kwargs['acao_pk'])
        user = self.request.user

        can_delete = acao.can_edit(user)

        if not can_delete:
            messages.warning(self.request, u'Não foi possível remover o local. Por favor, peça para que um administrador remova.')
        else:
            local_acao.delete()
            messages.success(self.request, u'Local removido com <strong>sucesso</strong>.')

        self.url = self.request.GET.get('next') or u'{url}{aba_hash}'.format(acao.get_child().get_absolute_url(), '#panel-ficha')

        return super(LocalRemoveView, self).get_redirect_url(*args, **kwargs)


local_remove = login_required(LocalRemoveView.as_view())

class LocalDetailView(AddRequestUserMixin, AddAcaoMixin, DetailView):
    model = Local
    template_name = 'acaolocal_detail.html'

    def dispatch(self, request, *args, **kwargs):
        dispatch_ = super(LocalDetailView, self).dispatch(request, *args, **kwargs)
        self.acao = get_object_or_404(Acao, pk=kwargs['acao_pk'])
        return dispatch_

    def get_context_data(self, **kwargs):
        context = super(LocalDetailView, self).get_context_data(**kwargs)
        context['acao'] = self.acao
        if self.object.point:
            perfil_setores = SetorCensitario.objects.entorno(
                self.object.point.y, self.object.point.x).perfil()

            total_equipamentos, equipamentos = self.object.equipamentos_entorno()

            # filtrando todos os tipos de equipamentos que tenham eq[1] >= 1
            # eq[1] é o número de equipamentos de um determinado tipo (zero é
            # quando não tem nenhum deste tipo no entorno)
            equipamentos_existentes = filter(lambda eq: eq[1] >= 1, equipamentos)

            # retorna apenas um dicionário com o model name do tipo de equipamento, exemplo:
            # {
            #     'escola': True,
            # }
            equipamentos_existentes = dict([(model_name, True, ) for tipo, ct, equips, model_name in equipamentos_existentes])

            context.update({
                'perfil_setores': perfil_setores,
                'equipamentos': equipamentos,
                'equipamentos_existentes': equipamentos_existentes,
                'total_equipamentos': total_equipamentos,
            })
        return context

    # def get_object(self):
    #     obj = super(LocalDetailView, self).get_object()

    #     # if not self.get_acao().can_edit(self.request.user):
    #     #     raise Http404

    #     # if obj.point:
    #     #     obj.point.transform(3857)

    #     return obj


local_detail = LocalDetailView.as_view()
lista_acaolocal = gzip_page(
    GenericGeoJSONListView.as_view(
        model=Local,
        geom_field='point',
        properties=['pk', 'target_url', 'local'],
        srid_out=4326
    )
)


class AcaoMembroAddRemoveMe(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        acao = get_object_or_404(Acao, pk=kwargs['pk'])
        member = self.request.user

        acaomembro_kwargs = {}

        if 'vinculo_tipo' in self.request.POST:
            acaomembro_kwargs.update({'vinculo_tipo': self.request.POST['vinculo_tipo']})

        if 'vinculo_outro' in self.request.POST:
            acaomembro_kwargs.update({'vinculo_outro': self.request.POST['vinculo_outro']})

        if kwargs['tipo'] == 'adiciona':
            acao.add_member(member, **acaomembro_kwargs)

            if acao.is_member(member):
                messages.success(self.request, u'Você está vinculado a esse(a) %s <strong>%s</strong>.' % (acao.tipo.lower(), acao.nome, ))
                acao.send_new_member_email(member)
            else:
                messages.warning(self.request, u'Não foi possível vincular acao. Por favor, peça para que um administrador te vincule.')

        elif kwargs['tipo'] == 'remove':

            acao.remove_member(member)

            if acao.is_member(member):
                messages.warning(self.request, u'Não foi possível sair. Por favor, peça para um administrador da ação te remover.')
            else:
                messages.success(self.request, u'Você não está mais vinculado a esse(a) %s.' % acao.tipo.lower())

        self.url = self.request.GET.get('next') or acao.get_child().get_absolute_url()

        return super(AcaoMembroAddRemoveMe, self).get_redirect_url(*args, **kwargs)


acao_membership_add_remove_me = login_required(AcaoMembroAddRemoveMe.as_view())


class AcaoAgenteVinculoAddRemove(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        agente = get_object_or_404(Agente, pk=kwargs['agente_pk'])
        acao = get_object_or_404(Acao, pk=kwargs['acao_pk'])
        user = self.request.user

        vinculo_autorizado = acao.can_edit(user) or agente.can_edit(user)

        if not vinculo_autorizado:
            messages.warning(self.request, u'Não foi possível vincular agente. Por favor, peça para que um administrador vincule.')
        else:
            if kwargs['tipo'] == 'adiciona':
                acao.add_vinculo(agente, user)

                if acao.has_vinculo(agente):
                    messages.success(self.request, u'Agente foi vinculado a esse(a) %s <strong>%s</strong>.' % (acao.tipo.lower(), acao.nome, ))
                    # agente.send_new_member_email(member)
                else:
                    messages.warning(self.request, u'Não foi possível vincular agente. Por favor, peça para que um administrador vincule.')

            elif kwargs['tipo'] == 'remove':
                acao.remove_vinculo(agente)

                if acao.has_vinculo(agente):
                    messages.warning(self.request, u'Não foi possível desvincular. Por favor, peça para um administrador do agente realizar a operação.')
                else:
                    messages.success(self.request, u'Agente foi desvinculado desse(a) %s com <strong>sucesso</strong>.' % acao.tipo.lower())

        self.url = self.request.GET.get('next') or u'{url}{aba_hash}'.format(acao.get_child().get_absolute_url(), '#panel-rede')

        return super(AcaoAgenteVinculoAddRemove, self).get_redirect_url(*args, **kwargs)


acao_agente_vinculo_add_remove = login_required(AcaoAgenteVinculoAddRemove.as_view())


# class AcaoAgenteVinculoAddRemoveMe(RedirectView):
#     permanent = False
#     query_string = True

#     def get_redirect_url(self, *args, **kwargs):
#         user = self.request.user
#         agente = get_object_or_404(Agente, pk=user.pk)
#         acao = get_object_or_404(Acao, pk=kwargs['acao_pk'])

#         if not acao.can_edit(user):
#             messages.warning(self.request, u'Não foi possível vincular agente. Por favor, peça para que um administrador vincule.')
#         else:
#             if kwargs['tipo'] == 'adiciona':
#                 acao.add_vinculo(agente, user)

#                 if acao.has_vinculo(agente):
#                     messages.success(self.request, u'Agente foi vinculado a esse(a) %s <strong>%s</strong>.' % (acao.tipo.lower(), acao.nome, ))
#                 else:
#                     messages.warning(self.request, u'Não foi possível vincular agente. Por favor, peça para que um administrador vincule.')

#             elif kwargs['tipo'] == 'remove':
#                 acao.remove_vinculo(agente)

#                 if acao.has_vinculo(agente):
#                     messages.warning(self.request, u'Não foi possível desvincular. Por favor, peça para um administrador do agente realizar a operação.')
#                 else:
#                     messages.success(self.request, u'Agente foi desvinculado desse(a) %s com <strong>sucesso</strong>.' % acao.tipo.lower())

#         self.url = self.request.GET.get('next') or u'{url}{aba_hash}'.format(acao.get_child().get_absolute_url(), '#panel-rede')

#         return super(AcaoAgenteVinculoAddRemove, self).get_redirect_url(*args, **kwargs)


# acao_agente_vinculo_add_remove_me = login_required(AcaoAgenteVinculoAddRemoveMe.as_view())


class AcaoMembershipMakeUndoAdmin(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        acao_membro = get_object_or_404(AcaoMembro, pk=kwargs['pk'])
        acao = acao_membro.acao

        if acao.is_administrador(self.request.user):

            if kwargs['tipo'] == 'make':
                acao_membro.is_administrador = True
            elif kwargs['tipo'] == 'undo':
                acao_membro.is_administrador = False

            acao_membro.save()

            membro = acao_membro.user
            messages.success(self.request, u'%s agora e um administrador desse(a) %s.' % (membro.nome, acao.tipo.lower(), ))
        else:
            messages.warning(self.request, u'Não foi possível concluir. Por favor, peça para um administrador realizar essa operação.')

        self.url = self.request.GET.get('next') or acao.get_child().get_absolute_url()

        return super(AcaoMembershipMakeUndoAdmin, self).get_redirect_url(*args, **kwargs)


acao_membership_make_undo_admin = login_required(AcaoMembershipMakeUndoAdmin.as_view())


class AcaoMembroAdd(CreateView):
    model = AcaoMembro
    form_class = AcaoMembroForm
    http_method_names = [u'post']

    def get_form_kwargs(self):
        kwargs = super(AcaoMembroAdd, self).get_form_kwargs()
        kwargs.update({
            'request_user': self.request.user,
            'acao': get_object_or_404(Acao, pk=self.kwargs['pk'])
        })
        return kwargs

    def form_valid(self, form):
        self.object = form.save()

        messages.success(self.request, u'''
            Novo membro adicionado com sucesso! <strong>%s</strong> está vinculado a esse(a) %s e receberá uma notificação via e-mail.
        ''' % (self.object.user.nome, self.object.acao.tipo.lower(), ))

        self.object.user.send_new_acaomember_email(self.object.acao)
        return super(AcaoMembroAdd, self).form_valid(form)

    def form_invalid(self, form):
        messages.success(self.request, u'Falha ao vincular novo membro. Por favor, tente novamente.')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return self.object.acao.get_child().get_absolute_url()


acao_membership_add = login_required(AcaoMembroAdd.as_view())
