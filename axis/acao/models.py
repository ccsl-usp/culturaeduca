# -*- coding: utf-8 -*-
from axis.core.models import (
    AreaAtuacao as Tema,
    Endereco as EnderecoBase,
    LinguagemArtistica,
    ManifestacaoCultural,
    PublicoFocal,
    UserAddUpd,
    ItemGaleria,
)
from django.conf import settings
# from django.core.files.storage import get_storage_class
from django.template.loader import get_template
from axis.agente.models import Agente
from django.contrib.gis.db import models
from django.contrib.gis.db.models.query import GeoQuerySet
from django.contrib.gis.geos import Point, MultiPoint, MultiPolygon
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse, reverse_lazy
from utils import equipamentos_entorno, clean_tilestache_cache
from django.db.models.signals import post_delete
from django.db.models import Case, When, Q, Value
# avatar_storage = get_storage_class(settings.DEFAULT_FILE_STORAGE)()


class Acao(UserAddUpd):
    ''' Ação (Projeto/Atividade) '''

    nome = models.CharField(u'Nome/Título', max_length=200)
    temas = models.ManyToManyField(Tema, verbose_name=u'Temas')  # AreaAtuacao
    temas_outro = models.CharField(
        verbose_name=u'Outro Tema', max_length=512, blank=True)
    publico_focal = models.ManyToManyField(
        PublicoFocal, verbose_name=u'Público Focal')
    publico_focal_outro = models.CharField(
        verbose_name=u'Outro Tipo de Público Focal', max_length=512, blank=True)
    linguagem_artistica = models.ManyToManyField(
        LinguagemArtistica, verbose_name=u'Linguagens Artísticas', blank=True)
    manifestacao_cultural = models.ManyToManyField(
        ManifestacaoCultural, verbose_name=u'Manifestações Culturais', blank=True)
    manifestacao_cultural_outro = models.CharField(
        verbose_name=u'Outro Tipo de Manifestação Cultural', max_length=512, null=True, blank=True)
    inicio = models.DateField(u'Data início da ação', null=True, blank=True)
    fim = models.DateField(u'Data fim da ação', null=True, blank=True)
    financiado = models.BooleanField(
        u'Financiado', help_text=u'Ação é financiada?')
    agente = models.ForeignKey(
        Agente, verbose_name=u'Agente', null=True, blank=True, related_name=u'acoes')
    locais = models.ManyToManyField('Local', through='LocalAcao')

    galeria = models.ManyToManyField(ItemGaleria)

    def __unicode__(self):
        return self.nome

    def get_child(self):
        for tipo in [u'atividade', u'projeto', ]:
            try:
                child = getattr(self, tipo)
                return child
            except AttributeError:
                pass

    def can_edit(self, user):
        if user.is_anonymous():
            return False

        # o usuário poderá editar a ação se for o agente que cadastrou ou se for admin do agente que cadastrou
        agentes = user.agentemembro_set.filter(
            is_administrador=True).values_list('agente__pk', flat=True)
        return self.user_add_id == user.id or self.agente_id in agentes

    def get_create_url(self):
        return self.__class__._get_create_url()

    @property
    def tipo(self):
        return self.get_child().__class__._tipo()

    @property
    def app_model(self):
        return 'acao.{}'.format(self.get_child().__class__.__name__)

    @property
    def agente_add(self):
        return self.agente if self.agente else self.user_add

    def is_member(self, user):
        return not user.is_anonymous() and self.membros_ativos().filter(user=user).count() >= 1

    def is_administrador(self, user):
        return not user.is_anonymous() and self.membros_ativos().filter(user=user, is_administrador=True).count() >= 1

    def can_change_gallery(self, user):
        return any([self.is_member(user), self.is_administrador(user)])

    def add_member(self, user, is_administrador=False, user_add=None, **kwargs):
        acaomembro = AcaoMembro(
            user=user, is_administrador=is_administrador, **kwargs)

        if user_add:
            acaomembro.user_add = user_add
            acaomembro.user_upd = user_add
        else:
            acaomembro.user_add = user
            acaomembro.user_upd = user

        self.acaomembro_set.add(acaomembro, bulk=False)

    def remove_member(self, user):
        if self.acaomembro_set.filter(is_administrador=True).exclude(user=user).count() >= 1:
            self.acaomembro_set.filter(user=user).delete()

    def add_vinculo(self, agente, user_add):
        vinculo = AcaoAgenteVinculo(
            agente=agente, user_add=user_add, user_upd=user_add)
        self.acaoagentevinculo_set.add(vinculo, bulk=False)

        return True

    def remove_vinculo(self, agente):
        self.acaoagentevinculo_set.filter(agente=agente).delete()

    def has_vinculo(self, agente):
        return self.acaoagentevinculo_set.filter(agente=agente).count() >= 1

    def membros_ativos(self):
        return self.acaomembro_set.filter(user__is_active=True)

    def send_new_member_email(self, user):
        from django.core.mail import send_mail

        current_site = Site.objects.get_current()
        subject = u'[Cultura Educa] - Novo membro em ação %s' % self.nome
        template = get_template('acao/email/email_novo_acaomembro.html')

        email = template.render({
            'site_name': current_site.name,
            'domain': current_site.domain,
            'protocol': u'https',
            'acao': self.get_child(),
            'user': user,
        })

        send_emails = self.acaomembro_set.filter(
            is_administrador=True).values_list('user__email', flat=True)

        return send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, send_emails, html_message=email)

    @property
    def locais_acoes(self):
        if not hasattr(self, '_locais'):
            locais_diretos_pk = list(
                self.locais_diretos.values_list('pk', flat=True))
            locais_atividades_pk = []
            if hasattr(self, 'atividades'):
                for atividade in self.atividades.all():
                    locais_atividades_pk.extend(
                        atividade.locais_diretos.filter(
                            local__point__isnull=False
                        ).values_list('pk', flat=True)
                    )
            locais_pks = set(locais_diretos_pk + locais_atividades_pk)
            setattr(self, '_locais', Local.objects.filter(
                locais_acoes__pk__in=locais_pks))
        return getattr(self, '_locais')

    @property
    def transformed_locais(self):
        """Transforma ponto para o SRID do mapa, utilizado na aba rede."""
        locais = self.locais_acoes
        locais.transform(3857)
        return locais

    def setores_censitarios_entorno(self, raio=1):
        if not hasattr(self, '_setores_censitarios_entorno'):
            from itertools import chain
            from ibge.models import SetorCensitario
            setores_pk = list(chain(*[
                SetorCensitario.objects.entorno(
                    local.point.y, local.point.x, raio
                ).values_list('pk', flat=True) for local in self.locais_acoes.filter(point__isnull=False)
            ]))
            setattr(self, '_setores_censitarios_entorno',
                    SetorCensitario.objects.filter(pk__in=set(setores_pk)))

        return getattr(self, '_setores_censitarios_entorno')

    @property
    def locais_multipoint(self):
        return MultiPoint(
            *self.locais_acoes.transform(srid_out=3857, geom_field='point').values_list('point', flat=True)
        )

    @property
    def setores_censitarios_entorno_polygon(self):
        from django.contrib.gis.db.models import Union
        return self.setores_censitarios_entorno().transform(
            srid_out=3857,
            geom_field='point'
        ).aggregate(area=Union('geom'))['area']

    @property
    def agentes(self):
        pks = self.acaoagentevinculo_set.values_list('agente__pk', flat=True)
        return Agente.objects.filter(pk__in=pks)

    @property
    def rede_acoes(self):
        pks = list(self.acaoagentevinculo_set.values_list(
            'agente__pk', flat=True))
        return Acao.objects.filter(agente__pk__in=pks).annotate(
            acao_outro_agente=Case(
                When(Q(agente__pk__in=pks), then=Value(True)),
                default=Value(False),
                distinct=True,
                output_field=models.BooleanField()
            )
        )

    @property
    def rede_projetos(self):
        return self.rede_acoes.filter(projeto__isnull=False)

    @property
    def rede_atividades(self):
        return self.rede_acoes.filter(atividade__isnull=False)


class Local(EnderecoBase):
    local_nome = models.CharField(
        u'Local',
        max_length=120,
        null=True,
        blank=True
    )
    objects = models.GeoManager()

    def __unicode__(self):
        return u'%s' % (
            self.local_nome if self.local_nome else u'Local #%s' % self.pk
        )

    def get_absolute_url(self):
        return reverse('acaolocal_detail', args=(self.pk,))

    @property
    def local(self):
        return u'%s' % self.local_nome

    @property
    def lista_url(self):
        return reverse_lazy('lista_acaolocal')

    def get_search_url(self):
        return reverse('local_search')

    @property
    def target_url(self):
        return self.get_absolute_url()

    def equipamentos_entorno(self, raio=1):
        return equipamentos_entorno(self, raio=raio, geom_field='point')


class LocalAcao(models.Model):
    acao = models.ForeignKey(Acao, related_name=u'locais_diretos')
    local = models.ForeignKey(Local, related_name=u'locais_acoes')

    def user_can_delete(self, user):
        return self.acao.can_edit(user)


def localacao_postdelete_signal(sender, instance, **kwargs):
    layer_name = u'acao_atividade' if isinstance(
        instance.acao.get_child(), Atividade) else u'acao_projeto'
    clean_tilestache_cache(point=instance.local.point, layer=layer_name)


post_delete.connect(localacao_postdelete_signal, sender=LocalAcao)


class AcaoMembro(UserAddUpd):

    class TIPO(object):
        ALUNO = 1
        APRESENTADOR = 2
        ARTISTA = 3
        AUTOR = 4
        COORDENADOR = 5
        CURADOR = 6
        DEBATEDOR = 7
        DIRETOR = 8
        DIVULGADOR = 9
        ESTAGIARIO = 26
        EXPOSITOR = 10
        FACILITADOR = 11
        IDEALIZADOR = 12
        MEDIADOR = 13
        MONITOR = 14
        OFICINEIRO = 15
        ORGANIZADOR = 16
        PALESTRANTE = 17
        PARTICIPANTE = 18
        PESQUISADOR = 19
        PRODUTOR = 20
        PROFESSOR = 21
        REALIZADOR = 22
        SUPERVISOR = 23
        OUTRO = 24
        NAO_DECLARAR = 25

        CHOICES = (
            (ALUNO, 'Aluno(a)'),
            (APRESENTADOR, 'Apresentador(a)'),
            (ARTISTA, 'Artista'),
            (AUTOR, 'Autor(a)'),
            (COORDENADOR, 'Coordenador(a)'),
            (CURADOR, 'Curador(a)'),
            (DEBATEDOR, 'Debatedor(a)'),
            (DIRETOR, 'Diretor(a)'),
            (DIVULGADOR, 'Divulgador(a)'),
            (ESTAGIARIO, 'Estagiário(a)'),
            (EXPOSITOR, 'Expositor(a)'),
            (FACILITADOR, 'Facilitador(a)'),
            (IDEALIZADOR, 'Idealizador(a)'),
            (MEDIADOR, 'Mediador(a)'),
            (MONITOR, 'Monitor(a)'),
            (OFICINEIRO, 'Oficineiro(a)'),
            (ORGANIZADOR, 'Organizador(a)'),
            (PALESTRANTE, 'Palestrante'),
            (PARTICIPANTE, 'Participante'),
            (PESQUISADOR, 'Pesquisador(a)'),
            (PRODUTOR, 'Produtor(a)'),
            (PROFESSOR, 'Professor(a)'),
            (REALIZADOR, 'Realizador(a)'),
            (SUPERVISOR, 'Supervisor(a)'),
            (OUTRO, 'Outro'),
            (NAO_DECLARAR, 'Não declarar'),
        )

    vinculo_tipo = models.SmallIntegerField(
        u'Tipo do Vínculo', choices=TIPO.CHOICES)
    vinculo_outro = models.CharField(
        u'Outro', max_length=100, null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Usuário')
    acao = models.ForeignKey(Acao)
    is_administrador = models.BooleanField(u'Membro Administrador')

    def user_can_delete(self, user):
        return self.user != user and self.acao.is_administrador(user)

    @property
    def vinculo(self):
        return u'%s' % (
            self.vinculo_outro if self.vinculo_tipo == self.TIPO.OUTRO else self.get_vinculo_tipo_display()
        )


class AcaoAgenteVinculo(UserAddUpd):
    acao = models.ForeignKey(Acao)
    agente = models.ForeignKey(Agente)

    def user_can_delete(self, user):
        return self.agente.is_administrador(user)


class AcaoFinanciamento(models.Model):
    class TIPO(object):
        ARRECADACAO_COMUNITARIA = 1
        COBRANCA_INGRESSO = 2
        CONCURSO_INICIATIVA_ESTATAL = 3
        CONCURSO_INICIATIVA_PRIVADA = 4
        EDITAL_INICIATIVA_ESTATAL = 5
        EDITAL_INICIATIVA_PRIVADA = 6
        FINANCIAMENTO_COLETIVO_CROWFUNDING = 7
        LEI_INCENTIVO_ESTADUAL = 8
        LEI_INCENTIVO_FEDERAL = 9
        LEI_INCENTIVO_FISCAL_MUNICIPAL = 10
        PATROCINIO_EMPRESARIAL = 11
        PATROCINIO_PESSOA_FISICA = 12
        PREMIO_INICIATIVA_ESTATAL = 13
        PREMIO_INICIATIVA_PRIVADA = 14
        FUNDOS_ORGAOS_OU_AGENCIAS_INTERNACIONAIS = 15
        OUTROS = 16
        NAO_DECLARAR = 17

        CHOICES = (
            (ARRECADACAO_COMUNITARIA, u'Arrecadação comunitária'),
            (COBRANCA_INGRESSO, u'Cobrança de Ingresso'),
            (CONCURSO_INICIATIVA_ESTATAL, u'Concurso Iniciativa Estatal'),
            (CONCURSO_INICIATIVA_PRIVADA, u'Concurso Iniciativa Privada'),
            (EDITAL_INICIATIVA_ESTATAL, u'Edital Iniciativa Estatal'),
            (EDITAL_INICIATIVA_PRIVADA, u'Edital Iniciativa Privada'),
            (FINANCIAMENTO_COLETIVO_CROWFUNDING,
             u'Financiamento Coletivo (crowfunding)'),
            (LEI_INCENTIVO_ESTADUAL, u'Lei de incentivo estadual'),
            (LEI_INCENTIVO_FEDERAL, u'Lei de incentivo federal'),
            (LEI_INCENTIVO_FISCAL_MUNICIPAL, u'Lei de incentivo fiscal municipal'),
            (PATROCINIO_EMPRESARIAL, u'Patrocínio Empresarial'),
            (PATROCINIO_PESSOA_FISICA, u'Patrocinio Pessoa Física'),
            (PREMIO_INICIATIVA_ESTATAL, u'Prêmio de Iniciativa Estatal'),
            (PREMIO_INICIATIVA_PRIVADA, u'Prêmio de Iniciativa Privada'),
            (FUNDOS_ORGAOS_OU_AGENCIAS_INTERNACIONAIS,
             u'Fundos de Órgãos ou  Agências Internacionais'),
            (OUTROS, u'Outros'),
            (NAO_DECLARAR, u'Não declarar'),
        )

    acao = models.ForeignKey(Acao)
    tipo = models.SmallIntegerField(
        u'Tipo de Financiamento', choices=TIPO.CHOICES)
    outro = models.CharField(u'Outro', max_length=100, null=True, blank=True)
    empresa = models.CharField(
        u'Nome da Empresa', max_length=100, null=True, blank=True)

    def __unicode__(self):
        financiamento = ''

        if self.tipo == self.TIPO.OUTROS:
            financiamento = self.outro
        elif self.tipo == self.TIPO.PATROCINIO_EMPRESARIAL:
            financiamento = '%s: %s' % (
                self.get_tipo_display(), self.empresa, )
        else:
            financiamento = self.get_tipo_display()

        return u'%s' % financiamento


class Projeto(Acao):
    ''' Ação - Projeto '''
    class PROPOSITOR_TIPO(object):
        PROPRIO_AGENTE = 1
        OUTROS = 2
        NAO_DECLARAR = 3

        CHOICES = (
            (PROPRIO_AGENTE, u'O próprio agente'),
            (OUTROS, u'Outro'),
            (NAO_DECLARAR, u'Não declarar'),
        )

    propositor_tipo = models.SmallIntegerField(
        u'Tipo do Propositor', choices=PROPOSITOR_TIPO.CHOICES, null=False, blank=False)
    propositor_outro = models.CharField(
        u'Outro', max_length=100, null=True, blank=True)
    objetivo = models.TextField(u'Objetivo/Fim')
    resumo = models.TextField(u'Resumo do Projeto')

    vinculo = models.TextField(
        u'Vínculo do Projeto',
        null=True,
        blank=True,
        help_text=u'''citar com qual(is) programa(s) ou ação(ões) de governo o projeto está vinculado''')

    valor = models.DecimalField(
        max_digits=18, decimal_places=2, null=True, blank=True)

    def get_absolute_url(self):
        return reverse('projeto_detail', args=(self.pk, ))

    def get_edit_url(self):
        return reverse('projeto_update', args=(self.pk, ))

    @property
    def lista_url(self):
        return reverse_lazy('lista_projeto')

    @property
    def local(self):
        return self.nome

    @classmethod
    def _get_create_url(cls):
        return reverse('projeto_create')

    @classmethod
    def _tipo(cls):
        return u'%s' % cls._meta.verbose_name

    @property
    def propositor(self):
        propositor = ''

        if self.propositor_tipo == self.PROPOSITOR_TIPO.OUTROS:
            propositor = u'%s' % self.propositor_outro
        elif self.propositor_tipo == self.PROPOSITOR_TIPO.PROPRIO_AGENTE:
            if self.agente:
                propositor = self.agente
            else:
                propositor = self.user_add
        else:
            propositor = u'%s' % self.get_propositor_tipo_display()

        return propositor

    class Meta:
        verbose_name = u'Projeto'
        verbose_name_plural = u'Projetos'

    @property
    def tipo(self):
        return self._meta.verbose_name

    '''
    OK Projeto:

        NOME/TÍTULO DO PROJETO
    PROPOSITOR DO PROJETO
    OBJETIVO DO PROJETO
        TEMA
        PÚBLICO FOCAL
        LINGUAGENS ARTÍSTICAS
        MANIFESTAÇÕES CULTURAIS
        INÍCIO
        FIM
            DURAÇÃO DO PROJETO (EM meses)
    RESUMO DO PROJETO
        FINANCIADO SIM OU NÃO
        TIPOS DE FINANCIAMENTO
    VÍNCULO DO PROJETO
    VALOR DO PROJETO
    LOCAL DE REALIZAÇÃO DO PROJETO
                ATIVIDADES DO PROJETO ?
    '''


class Atividade(Acao):
    class FORMATO_TIPO(object):
        APRESENTACAO_CIRCENSE = 1
        APRESENTACAO_MUSICAL = 2
        APRESENTACAO_TEATRAL = 3
        ATO = 4
        AULA = 5
        COLOQUIO = 6
        CONCURSO = 7
        CONGRESSO = 8
        CURSO = 9
        DEBATE = 10
        DESFILE = 11
        DIALOGO = 12
        EXPOSICAO = 13
        FEIRA = 14
        FESTA = 15
        FORUM = 16
        INTERVENCAO_URBANA = 17
        MESA_REDONDA = 18
        MOSTRA = 19
        OFICINA = 20
        PAINEL = 21
        PALESTRA = 22
        PERFORMANCE = 23
        PREMIO = 24
        REUNIAO = 25
        RODA_DE_CONVERSA = 26
        SARAU = 27
        SEMANA = 28
        SEMINARIO = 29
        SIMPOSIO = 30
        TORNEIO = 31
        VERNISSAGE = 32
        VIVENCIA = 33
        WORKSHOP = 34
        OUTRO = 35
        NAO_DECLARAR = 36
        APRESENTACAO_DE_DANCA = 37
        ENTREVISTA = 38

        CHOICES = (
            (APRESENTACAO_CIRCENSE, u'Apresentação Circense'),
            (APRESENTACAO_MUSICAL, u'Apresentação Musical'),
            (APRESENTACAO_TEATRAL, u'Apresentação Teatral'),
            (ATO, u'Ato'),
            (AULA, u'Aula'),
            (COLOQUIO, u'Colóquio'),
            (CONCURSO, u'Concurso'),
            (CONGRESSO, u'Congresso'),
            (CURSO, u'Curso'),
            (DEBATE, u'Debate'),
            (DESFILE, u'Desfile'),
            (DIALOGO, u'Diálogo'),
            (EXPOSICAO, u'Exposição'),
            (FEIRA, u'Feira'),
            (FESTA, u'Festa'),
            (FORUM, u'Fórum'),
            (INTERVENCAO_URBANA, u'Intervenção Urbana'),
            (MESA_REDONDA, u'Mesa Redonda'),
            (MOSTRA, u'Mostra'),
            (OFICINA, u'Oficina'),
            (PAINEL, u'Painel'),
            (PALESTRA, u'Palestra'),
            (PERFORMANCE, u'Performance'),
            (PREMIO, u'Prêmio'),
            (REUNIAO, u'Reunião'),
            (RODA_DE_CONVERSA, u'Roda de Conversa'),
            (SARAU, u'Sarau'),
            (SEMANA, u'Semana'),
            (SEMINARIO, u'Seminário'),
            (SIMPOSIO, u'Simpósio'),
            (TORNEIO, u'Torneio'),
            (VERNISSAGE, u'Vernissage'),
            (VIVENCIA, u'Vivência'),
            (WORKSHOP, u'Workshop'),
            (OUTRO, u'Outro'),
            (NAO_DECLARAR, u'Não Declarar'),
            (APRESENTACAO_DE_DANCA, u'Apresentação de Dança'),
            (ENTREVISTA, u'Entrevista'),
        )

    formato_tipo = models.SmallIntegerField(
        u'Formato da Atividade', choices=FORMATO_TIPO.CHOICES)
    formato_outro = models.CharField(
        u'Outro', max_length=100, null=True, blank=True)
    vinculo_projeto = models.ForeignKey(
        Projeto, null=True, related_name=u'atividades')
    descricao = models.TextField(
        u'Descrição da Atividade', null=True, blank=True)

    @property
    def formato(self):
        return u'%s' % self.get_formato_tipo_display() if self.formato_tipo != self.FORMATO_TIPO.OUTRO else self.formato_outro

    def get_absolute_url(self):
        return reverse('atividade_detail', args=(self.pk, ))

    def get_edit_url(self):
        return reverse('atividade_update', args=(self.pk, ))

    @property
    def lista_url(self):
        return reverse_lazy('lista_atividade')

    @property
    def local(self):
        return self.nome

    @classmethod
    def _get_create_url(cls):
        return reverse('atividade_create')

    @classmethod
    def _tipo(cls):
        return u'%s' % cls._meta.verbose_name

    class Meta:
        verbose_name = u'Atividade'
        verbose_name_plural = u'Atividades'

    @property
    def tipo(self):
        return self._meta.verbose_name

    '''
    Atividade:

        NOME DA ATIVIDADE
        INÍCIO DA ATIVIDADE
        FIM DA ATIVIDADE
            DURAÇÃO DA ATIVIDADE (em dias)
        TEMA
    FORMATO DA ATIVIDADE
            PARTICIPAÇÃO DO AGENTE NA ATIVIDADE
        PÚBLICO FOCAL
        LINGUAGENS ARTÍSTICAS
        MANIFESTAÇÕES CULTURAIS
        ATIVIDADE FINANCIADA
        TIPOS DE FINANCIAMENTO
    LOCAL/ESPAÇO DA ATIVIDADE
    ENDEREÇO DE REALIZAÇÃO DA ATIVIDADE

                PROJETO ?
    '''
