from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^projeto/cadastro/$', views.projeto_create, name='projeto_create'),
    url(r'^projeto/editar/(?P<pk>\d+)/$', views.projeto_update, name='projeto_update'),
    url(r'^projeto/(?P<pk>\d+)/$', views.projeto_detail, name='projeto_detail'),
    url(r'^projeto/(?P<projeto_pk>\d+)/atividade/cadastro/$', views.atividade_create, name='atividade_create'),
    url(r'^rede/projeto/$', views.projeto_list, name='projeto_list'),

    url(r'^atividade/cadastro/$', views.atividade_create, name='atividade_create'),
    url(r'^atividade/editar/(?P<pk>\d+)/$', views.atividade_update, name='atividade_update'),
    url(r'^atividade/(?P<pk>\d+)/$', views.atividade_detail, name='atividade_detail'),
    url(r'^rede/atividade/$', views.atividade_list, name='atividade_list'),

    url(r'^(?P<acao_pk>\d+)/gerenciar-locais/$', views.acaolocal_manage_view, name='acaolocal_manage_view'),
    url(r'^(?P<acao_pk>\d+)/gerenciar-locais/cadastro$', views.local_create, name='acaolocal_create'),
    url(r'^(?P<acao_pk>\d+)/gerenciar-locais/editar/(?P<pk>\d+)/$', views.local_update, name='acaolocal_update'),
    url(r'^(?P<acao_pk>\d+)/gerenciar-locais/remover/(?P<pk>\d+)/$', views.local_remove, name='acaolocal_remove'),
    url(r'^(?P<tipo_acao>atividade|projeto)/(?P<acao_pk>\d+)/local/(?P<pk>\d+)/$', views.local_detail, name='acaolocal_detail'),
    url(r'^local/(?P<pk>\d+)/$', views.local_detail, name='acaolocal_detail'),
    url(r'^search-local/$', views.local_search, name='local_search'),

    url(r'^lista_projeto.geojson$', views.lista_projeto, name='lista_projeto'),
    url(r'^lista_atividade.geojson$', views.lista_atividade, name='lista_atividade'),
    url(r'^lista_acaolocal.geojson$', views.lista_acaolocal, name='lista_acaolocal'),

    url(r'^(?P<tipo>(adiciona|remove))-membro/(?P<pk>\d+)/me/$', views.acao_membership_add_remove_me, name='acao_membership_add_remove_me'),
    url(r'^adiciona-membro/(?P<pk>\d+)/$', views.acao_membership_add, name='acao_membership_add'),
    url(r'^(?P<tipo>(make|undo))-administrador/(?P<pk>\d+)/$', views.acao_membership_make_undo_admin, name='acao_membership_make_undo_admin'),

    url(r'^(?P<tipo>(adiciona|remove))-agente/(?P<acao_pk>\d+)/(?P<agente_pk>\d+)/$', views.acao_agente_vinculo_add_remove, name='acao_agente_vinculo_add_remove'),
]
