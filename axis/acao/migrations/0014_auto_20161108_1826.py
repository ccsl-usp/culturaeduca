# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0013_auto_20161108_1601'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='acaolocal',
            name='acao',
        ),
        migrations.RemoveField(
            model_name='acaolocal',
            name='municipio',
        ),
        migrations.AlterField(
            model_name='acao',
            name='locais',
            field=models.ManyToManyField(to='acao.Local', null=True, through='acao.LocalAcao'),
        ),
        migrations.DeleteModel(
            name='AcaoLocal',
        ),
    ]
