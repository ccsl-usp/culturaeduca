# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def transfer_data_models(apps, schema_editor):
    Acao = apps.get_model('acao', 'Acao')
    AcaoLocal = apps.get_model('acao', 'AcaoLocal')
    Local = apps.get_model('acao', 'Local')
    LocalAcao = apps.get_model('acao', 'LocalAcao')

    for acao in Acao.objects.all():
        locais = AcaoLocal.objects.filter(acao__pk=acao.pk)
        for local in locais:
            instance_local = Local.objects.create(
                cep=local.cep,
                logradouro=local.logradouro,
                numero=local.numero,
                complemento=local.complemento,
                bairro=local.bairro,
                point=local.point,
                local_nome=local.local_nome,
                municipio=local.municipio,
            )
            LocalAcao.objects.create(acao=acao, local=instance_local)


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0012_acao_locais'),
    ]

    operations = [
        migrations.RunPython(transfer_data_models)
    ]
