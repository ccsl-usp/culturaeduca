# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('agente', '0018_auto_20161024_1338'),
        #('municipios', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0036_auto_20161025_0958'),
    ]

    operations = [
        migrations.CreateModel(
            name='Acao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_add', models.DateTimeField(auto_now_add=True)),
                ('date_upd', models.DateTimeField(auto_now=True)),
                ('nome', models.CharField(max_length=200, verbose_name='Nome/T\xedtulo')),
                ('inicio', models.DateField(null=True, verbose_name='Data in\xedcio da a\xe7\xe3o', blank=True)),
                ('fim', models.DateField(null=True, verbose_name='Data fim da a\xe7\xe3o', blank=True)),
                ('financiado', models.BooleanField(help_text='A\xe7\xe3o \xe9 financiada?', verbose_name='Financiado')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AcaoLocal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cep', models.CharField(help_text='formato: 12232054', max_length=8)),
                ('logradouro', models.CharField(max_length=100)),
                ('numero', models.CharField(max_length=50, verbose_name='N\xfamero')),
                ('complemento', models.CharField(max_length=100, null=True, blank=True)),
                ('bairro', models.CharField(max_length=100)),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4674, null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Financiamento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.SmallIntegerField(verbose_name='Tipo de Financiamento', choices=[(1, 'Arrecada\xe7\xe3o comunit\xe1ria'), (2, 'Cobran\xe7a de Ingresso'), (3, 'Concurso Iniciativa Estatal'), (4, 'Concurso Iniciativa Privada'), (5, 'Edital Iniciativa Estatal'), (6, 'Edital Iniciativa Privada'), (7, 'Financiamento Coletivo (crowfunding)'), (8, 'Lei de incentivo estadual'), (9, 'Lei de incentivo federal'), (10, 'Lei de incentivo fiscal municipal'), (11, 'Patroc\xednio Empresarial'), (12, 'Patrocinio Pessoa F\xedsica'), (13, 'Pr\xeamio de Iniciativa Estatal'), (14, 'Pr\xeamio de Iniciativa Privada'), (15, 'Fundos de \xd3rg\xe3os ou  Ag\xeancias Internacionais'), (16, 'Outros'), (17, 'N\xe3o declarar')])),
                ('outro', models.CharField(max_length=100, verbose_name='Outro')),
                ('empresa', models.CharField(max_length=100, verbose_name='Nome da Empresa')),
            ],
        ),
        migrations.CreateModel(
            name='Atividade',
            fields=[
                ('acao_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='acao.Acao')),
                ('formato_tipo', models.SmallIntegerField(verbose_name='Formato da Atividade', choices=[(1, 'Apresenta\xe7\xe3o Circense'), (2, 'Apresenta\xe7\xe3o Musical'), (3, 'Apresenta\xe7\xe3o Teatral'), (4, 'Ato'), (5, 'Aula'), (6, 'Col\xf3quio'), (7, 'Concurso'), (8, 'Congresso'), (9, 'Curso'), (10, 'Debate'), (11, 'Desfile'), (12, 'Di\xe1logo'), (13, 'Exposi\xe7\xe3o'), (14, 'Feira'), (15, 'Festa'), (16, 'F\xf3rum'), (17, 'Interven\xe7\xe3o Urbana'), (18, 'Mesa Redonda'), (19, 'Mostra'), (20, 'Oficina'), (21, 'Painel'), (22, 'Palestra'), (23, 'Performance'), (24, 'Pr\xeamio'), (25, 'Reuni\xe3o'), (26, 'Roda de Conversa'), (27, 'Sarau'), (28, 'Semana'), (29, 'Semin\xe1rio'), (30, 'Simp\xf3sio'), (31, 'Torneio'), (32, 'Vernissage'), (33, 'Viv\xeancia'), (34, 'Workshop'), (35, 'Outro'), (36, 'N\xe3o Declarar'), (37, 'Apresenta\xe7\xe3o de Dan\xe7a'), (38, 'Entrevista')])),
                ('formato_outro', models.CharField(max_length=100, null=True, verbose_name='Outro', blank=True)),
                ('local', models.CharField(max_length=120, verbose_name='Local')),
            ],
            options={
                'abstract': False,
            },
            bases=('acao.acao',),
        ),
        migrations.CreateModel(
            name='Projeto',
            fields=[
                ('acao_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='acao.Acao')),
                ('propositor_tipo', models.SmallIntegerField(verbose_name='Tipo de Financiamento', choices=[(1, 'O pr\xf3prio agente'), (2, 'Outros'), (3, 'N\xe3o declarar')])),
                ('propositor_outro', models.CharField(max_length=100, null=True, verbose_name='Outro', blank=True)),
                ('objetivo', models.CharField(max_length=200, verbose_name='Objetivo do Projeto')),
                ('resumo', models.TextField(verbose_name='Resumo do Projeto')),
                ('vinculo', models.TextField(help_text='citar com qual(is) programa(s) ou \n            a\xe7\xe3o(\xf5es) de governo o projeto est\xe1 vinculado', null=True, verbose_name='V\xednculo do Projeto', blank=True)),
                ('valor', models.DecimalField(null=True, max_digits=18, decimal_places=2, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('acao.acao',),
        ),
        migrations.AddField(
            model_name='acaolocal',
            name='acao',
            field=models.ForeignKey(related_name='locais', to='acao.Acao'),
        ),
        migrations.AddField(
            model_name='acaolocal',
            name='municipio',
            field=models.ForeignKey(to='municipios.Municipio'),
        ),
        migrations.AddField(
            model_name='acao',
            name='agente',
            field=models.ForeignKey(related_name='acoes', verbose_name='Agente', blank=True, to='agente.Agente', null=True),
        ),
        migrations.AddField(
            model_name='acao',
            name='financiamentos',
            field=models.ManyToManyField(to='acao.Financiamento', verbose_name='Financiamentos'),
        ),
        migrations.AddField(
            model_name='acao',
            name='linguagem_artistica',
            field=models.ManyToManyField(to='core.LinguagemArtistica', verbose_name='Linguagens Art\xedsticas'),
        ),
        migrations.AddField(
            model_name='acao',
            name='manifestacao_cultural',
            field=models.ManyToManyField(to='core.ManifestacaoCultural', verbose_name='Manifesta\xe7\xf5es Culturais'),
        ),
        migrations.AddField(
            model_name='acao',
            name='publico_focal',
            field=models.ManyToManyField(to='core.PublicoFocal', verbose_name='P\xfablico Focal'),
        ),
        migrations.AddField(
            model_name='acao',
            name='temas',
            field=models.ManyToManyField(to='core.AreaAtuacao', verbose_name='Temas'),
        ),
        migrations.AddField(
            model_name='acao',
            name='user_add',
            field=models.ForeignKey(related_name='acao_acao_created_by', editable=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='acao',
            name='user_upd',
            field=models.ForeignKey(related_name='acao_acao_modified_by', editable=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='atividade',
            name='projeto',
            field=models.ForeignKey(to='acao.Projeto', null=True),
        ),
    ]
