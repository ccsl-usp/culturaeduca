# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0050_user_galeria'),
        ('acao', '0021_auto_20170815_1923'),
    ]

    operations = [
        migrations.AddField(
            model_name='acao',
            name='galeria',
            field=models.ManyToManyField(to='core.ItemGaleria'),
        ),
    ]
