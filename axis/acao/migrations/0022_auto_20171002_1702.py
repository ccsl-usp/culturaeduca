# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0021_auto_20170815_1923'),
    ]

    operations = [
        migrations.AlterField(
            model_name='local',
            name='cep',
            field=models.CharField(max_length=8),
        ),
    ]
