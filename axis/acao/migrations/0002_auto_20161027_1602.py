# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AcaoFinanciamento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.SmallIntegerField(verbose_name='Tipo de Financiamento', choices=[(1, 'Arrecada\xe7\xe3o comunit\xe1ria'), (2, 'Cobran\xe7a de Ingresso'), (3, 'Concurso Iniciativa Estatal'), (4, 'Concurso Iniciativa Privada'), (5, 'Edital Iniciativa Estatal'), (6, 'Edital Iniciativa Privada'), (7, 'Financiamento Coletivo (crowfunding)'), (8, 'Lei de incentivo estadual'), (9, 'Lei de incentivo federal'), (10, 'Lei de incentivo fiscal municipal'), (11, 'Patroc\xednio Empresarial'), (12, 'Patrocinio Pessoa F\xedsica'), (13, 'Pr\xeamio de Iniciativa Estatal'), (14, 'Pr\xeamio de Iniciativa Privada'), (15, 'Fundos de \xd3rg\xe3os ou  Ag\xeancias Internacionais'), (16, 'Outros'), (17, 'N\xe3o declarar')])),
                ('outro', models.CharField(max_length=100, null=True, verbose_name='Outro', blank=True)),
                ('empresa', models.CharField(max_length=100, null=True, verbose_name='Nome da Empresa', blank=True)),
            ],
        ),
        migrations.AlterModelOptions(
            name='atividade',
            options={'verbose_name': 'Atividade', 'verbose_name_plural': 'Atividades'},
        ),
        migrations.AlterModelOptions(
            name='projeto',
            options={'verbose_name': 'Projeto', 'verbose_name_plural': 'Projetos'},
        ),
        migrations.RemoveField(
            model_name='acao',
            name='financiamentos',
        ),
        migrations.AlterField(
            model_name='projeto',
            name='propositor_tipo',
            field=models.SmallIntegerField(verbose_name='Tipo do Propositor', choices=[(1, 'O pr\xf3prio agente'), (2, 'Outro'), (3, 'N\xe3o declarar')]),
        ),
        migrations.DeleteModel(
            name='Financiamento',
        ),
        migrations.AddField(
            model_name='acaofinanciamento',
            name='acao',
            field=models.ForeignKey(to='acao.Acao'),
        ),
    ]
