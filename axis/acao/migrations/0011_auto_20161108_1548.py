# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0010_local'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocalAcao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('acao', models.ForeignKey(related_name='locais_diretos', to='acao.Acao')),
                ('local', models.ForeignKey(related_name='locais_acoes', to='acao.Local', null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='acaolocal',
            name='acao',
            field=models.ForeignKey(related_name='local_direto', to='acao.Acao'),
        ),
    ]
