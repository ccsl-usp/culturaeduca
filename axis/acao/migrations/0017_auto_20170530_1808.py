# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0016_auto_20161110_1548'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acao',
            name='linguagem_artistica',
            field=models.ManyToManyField(to='core.LinguagemArtistica', null=True, verbose_name='Linguagens Art\xedsticas', blank=True),
        ),
        migrations.AlterField(
            model_name='acao',
            name='manifestacao_cultural',
            field=models.ManyToManyField(to='core.ManifestacaoCultural', null=True, verbose_name='Manifesta\xe7\xf5es Culturais', blank=True),
        ),
    ]
