# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0005_acaomembro'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projeto',
            name='objetivo',
            field=models.TextField(verbose_name='Objetivo/Fim'),
        ),
    ]
