# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0024_acaoagentevinculo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acaomembro',
            name='vinculo_tipo',
            field=models.SmallIntegerField(verbose_name='Tipo do V\xednculo', choices=[(1, b'Aluno(a)'), (2, b'Apresentador(a)'), (3, b'Artista'), (4, b'Autor(a)'), (5, b'Coordenador(a)'), (6, b'Curador(a)'), (7, b'Debatedor(a)'), (8, b'Diretor(a)'), (9, b'Divulgador(a)'), (26, b'Estagi\xc3\xa1rio(a)'), (10, b'Expositor(a)'), (11, b'Facilitador(a)'), (12, b'Idealizador(a)'), (13, b'Mediador(a)'), (14, b'Monitor(a)'), (15, b'Oficineiro(a)'), (16, b'Organizador(a)'), (17, b'Palestrante'), (18, b'Participante'), (19, b'Pesquisador(a)'), (20, b'Produtor(a)'), (21, b'Professor(a)'), (22, b'Realizador(a)'), (23, b'Supervisor(a)'), (24, b'Outro'), (25, b'N\xc3\xa3o declarar')]),
        ),
    ]
