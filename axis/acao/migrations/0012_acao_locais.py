# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0011_auto_20161108_1548'),
    ]

    operations = [
        migrations.AddField(
            model_name='acao',
            name='locais',
            field=models.ManyToManyField(to='acao.Local', through='acao.LocalAcao'),
        ),
    ]
