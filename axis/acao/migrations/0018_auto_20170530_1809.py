# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0017_auto_20170530_1808'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acao',
            name='linguagem_artistica',
            field=models.ManyToManyField(to='core.LinguagemArtistica', verbose_name='Linguagens Art\xedsticas', blank=True),
        ),
        migrations.AlterField(
            model_name='acao',
            name='manifestacao_cultural',
            field=models.ManyToManyField(to='core.ManifestacaoCultural', verbose_name='Manifesta\xe7\xf5es Culturais', blank=True),
        ),
    ]
