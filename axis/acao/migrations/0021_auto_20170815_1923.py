# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0020_auto_20170815_1836'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acao',
            name='temas_outro',
            field=models.CharField(max_length=512, verbose_name='Outro Tema', blank=True),
        ),
    ]
