# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0015_auto_20161108_1832'),
    ]

    operations = [
        migrations.AlterField(
            model_name='localacao',
            name='local',
            field=models.ForeignKey(related_name='locais_acoes', to='acao.Local'),
        ),
    ]
