# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0006_auto_20161102_1527'),
    ]

    operations = [
        migrations.RenameField(
            model_name='atividade',
            old_name='projeto',
            new_name='vinculo_projeto',
        ),
    ]
