# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0008_auto_20161103_1729'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acaolocal',
            name='acao',
            field=models.ForeignKey(related_name='locais_diretos', to='acao.Acao'),
        ),
    ]
