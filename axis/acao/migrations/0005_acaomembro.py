# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('acao', '0004_auto_20161101_1102'),
    ]

    operations = [
        migrations.CreateModel(
            name='AcaoMembro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_add', models.DateTimeField(auto_now_add=True)),
                ('date_upd', models.DateTimeField(auto_now=True)),
                ('vinculo_tipo', models.SmallIntegerField(verbose_name='Tipo do V\xednculo', choices=[(1, b'Aluno(a)'), (2, b'Apresentador(a)'), (3, b'Artista'), (4, b'Autor(a)'), (5, b'Coordenador(a)'), (6, b'Curador(a)'), (7, b'Debatedor(a)'), (8, b'Diretor(a)'), (9, b'Divulgador(a)'), (10, b'Expositor(a)'), (11, b'Facilitador(a)'), (12, b'Idealizador(a)'), (13, b'Mediador(a)'), (14, b'Monitor(a)'), (15, b'Oficineiro(a)'), (16, b'Organizador(a)'), (17, b'Palestrante'), (18, b'Participante'), (19, b'Pesquisador(a)'), (20, b'Produtor(a)'), (21, b'Professor(a)'), (22, b'Realizador(a)'), (23, b'Supervisor(a)'), (24, b'Outro'), (25, b'N\xc3\xa3o declarar')])),
                ('vinculo_outro', models.CharField(max_length=100, null=True, verbose_name='Outro', blank=True)),
                ('is_administrador', models.BooleanField(verbose_name='Membro Administrador')),
                ('acao', models.ForeignKey(to='acao.Acao')),
                ('user', models.ForeignKey(verbose_name='Usu\xe1rio', to=settings.AUTH_USER_MODEL)),
                ('user_add', models.ForeignKey(related_name='acao_acaomembro_created_by', editable=False, to=settings.AUTH_USER_MODEL)),
                ('user_upd', models.ForeignKey(related_name='acao_acaomembro_modified_by', editable=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
