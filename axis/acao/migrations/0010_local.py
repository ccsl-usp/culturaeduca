# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        #('municipios', '0001_initial'),
        ('acao', '0009_auto_20161103_1837'),
    ]

    operations = [
        migrations.CreateModel(
            name='Local',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cep', models.CharField(help_text='formato: 12232054', max_length=8)),
                ('logradouro', models.CharField(max_length=100)),
                ('numero', models.CharField(max_length=50, verbose_name='N\xfamero')),
                ('complemento', models.CharField(max_length=100, null=True, blank=True)),
                ('bairro', models.CharField(max_length=100)),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4674, null=True, blank=True)),
                ('local_nome', models.CharField(max_length=120, null=True, verbose_name='Local', blank=True)),
                ('municipio', models.ForeignKey(to='municipios.Municipio')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
