# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0018_auto_20170530_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='atividade',
            name='descricao',
            field=models.TextField(null=True, verbose_name='Descri\xe7\xe3o da Atividade', blank=True),
        ),
    ]
