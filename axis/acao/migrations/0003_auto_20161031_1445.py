# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0002_auto_20161027_1602'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='atividade',
            name='local',
        ),
        migrations.AddField(
            model_name='acaolocal',
            name='local',
            field=models.CharField(max_length=120, null=True, verbose_name='Local', blank=True),
        ),
        migrations.AlterField(
            model_name='projeto',
            name='vinculo',
            field=models.TextField(help_text='citar com qual(is) programa(s) ou a\xe7\xe3o(\xf5es) de governo o projeto est\xe1 vinculado', null=True, verbose_name='V\xednculo do Projeto', blank=True),
        ),
    ]
