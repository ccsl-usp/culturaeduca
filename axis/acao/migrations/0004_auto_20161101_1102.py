# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0003_auto_20161031_1445'),
    ]

    operations = [
        migrations.RenameField(
            model_name='acaolocal',
            old_name='local',
            new_name='local_nome',
        ),
    ]
