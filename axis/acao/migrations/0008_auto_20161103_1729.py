# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0007_auto_20161103_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='atividade',
            name='vinculo_projeto',
            field=models.ForeignKey(related_name='atividades', to='acao.Projeto', null=True),
        ),
    ]
