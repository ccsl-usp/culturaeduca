# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acao', '0019_atividade_descricao'),
    ]

    operations = [
        migrations.AddField(
            model_name='acao',
            name='manifestacao_cultural_outro',
            field=models.CharField(max_length=512, null=True, verbose_name='Outro Tipo de Manifesta\xe7\xe3o Cultural', blank=True),
        ),
        migrations.AddField(
            model_name='acao',
            name='publico_focal_outro',
            field=models.CharField(max_length=512, verbose_name='Outro Tipo de P\xfablico Focal', blank=True),
        ),
        migrations.AddField(
            model_name='acao',
            name='temas_outro',
            field=models.CharField(max_length=512, verbose_name='Outro Temas', blank=True),
        ),
    ]
