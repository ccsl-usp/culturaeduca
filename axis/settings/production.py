# coding: utf-8
import os

from .base import *

DEBUG = False

TEMPLATE_DEBUG = False

SECRET_KEY = os.environ['SECRET_KEY']

ALLOWED_HOSTS = os.environ['ALLOWED_HOSTS']

MEDIA_ROOT = os.environ['MEDIA_ROOT']
STATIC_ROOT = os.environ['STATIC_ROOT']

# email
REQUIRE_VERIFICATION_EMAIL = True
DEFAULT_FROM_EMAIL = os.environ['DEFAULT_FROM_EMAIL']
EMAIL_HOST = os.environ['EMAIL_HOST']
EMAIL_HOST_USER = os.environ['EMAIL_HOST_USER']
EMAIL_HOST_PASSWORD = os.environ['EMAIL_HOST_PASSWORD']
EMAIL_PORT = os.environ['EMAIL_PORT']
EMAIL_USE_TLS = True

# database
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.environ['DB_NAME'],
        'USER': os.environ['DB_USER'],
        'PASSWORD': os.environ['DB_PASSWORD'],
        'HOST': os.environ['DB_HOST'],
        'PORT': os.environ['DB_PORT'],
        'CONN_MAX_AGE': 600,
    },
}

OPT_TILESTACHE_CONF_FILE = '/opt/culturaeduca/tsconfig.json'

# Set your DSN value
RAVEN_CONFIG = {
    'dsn': '',
}

# Add raven to the list of installed apps
INSTALLED_APPS = INSTALLED_APPS + (
    'raven.contrib.django.raven_compat',
)

TILESTACHE_BASE_URL = os.environ['TILESTACHE_BASE_URL']
TILESTACHE_CACHE = {
    "name": "Disk",
    "path": os.environ['TILESTACHE_LOCATION']
}

# IBGE cache location
CACHES['ibge']['LOCATION'] = os.environ['IBGE_CACHE_LOCATION']

# Geojson cache location
CACHES['geojson']['LOCATION'] = os.environ['GEOJSON_CACHE_LOCATION']
