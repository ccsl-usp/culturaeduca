# -*- coding: utf-8 -*-

"""
Django settings for axis project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.core.urlresolvers import reverse_lazy
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'secret-key'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ADMINS = (
    ('nome', 'email'),
)

MANAGERS = ADMINS

DEFAULT_FROM_EMAIL = ''
EMAIL_HOST = ''
EMAIL_HOST_USER = DEFAULT_FROM_EMAIL
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# Application definition

INSTALLED_APPS = (
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.humanize',
    'django_extensions',
    'floppyforms',
    'formtools',
    'front',
    'ibge',
    'mapa',
    'ibge_amostra',
    'ibge_universo',
    'inep',
    'foundation_filter',
    'equipamento',
    'django_comments',
    'django_social_share',
    'mptt',
    'tagging',
    'zinnia',
    'zinnia_wymeditor',
    'blog',
    'busca',
    'municipios',  # aplicacao ZNC Sistemas django-m
    'axis.core',
    'axis.agente',
    'axis.acao',
    'django_select2',
    'django.contrib.postgres',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'axis.core.middleware.RedirecionaParaFormProfileMiddleware'
)

ROOT_URLCONF = 'axis.urls'

WSGI_APPLICATION = 'axis.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'axis',
        'USER': '',
        'CONN_MAX_AGE': 600,
    },
    'ibge_axis': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'ibge_axis',
        'USER': '',
        'CONN_MAX_AGE': 600,
    }

}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
# STATIC_ROOT = BASE_DIR + '/static'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

MEDIA_ROOT = os.path.join(BASE_DIR, 'static/media')
MEDIA_URL = '/media/'

# Templates

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR + '/templates',
            BASE_DIR + '/blog/templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'axis.context_processors.export_vars'
            ],
            'debug': True,
        },
    },
]

# Grappelli settings
GRAPPELLI_ADMIN_TITLE = 'Axis Framework'

# Usuarios

LOGIN_URL = reverse_lazy('login')
# cadu: 20160713 LOGIN_REDIRECT_URL = reverse_lazy('smdhc')
# LOGIN_REDIRECT_URL = reverse_lazy("pessoa_fisica_detalhe")
LOGIN_REDIRECT_URL = "/"

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {module} {process:d} {thread:d} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

SRID = 4674

# django sites settings
SITE_ID = 1

# Custom user class
AUTH_USER_MODEL = 'core.User'
AUTH_PROFILE_MODULE = 'core.Profile'

# zinnia settings
ZINNIA_PING_EXTERNAL_URLS = False
ZINNIA_SAVE_PING_DIRECTORIES = False
ZINNIA_AUTO_CLOSE_PINGBACKS_AFTER = 0
ZINNIA_AUTO_CLOSE_TRACKBACKS_AFTER = 0
ZINNIA_MAIL_COMMENT_AUTHORS = False
ZINNIA_PAGINATION = 5

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'ibge': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/tmp/ibge_cache',
    },
    'geojson': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/tmp/geojson_cache',
    },
    'select2': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': 60 * 60 * 24,
    }
}

# Custom Serializer
SERIALIZATION_MODULES = {
    "custom_geojson": "utils.geojson_serializer",
}

# Set the cache backend to select2
SELECT2_CACHE_BACKEND = 'select2'

# Ordem que irá aparecer na listagem de equipamentos no entorno de
# algum ponto (detalhes de agentes, equipamentos, ações, etc...)
EQUIPAMENTOS_ENTORNO_LIST_ORDER = (
    'cras',
    'centrociencia',
    'biblioteca',
    'museu',
    'cinema',
    'teatro',
    'pontocultura',
    'escola',
    'salaverde',
    'saude',
)

# set this var on your local settings file it will be used
# only on the method clean_tilestache_cache@utils/__init__.py
# if you are looking for a way to change it's real location
# look at fabfile.py and change it on deploy_tileserver task
OPT_TILESTACHE_CONF_FILE = ''

TILESTACHE_BASE_URL = 'http://localhost:8181/'
TILESTACHE_CACHE = {
    "name": "Test",
    "debug": True,
}

REQUIRE_VERIFICATION_EMAIL = True

# *** AINDA NÃO FOI IMPLEMENTADO ***
# Setando ONLY_PUBLIC_SCHOOLS pra True será
# usado ONLY_PUBLIC_SCHOOLS_EXCLUDE_FILTER
# para excluir escolas privadas
# ONLY_PUBLIC_SCHOOLS = True
# ONLY_PUBLIC_SCHOOLS_EXCLUDE_FILTER = {
#     'escolaperfilinep__tp_dependencia': 4
# }
# *** AINDA NÃO FOI IMPLEMENTADO ***
