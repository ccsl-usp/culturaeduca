# -*- coding: utf-8 -*-

# CRIE UM ARQUIVO local.py A PARTIR DESTE ARQUIVO E ALTERE O QUE FOR NECESSÁRIO

"""Development settings and globals."""

import os
from .base import *

SECRET_KEY = os.environ['SECRET_KEY']

DEBUG = True

# email
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# database config
DATABASES['default']['NAME'] = os.environ['DB_NAME']
DATABASES['default']['USER'] = os.environ['DB_USER']
DATABASES['default']['PASSWORD'] = os.environ['DB_PASSWORD']
DATABASES['default']['HOST'] = os.environ['DB_HOST']
DATABASES['default']['PORT'] = os.environ['DB_PORT']

# tilestache
OPT_TILESTACHE_CONF_FILE = os.path.join(BASE_DIR, 'tsconfig.json'),
TILESTACHE_CACHE = {
    "name": "Disk",
    "path": os.environ['TILESTACHE_LOCATION']
}

ALLOWED_HOSTS = [os.environ["ALLOWED_HOSTS"]]

# IBGE cache location
CACHES['ibge']['LOCATION'] = os.environ['IBGE_CACHE_LOCATION']

# Geojson cache location
CACHES['geojson']['LOCATION'] = os.environ['GEOJSON_CACHE_LOCATION']
