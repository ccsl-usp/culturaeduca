from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from django.contrib import admin
from django.views.static import serve
from axis import settings
from axis.core.forms import AuthenticationForm
from axis.core.views import uf_detail, municipio_detail

from zinnia.views.channels import EntryChannel
import front.views
import django.contrib.auth.views
import axis.core.views

urlpatterns = [
    # Examples:
    # url(r'^$', 'axis.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^select2/', include('django_select2.urls')),

    url(r'^$', front.views.index, name='index'),

    url(r'^sobre/', front.views.sobre, name='sobre'),
    url(r'^rede/', front.views.rede, name='rede'),
    url(r'^accounts/login/$', django.contrib.auth.views.login),

    url(r'^entrar/$', django.contrib.auth.views.login, {
        'template_name': 'login.html',
        'authentication_form': AuthenticationForm
    }, name='login'),
    url(r'^sair/$', django.contrib.auth.views.logout, {'next_page': reverse_lazy('login')}, name='logout'),

    url(
        r'^delete/(?P<app_model>(.*))/(?P<object_id>\d+)/$',
        axis.core.views.generic_delete_from_model,
        name="generic_delete"
    ),

    url(r'^uf/(?P<uf_sigla>[A-Z]{2})/$', uf_detail, name='uf_detail'),
    url(r'^uf/([A-Z]{2})/municipio/(.*)-(?P<mun_cod>\d+)/$', municipio_detail, name='municipio_detail'),

    # # Apps
    url(r'^mapa/', include('mapa.urls')),
    url(r'^ibge/', include('ibge.urls')),
    
    url(r'^usuario/', include('axis.core.urls')), 

    url(r'^municipios_app/', include('municipios.urls')),

    url(r'^busca/', include('busca.urls')),
    url(r'^agente/', include('axis.agente.urls')),
    url(r'^acao/', include('axis.acao.urls')),

    url(r'^equipamento/', include('equipamento.urls')),
    url(r'^api/', include('api.urls')),

    # TMP Retirar depois
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT,}),

    # Blog urls
    url(r'^blog/', include('zinnia.urls', namespace='zinnia')),
    url(r'^blog/artigo/', include('blog.urls', namespace='zinnia_artigos')),
    url(r'^comments/', include('django_comments.urls')),
]