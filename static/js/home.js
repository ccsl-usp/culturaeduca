var MOUSE_HOVERING = false;
$(function() {


    $('#block-one').controlsBlock();

    $('#block-one').hover(function(){
        MOUSE_HOVERING = true;
    }, function() {
        MOUSE_HOVERING = false;
    });
    
    INITIAL_BLOCK = Math.floor((Math.random() * 3) + 1) - 1;

    var timer = setInterval(function() {
        if (! MOUSE_HOVERING) {
            actual_block = parseInt($('#block-one .content-inner div.active').data('index'));
            next_block = actual_block + 1;
            if (actual_block == 2) next_block = 0
            $('#block-one').controlsBlock(next_block);
        }
    }, 6500);

    $('#block-one').controlsBlock(INITIAL_BLOCK);

});

$.fn.controlsBlock = function(block_index) {

    var my = this,
        id = '#' + this.attr('id')
        controls = this.children('.controls-block').eq(0),
        read_more = this.children('.read-more').eq(0),
        blocks = this.children('.content-inner').eq(0);

    function change_block(e) {
        e.preventDefault();
        $(this).trigger('change');
        if($(this).hasClass('active'))
            return;
        var index = $(this).data('index');

        $(id + ' .content-inner div').removeClass('active');
        $(id + ' .controls-block li a').removeClass('active');
        $(id +' .content-inner div[data-index="' + index + '"]').addClass('active');
        $(this).addClass('active');
        update_current_block();
        $(this).trigger('changed');
    }

    function update_current_block() {
        var index = parseInt($(id + ' .content-inner div.active').data('index')) + 1;
        var read_more_href = $(id + ' .content-inner div.active').data('read-more-href');
        $(id + ' .read-more').attr('href', read_more_href);
        $(id + ' .current-block').html(index);
    }

    if (typeof block_index != 'undefined') {

        $('.controlsBlock-link[data-index="' + block_index + '"]', controls).trigger('click');

    } else {

        my.children('.close-block').on('click', function(e) {
            e.preventDefault();
            $(this).closest('.content-block').fadeOut();
        });

        blocks.children('div').each(function(index) {
            var block = $(this);
            var li = $('<li/>');
            var a = $('<a/>')
                .addClass('controlsBlock-link')
                .attr({'href': '#', 'data-index': index})
                .on('click', change_block);

            block.attr({'data-index': index});
            if(index === 0) {
                block.addClass('active');
                a.addClass('active');
            }
            li.append(a);
            controls.append(li);
        });

        update_current_block();

        return my;

    }

    
}
