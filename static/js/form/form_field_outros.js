$(document).ready(function () {
    
    $(".outro input").not('[data-dont-require]').each(function() {
        $(this).before('<i class="fa fa-asterisk fdt_required" style="" aria-hidden="true"></i>');
    });
        

    $('.has-other-choice').on('change', function () {
        var relatedField = $(this);
        var other_field = $('#' + relatedField.data('other-choice-field-id'));
        var relatedFieldValue = relatedField.val();

        if ($.isArray(relatedFieldValue)){
            var found = false;
            $.each(relatedFieldValue, function(index, value){
                if (value == relatedField.data('other-choice-id')){
                    if (other_field.data('dont-require') === undefined) other_field.prop('required', true);
                    other_field.parents('div.columns:first').slideDown('fast');
                    found = true;
                    return false;
                }
            })
            if (!found){
                if (other_field.data('dont-require') === undefined) other_field.prop('required', false);
                other_field.parents('div.columns:first').slideUp('fast');
            }
        }else if (relatedFieldValue == relatedField.data('other-choice-id')) {
            if (other_field.data('dont-require') === undefined) other_field.prop('required', true);
            other_field.parents('div.columns:first').slideDown('fast');
        }else{
            if (other_field.data('dont-require') === undefined) other_field.prop('required', false);
            other_field.parents('div.columns:first').slideUp('fast');
        }
    })

    $('.has-other-choice').change();

});