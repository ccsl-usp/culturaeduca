
$(document).ready(function() {

    $("#bloco_publico_alvo").hide();

    $("#publico_alvo_sim").click(function() {
        $("#bloco_publico_alvo").show();
    });

    $("#publico_alvo_nao").click(function() {
        $("#bloco_publico_alvo").hide();
    });

    $("#id_acao").change(function() {
    	var id_acao = $("select#id_acao option:selected").attr("value");

    	$.ajax({
    		type: "GET",
    		url: "/smdhc/ajax_atividade_etapa/" + id_acao + "/",
    		success: function(data) {
    			obj = JSON.parse(data);
    			var data_inicio = obj.data_inicio;
    			var data_fim = obj.data_fim;
    			$("#id_data_inicio").attr('value', data_inicio);
    			$("#id_data_fim").attr('value', data_fim);
    		 }
        });

    });


});
