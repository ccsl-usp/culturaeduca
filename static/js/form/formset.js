var initFormSets = function(options){

    // ----------------------------------------------- FormSet Initial -------------------------------------------------------//
    options.initialForms = {};
    options.totalForms = {};

    for (var index in options.prefixes){

        var formset_class = options.prefixes[index];
        options.initialForms[formset_class] = parseInt($('#id_'+formset_class+'_set-TOTAL_FORMS').val());
        options.totalForms[formset_class] = options.initialForms[formset_class];

        for (var field = 0; field < options.totalForms[formset_class]; field++){
           $('#id_'+ formset_class + '_set-' + field + '-DELETE, label[for="id_' + formset_class + '_set-' + field + '-DELETE"]').hide();
        }
    };

    // ----------------------------------------------- FormSet Functions -----------------------------------------------//
    $('.addForm').on('click', function(){
        var formset = $(this).data('formset'); 
        addForm(formset);
    });
    $('.removeForm').on('click', function(){
        var formset = $(this).data('formset'); 
        removeForm(formset);
    });

    function addForm(form_class){ 
        if (options.totalForms[form_class] < options.initialForms[form_class]){
            var fields = $('.' + form_class + '_row .wrapper').filter(':hidden').first();
            $('[id*="DELETE"]', fields).prop('checked', false);
            fields.show();
        } else {
            var fields = $('.'+form_class+'_row .wrapper').first().clone();
            updateFieldsAttrs(fields, form_class);
            cleanFields(fields);
            $('.'+form_class+'_row').append(fields);         
        }
        options.totalForms[form_class] = options.totalForms[form_class] + 1;
        if (options.totalForms[form_class] > 0){
            $('.removeForm[data-formset=' + form_class + ']').show()
        }
    };

    function cleanFields(fields){
        $('select, input[type="url"], input[type="text"]', fields).val('');
    }

    function updateFieldsAttrs(fields, form_class){
        $('[name^="' + form_class + '_set-"], [for^="id_' + form_class + '_set-"] ', fields).each(function(){
            if ($(this).attr('for') !== undefined){
                $(this).attr('for', updateAttrs($(this), 'for', form_class));
            } else{
                $(this).attr('name', updateAttrs($(this), 'name', form_class));
                $(this).attr('id', updateAttrs($(this), 'id', form_class));
            }
        });
    };

    function updateAttrs(field, attr, form_class){
        var field_attr = $(field).attr(attr).split('-');
        field_attr[1] = options.totalForms[form_class];
        return field_attr.join('-');
    };


    function removeForm(form_class){
        fields = $('.' + form_class + '_row .wrapper').not(':hidden').last();
        if (options.totalForms[form_class] <= options.initialForms[form_class]){
            $('[id*="DELETE"]', fields).prop('checked', true);
            fields.hide();
        } else{
            fields.remove();
        }
        options.totalForms[form_class] = options.totalForms[form_class] - 1;
        if (options.totalForms[form_class] < 1){
            $('.removeForm[data-formset=' + form_class + ']').hide();
        };
     };

    function validateEmptyFields(){
        for (var index in options.prefixes) {
            $('.wrapper input:not(:hidden), .wrapper select').each(function (index) {
                if ($(this).val() == ""){
                    fields = $(this).prop('id').split('-');
                    field_id = fields.slice(0, 2);
                    field_id.push('DELETE');
                    delete_input = field_id.join('-');
                    if ($('#' + delete_input + ':checked')){
                        $('#' + delete_input).prop('checked', true);
                    }
                }
            })
        }
    };


    // ----------------------------------------------- Form Submition -----------------------------------------------//
    $('form').submit(function(event){
        validateEmptyFields();
        for (var index in options.prefixes){
            if (options.totalForms[options.prefixes[index]] < options.initialForms[options.prefixes[index]]){
                options.totalForms[options.prefixes[index]] = options.initialForms[options.prefixes[index]];
            }
            $('#id_'+options.prefixes[index]+'_set-TOTAL_FORMS').val(options.totalForms[options.prefixes[index]]);
        }
    });
}