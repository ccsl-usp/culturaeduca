
$(document).ready(function(){

    var SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
      onKeyPress: function(val, e, field, options) {
          field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };


    $('.date').mask('00/00/0000');
    $('.money').mask('###.###.###.###.##0,00', {reverse: true});
    $('.hour').prop('placeholder', 'HH:MM');
    $('.hour').mask('00:00');
    $('.cep').mask('00.000-000');
    $('.cnpj').mask('00.000.000/0000-00');
    $('.phone').mask(SPMaskBehavior, spOptions);

})