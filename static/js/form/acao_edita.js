
$(document).ready(function() {


    $("#qual_vinculo").hide();
    $("#vinculo_patrocinio").hide();
    $("#op01").hide();
    $("#op02").hide();
    $("#op03").hide();
    $("#op04").hide();
    $("#op05").hide();
    $("#v01").hide();
    $("#v02").hide();
    $("#v03").hide();
    $("#v04").hide();
    $("#v05").hide();
    $("#bloco_publico_alvo").hide();
    $("#bloco_plano_meta").hide();

    $("#possui_vo_sim").click(function() {
        $("#qual_vinculo").show();
        $("#vinculo_patrocinio").show();
    });

    $("#possui_vo_nao").click(function() {
        $("#qual_vinculo").hide();
        $("#vinculo_patrocinio").hide();
        $("#v01").hide();
        $("#v02").hide();
        $("#v03").hide();
        $("#v04").hide();
        $("#v05").hide();

    });

    $("#plano_meta_sim").click(function() {
        $("#bloco_plano_meta").show();
    });

    $("#plano_meta_nao").click(function() {
        $("#bloco_plano_meta").hide();
    });

    $("#publico_alvo_sim").click(function() {
        $("#bloco_publico_alvo").show();
    });

    $("#publico_alvo_nao").click(function() {
        $("#bloco_publico_alvo").hide();
    });



    $("#id_orcamento_patrocinio").click(function(){
        if($("#id_orcamento_patrocinio").is(':checked')) {
            $("#op01").show();
        } else {
            $("#op01").hide();
            $("#op02").hide();
            $("#op03").hide();
        }

    });
    
    $("#id_orcamento_3").click(function() {
        if($('#id_orcamento_3').is(':checked')) {
            $("#v01").show();

        } else {
            $("#v01").hide();
            $("#v02").hide();
            $("#v03").hide();
            $("#v04").hide();
            $("#v05").hide();

        }
    });

    $("#add_v02").click(function() {
        $("#v02").show();
    });

    $("#add_v03").click(function() {
        $("#v03").show();
    });

    $("#add_v04").click(function() {
        $("#v04").show();
    });

    $("#add_v05").click(function() {
        $("#v05").show();
    });

    $("#add_op02").click(function() {
        $("#op02").show();
    });

    $("#add_op03").click(function() {
        $("#op03").show();
    });


    $("#id_orcamento_2").click(function() {
        if($('#id_orcamento_3').is(':checked')) {
            $("#combo_vinculo").hide();
            $("#da").hide();
            $("#fonte").hide();
            $("#valor_orcamento").hide();
            $("#combo_vinculo2").hide();
            $("#da2").hide();
            $("#valor_orcamento2").hide();
            $("#combo_vinculo3").hide();
            $("#da3").hide();
            $("#valor_orcamento3").hide();

        }
    });

    $("#id_meta_responsavel").change(function() {
        $("#desc_meta_responsavel").empty();
        var descricao = $("#id_meta_responsavel").val();
        $.get( "/smdhc/desc_meta_prefeitura/" + descricao, function( data ) {
            $("#desc_meta_responsavel").append( data );
        })
    });

    $("#id_meta_coresponsavel").change(function() {
        $("#desc_meta_coresponsavel").empty();
        var descricao = $("#id_meta_coresponsavel").val();
        $.get( "/smdhc/desc_meta_prefeitura/" + descricao, function( data ) {
            $("#desc_meta_coresponsavel").append( data );
        })
    });

    $("#id_meta_relacionada").change(function() {
        $("#desc_meta_relacionada").empty();
        var descricao = $("#id_meta_relacionada").val();
        $.get( "/smdhc/desc_meta_prefeitura/" + descricao, function( data ) {
            $("#desc_meta_relacionada").append( data );
        })
    });

    var sel_tipo = 0;

    $("#ms-id_tipo_acao .ms-selectable").click(function() {
        sel_tipo = sel_tipo + 1;
        if (sel_tipo >= 3) {
            $('#id_tipo_acao').multiSelect('deselect_all');
            sel_tipo = 0;
            alert('Selecione apenas 2 tipos');
        }
    });

    var eixo_antigo_id = null;
    var eixo_antigo_texto = null;

    $("#id_eixo").change(function() {
        var eixo = $("#id_eixo").val();

        if (eixo_antigo_id != null) {
            $('#id_eixo_secundario').append($('<option>', {
                value: eixo_antigo_id,
                text: eixo_antigo_texto
            }));

        };

        eixo_antigo_id = $( "#id_eixo option:selected" ).val();
        eixo_antigo_texto = $( "#id_eixo option:selected" ).text();
        $("#id_eixo_secundario option[value='" + eixo + "']").remove();

    });

    // Ajax

    $("#cadastrar_parceria_externa").click(function() {
        var form_data = $("#form_parceria_externa").serializeArray();
        $.ajax({
            type: "POST",
            url: "/smdhc/form_parceria_externa/",
            data: form_data,
            success: function(data) {
                obj = JSON.parse(data);
                var cod = obj.cod;
                var nome = obj.nome;
                $('#id_parcerias_externas').multiSelect('addOption', { value: cod, text: nome, index: 0, nested: 'optgroup_label' });
                $('#id_parcerias_externas').multiSelect('select', cod);
            }
        });
        $('#modal-parceria').foundation('reveal', 'close');
        
        return false;
    });


    $("#id_vinculo_orcamentario01").change(function() {
        var id_vinculo = $("select#id_vinculo_orcamentario01 option:selected").attr("value");

        $.ajax({
            type: "GET",
            url: "/smdhc/ajax_da/" + id_vinculo + "/",
            success: function(data) {
                $("#id_da01").html(data);
            }
        });

    });

    $("#id_vinculo_orcamentario02").change(function() {
        var id_vinculo = $("select#id_vinculo_orcamentario02 option:selected").attr("value");

        $.ajax({
            type: "GET",
            url: "/smdhc/ajax_da/" + id_vinculo + "/",
            success: function(data) {
                $("#id_da02").html(data);
            }
        });

    });

    $("#id_vinculo_orcamentario03").change(function() {
        var id_vinculo = $("select#id_vinculo_orcamentario03 option:selected").attr("value");

        $.ajax({
            type: "GET",
            url: "/smdhc/ajax_da/" + id_vinculo + "/",
            success: function(data) {
                $("#id_da03").html(data);
            }
        });

    });

    $("#id_vinculo_orcamentario04").change(function() {
        var id_vinculo = $("select#id_vinculo_orcamentario04 option:selected").attr("value");

        $.ajax({
            type: "GET",
            url: "/smdhc/ajax_da/" + id_vinculo + "/",
            success: function(data) {
                $("#id_da04").html(data);
            }
        });

    });

    $("#id_vinculo_orcamentario05").change(function() {
        var id_vinculo = $("select#id_vinculo_orcamentario05 option:selected").attr("value");

        $.ajax({
            type: "GET",
            url: "/smdhc/ajax_da/" + id_vinculo + "/",
            success: function(data) {
                $("#id_da05").html(data);
            }
        });

    });




});
