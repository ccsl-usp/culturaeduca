
$(document).ready(function() {

    $("#id_acao").change(function() {
        var id_acao = $("select#id_acao option:selected").attr("value");

        $.ajax({
            type: "GET",
            url: "/smdhc/ajax_etapa_acao_atividade/" + id_acao + "/",
            success: function(data) {
                $("#id_atividade").html(data);
            }
        });

    });

    $("#id_pa_codigo").mask("9999-9.999.999-9");
    $("#modalpa_codigo").mask("9999-9.999.999-9");

    $("#cadastrar_pa").click(function() {
        var form_data = $("#form_pa").serializeArray();
        $.ajax({
            type: "POST",
            url: "/smdhc/form_pa/",
            data: form_data,
            success: function(data) {
                obj = JSON.parse(data);
                var cod = obj.cod;
                var nome = obj.nome;
                $('#id_processos_autuados').multiSelect('addOption', { value: cod, text: nome, index: 0, nested: 'optgroup_label' });
                $('#id_processos_autuados').multiSelect('select', cod);
            }
        });
        $('#modal-pa').foundation('reveal', 'close');
        
        return false;
    });



});
