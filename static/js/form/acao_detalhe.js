function plota_mapa(div, longitude, latitude) {

    var iconFeature = new ol.Feature({geometry: new ol.geom.Point(ol.proj.transform([longitude, latitude], 'EPSG:4326', 'EPSG:900913')),});

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(({
        src: '/static/img/map.png'
        }))
    });

    iconFeature.setStyle(iconStyle);

    var vectorSource = new ol.source.Vector({
        features: [iconFeature]
    });

    var vectorLayer = new ol.layer.Vector({
        source: vectorSource
    });

    var map = new ol.Map({
        target: div,
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }),
            vectorLayer
        ],
        view: new ol.View({
            center: ol.proj.transform([longitude, latitude], 'EPSG:4326', 'EPSG:900913'),
            zoom: 16
        })
    });
};