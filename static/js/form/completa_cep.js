$(function(){
    function onAjaxSuccess(data) {
        // alert(data.logradouro);
        var tipo_logradouro = data.logradouro.split(' ')[0];
        $("#id_logradouro").val(data.logradouro);
        //$("#id_bairro").val(data.bairro);
        //$("#id_municipio").val(data.cidade_info.codigo_ibge);

        // $("<p>teste</p>").insertAfter($("#municipio"));
          
        // alert(tipo_logradouro);

        //$("#id_tipo_logradouro option:selected").text(tipo_logradouro);

        if(data.result == 200) {

            $("#result_content")
                .hide("slow", function() {
                    $("#result_content")
                        .empty()
                        .append("<p>" +
                                "<label>Logradouro: </label>" + data.data['logradouro'] + 
                            "<br />" +
                                "<label>Bairro: </label>" + data.data.bairro + 
                            "<br />" +
                            "<label>Cidade: </label>" + data.data.cidade + 
                            "<br />" +
                            "<label>Estado: </label>" + data.data.estado + 
                            "</p>")
                        .show("slow");
                    });
        } else {
            onAjaxError(data);
        } 
    }

    function onAjaxError(data) {
        $("#result_content")
            .empty()
            .append(JSON.stringify(data))
            .show("slow");
    }

    function onSubmitClick() {
        $("#result_content").hide("slow");

        $.getJSON("https://api.postmon.com.br/v1/cep/" + $("#id_cep").val()).
                success(onAjaxSuccess).
                error(onAjaxError);
    };

    $("#result_content").hide();

    $("#pesquisar_endereco").click(function() {
        onSubmitClick();
    });
});