$(function() {
	$('.removerAcaoBtn').on('click', function() {
	    var remove_url = $(this).data('href');
	    $("#remover_atividade_modal .remove-atividade-link").attr('href', remove_url);
	    $("#remover_atividade_modal").foundation('reveal', 'open');
	});
});