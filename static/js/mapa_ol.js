var urls_list = {},
    vectors_list = {},
    container = document.getElementById('popup'),
    content = document.getElementById('popup-content'),
    closer = document.getElementById('popup-closer');

closer.onclick = function() {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
};

var overlay = new ol.Overlay({element: container});

nunjucks.configure({ autoescape: true });

$(function() {
    $('input[data-geojson-url]').each(function() {
        var key = $(this).val(),
            value = $(this).data('geojson-url'),
            popup_order = $(this).data('popup-order'),
            style = $(this).data('style'),
            shapestyle = $(this).data('shapestyle'),
            title = $(this).parents('label').text(),
            color = $(this).data('color'),
            type = $(this).data('type'),
            icon = $(this).data('icon'),
            tiled = $(this).data('tiled');

        urls_list[key] = {'popup_order': popup_order, 'title': title, 'url': value, 'style': style, 'color': color, 'type': type, 'shapestyle': shapestyle, 'tiled': tiled};
    });

    init_map();

    $('.list-checkbox > li > label > input[type="checkbox"]:checked').each(function() {
        var vector = vectors_list[$(this).val()];
        vector.setVisible(true);
    });

    $('.list-checkbox > li > label > input[type="checkbox"]').on('click', function(e) {
        var vector = vectors_list[$(this).val()],
            checked = $(this).is(':checked');

        if(vector) {
            vector.setVisible(checked);
            if(!checked)
                closer.click();
        }
    });
});

function init_map() {
    var projection = ol.proj.get('EPSG:3857');
    var static_url = $('body').data('static-url');

    var fundo = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
        }),
        opacity: 0.5
    });

    for(var key in urls_list) {
        var url = urls_list[key].url,
            is_style = urls_list[key].style,
            popup_order = urls_list[key].popup_order,
            is_shapestyle = urls_list[key].shapestyle,
            is_tiled = urls_list[key].tiled,
            color = urls_list[key].color,
            type = urls_list[key].type,
            title = urls_list[key].title,
            style = undefined,
            vector_source = undefined;

        if(is_style) {
            if(is_tiled) {
                function stl(cl, tp){
                    return function(feature) {
                        var num_points = feature.get("count");
                        var radius = 5 + (num_points.toString().length * 2);
                        var st;
                        if (num_points == 1) {
                            // pin differentiation, based on type (equipamento/agente/acao)
                            if (tp == "agente") {
                                st = new ol.style.Style({
                                    image: new ol.style.RegularShape({
                                        fill: new ol.style.Fill({color: cl}),
                                        points: 3,
                                        radius: 5,
                                        angle: 3*Math.PI
                                    }),
                                    zIndex: 2
                                });
                            } else if(tp == "acao") {
                                st = new ol.style.Style({
                                    image: new ol.style.RegularShape({
                                        fill: new ol.style.Fill({color: cl}),
                                        points: 10,
                                        radius: 5,
                                        angle: Math.PI / 4
                                    }),
                                    zIndex: 2
                                });
                            } else {
                                st = new ol.style.Style({
                                    image: new ol.style.RegularShape({
                                        fill: new ol.style.Fill({color: cl}),
                                        points: 4,
                                        radius: 5,
                                        angle: Math.PI / 4
                                    }),
                                    zIndex: 2
                                });
                            }
                        } else {
                            st = new ol.style.Style({
                                image: new ol.style.Circle({
                                    fill: new ol.style.Fill({
                                        color: convertHex(cl, 0.7)
                                    }),
                                    stroke: new ol.style.Stroke({
                                        width: 1,
                                        color: convertHex(cl, 1.0)
                                    }),
                                    radius: radius
                                }),
                                text: new ol.style.Text({
                                    fill: new ol.style.Fill({color:'#FFFFFF'}),
                                    fontFamily: 'Calibri,sans-serif',
                                    fontSize: 12,
                                    text: num_points.toString(),
                                    labelYOffset: -12
                                })
                            });
                        }
                        return st;
                    };
                };
                style = stl(color, type);

            } else {
                style = [
                    new ol.style.Style({

                        // teste icone

                        //image: new ol.style.Icon({
                        //    anchor: [0.5, 0.5],
                        //    size: [32, 37],
                        //    offset: [52, 0],
                        //    opacity: 1,
                        //    scale: 0.5,
                        //    src: '../img/icons/theater.png'
                        //}),

                        image: new ol.style.RegularShape({
                            fill: new ol.style.Fill({color: color}),
                            points: 4,
                            radius: 5,
                            angle: Math.PI / 4
                        }),
                        zIndex: 2
                    })
                ];
            }
        } else if(is_shapestyle) {
            style = [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: color,
                    width: 1.5,
                }),
                // fill: new ol.style.Fill({
                //     color: color,
                //     opacity : 0.2,
                // })
            })
        ];
        } else {
            style = [
                new ol.style.Style({

                    // image: new ol.style.Circle({
                    //     fill: new ol.style.Fill({color: color}),
                    //     points: 20,
                    //     radius: 5,
                    //     angle: Math.PI / 4
                    // }),
                    image: new ol.style.Circle({
                        fill: new ol.style.Fill({
                            color: color
                        }),
                    stroke: new ol.style.Stroke({
                            width: 1,
                            color: 'rgba(105, 105, 105, 0.3)'
                        }),
                    radius: 4
                    }),
                    zIndex: 2
                })
            ];
        }

        if (is_tiled) {
            vector_source = new ol.source.VectorTile({
                url: url,
                format: new ol.format.MVT(),
                overlaps: is_style,
                tileGrid: ol.tilegrid.createXYZ({maxZoom: 22}),
                tilePixelRatio: 16
            });
            vectors_list[key] = new ol.layer.VectorTile({
                name: key,
                source: vector_source,
                style: style
            });

        } else {
            vector_source = new ol.source.Vector({
                url: url,
                projection: 'EPSG:3857',
                format: new ol.format.GeoJSON(),
            });
            vectors_list[key] = new ol.layer.Vector({
                name: key,
                source: vector_source,
                style: style
            });
        }

        vectors_list[key].set('_order', popup_order);
        vectors_list[key].set('_title', title);

        
    }

    var displayInfo = function(e) {
        var coordinate = e.coordinate;
        var features = [];

        map.forEachFeatureAtPixel(e.pixel, function(feature, layer) {
            features.push({
                'feature_properties': feature.getProperties(),
                'layer': layer,
            });
        });

        features.sort(function(featureA, featureB) {
            if (featureA.layer.get('name') == featureB.layer.get('name')) {
                if (featureA.feature_properties['name'] > featureB.feature_properties['name']) {
                    return -1;
                } else {
                    return 1;
                }
            } else {
                if (featureA.layer.get('_order') < featureB.layer.get('_order')) {
                    return -1;
                } else {
                    return 1;
                }
            }
            return 0;

        });

        features = features.filter(function(element) {
            return 'count' in element.feature_properties == false || element.feature_properties['count'] == 1;
        });

        var header_title = '';
        var new_title;
        var innerHTML = '';
        var template_name = 'default';

        if(features.length >= 1) {

            var vertical_menu = $('<ul>');
            vertical_menu.attr('id', 'popup-menu');
            vertical_menu.addClass('vertical menu');
            vertical_menu.data('accordion-menu', '');

            $.each(features, function(i, feature) {
                new_title = header_title != $.trim(feature.layer.get('_title'));
                if (new_title) {
                    if (i != 0) {
                        innerHTML += "</ul></li>";
                    }
                    header_title = $.trim(feature.layer.get('_title'));
                    innerHTML += '<li class="is-accordion-submenu-parent" aria-expanded="false"><a href="#" style="display: block;" onclick="openCloseSubmenu(this, event);"><h6>' + header_title + '</h6></a>'
                    is_active = '';
                    style = ' style="display: none;"';
                    if (features.length == 1) {
                        is_active = ' is-active';
                        style = '';
                    }
                    innerHTML += '<ul class="menu vertical nested'+ is_active +'"'+ style +'>';
                }
                template_name = 'default'
                if (feature.layer.get('name') in popup_layer_templates) {
                    template_name = feature.layer.get('name');
                }
                innerHTML += "<li>" + nunjucks.renderString(popup_layer_templates[template_name], feature.feature_properties) + "</li>";
            });
            
            content.innerHTML = vertical_menu.html(innerHTML).prop('outerHTML');
            /*var elem = new Foundation.AccordionMenu($('ul#popup-menu'));*/
            overlay.setPosition(coordinate);

        } else {
            closer.onclick();
        }
    }

    var map = new ol.Map({
        layers: [fundo],
        overlays: [overlay],
        target: 'map',
        renderer: 'canvas',
        // interactions: new ol.interaction.defaults({mouseWheelZoom:false}),
        view: new ol.View({
            center: ol.proj.transform(
                [-51.4131787991182, -14.2401200966752],
                'EPSG:4326',
                'EPSG:3857'
            ),
            zoom: 4,
            minZoom: 4,
            maxZoom: 18,
        })
    });

    // var mouseWheelInt = new ol.interaction.MouseWheelZoom();
    // map.addInteraction(mouseWheelInt);
    // map.on('wheel', function (event) {
    //     mouseWheelInt.setActive(ol.events.condition.shiftKeyOnly(event));
    // });

    var vector;
    for(var key in vectors_list) {
        vector = vectors_list[key];
        map.addLayer(vector);
        vector.setVisible(false);
    }

    map.on('click', function(e) {
        displayInfo(e);
    });

    // $(".caption-closer").on("click", function (event) {
    //     event.preventDefault();
    //     $(".shift-message").css("display", "none");
    // });
}

function select_multiple(id1, id2) {
    document.getElementById(id1).click();
    document.getElementById(id2).click();
}