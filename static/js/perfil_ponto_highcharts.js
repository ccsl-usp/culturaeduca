// Rendimento pessoas
$(function () {
    $('#hc-renda-pessoas').highcharts({
        chart: {
            type: 'column',
            spacingLeft: 25,
        },
        credits: {
            text: 'Fonte: Censo 2010 - IBGE / culturaeduca.cc',
            href: 'http://censo2010.ibge.gov.br/',
        },
        title: {
            text: 'Classes de rendimento nominal mensal',
            align: 'left',
            x: 0,
            style: {
                color: '#666',
                fontSize: '14px'
            }
        },
        subtitle: {
            text: 'População residente de 10 anos ou mais - Sexo',
            align: 'left',
            x: 0,
            style: {
                color: '#999',
            }
        },
        legend: {
            backgroundColor: '#f2f2f2',
            borderColor: null,
            align: 'right',
            verticalAlign: 'top',
            x:2,
            y: 40,
            floating: true,
            itemStyle: {
                color: '#9e9e9e',
                fontSize: '11px',
            }
        },
        xAxis: {
            categories: [
                'Sem rendimento',
                'Até 1/2 SM',
                'De 1/2 a 1 SM',
                'De 1 a 2 SM',
                'De 2 a 3 SM',
                'De 3 a 5 SM',
                'De 5 a 10 SM',
                'De 10 a 15 SM',
                'De 15 a 20 SM',
                'Mais de 20 SM'
            ],
            labels: {
                rotation: -65,
                align: 'right',
                style: {
                    fontSize: '10px',
                }
            },
        },
        yAxis: {
            title: {
                text: null
            },
            labels: {
                enabled: false
            },
            tickPixelInterval: 50
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.1,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Homens',
            data: renda_p_h,
            color: '#99cccc'

        }, {
            name: 'Mulheres',
            data: renda_p_m,
            color: '#ffcc33'

        }],

        exporting: {
            filename: 'rendimento-mensal',
        }

    });
});

// Rendimento domiciliar

$(function () {
    $('#hc-renda-domicilios').highcharts({
        chart: {
            type: 'bar',
        },
        credits: {
            text: 'Fonte: Censo 2010 - IBGE / culturaeduca.cc',
            href: 'http://censo2010.ibge.gov.br/',
        },
        title: {
            text: 'Classes de rendimento nominal mensal domiciliar',
            align: 'left',
            x: 0,
            style: {
                color: '#666',
                fontSize: '14px'
            }
        },
        subtitle: {
            text: 'Domicílios particulares permanentes',
            align: 'left',
            x: 0,
            style: {
                color: '#999',
            }
        },
        xAxis: {
            categories: [
                'Sem rendimento',
                'Até 1/2 SM',
                'De 1/2 a 1 SM',
                'De 1 a 2 SM',
                'De 2 a 3 SM',
                'De 3 a 5 SM',
                'De 5 a 10 SM',
                'De 10 a 15 SM'
            ],
            title: {
                text: null
            },
            labels: {
                align: 'right',
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            },
            labels: {
                enabled: false
            },
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Domicílios',
            data: renda_domicilios,
            color: '#669966'
        }],
        exporting: {
            filename: 'rendimento-domiciliar',
        }
    });
});

// Piramide Etária
$(function () {
    var chart,
        categories = ['0-4', '5-9', '10-14', '15-19',
            '20-24', '25-29', '30-34', '35-39', '40-44',
            '45-49', '50-54', '55-59', '60-64', '65-69',
            '70-74', '75-79', '80-84', '85-89', '90-94',
            '95-99', '100 +'];
    $(document).ready(function() {
        $('#hc-piramide-etaria').highcharts({
            chart: {
                type: 'bar',
            },
            credits: {
                text: 'Fonte: Censo 2010 - IBGE / culturaeduca.cc',
                href: 'http://censo2010.ibge.gov.br/',
            },
            title: {
                text: 'Piramide Etária - População residente',
                align: 'left',
                x: 0,
                style: {
                    color: '#666',
                    fontSize: '14px'
                }
            },
            legend: {
                borderColor: null,
                x:30,
                itemDistance: 130,
                itemStyle: {
                    color: '#9e9e9e',
                    fontSize: '11px',
                }
            },
            xAxis: [{
                categories: categories,
                reversed: false
            },
            //{ // mirror axis on right side
            //    opposite: true,
            //    reversed: false,
            //    categories: categories,
            //    linkedTo: 0
            //}
            ],
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function(){
                        return (Math.abs(this.value) );
                    }
                },
            },

            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },

            tooltip: {
                formatter: function(){
                    return '<b>'+ this.series.name +', '+ this.point.category +' anos </b><br/>'+
                        'População: '+ Highcharts.numberFormat(Math.abs(this.point.y), 0);
                }
            },

            series: [{
                name: 'Homens',
                data: piramide_etaria_h,
                color: '#99cccc'

            }, {
                name: 'Mulheres',
                data: piramide_etaria_m,
                color: '#ffcc33'

            }],

            exporting: {
                filename: 'piramide-etaria',
            }

        });

    });

});

// População Residente - Raça/Cor
$(function () {
    var chart;

    $(document).ready(function () {

        // Build the chart
        $('#hc-populacao-cor').highcharts({
            chart: {
                marginLeft: 70
            },
            credits: {
                text: 'Fonte: Censo 2010 - IBGE / culturaeduca.cc',
                href: 'http://censo2010.ibge.gov.br/',
            },
            title: {
                text: 'Raça/Cor - População Residente',
                align: 'left',
                x: 0,
                style: {
                    color: '#666',
                    fontSize: '14px'
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            legend: {
                backgroundColor: '#f2f2f2',
                borderColor: null,
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'middle',
                floating: true,
                itemStyle: {
                    color: '#9e9e9e',
                    fontSize: '11px',
                }
            },

            series: [{
                type: 'pie',
                name: '',
                data: [
                    ['Branca',   raca_cor['branca']],
                    ['Preta',    raca_cor['preta']],
                    ['Amarela',  raca_cor['amarela']],
                    ['Parda',    raca_cor['parda']],
                    ['Indígena', raca_cor['indigena']]
                ],
                colors: [
                    '#99cccc',
                    '#8f85ba',
                    '#edc13e',
                    '#7f7a66',
                    '#54ae66'
                ],
            }],

            exporting: {
                filename: 'raca-cor',

            }

        });
    });

});
$(function () {
    $('#tabs').on('toggled', function (event, panel) {
        if ($(panel).is("#panel-perfil:not(.already-loaded)")) {
            $(panel).addClass("already-loaded");
            $('#hc-renda-pessoas').highcharts().reflow();
            $('#hc-renda-domicilios').highcharts().reflow();
            $('#hc-piramide-etaria').highcharts().reflow();
            $('#hc-populacao-cor').highcharts().reflow();
        }
    });
});