
$(document).ready(function(){

    $('#id_publication_date_0').prop('placeholder', 'DD/MM/AAAA');
    $('#id_publication_date_0').mask('00/00/0000');
    $('#id_publication_date_1').prop('placeholder', 'HH:MM');
    $('#id_publication_date_1').mask('00:00');

    $('#id_start_publication_0').prop('placeholder', 'DD/MM/AAAA');
    $('#id_start_publication_0').mask('00/00/0000');
    $('#id_start_publication_1').prop('placeholder', 'HH:MM');
    $('#id_start_publication_1').mask('00:00');

    $('#id_end_publication_0').prop('placeholder', 'DD/MM/AAAA');
    $('#id_end_publication_0').mask('00/00/0000');
    $('#id_end_publication_1').prop('placeholder', 'HH:MM');
    $('#id_end_publication_1').mask('00:00');

})

