$(document).ready(function() {

    $(".agente_vinculado").change(function() {
        $("#btnVincular:not(.already_blinked)")
            .fadeTo('fast', 0.25)
            .fadeTo('fast', 1)
            .fadeTo('fast', 0.25)
            .fadeTo('fast', 1);
        $("#btnVincular").addClass('already_blinked');
    });

    $("#btnAddMembro").click(function() {
        var pk = $('.agente').val();
        if(pk) {
            var url = $(this).data('url').replace('/0/', '/' + pk + '/');
            window.location.href = url;
        }
    });

    $("#btnVincular").click(function() {
        var pk = $('.agente_vinculado').val();
        if(pk) {
            var url = $(this).data('url').replace('/0/', '/' + pk + '/');
            window.location.href = url;
        }
    });

    $("#btnAcaoVincular").click(function() {
        var pk = $('.acao-vinculo').val();
        if(pk) {
            var url = $(this).data('url').replace('/0/', '/' + pk + '/');
            window.location.href = url;
        }
    });

});
