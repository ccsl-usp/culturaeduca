$(function() {
    $('.filter').on('click', function(e) {
        e.preventDefault();
        var my = $(this);
        var element = my.parent().children('.content-label-map');
        if(!element.is(':visible')) {
            $('.content-label-map').hide();
            $('.filter').removeClass('active');
            my.addClass('active');
            element.fadeIn();
        } else {
            element.fadeOut();
            my.removeClass('active');
        }
    });

    $('.ul-filters > li > label').on('click', function(e) {
        var li = $(this).parent(),
            ul = $(this).next();

        if(!li.hasClass('active')) {
            li.addClass('active');
            if(ul)
                ul.show();
        } else {
            li.removeClass('active');
            if(ul)
                ul.hide();
        }
    });

    $('.clear-filter').on('click', function(e) {
        e.preventDefault();
        var inputs = $('.list-checkbox').children('li')
                                        .children('label')
                                        .children('input');

        inputs.each(function() {
            if($(this).is(':checked'))
                $(this).click();
        });
    });
});
