// var get_endereco_rua = function() {
//   endereco = ''
//   if($('#id_logradouro').val()!="") {
//     endereco += $('#id_logradouro').val() + ', ';
//   }
//   return endereco + ', Brasil'

// };

// var get_endereco_concatenado_ = function() {
//   var query = {}
//   endereco = ''
//   if($('#id_logradouro').val()!="") {
//     endereco += $('#id_numero').val() + ' ' + $('#id_logradouro').val() + ', ';
//   }

//   if($('#id_bairro').val()!="") {
//     endereco += $('#id_bairro').val();
//     if($('#id_municipio option:selected').text()!="") {
//       endereco += ', ' + $('#id_municipio option:selected').text() + ', ' + $('#municipio_uf option:selected').text();
//     }
//   } else if($('#id_municipio option:selected').text()!="") {
//     endereco += $('#id_municipio option:selected').text() + ', ' + $('#municipio_uf option:selected').text();
//   }
  
// //   endereco += $('#id_municipio option:selected').text() + ' ' + $('#municipio_uf option:selected').text() + ', Brasil';
//   return query.endereco = endereco + ', Brasil'
// };

var checkUfMunicipio = function () {
  var municipio = $('#id_municipio option:selected').text();
  var uf = $('#municipio_uf option:selected').text();
  var response = false;
  
  if (
    municipio !== '' &&
    municipio !== '- Selecione -' &&
    municipio !== undefined &&
    municipio !== "--" &&
    uf !== '' &&
    uf !== undefined &&
    uf !== "--"
    ) response = true;

  return response;
}

var get_endereco_json = function () {
  // Monta json com endereço para o geocoding buscar no nominatim!
  var endereco_json = {};
  var $municipio = $('#id_municipio option:selected').text();
  var $uf = $('#municipio_uf option:selected').text();
  endereco_json.postalcode = $('#id_cep').val() || "";
  endereco_json.house_number = $('#id_numero').val() || "";
  endereco_json.street = $('#id_logradouro').val() || "";
  // Tem Bairro que é válido para os endereços dos correios, mas não existe no openstreetmap
  endereco_json.district = $('#id_bairro').val() || "";
  endereco_json.city = $municipio === "--" ? "" : $municipio;
  endereco_json.state = $uf === "--" ? "" : $uf;
  endereco_json.country = "Brasil";

  return endereco_json;
}

// var get_municipio_concatenado = function(com_bairro) {
//   com_bairro = com_bairro == undefined ? true : com_bairro;

//   endereco = ''
//   if(com_bairro && $('#id_bairro').val()!="") {
//     endereco += $('#id_bairro').val();
//     if($('#id_municipio option:selected').text()!="") {
//       endereco += ', ';
//     }
//   }

//   if($('#id_municipio option:selected').text()!="") {
//     endereco += $('#id_municipio option:selected').text() + ', ' + $('#municipio_uf option:selected').text();
//   }

// //   endereco += $('#id_municipio option:selected').text() + ' ' + $('#municipio_uf option:selected').text() + ', Brasil';
//   return endereco + ', Brasil'
// };

function confirma_endereco_by_id(id) {
  // dentro da 'tabela_opcoes_enderecos'
  // passará o endereco correto, pelo id E
  // troca o geoEvent
  var address = lista_dict_enderecos[id];
  znc_ola_functions.setMapPin(address, address.coordinates);
  geocoding_reverse_fields(address);
  close_confirm_modal();
}

var geocoding_reverse_fields = function(address) {
  if (jQuery.isEmptyObject(address)) return;
  $('#id_logradouro').val(address.street);
  // #117 git lab Necto: alterou a funcao,  CEP
  if (address.postcode) $('#id_cep').val(address.postcode);
  $('#id_bairro').val(address.suburb);
  $('#id_municipio').data("municipio-selected", address.city_cod);
  $("#municipio_uf").val(address.state_cod); // seta valor no input de UF
  $("#municipio_uf").trigger('onchange'); // dispara evento para carga do

};
// ====================//=======================//


var geo_coding_confirm = function(address) {
  //debugger;
  // address vem de map.js!!!!!!
  if (address.length >= 1) {
    // $('#idConfirmStreet').html(address[0].street || '---');
    // $('#idConfirmPostcode').html(address[0].postcode || '---');
    // $('#idConfirmSuburb').html(address[0].suburb || '---');
    // $('#idConfirmCity').html(address[0].city || '---');
    $('#geoModalConfirm').foundation('reveal', 'open');
    construtor_tabela_enderecos(address)
  }
}

// ====================//=======================//

var construtor_tabela_enderecos = function(address){
  // #117 git necto
  // 'address' = lista_dict_enderecos que vem de maps.js
  // Constroi uma tabela de endereços usada em 'endereco_fildset.html'
  // inserida por meio da '<div id="tabela_opcoes_enderecos"></div>'
  var tabela_opcoes_enderecos = document.createElement('table');
  tabela_opcoes_enderecos.style = 'width:700px; border:1px solid #CCC;';
  var tbody = document.createElement('tbody');

  for(let i = 0; i < address.length; i++){
      let tr = document.createElement('tr');

      // 1
      let td = document.createElement('td');
      td.style = 'width:100px;border:1px solid #CCC; text-align:center;'
      let span = document.createElement('span');

      span.innerHTML = i + 1
      td.appendChild(span);
      tr.appendChild(td);
      tbody.appendChild(tr);

      //1.5
      td = document.createElement('td');
      td.style = 'border:1px solid #CCC;';
      span = document.createElement('span');

      var confirm_btn = '<button onclick=confirma_endereco_by_id(' + i + ') class="secondary button success">Selecionar</button>'
      span.innerHTML = confirm_btn;
      td.appendChild(span);
      tr.appendChild(td);

      // 2
      td = document.createElement('td');
      td.style = 'border:1px solid #CCC;';
      span = document.createElement('span');
      span.style = 'display: flex; flex-wrap: wrap';
      span.innerHTML = '';
      if (address[i].street){
          span.innerHTML += '<span>' + '<b>' + address[i].street + '</b>' + ',&nbsp;' +'</span>'
        }
      if (address[i].postcode){
         span.innerHTML += '<span>' + '<b>CEP:</b> ' + address[i].postcode + ',&nbsp;' + '</span>'
       }
      if (address[i].suburb){
         span.innerHTML += '<span>' + '<b>Bairro:</b> ' + address[i].suburb + ',&nbsp;' + '</span>'
       }
      if (address[i].city){
         span.innerHTML += '<span>' + '<b>Cidade:</b> ' + address[i].city + ',&nbsp;' + '</span>'
       }
      if (address[i].state){
         span.innerHTML += '<span>' + '<b>UF:</b> ' + address[i].state + '.'  + '</span>' //'texto '+(i+1);
       }
      td.appendChild(span);
      tr.appendChild(td);
      tbody.appendChild(tr);
  }
  tabela_opcoes_enderecos.appendChild(tbody);

  $("#tabela_opcoes_enderecos").html(tabela_opcoes_enderecos);
};
//=====================//===================//


var close_confirm_modal = function() {
  // Modal de Endereços
  $('#geoModalConfirm').foundation('reveal', 'close');
};

function cepSuccess(data) {
  if (data.endereco){
    var endereco = data.endereco.split(",");
    var logradouro = endereco[0];
    var numero = endereco[1];
  } else{
    var logradouro = data.logradouro;
    var numero = null;
  } 
  $("#id_logradouro").val(logradouro);
  $("#id_bairro").val(data.bairro);
  $("#id_numero").val(numero);
  $('#id_municipio').data("municipio-selected", data.cidade_info.codigo_ibge); // add data attribute
  $("#municipio_uf").val(data.estado_info.codigo_ibge); // seta valor no input de UF
  $("#municipio_uf").trigger('onchange'); // dispara evento para carga do
}

function cepError(data) {
  alert("endereço nao encontrado");
}

function getCep() {
  // retorna JSON de CEP via ajax
  var jqxhrResponse = $.ajax({
    url: "https://api.postmon.com.br/v1/cep/" + $("#id_cep").val(),
    statusCode: {
      404: cepError,
      200: cepSuccess
    }
  });
};

var callGeocoding = function () {
  /*
  Chama a função geocoding, que busca o endereço colocado no formulário
  no serviço do nominatim, caso não tenha sido informado uf e município,
  levanta um alerta para o user e cancela a operação.
   */
  var hasUfMunicipio = checkUfMunicipio();
  var result = undefined;
  var address = get_endereco_json();

  if (hasUfMunicipio) {
    Geocoding(address)
  } else {
    window.alert("Por favor, preencha o UF e município");
  }
  return result;
}


$(function() {
  initZncMap({
    elementReverseGeocodingCallback: geo_coding_confirm,
    elementGeocodingCallback: geo_coding_confirm,
    elementPointWKT: $("#id_point"),
    initialPointWKT: initialPointWKT,
    center: [-5192415.104554701, -2699687.1651398255],
    canSetPoint: true,
    zoom: 15,
  });

  $('.btn_geo_coding').on('click', function (){
    callGeocoding();
  });

  $('.setMapCenterOnCity').on('click', function(event) {
    SetMapCenterOn(get_endereco_json());
    var layer = znc_ola_map.getLayer('znc_ola_novos_pontos');
    layer.getSource().clear();  // Limpa o source da layer de novos pontos
  });

  //   // tenta com endereço completo
  //   result = Geocoding(get_endereco_concatenado(), geo_coding_confirm, function() {
  //     // tenta com municipio
  //     result = Geocoding(get_municipio_concatenado(), geo_coding_confirm, function() {
  //       result = Geocoding(get_municipio_concatenado(false), geo_coding_confirm, undefined, true);
  //     }, false);
  //   }, true);
  // });

  $('#btnConfirmGeo').on('click', function(event) {
    event.preventDefault();
    //geocoding_reverse_fields(geoEvent);
    close_confirm_modal();
  });
  $('#btnNotConfirmGeo').on('click', function(event) {
    event.preventDefault();
    $('#geoModalConfirm').foundation('reveal', 'close');
    close_confirm_modal();
  });
  $('#id_municipio').on('endLoadMunicipios', function(event) {
    $('#id_municipio').val($('#id_municipio').data("municipio-selected"));
    $('#id_municipio').trigger('change');
  });
  $('#id_cep').change(function(){
    if ($(this).val().length == 8) getCep();
    return false;
  });

  var pointValidationMessages = {
    ok: "<strong>Ponto válido!</strong> Clique no mapa para alterar o local ou digite o endereço no formulário acima e clique em <strong>Mostrar no Mapa</strong>.",
    not_ok: "Digite o endereço completo no formulário acima e clique em <strong>Mostrar no Mapa</strong> ou clique diretamente no mapa para marcar o ponto."
  }

  function getPointValidationTitle() {
    if ($('#id_point').val()!="")
      return pointValidationMessages['ok']
    return pointValidationMessages['not_ok']
  }

  $('#id_point').change(function() {
      if ($(this).val() == "") {
        $(".pointFieldValidator").removeClass('ok');
        $(".pointFieldValidator").addClass('not_ok');
      } else {
        $(".pointFieldValidator").removeClass('not_ok');
        $(".pointFieldValidator").addClass('ok');
      }

      Foundation.libs.tooltip.showTip( $('.pointFieldValidator') );

      $(".pointFieldValidator").each(function() {
        var selector = '#' + $(this).data('selector');
        $(selector).html(
          getPointValidationTitle()
        ).append($('<span />').addClass('nub'));
      });
  });

  $(".pointFieldValidator").each(function() {
    var selector = '#' + $(this).data('selector');
    $(selector).html(
      getPointValidationTitle()
    ).append($('<span />').addClass('nub'));
  });


$(function() {
  // This function gets cookie with a given name
  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');

  /*
  The functions below will create a header with csrftoken
  */

  function csrfSafeMethod(method) {
      // these HTTP methods do not require CSRF protection
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  function sameOrigin(url) {
      // test that a given url is a same-origin URL
      // url could be relative or scheme relative or absolute
      var host = document.location.host; // host + port
      var protocol = document.location.protocol;
      var sr_origin = '//' + host;
      var origin = protocol + sr_origin;
      // Allow absolute or scheme relative URLs to same origin
      return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
          (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
          // or any other URL that isn't scheme relative or absolute i.e relative.
          !(/^(\/\/|http:|https:).*/.test(url));
  }

  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
              // Send the token to same-origin, relative URLs only.
              // Send the token only if the method warrants CSRF protection
              // Using the CSRFToken value acquired earlier
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
      }
  });

});
  var id_local_nome_val = '';
  $('#id_local_nome').keyup(function (e) {
    if ($(this).val() == id_local_nome_val) {
      return false;
    }

    id_local_nome_val = $(this).val();

    if ($(this).val().length >= 3 || $('#id_cep').val() != '' || $('#id_logradouro').val() != '' && $('#id_numero').val() != ''){
      localSearch();
    } else {
      if ($('#drop_local_nome_list').html() == '') {
        $('#drop_local_nome_list').html('<li class="empty">Nenhuma sugestão disponível.</li>');
      }
    }
  });

  $('#id_logradouro, #id_cep, #id_numero').change(function () {
    localSearch();
  });

  $('#close_drop_local_nome').click(function() {
    $('#drop_local_nome').slideUp('fast');
  });

  var locais = [];
  
  $(document).on('click', '.sugestaoLocalLink', function() {
    var pk = $(this).data('local-pk');
    if ($('input[name=local_pk]').length >= 1) {
      $('input[name=local_pk]').val(pk);
    } else {
      $('#endereco_fieldset_form').append(
        $('<input>').attr({'type': 'hidden', 'name': 'local_pk'}).val(pk)
      );
    }

    $.each(['cep', 'local_nome', 'logradouro', 'numero', 'bairro', 'complemento'], function(i, key) {
      $("#id_" + key).val( locais[pk][key] );
    });

    $('#id_municipio').data("municipio-selected", locais[pk].municipio_id); // add data attribute
    $("#municipio_uf").val(locais[pk].municipio_uf_id); // seta valor no input de UF
    $("#municipio_uf").trigger('onchange'); // dispara evento para carga do
    $('#id_municipio').on('change', function() {
      $("#endereco_fieldset_form input, #endereco_fieldset_form select").prop('readonly', true);
    });

    var ponto_geom = (new ol.format.WKT()).readGeometry(locais[pk].point);
    var transformed_coords = ol.proj.transform(
      ponto_geom.getCoordinates(),
      'EPSG:4326',
      'EPSG:3857'
    );

    znc_ola_functions.onClick({
      coordinate: transformed_coords
    }, false);
    znc_ola_map.getView().setCenter(transformed_coords);

    znc_ola_opt.canSetPoint = false;

  });

  var localSearch = function () {

    if ($('#id_local_nome').data('search-url') == undefined) {
      return;
    }

    $('#drop_local_nome_list').html('');

    data = {
      'local_nome': $('#id_local_nome').val(),
      'cep': $('#id_cep').val(),
      'logradouro': $('#id_logradouro').val(),
      'numero': $('#id_numero').val(),
    };

    try {
      if (acao_pk != undefined) {
        data['acao'] = acao_pk;
      }
    }
    catch(err) {
        
    }

    $.ajax({
        url: $('#id_local_nome').data('search-url'),
        data: data,
        success: function (response) {
          locais = [];
          if (response.length >= 1) {
            $.each(response, function (key, obj) {
              locais[obj.id] = obj.fields;
              $('#drop_local_nome_list').append(
                $('<li>').html(
                  $('<a>').addClass('sugestaoLocalLink').attr('href', '#').data('local-pk', obj.id).html(
                    '<i class="fa fa-map-marker" aria-hidden="true"></i> ' + obj.fields.local_nome + ' <small style="font-size: 80%;">' + obj.fields.logradouro + ', ' + obj.fields.numero + ' - ' + obj.fields.municipio_nome + ' - ' + obj.fields.municipio_uf_sigla + '</small>'
                  )
                )
              );
            });
            $('#drop_local_nome').slideDown('fast');
          } else {
            $('#drop_local_nome_list').html('<li class="empty">Nenhuma sugestão disponível.</li>');
          }

        }
      })
  }
});
