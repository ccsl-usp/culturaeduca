var blured_map = false, latitude, longitude, url_link, id_ponto, _title, _order = 0, _layer_name = false;

if (document.getElementById("blured_map") != undefined) {
    blured_map = (document.getElementById("blured_map").getAttribute('data-value') == 'true');
}

if (document.getElementById("latitude") != undefined) {
    latitude = parseFloat(document.getElementById("latitude").innerHTML);
}

if (document.getElementById("longitude")) {
    longitude = parseFloat(document.getElementById("longitude").innerHTML);
}

if (document.getElementById("url_link")) {
    url_link = document.getElementById("url_link").innerHTML;
}

if (document.getElementById("id_ponto")) {
    id_ponto = document.getElementById("id_ponto").innerHTML;
}

if (document.getElementById("_title")) {
    _title = document.getElementById("_title").innerHTML;
}

if (document.getElementById("_order")) {
    _order = parseInt(document.getElementById("_order").innerHTML);
}

if (document.getElementById("_layer_name")) {
    _layer_name = document.getElementById("_layer_name").innerHTML;
}

var map;

var urls_list = {},
    vectors_list = {},
    container = document.getElementById('popup'),
    content = document.getElementById('popup-content'),
    closer = document.getElementById('popup-closer');

if (closer) {
    closer.onclick = function() {
        overlay.setPosition(undefined);
        closer.blur();
        return false;
    };
}

var overlay = new ol.Overlay({element: container});

nunjucks.configure({ autoescape: true });

$(function() {
    $('input[data-geojson-url]').each(function() {
        var key = $(this).val(),
            value = $(this).data('geojson-url'),
            popup_order = $(this).data('popup-order'),
            style = $(this).data('style'),
            shapestyle = $(this).data('shapestyle'),
            title = $(this).parents('label').text(),
            color = $(this).data('color'),
            type = $(this).data('type'),
            icon = $(this).data('icon'),
            tiled = $(this).data('tiled');
        urls_list[key] = {'popup_order': popup_order, 'title': title, 'url': value, 'style': style, 'color': color, 'type': type, 'shapestyle': shapestyle, 'tiled': tiled};

    });
    init_map();

    $('.list-checkbox > li > label > input[type="checkbox"]:checked').each(function() {
        var vector = vectors_list[$(this).val()];
        vector.setVisible(true);
    });

    $('.list-checkbox > li > label > input[type="checkbox"]').on('click', function(e) {
        var vector = vectors_list[$(this).val()],
            checked = $(this).is(':checked');

        if(vector) {
            vector.setVisible(checked);
            if(!checked)
                closer.click();
        }
    });
});

function init_map() {
    var projection = ol.proj.get('EPSG:3857');
    var static_url = $('body').data('static-url');

    var fundo = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
        }),
        opacity: 0.5
    });

    var styles = [
    new ol.style.Style({
        image: new ol.style.RegularShape({
            fill: new ol.style.Fill({color: '#bb1944'}),
            points: 3,
            radius: 5,
            angle: 3*Math.PI
        }),
        zIndex: 2
    }),
    new ol.style.Style({
        image: new ol.style.Circle({
            radius: 10,
            fill: new ol.style.Fill({
                color: 'rgba(99, 36, 110, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#bb1944',
                width: 1
            })
        }),
        zIndex: 1
    })
];

    for(var key in urls_list) {
        var url = urls_list[key].url,
            is_style = urls_list[key].style,
            is_shapestyle = urls_list[key].shapestyle,
            popup_order = urls_list[key].popup_order,
            is_tiled = urls_list[key].tiled,
            color = urls_list[key].color,
            type = urls_list[key].type,
            title = urls_list[key].title,
            style = undefined,
            vector_source = undefined;

        if(is_style) {
            if(is_tiled) {
                function stl(cl, tp){
                    return function(feature) {
                        var num_points = feature.get("count");
                        var radius = 5 + (num_points.toString().length * 2);
                        var st;
                        if (num_points == 1) {
                            // pin differentiation, based on type (equipamento/agente/acao)
                            if (tp == "agente") {
                                st = new ol.style.Style({
                                    image: new ol.style.RegularShape({
                                        fill: new ol.style.Fill({color: cl}),
                                        points: 3,
                                        radius: 5,
                                        angle: 3*Math.PI
                                    }),
                                    zIndex: 2
                                });
                            } else if(tp == "acao") {
                                st = new ol.style.Style({
                                    image: new ol.style.RegularShape({
                                        fill: new ol.style.Fill({color: cl}),
                                        points: 10,
                                        radius: 5,
                                        angle: Math.PI / 4
                                    }),
                                    zIndex: 2
                                });
                            } else {
                                st = new ol.style.Style({
                                    image: new ol.style.RegularShape({
                                        fill: new ol.style.Fill({color: cl}),
                                        points: 4,
                                        radius: 5,
                                        angle: Math.PI / 4
                                    }),
                                    zIndex: 2
                                });
                            }
                        } else {
                            st = new ol.style.Style({
                                image: new ol.style.Circle({
                                    fill: new ol.style.Fill({
                                        color: convertHex(cl, 0.7)
                                    }),
                                    stroke: new ol.style.Stroke({
                                        width: 1,
                                        color: convertHex(cl, 1.0)
                                    }),
                                    radius: radius
                                }),
                                text: new ol.style.Text({
                                    fill: new ol.style.Fill({color:'#FFFFFF'}),
                                    fontFamily: 'Calibri,sans-serif',
                                    fontSize: 12,
                                    text: num_points.toString(),
                                    labelYOffset: -12
                                })
                            });
                        }
                        return st;
                    };
                };
                style = stl(color, type);

            } else {
                style = [
                    new ol.style.Style({

                        // teste icone

                        //image: new ol.style.Icon({
                        //    anchor: [0.5, 0.5],
                        //    size: [32, 37],
                        //    offset: [52, 0],
                        //    opacity: 1,
                        //    scale: 0.5,
                        //    src: '../img/icons/theater.png'
                        //}),

                        image: new ol.style.RegularShape({
                            fill: new ol.style.Fill({color: color}),
                            points: 4,
                            radius: 5,
                            angle: Math.PI / 4
                        }),
                        zIndex: 2
                    })
                ];
            }
        } else if(is_shapestyle) {
            style = [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: color,
                    width: 1.5,
                }),
                // fill: new ol.style.Fill({
                //     color: color,
                //     opacity : 0.2,
                // })
            })
        ];
        } else {
            style = [
                new ol.style.Style({

                    // image: new ol.style.Circle({
                    //     fill: new ol.style.Fill({color: color}),
                    //     points: 20,
                    //     radius: 5,
                    //     angle: Math.PI / 4
                    // }),
                    image: new ol.style.Circle({
                        fill: new ol.style.Fill({
                            color: color
                        }),
                    stroke: new ol.style.Stroke({
                            width: 1,
                            color: 'rgba(105, 105, 105, 0.3)'
                        }),
                    radius: 4
                    }),
                    zIndex: 2
                })
            ];
        }

        if (is_tiled) {
            vector_source = new ol.source.VectorTile({
                url: url,
                format: new ol.format.MVT(),
                overlaps: is_style,
                tileGrid: ol.tilegrid.createXYZ({maxZoom: 22}),
                tilePixelRatio: 16
            });
            vectors_list[key] = new ol.layer.VectorTile({
                name: key,
                source: vector_source,
                style: style
            });

        } else {
            vector_source = new ol.source.Vector({
                url: url,
                projection: 'EPSG:3857',
                format: new ol.format.GeoJSON(),
            });
            vectors_list[key] = new ol.layer.Vector({
                name: key,
                source: vector_source,
                style: style
            });
        }

        vectors_list[key].set('_order', popup_order);
        vectors_list[key].set('_title', title);
    }

    var displayInfo = function(e) {
        var coordinate = e.coordinate;
        var features = [];

        map.forEachFeatureAtPixel(e.pixel, function(feature, layer) {
            features.push({
                'feature_properties': feature.getProperties(),
                'layer': layer,
            });
        });

        features.sort(function(featureA, featureB) {
            if (featureA.layer.get('name') == featureB.layer.get('name')) {
                if (featureA.feature_properties['name'] > featureB.feature_properties['name']) {
                    return -1;
                } else {
                    return 1;
                }
            } else {
                if (featureA.layer.get('_order') < featureB.layer.get('_order')) {
                    return -1;
                } else {
                    return 1;
                }
            }
            return 0;

        });

        features = features.filter(function(element) {
            var has_count_higher_than_1 = 'count' in element.feature_properties == false || element.feature_properties['count'] == 1;
            var has_named_layer = element.layer.get('name') != undefined;
            return has_count_higher_than_1 && has_named_layer;
        });

        var header_title = '';
        var new_title;
        var innerHTML = '';
        var template_name = 'default';
        if(features.length >= 1) {

            var vertical_menu = $('<ul>');
            vertical_menu.attr('id', 'popup-menu');
            vertical_menu.addClass('vertical menu');
            vertical_menu.data('accordion-menu', '');

            $.each(features, function(i, feature) {
                new_title = header_title != $.trim(feature.layer.get('_title'));
                if (new_title) {
                    if (i != 0) {
                        innerHTML += "</ul></li>";
                    }
                    header_title = $.trim(feature.layer.get('_title'));
                    innerHTML += '<li class="is-accordion-submenu-parent" aria-expanded="false"><a href="#" style="display: block;" onclick="openCloseSubmenu(this, event);"><h6>' + header_title + '</h6></a>'
                    is_active = '';
                    style = ' style="display: none;"';
                    if (features.length == 1) {
                        is_active = ' is-active';
                        style = '';
                    }
                    innerHTML += '<ul class="menu vertical nested'+ is_active +'"'+ style +'>';
                }
                template_name = 'default'
                if (feature.layer.get('name') in popup_layer_templates) {
                    template_name = feature.layer.get('name');
                }
                innerHTML += "<li>" + nunjucks.renderString(popup_layer_templates[template_name], feature.feature_properties) + "</li>";
            });
            
            content.innerHTML = vertical_menu.html(innerHTML).prop('outerHTML');
            /*var elem = new Foundation.AccordionMenu($('ul#popup-menu'));*/
            overlay.setPosition(coordinate);

        } else {
            closer.onclick();
        }
    }

    var options = {
        layers: [fundo],
        overlays: [overlay],
        target: 'map',
        renderer: 'canvas',
        view: new ol.View({
            maxZoom: 22
        })
    };

    if (blured_map) {
        options['interactions'] = new ol.interaction.defaults({mouseWheelZoom:false});
    }

    map = new ol.Map(options);

    if (isNaN(longitude) == false && isNaN(latitude) == false) {

        map.getView().setCenter(ol.proj.transform(
            [longitude, latitude],
            'EPSG:4326',
            'EPSG:3857'
        ));

        var setores_censitarios = new ol.layer.Vector({
            source: new ol.source.Vector({
                url: '/mapa/setores_censitarios.geojson?longitude=' + longitude + '&latitude=' + latitude,
                projection: 'EPSG:3857',
                format: new ol.format.GeoJSON()
            }),
        });

        map.addLayer(setores_censitarios);

    } else {
        map.getView().setCenter([0, 0])
    }

    map.getView().setZoom(blured_map ? 4 : 14);

    if (blured_map) {
        var extent = map.getView().calculateExtent(map.getSize());
    }

    var vector;
    for(var key in vectors_list) {
        vector = vectors_list[key];
        map.addLayer(vector);
        vector.setVisible(false);
    }
    if (isNaN(id_ponto) == false && url_link != undefined) {

        var acoes_detalhes = new ol.layer.Vector({
            source: new ol.source.Vector({
                url: url_link + '?pk=' + id_ponto,
                projection: 'EPSG:3857',
                format: new ol.format.GeoJSON(),
            }),
            style: styles
        });

        if (_layer_name) {
            acoes_detalhes.set('name', _layer_name);
        }

        acoes_detalhes.set('_order', _order);
        acoes_detalhes.set('_title', _title);
        acoes_detalhes.setZIndex(10);

        map.addLayer(acoes_detalhes);

    }

    map.on('click', function(e) {
        displayInfo(e);
    });

    var getVinculosGeometries = function (layerName) {
        // Get the geojson from vinculos
        var vinculosGeometries = [];
        $("input[data-layer=" + layerName + "]").each(function () {
            vinculosGeometries.push($(this).data("geometry"));
        });
        return vinculosGeometries
        console.log(vinculosObject)
    }
    var getVinculosProperties = function (layerName) {
        // Get the geojson from vinculos
        var vinculosProperties = [];
        $("input[data-layer=" + layerName + "]").each(function () {
            var properties = {}
            properties['count'] = 1;
            properties['target_url'] = $(this).data("target-url");
            properties['local'] = $(this).data("local");
            properties['name'] = $(this).data("name");
            properties['layer'] = $(this).data("layer");
            vinculosProperties.push(properties);
        });
        return vinculosProperties
    }
    var getVinculosObject = function (vinculoGeoms, vinculosProperties) {
        // Construct feature collection object
        var feature = undefined;
        var vinculosObject = getGeoObjectFor();
        if (!vinculoGeoms.length) {
            return false;
        }
        for (var index in vinculoGeoms) {
            if (!vinculoGeoms[index]){
                continue;
            }
            feature = {
                'type': 'Feature',
                'geometry': vinculoGeoms[index],
                'properties': vinculosProperties[index]
            }  
            vinculosObject['features'].push(feature);
        }
        return vinculosObject
    }
    var getGeoObjectFor = function () {
        return {
            'type': 'FeatureCollection',
            'crs': {
                'type': "name",
                'properties': {
                    'name': 'EPSG:3857'
                }
            },
            'features': [],
        };
    }

    $('.clear-filter').on('click', function(e) {
        map.getLayers().forEach(function (layer, i) {
            var removed_layer = layer.get("name") == "agentes_vinculados" ? map.removeLayer(layer) : false
            var removed_layer = layer.get("name") == "acoes_vinculadas" ? map.removeLayer(layer) : false
        });
    })

    $(".map-show-vinculos").on("click", function (event) {
        event.preventDefault();
        // Source Features
        var layerName = $(this).data("layer-name");
        var layerTitle = $(this).data("layer-title");
        var vinculosObject = getVinculosObject(
            getVinculosGeometries(layerName),
            getVinculosProperties(layerName)
        );
        if (!vinculosObject.features) {
            return alert("Não há locais cadastrados nas Ações")
        }
        // Clean obsolet layers
        $('.clear-filter').click();

        // Map position
        var position = $("#map:first").position();
        window.scrollTo(position.top, position.left);

        // Source Vector init
        var vinculoVectorSource = new ol.source.Vector({
            projection: 'EPSG:3857',
            features: new ol.format.GeoJSON().readFeatures(vinculosObject)
        })

        // Layer init
        var vinculoLayer = new ol.layer.Vector({
            name: layerName,
            _title: layerTitle,
            source: vinculoVectorSource,
            style: styles[0],
        });

        // Map update
        map.addLayer(vinculoLayer);
        map.getView().fit(vinculoLayer.getSource().getExtent(), map.getSize());
    });
}
