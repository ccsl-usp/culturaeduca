from django.conf.urls import include, url
from tastypie.api import Api

from .resources import UfResource
from .resources import MunicipioResource

v1_api = Api(api_name='v1')
v1_api.register(UfResource())
v1_api.register(MunicipioResource())

urlpatterns = [
    url(r'^', include(v1_api.urls)),
]