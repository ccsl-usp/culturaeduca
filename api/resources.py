from tastypie.contrib.gis.resources import ModelResource
from tastypie.serializers import Serializer
from tastypie.resources import ALL, ALL_WITH_RELATIONS

from ibge.models import UF
from ibge.models import Municipio



class UfResource(ModelResource):
    class Meta:
        queryset = UF.objects.all()
        allowed_methods = ['get']
        filtering = {
            'uf': ALL,
            'geom': ALL,
        }


class MunicipioResource(ModelResource):
    class Meta:
        queryset = Municipio.objects.all()
        serializer = Serializer(formats=['json'])
        collection = 'coordinates'

        include_resource_uri = False
        fields = ['geom']
        remove_api_resource_names = True
        
        
        object_class = 'coordinates'

        filtering = {
            'codigo_ibge': ALL,
            'geom': ALL,
            'cd_geocodm': ALL,
        }

