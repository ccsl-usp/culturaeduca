# CulturaEduca 2 #

Reformulação do CulturaEduca com base no Axis Framework extraído do projeto Território de Direitos

Servidor de testes: http://culturaeduca.znc.com.br

### O Projeto ###

* Versão 1.2

### Dependências e versões ###

* Django 1.8
* Psycopg 2.5.4
* Geopy 1.4.0

### Projeção Padrão ###

SIRGAS 2000 - EPGS 4674

### Consulta API Exemplo ###

http://localhost:8000/api/v1/uf/?format=json&geom__contains={%22type%22:%20%22Point%22,%20%22coordinates%22:%20[-46.535828,%20-23.363497]}

http://localhost:8000/api/v1/uf/?format=json&uf=SP

### Autores ###

Instituto Lidas lidas@lidas.org.br

